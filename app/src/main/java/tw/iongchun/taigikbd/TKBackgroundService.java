/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import org.supercsv.io.CsvListReader;
import org.supercsv.prefs.CsvPreference;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicBoolean;

import static androidx.core.app.NotificationCompat.PRIORITY_MIN;

/**
 * Created by iongchun on 9/21/17.
 */

public class TKBackgroundService extends IntentService {
	public static final String ACTION = "action";
	public static final int ACTION_UPDATE_DATABASE = 1;
	public static final int ACTION_INCREMENT_WORD_WEIGHT = 2;
	public static final String WORD = "word";
	public static final String INTENT_DB_READY = "tw.iongchun.taigikbd.DATABASE_READY";
	private static final int PHRASE_ID_MAX = 60390;
	private static final int ALT_ID_MAX = 1430;
	private static final int WORDS_TOTAL = 53092;
	private boolean appDebug;
	private SQLiteDatabase db;

	public TKBackgroundService() {
		super("bg");
	}

	private static final AtomicBoolean dbStop = new AtomicBoolean(false);

	public static void updateDatabase(Context context) {
		Intent intent = new Intent(context, TKBackgroundService.class);
		intent.putExtra(ACTION, ACTION_UPDATE_DATABASE);
		dbStop.set(false);
		context.startService(intent);
	}

	public static void terminateDatabase() {
		dbStop.set(true);
	}

	public static void incrementWordWeight(Context context, String word) {
		Intent intent = new Intent(context, TKBackgroundService.class);
		intent.putExtra(ACTION, ACTION_INCREMENT_WORD_WEIGHT);
		intent.putExtra(WORD, word);
		context.startService(intent);
	}

	private static String wordTailoToPoj(String word) {
		StringBuilder buf = new StringBuilder();
		StringTokenizer toks = new StringTokenizer(word, "-", true);
		while (toks.hasMoreTokens()) {
			String sylb = toks.nextToken();
			if (sylb.equals("-")) {
				buf.append('-');
				continue;
			}
			TKInputState tailo = TKComposingUtils.parseState(sylb);
			TKInputState poj = TKComposingUtils.tailoToPoj(tailo);
			String sylbPoj = TKComposingUtils.getComposingTextPOJ(poj);
			buf.append(sylbPoj);
		}
		return buf.toString();
	}

	@Override
	public void onCreate() {
		if (getPackageName().endsWith("debug"))
			appDebug = true;
		db = new TKDatabase(this).getWritableDatabase();
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		db.close();
	}

	protected void log(String text) {
		if (appDebug) {
			String pfx = String.valueOf(System.currentTimeMillis() % (60 * 60 * 1000)) + ' ';
			pfx += "[" + Thread.currentThread().getName() + "] ";
			Intent intent = new TKLogIntent(this, pfx + text);
			startService(intent);
		}
	}

	protected void log(String text, Object... args) {
		if (args != null && args.length >= 1) {
			StringBuilder sb = new StringBuilder(text);
			for (Object arg : args) {
				if (arg == null)
					sb.append("(null)");
				else
					sb.append(arg.toString());
			}
			text = sb.toString();
		}
		log(text);
	}

	protected void broadcastDatabaseReady() {
		Intent intent = new Intent(INTENT_DB_READY);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
	}

	@Override
	protected void onHandleIntent(Intent workIntent) {
		if (workIntent == null)
			return;
		switch (workIntent.getIntExtra(ACTION, 0)) {
		case ACTION_UPDATE_DATABASE:
			updateDatabase();
			break;
		case ACTION_INCREMENT_WORD_WEIGHT:
			String word = workIntent.getStringExtra(WORD);
			incrementWordWeight(word);
			break;
		}
	}

	private int queryLastPhraseId() {
		Cursor c = db.rawQuery("select max(phrase_id) from words", new String[] {});
		c.moveToNext();
		int phraseId = 0;
		if (!c.isNull(0))
			phraseId = c.getInt(0);
		c.close();
		return phraseId;
	}

	private int queryLastAltId() {
		Cursor c = db.rawQuery("select max(alt_id) from words", new String[] {});
		c.moveToNext();
		int id = 0;
		if (!c.isNull(0))
			id = c.getInt(0);
		c.close();
		return id;
	}

	private int queryWordCount(String table) {
		Cursor c = db.rawQuery("select count(*) from " + table, new String[] {});
		c.moveToNext();
		int count = c.getInt(0);
		c.close();
		return count;
	}

	private int queryWordCount() {
		return queryWordCount("words") + queryWordCount("words_poj");
	}

	private void updateDatabase() {
		int curPhraseId = queryLastPhraseId();
		int curAltId = queryLastAltId();
		if (curPhraseId >= PHRASE_ID_MAX && curAltId >= ALT_ID_MAX) {
			broadcastDatabaseReady();
			return;
		}

		NotificationManager notifManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		NotificationCompat.Builder notifBuilder = createNotification();
		notifBuilder.setProgress(WORDS_TOTAL, 0, false);
		notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());

		boolean result = true;
		if (curPhraseId < PHRASE_ID_MAX)
			result = updatePhrases(curPhraseId, notifBuilder);

		if (result && curAltId < ALT_ID_MAX)
			result = updateAlts(curAltId, notifBuilder);

		if (result) {
			if (dbStop.get()) {
				notifBuilder.setContentText(getResources().getString(R.string.dict_db_cancel));
			} else {
				migrateTable("words");
				migrateTable("words_poj");
				broadcastDatabaseReady();
				notifBuilder.setContentText(getResources().getString(R.string.dict_db_done));
			}
		} else {
			notifBuilder.setContentText(getResources().getString(R.string.dict_db_error));
		}
		notifBuilder.setProgress(0, 0, false);
		notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
	}

	private NotificationCompat.Builder createNotification() {
		NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(this, TKNotification.CHANNEL_INFO);
		notifBuilder.setSmallIcon(android.R.drawable.ic_dialog_info);
		notifBuilder.setContentTitle(getResources().getString(R.string.app_name));
		notifBuilder.setContentText(getResources().getString(R.string.dict_db_progressing));
		notifBuilder.setPriority(PRIORITY_MIN);
		notifBuilder.setOnlyAlertOnce(true);
		return notifBuilder;
	}

	private boolean updatePhrases(int curPhraseId, NotificationCompat.Builder notifBuilder) {
		log("begin updating database from main data");
		NotificationManager notifManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		int curWords = queryWordCount();
		notifBuilder.setProgress(WORDS_TOTAL, curWords, false);
		notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
		InputStream ins = null, insAlts = null;
		// try the first 100 inserts to measure speed
		int notifNext = curWords + 100;
		long start = System.currentTimeMillis();
		boolean result;
		try {
			ins = getAssets().open("詞目總檔.csv");
			log("open phrases: ", ins);
			CsvListReader reader = new CsvListReader(new InputStreamReader(ins), CsvPreference.STANDARD_PREFERENCE);

			// first line is header
			List<String> data = reader.read();
			if (data == null)
				return true;

			int[] stats = new int[] {0, 0, 0};
			int cntWords = curWords;
			while (!dbStop.get()) {
				data = reader.read();
				if (data == null)
					break;

				// update progress
				if (cntWords >= notifNext) {
					notifBuilder.setProgress(WORDS_TOTAL, cntWords, false);
					notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
					// measure speed again
					long elapsed = System.currentTimeMillis() - start;
					if (elapsed <= 0) // too fast!
						elapsed = 1;
					// update once every 10 seconds in average
					int delta = (int)((cntWords - curWords) * 10000 / elapsed);
					if (delta <= 0) // too slow!
						delta = 1;
					notifNext += delta;
				}

				int phraseId = Integer.parseInt(data.get(0));
				if (phraseId < curPhraseId)
					continue;
				String phrases = data.get(3);
				if (phrases == null)
					continue;
				String hans = data.get(2);
				updatePhrases(phraseId, phrases, hans, stats);

				cntWords = curWords + stats[1] + stats[2];
			}
			log("total phrases: " + stats[0]);
			log("total tailo words: ", stats[1]);
			log("total poj words: ", stats[2]);
			result = true;
		} catch (IOException ex) {
			log("read phrases error: ", ex);
			result = false;
		} finally {
			if (ins != null) {
				try {
					log("close phrases: ", ins);
					ins.close();
				} catch (IOException ex) {
					log("close phrases error: ", ex);
				}
			}
		}
		log("end updating database from main data");
		return result;
	}

	private String queryHan(int phraseId) {
		Cursor c = db.rawQuery("select han from words where phrase_id=?",
		                       new String[] {String.valueOf(phraseId)});
		String han = null;
		if (c.moveToNext())
			han = c.getString(0);
		c.close();
		return han;
	}

	private boolean updateAlts(int curAltId, NotificationCompat.Builder notifBuilder) {
		log("begin updating database from alt data");
		NotificationManager notifManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		int curWords = queryWordCount();
		notifBuilder.setProgress(WORDS_TOTAL, curWords, false);
		notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
		InputStream ins = null;
		// try the first 100 inserts to measure speed
		int notifNext = curWords + 100;
		long start = System.currentTimeMillis();
		boolean result;
		try {
			ins = getAssets().open("又音.csv");
			log("open alts: ", ins);
			CsvListReader reader = new CsvListReader(new InputStreamReader(ins), CsvPreference.STANDARD_PREFERENCE);

			// first line is header
			List<String> data = reader.read();
			if (data == null)
				return true;

			int[] stats = new int[] {0, 0, 0};
			int cntWords = curWords;
			while (!dbStop.get()) {
				data = reader.read();
				if (data == null)
					break;

				// update progress
				if (cntWords >= notifNext) {
					notifBuilder.setProgress(WORDS_TOTAL, cntWords, false);
					notifManager.notify(TKNotification.NOTIF_DICT_DB, notifBuilder.build());
					// measure speed again
					long elapsed = System.currentTimeMillis() - start;
					if (elapsed <= 0) // too fast!
						elapsed = 1;
					// update once every 10 seconds in average
					int delta = (int)((cntWords - curWords) * 10000 / elapsed);
					if (delta <= 0) // too slow!
						delta = 1;
					notifNext += delta;
				}

				int altId = Integer.parseInt(data.get(0));
				if (altId < curAltId)
					continue;
				String phrases = data.get(2);
				if (phrases == null)
					continue;
				int phraseId = Integer.parseInt(data.get(1));
				String hans = queryHan(phraseId);
				updatePhrases(phraseId, altId, phrases, hans, stats);

				cntWords = curWords + stats[1] + stats[2];
			}
			log("total words: " + stats[0]);
			log("total tailo words: ", stats[1]);
			log("total poj words: ", stats[2]);
			result = true;
		} catch (IOException ex) {
			log("read phrases error: ", ex);
			result = false;
		} finally {
			if (ins != null) {
				try {
					log("close alts: ", ins);
					ins.close();
				} catch (IOException ex) {
					log("close alts error: ", ex);
				}
			}
		}
        /*
        InputStream insLocals = getAssets().open("詞彙方言差.csv");
        log("open locals: ", insLocals);
        CsvListReader rdrLocals = new CsvListReader(new InputStreamReader(insLocals), CsvPreference.STANDARD_PREFERENCE);
        rdrLocals.read();
        try {
            log("close locals: ", insLocals);
            insLocals.close();
        } catch (IOException ex) {
            log("close locals error: ", ex);
        }
        */
		log("end updating database from alt data");
		return result;
	}

	private void updatePhrases(int phraseId, String phrases, String hans, int[] stats) {
		updatePhrases(phraseId, null, phrases, hans, stats);
	}

	private void updatePhrases(int phraseId, Integer altId, String phrases, String hans, int[] stats) {
		try {
			//log("phrase: ", phrase);
			phrases = phrases.trim();
			if (phrases.indexOf('/') >= 0) {
				//log("phrase: ", phrase);
				String[] alts = phrases.split("/");
				for (String alt : alts) {
					//log(" alt: ", alt);
					stats[0]++;
					updatePhrase(phraseId, altId, alt.trim(), hans, stats);
				}
			} else if (phrases.indexOf('(') >= 0) {
					String[] alts = phrases.split("[()]");
					for (String alt : alts) {
						stats[0]++;
						updatePhrase(phraseId, altId, alt.trim(), hans, stats);
					}
			} else {
				stats[0]++;
				updatePhrase(phraseId, altId, phrases, hans, stats);
			}
		} catch (Exception ex) {
			log("process phrase error (", ex, "): ", phrases);
		}
	}

	private void updatePhrase(int phraseId, Integer altId, String phrase, String han, int[] stats) {
		if (phrase.startsWith("--"))
			phrase = phrase.substring(2);

		int sep = han.indexOf("、");
		if (sep >= 0)
			han = han.substring(0, sep);
		StringBuilder hanb = new StringBuilder();
		for (String str : han.split("[，；。？—]+"))
			hanb.append(str);
		String hanChars = hanb.toString();
		int hanLen = hanChars.length();

		String[] words = phrase.split("[\\s,.—]+");
		for (int i = 0; i < words.length; i++) {
			String word = words[i].toLowerCase(Locale.ROOT);

			String[] syls = word.split("[\\s,.—-]+");
			String hanWord = null;
			if (hanLen > 0) {
				if (syls.length > hanLen || i == words.length - 1) {
					hanWord = hanChars;
					hanChars = "";
					hanLen = 0;
				} else {
					hanWord = hanChars.substring(0, syls.length);
					hanChars = hanChars.substring(syls.length);
					hanLen -= syls.length;
				}
			} else {
				StringBuilder sylb = new StringBuilder(syls[0]);
				for (int j = 1; j < syls.length; j++) {
					sylb.append(',');
					sylb.append(syls[j]);
				}
				final String syl = sylb.toString();
				log("syls=", syl, "(", syls.length, ") han=", hanChars, "(", hanLen, ")");
			}

			if (updateWord(phraseId, altId, word, hanWord, false))
				stats[1]++;
			String wordPoj = wordTailoToPoj(word);
            /*
            if (!wordPoj.equals(word))
                log("tailo: ", word, ", poj: ", wordPoj);
                */
			if (updateWord(phraseId, altId, wordPoj, hanWord, true))
				stats[2]++;
		}
	}

	private boolean updateWord(int phraseId, Integer altId, String word, String han, boolean poj) {
		String table = poj ? "words_poj" : "words";
		ContentValues values = new ContentValues();
		String toneless = new String(TKComposingUtils.removeTone(word));
		values.put("word", word);
		values.put("toneless", toneless);
		values.put("phrase_id", phraseId);
		values.put("alt_id", altId);
		values.put("han", han);
		values.put("weight", 1.0d);
		long id = db.insertWithOnConflict(table, null, values, SQLiteDatabase.CONFLICT_IGNORE);
		if (id == -1L && altId != null) {
			// migration from words table without alt_id
			values = new ContentValues();
			values.put("alt_id", altId);
			db.updateWithOnConflict(table, values, "word=?", new String[] {word}, SQLiteDatabase.CONFLICT_IGNORE);
		}
		return id != -1L;
	}

	private void incrementWordWeight(String word) {
		word = word.toLowerCase(Locale.ROOT);
		String[] args = new String[] {word};
		// for TL
		try (Cursor c = db.rawQuery("select weight from words where word=?", args)) {
			if (c.moveToNext()) {
				double weight = c.getDouble(0);
				ContentValues values = new ContentValues();
				values.put("weight", Math.sqrt(weight + 1.0d));
				db.update("words", values, "word=?", args);
			}
		}
		// for TL Han
		try (Cursor c = db.rawQuery("select weight from words where han=?", args)) {
			if (c.moveToNext()) {
				double weight = c.getDouble(0);
				ContentValues values = new ContentValues();
				values.put("weight", Math.sqrt(weight + 1.0d));
				db.update("words", values, "han=?", args);
			}
		}
		// for POJ
		try (Cursor c = db.rawQuery("select weight from words_poj where word=?", args)) {
			if (c.moveToNext()) {
				double weight = c.getDouble(0);
				ContentValues values = new ContentValues();
				values.put("weight", Math.sqrt(weight + 1.0d));
				db.update("words_poj", values, "word=?", args);
			}
		}
		// for POJ Han
		try (Cursor c = db.rawQuery("select weight from words_poj where han=?", args)) {
			if (c.moveToNext()) {
				double weight = c.getDouble(0);
				ContentValues values = new ContentValues();
				values.put("weight", Math.sqrt(weight + 1.0d));
				db.update("words_poj", values, "han=?", args);
			}
		}
	}

	private void migrateTable(String name) {
		String nameOrg = name + "_org";
		if (!TKDatabase.checkTableExist(db, nameOrg))
			return;
		// migrate weights
		String index = "idx_" + nameOrg + "_with_weight";
		db.execSQL("drop index if exists " + index);
		db.execSQL("create index " + index + " on " + nameOrg + " (weight)");
		try (Cursor c = db.rawQuery("select word, weight from " + nameOrg + " where weight > 1", new String[] {})) {
			while (c.moveToNext()) {
				String word = c.getString(0);
				double weight = c.getDouble(1);
				ContentValues values = new ContentValues();
				values.put("weight", weight);
				db.update(name, values, "word=?", new String[] {word});
			}
		}
		db.execSQL("drop index if exists " + index);
		db.execSQL("drop table " + nameOrg);
	}
}
