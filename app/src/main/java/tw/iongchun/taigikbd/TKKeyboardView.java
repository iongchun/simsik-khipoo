/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import androidx.core.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;

import java.util.List;
import java.util.Locale;

/**
 * Created by iongchun on 6/7/17.
 */

public class TKKeyboardView extends KeyboardView {
	// color-blind-friendly 7-color palette; HSV
	public static final int ORANGE = Color.rgb(230, 159, 0); // 41, 100, 90
	public static final int SKY_BLUE = Color.rgb(86, 180, 233); // 202, 63, 91
	public static final int BLUISH_GREEN = Color.rgb(0, 158, 115); // 164, 100, 62
	public static final int YELLOW = Color.rgb(240, 228, 66); // 56, 73, 94
	public static final int BLUE = Color.rgb(0, 114, 178); // 202, 100, 70
	public static final int VERMILLION = Color.rgb(230, 94, 0); // 25, 100, 92
	public static final int REDDISH_PURPLE = Color.rgb(204, 121, 167); // 327, 41, 80
	public static final int BLACK = Color.rgb(0, 0, 0); // 0, 0, 0
	public static final int WHITE = Color.rgb(255, 255, 255); // 0, 0, 100
	public static final Theme dark = new Theme(BLACK, Color.DKGRAY, BLUISH_GREEN,
	                                           Color.LTGRAY, Typeface.DEFAULT,
	                                           Color.LTGRAY, Typeface.DEFAULT_BOLD,
	                                           REDDISH_PURPLE);
	public static final Theme light = new Theme(Color.LTGRAY, WHITE, BLUISH_GREEN,
	                                            BLACK, Typeface.DEFAULT,
	                                            Color.DKGRAY, Typeface.DEFAULT_BOLD,
	                                            BLUISH_GREEN);
	public static final Theme orange = new Theme(Color.DKGRAY, ORANGE, SKY_BLUE,
	                                             WHITE, Typeface.DEFAULT,
	                                             WHITE, Typeface.DEFAULT_BOLD,
	                                             YELLOW);
	public static final Theme bluishGreen = new Theme(Color.DKGRAY, BLUISH_GREEN, YELLOW,
	                                                  WHITE, Typeface.DEFAULT,
	                                                  WHITE, Typeface.DEFAULT_BOLD,
	                                                  YELLOW);
	public static final Theme blue = new Theme(Color.DKGRAY, BLUE, YELLOW,
	                                           WHITE, Typeface.DEFAULT,
	                                           WHITE, Typeface.DEFAULT_BOLD,
	                                           YELLOW);
	public static final Theme reddishPurple = new Theme(Color.DKGRAY, REDDISH_PURPLE, YELLOW,
	                                                    WHITE, Typeface.DEFAULT,
	                                                    WHITE, Typeface.DEFAULT_BOLD,
	                                                    YELLOW);
	public static final Theme vermillion = new Theme(Color.DKGRAY, VERMILLION, SKY_BLUE,
	                                                 WHITE, Typeface.DEFAULT,
	                                                 WHITE, Typeface.DEFAULT_BOLD,
	                                                 YELLOW);
	public static final Theme skyBlue = new Theme(Color.GRAY, SKY_BLUE, VERMILLION,
	                                              WHITE, Typeface.DEFAULT,
	                                              WHITE, Typeface.DEFAULT_BOLD,
	                                              YELLOW);
	public static final Theme yellow = new Theme(ORANGE, YELLOW, BLUE,
	                                             BLUISH_GREEN, Typeface.DEFAULT,
	                                             BLUISH_GREEN, Typeface.DEFAULT_BOLD,
	                                             WHITE);
	private Paint paint;
	private float charSize, textSize;
	private final RectF rectf = new RectF();
	private final Rect rect = new Rect();
	private Theme theme = null;

	public TKKeyboardView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initDrawResource();
	}

	public TKKeyboardView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initDrawResource();
	}

	private void initDrawResource() {
		paint = new Paint();
		Resources res = getResources();
		charSize = res.getDimension(R.dimen.key_char_size);
		textSize = res.getDimension(R.dimen.key_text_size);
	}

	public Theme getTheme() {
		return theme;
	}

	public void setTheme(Theme value) {
		theme = value;
	}

	@Override
	public void onDraw(Canvas canvas) {
		if (theme == null) {
			super.onDraw(canvas);
			return;
		}

		setBackgroundColor(theme.bgColor);
		paint.setTextAlign(Paint.Align.CENTER);
        /* use embedded font: Andika
        Typeface font = Typeface.createFromAsset(
                getContext().getAssets(),
                "fonts/Andika-R.ttf");
        paint.setTypeface(font);
                */
		//paint.setTextSize(size / getResources().getDisplayMetrics().density);
		paint.setTextAlign(Paint.Align.CENTER);

		boolean shifted = isShifted();
		List<Keyboard.Key> keys = getKeyboard().getKeys();
		float gapX = Float.MAX_VALUE;
		float gapY = Float.MAX_VALUE;
		float radiusX = Float.MAX_VALUE;
		float radiusY = Float.MAX_VALUE;
		for (Keyboard.Key key : keys) {
			if (key.width <= 0)
				continue;

			int left = key.x;
			int top = key.y;
			int right = key.x + key.width - 1;
			int bottom = key.y + key.height - 1;
			if (gapX == Float.MAX_VALUE) {
				gapX = (float)key.width / 20;
				gapY = (float)key.height / 20;
				radiusX = (float)key.width / 16;
				radiusY = (float)key.height / 16;
			}
			rectf.set(left + gapX, top + gapY, right - gapX, bottom - gapY);
			paint.setColor(theme.keyColor);
			canvas.drawRoundRect(rectf, radiusX, radiusY, paint);

			if (key.icon != null) {
				Drawable icon = key.icon;
				float rwidth = rectf.width();
				float rheight = rectf.height();
				int iwidth = icon.getIntrinsicWidth();
				int iheight = icon.getIntrinsicHeight();
				rectf.round(rect);
				if (((float)iwidth / iheight) > (rwidth / rheight)) {
					// wide: width is charSize, height is (iheight * charSize / iwidth)
					int paddingX = (int)(rwidth - charSize);
					rect.left += paddingX / 2;
					rect.right -= paddingX / 2;
					int paddingY = (int)(rheight - iheight * charSize / iwidth);
					rect.top += paddingY / 2;
					rect.bottom -= paddingY / 2;
				} else {
					// long: height is charSize, width is (iwidth * charSize / iheight)
					int paddingY = (int)(rheight - charSize);
					rect.top += paddingY / 2;
					rect.bottom -= paddingY / 2;
					int paddingX = (int)(rwidth - iwidth * charSize / iheight);
					rect.left += paddingX / 2;
					rect.right -= paddingX / 2;
				}
				icon.setBounds(rect);
				DrawableCompat.setTint(icon, theme.iconColor);
				icon.draw(canvas);
			} else if (key.label != null) {
				int centerX = left + (key.width / 2);
				int centerY = top + (key.height / 2);

				String text = key.label.toString();
				if (shifted)
					text = text.toUpperCase(Locale.ROOT);

				int textCnt = 0;
				for (int textLen = text.length(), i = 0; i < textLen; i++) {
					if (Character.isLetterOrDigit(text.charAt(i)))
						textCnt++;
				}
				float size;
				Typeface font;
				paint.setColor(theme.textColor);
				if (textCnt > 1) {
					size = textSize;
					font = theme.textFont;
					paint.setColor(theme.textColor);
				} else {
					size = charSize;
					font = theme.charFont;
					paint.setColor(theme.charColor);
				}
				paint.setTextSize(size);
				paint.setTypeface(font);
				canvas.drawText(text, centerX, centerY + size / 2, paint);
			}

			// shift key with a green LED
			if (key.codes[0] == -1) {
				paint.setColor(shifted ? theme.ledColor : theme.bgColor);
				float radius = Math.min(radiusX, radiusY);
				canvas.drawCircle(right - gapX - radius * 2, top + gapY + radius * 2, radius, paint);
			}

		}
	}

	public static class Theme {
		public final int bgColor;
		public final int keyColor;
		public final int ledColor;
		public final Typeface textFont;
		public final int textColor;
		public final int textColor2;
		public final Typeface charFont;
		public final int charColor;
		public final int iconColor;

		public Theme(int backgroundColor, int keyColor, int ledColor,
		             int charColor, Typeface charFont,
		             int textColor, Typeface textFont,
		             int textColor2) {
			this.bgColor = backgroundColor;
			this.keyColor = keyColor;
			this.ledColor = ledColor;
			this.charColor = charColor;
			this.charFont = charFont;
			this.textColor = textColor;
			this.textFont = textFont;
			this.textColor2 = textColor2;
			this.iconColor = charColor;
		}
	}

	// Fix portrait to landscape switch
	// TODO: also fix landscape to portrait switch
	// TODO: will fix KeyboardView one day...

	private int originWidth = -1;

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if (originWidth < 0)
			originWidth = getMeasuredWidth();
		int width = MeasureSpec.getSize(widthMeasureSpec);
		int height = getMeasuredHeight();
		setMeasuredDimension(width, height);
	}

	@Override
	public void onSizeChanged(int newWidth, int newHeight, int oldWidth, int oldHeight) {
		super.onSizeChanged(newWidth, newHeight, oldWidth, oldHeight);
		if (newWidth == oldWidth || originWidth <= 0)
			return;
		int width = getWidth();
		if (width == originWidth)
			return;
		float ratioX = (float)newWidth / originWidth;
		for (Keyboard.Key key : getKeyboard().getKeys()) {
			if (key.width <= 0)
				continue;
			key.x = (int)((float)key.x * ratioX);
			key.width = (int)((float)key.width * ratioX);
		}
	}
}
