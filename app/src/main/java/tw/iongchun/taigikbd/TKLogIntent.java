/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.Intent;

/**
 * Created by iongchun on 6/5/17.
 */

public class TKLogIntent extends Intent {
	public static final String EXTRA_LOGTXT = "tw.iongchun.testkbd.LOGTXT";

	public TKLogIntent(Context context, String logText) {
		super(context, TKLogService.class);
		putExtra(EXTRA_LOGTXT, logText);
	}
}
