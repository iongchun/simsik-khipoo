/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.appcompat.app.ActionBar;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;

import java.util.List;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class TKSettingsActivity extends TKPreferenceActivity {
	public static final String PREF_DICT_SUGGEST = "dict_suggest_switch";
	public static final String PREF_DICT_HANJI = "dict_hanji_switch";
	public static final String PREF_AUTO_DASH = "auto_dash_switch";
	public static final String PREF_POJ_TONE_MARK = "poj_tone_mark_switch";
	public static final String PREF_TONE_MARK_STYLE = "tone_mark_style";
	public static final String PREF_ASPIRATION_TOGGLE = "aspiration_toggle_switch";
	public static final String PREF_VOICELESS_TOGGLE = "voiceless_toggle_switch";
	public static final String PREF_LEAD_REPLACE_LEAD = "lead_replace_lead_switch";
	public static final String PREF_GLOTTAL_REPLACE = "glotal_replace_glotal_switch";
	public static final String PREF_GLOTTAL_TOGGLE = "glotal_tone_toggle_switch";
	public static final String PREF_DEL_LEADINGS = "delete_leadings";
	public static final String PREF_DEL_MULTICHAR_VOWEL = "delete_multi_char_vowel_switch";
	public static final String PREF_DEL_VOWEL_CLUSTER = "delete_vowel_cluster_switch";
	public static final String PREF_DEL_VOWELS = "delete_vowels";
	public static final String PREF_PHONETIC_INPUT = "phonetic_input_switch";
	public static final String PREF_DEL_AFTER_CLUSTER = "delete_after_cluster_switch";
	public static final String PREF_UI_LOCALE = "ui_locale";
	public static final String PREF_IM_SWITCH = "im_switch";
	public static final String PREF_KEYBOARD_THEME = "keyboard_theme";

	/**
	 * A preference value change listener that updates the preference's summary
	 * to reflect its new value.
	 */
	private static final Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = (preference, value) -> {
		String stringValue = value.toString();

		if (preference instanceof ListPreference) {
			// For list preferences, look up the correct display value in
			// the preference's 'entries' list.
			ListPreference listPreference = (ListPreference)preference;
			int index = listPreference.findIndexOfValue(stringValue);

			// Set the summary to reflect the new value.
			preference.setSummary(
				index >= 0
					? listPreference.getEntries()[index]
					: null);
		} else {
			// For all other preferences, set the summary to the value's
			// simple string representation.
			preference.setSummary(stringValue);
		}
		return true;
	};
	private static String sPackageName;
	private static String sInputMethodID;
	private static final Preference.OnPreferenceClickListener sSubtypeSettingsClick = new Preference.OnPreferenceClickListener() {
		@Override
		public boolean onPreferenceClick(Preference preference) {
			Intent intent = preference.getIntent();
			if (intent != null && intent.getAction() != null &&
			    intent.getAction().equals(Settings.ACTION_INPUT_METHOD_SUBTYPE_SETTINGS)) {
				intent.putExtra(Settings.EXTRA_INPUT_METHOD_ID, sInputMethodID);
				// without these flags settings may crash when click "Back"
				// since the old keyboard view is no longer there
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				                | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
				                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			}
			return false;
		}
	};

	/**
	 * Helper method to determine if the device has an extra-large screen. For
	 * example, 10" tablets are extra-large.
	 */
	private static boolean isXLargeTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout
		        & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
	}

	/**
	 * Binds a preference's summary to its value. More specifically, when the
	 * preference's value is changed, its summary (line of text below the
	 * preference title) is updated to reflect the value. The summary is also
	 * immediately updated upon calling this method. The exact display format is
	 * dependent on the type of preference.
	 *
	 * @see #sBindPreferenceSummaryToValueListener
	 */
	private static void bindPreferenceSummaryToValue(Preference preference) {
		// Set the listener to watch for value changes.
		preference.setOnPreferenceChangeListener(sBindPreferenceSummaryToValueListener);

		// Trigger the listener immediately with the preference's
		// current value.
		sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
		                                                         PreferenceManager
			                                                         .getDefaultSharedPreferences(preference.getContext())
			                                                         .getString(preference.getKey(), ""));
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(TKLocaleHelper.overrideContext(newBase));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupActionBar();

		sPackageName = getPackageName();
		InputMethodManager imMgr = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		List<InputMethodInfo> imis = imMgr.getInputMethodList();
		InputMethodInfo info = null;
		for (InputMethodInfo imi : imis) {
			if (imi.getPackageName().equals(getPackageName())) {
				info = imi;
				break;
			}
		}
		if (info == null)
			return;
		sInputMethodID = info.getId();

		PreferenceManager.setDefaultValues(this, R.xml.pref_intro, false);
		PreferenceManager.setDefaultValues(this, R.xml.pref_interface, false);
		PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
		PreferenceManager.setDefaultValues(this, R.xml.pref_leadings, false);
		PreferenceManager.setDefaultValues(this, R.xml.pref_delete_key, false);

		TKLocaleHelper.setLocale(this);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	private void setupActionBar() {
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			// Show the Up button in the action bar.
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean onIsMultiPane() {
		return isXLargeTablet(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onBuildHeaders(List<Header> target) {
		loadHeadersFromResource(R.xml.pref_headers, target);
	}

	/**
	 * This method stops fragment injection in malicious applications.
	 * Make sure to deny any unknown fragments here.
	 */
	protected boolean isValidFragment(String fragmentName) {
		return PreferenceFragment.class.getName().equals(fragmentName)
		       || IntroPreferenceFragment.class.getName().equals(fragmentName)
		       || InterfacePreferenceFragment.class.getName().equals(fragmentName)
		       || GeneralPreferenceFragment.class.getName().equals(fragmentName)
		       || LeadingsPreferenceFragment.class.getName().equals(fragmentName)
		       || DeleteKeyPreferenceFragment.class.getName().equals(fragmentName);
	}

	/**
	 * This fragment shows intro preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static class IntroPreferenceFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_intro);
			setHasOptionsMenu(true);
			findPreference("subtype_settings").setOnPreferenceClickListener(sSubtypeSettingsClick);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			int id = item.getItemId();
			if (id == android.R.id.home) {
				startActivity(new Intent(getActivity(), TKSettingsActivity.class));
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * This fragment shows interface preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static class InterfacePreferenceFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_interface);
			setHasOptionsMenu(true);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			int id = item.getItemId();
			if (id == android.R.id.home) {
				startActivity(new Intent(getActivity(), TKSettingsActivity.class));
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * This fragment shows general preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static class GeneralPreferenceFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_general);
			setHasOptionsMenu(true);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			int id = item.getItemId();
			if (id == android.R.id.home) {
				startActivity(new Intent(getActivity(), TKSettingsActivity.class));
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * This fragment shows leadings preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static class LeadingsPreferenceFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_leadings);
			setHasOptionsMenu(true);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			int id = item.getItemId();
			if (id == android.R.id.home) {
				startActivity(new Intent(getActivity(), TKSettingsActivity.class));
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * This fragment shows delete preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static class DeleteKeyPreferenceFragment extends PreferenceFragment {
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_delete_key);
			setHasOptionsMenu(true);
		}

		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			int id = item.getItemId();
			if (id == android.R.id.home) {
				startActivity(new Intent(getActivity(), TKSettingsActivity.class));
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
	}
}
