/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

/**
 * Created by iongchun on 6/22/17.
 */

public class TKInputState {
	public static final TKInputState EMPTY = new TKInputState();
	private static int keyIndex = 0;
	// parameters
	public static final TKState.Key DictSuggest = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key DictHanji = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key AutoDash = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ToneMarkStyle = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key GlottalToggleTone = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key GlottalReplaceGlottal = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key DeleteAfterCluster = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key DeleteVowelsMode = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key DeleteLeadingsMode = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key AspirationToggle = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key VoicelessToggle = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key LeadReplaceLead = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key Subtype = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key InputStarted = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key TextMode = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key CapState = TKState.Key.valueOf(keyIndex++);
	// capitalization: TextMode=true
	public static final TKState.Key CapMode = TKState.Key.valueOf(keyIndex++);
	// language key
	public static final TKState.Key LanguageKey = TKState.Key.valueOf(keyIndex++);
	// composing: TextMode=true
	public static final TKState.Key ComposingMode = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key PhoneticInput = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key PhoneticInputTempDisabled = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingStarted = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingState = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingRegion = TKState.Key.valueOf(keyIndex++);
	// composing state
	public static final TKState.Key Hat = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key Leads = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key Vowels = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key Tone = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key Afters = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key Glottal = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key PojNN = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key Tail = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key Boot = TKState.Key.valueOf(keyIndex++);
	private final TKInputState from;
	private final TKState state;

	public TKInputState() {
		state = new TKState();
		from = null;
	}

	private TKInputState(TKState state, TKInputState from) {
		this.state = state;
		this.from = from;
	}

	public boolean contains(TKState.Key key) {
		return state.contains(key);
	}

	public TKInputState set(TKState.Key key, Object value) {
		TKState newState = state.set(key, value);
		if (newState == state)
			return this;
		return new TKInputState(newState, from);
	}

	public TKInputState setBoolean(TKState.Key key, Boolean value) {
		return set(key, value);
	}

	public TKInputState setString(TKState.Key key, String value) {
		return set(key, value);
	}

	public TKInputState setInteger(TKState.Key key, Integer value) {
		return set(key, value);
	}

	public TKInputState setCharacter(TKState.Key key, Character value) {
		return set(key, value);
	}

	public Object get(TKState.Key key) {
		return state.get(key);
	}

	public Boolean getBoolean(TKState.Key key) {
		return (Boolean)get(key);
	}

	public Boolean getBoolean(TKState.Key key, boolean defaultValue) {
		if (state.contains(key))
			return (Boolean)state.get(key);
		else
			return defaultValue;
	}

	public String getString(TKState.Key key) {
		return (String)get(key);
	}

	public String getString(TKState.Key key, String defaultValue) {
		if (state.contains(key))
			return (String)get(key);
		else
			return defaultValue;
	}

	public Integer getInteger(TKState.Key key) {
		return (Integer)get(key);
	}

	public Integer getInteger(TKState.Key key, int defaultValue) {
		if (state.contains(key))
			return (Integer)get(key);
		else
			return defaultValue;
	}

	public Character getCharacter(TKState.Key key) {
		return (Character)get(key);
	}

	public Character getCharacter(TKState.Key key, char defaultValue) {
		if (state.contains(key))
			return (Character)get(key);
		else
			return defaultValue;
	}

	public TKInputState setState(TKState.Key key, TKInputState value) {
		return set(key, value);
	}

	public TKInputState getState(TKState.Key key) {
		return (TKInputState)get(key);
	}

	public TKInputState remove(TKState.Key key) {
		TKState newState = state.remove(key);
		if (newState == state)
			return this;
		return new TKInputState(newState, from);
	}
}
