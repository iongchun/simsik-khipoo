package tw.iongchun.taigikbd;

class TKQueryParameter {
	public final String prefix;
	public final TKInputState inputState;
	public final TKViewState viewState;

	public TKQueryParameter(String prefix, TKInputState inputState, TKViewState viewState) {
		this.prefix = prefix;
		this.inputState = inputState;
		this.viewState = viewState;
	}

	public TKQueryParameter(TKInputState inputState, TKViewState viewState) {
		this(null, inputState, viewState);
	}
}
