/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_POJ;
import static tw.iongchun.taigikbd.TKInputState.DictHanji;
import static tw.iongchun.taigikbd.TKInputState.Subtype;
import static tw.iongchun.taigikbd.TKViewState.Candidates;
import static tw.iongchun.taigikbd.TKViewState.ComposingText;
import static tw.iongchun.taigikbd.TKViewState.HasSuggestions;

/**
 * Created by iongchun on 9/21/17.
 */

public class TKQueryCandidatesTask extends AsyncTask<TKQueryParameter, Void, List<String>> {
	public static final int CANDIDATE_MAX_NUM = 50;
	public static final int CANDIDATE_TONELESS_NUM = 25;
	private static final AtomicInteger queued = new AtomicInteger(0);
	@SuppressLint("StaticFieldLeak")
	private final TKInputMethodService svc;
	private final SQLiteDatabase db;

	public TKQueryCandidatesTask(TKInputMethodService svc, SQLiteDatabase db) {
		this.svc = svc;
		this.db = db;
		queued.incrementAndGet();
	}

	@Override
	protected List<String> doInBackground(TKQueryParameter[] params) {
		Cursor c = null;
		try {
			TKQueryParameter param = params[params.length - 1];
			String prefix = param.prefix;
			TKInputState inputState = param.inputState;
			TKViewState viewState = param.viewState;
			if (prefix != null)
				return queryPrefix(prefix, inputState);

			// query table
			int subtype = inputState.getInteger(Subtype);
			String wordsTable = (subtype == SUBTYPE_POJ ? "words_poj" : "words");

			boolean useSugg = viewState.getBoolean(HasSuggestions);
			boolean useHanji = inputState.getBoolean(DictHanji);
			String text = viewState.getString(ComposingText);
			String toneless = new String(TKComposingUtils.removeTone(text));
			boolean showPrefix = useHanji || useSugg;

			ArrayList<Boolean> ucList = new ArrayList<>();
			for (char ch : text.toCharArray()) {
				if (Character.isLetter(ch)) {
					ucList.add(Character.isUpperCase(ch));
				}
			}
			Boolean[] ucs = ucList.toArray(new Boolean[0]);

			// query exact prefix
			String query1 = "select word, han from " + wordsTable
			                +" where word like ? order by weight desc, word limit "
			                + CANDIDATE_MAX_NUM;
			c = db.rawQuery(query1, new String[] {text + (showPrefix ? '%' : "")});
			if (queued.get() > 1)
				return null;
			HashSet<String> words = new HashSet<>();
			List<String> candidates = new ArrayList<>();
			String lastLatin = null;
			String lastHan = null;
			int count = 0;
			while (count < CANDIDATE_MAX_NUM) {
				if (!c.moveToNext())
					break;
				String word = stringWithCases(c.getString(0), ucs);
				if (useHanji) {
					String han = c.getString(1);
					if (han != null && !han.equals(lastHan)) {
						if (lastHan != null)
							candidates.add(lastHan);
						lastHan = han;
					}
				}
				if (useSugg && !word.equals(lastLatin)) {
					candidates.add(word);
					lastLatin = word;
				}
				words.add(word);
				count++;
			}

			// query prefix without tone
			String query2 = "select word, han from " + wordsTable
			                + " where toneless like ? order by weight desc, word limit "
			                + CANDIDATE_TONELESS_NUM;
			c = db.rawQuery(query2, new String[] {toneless + (showPrefix ? '%' : "")});
			if (queued.get() > 1) {
				candidates.clear();
				words.clear();
				return null;
			}
			while (count < CANDIDATE_TONELESS_NUM) {
				if (!c.moveToNext())
					break;
				String word = stringWithCases(c.getString(0), ucs);
				if (!words.contains(word)) {
					if (useHanji) {
						String han = c.getString(1);
						if (han != null && !han.equals(lastHan)) {
							if (lastHan != null)
								candidates.add(lastHan);
							lastHan = han;
						}
					}
					if (useSugg && !word.equals(lastLatin)) {
						candidates.add(word);
						lastLatin = word;
					}
					words.add(word);
					count++;
				}
			}
			if (useHanji && lastHan != null)
				candidates.add(lastHan);

			words.clear();
			if (queued.get() > 1) {
				candidates.clear();
				return null;
			}
			return candidates;
		} catch (Exception ex) {
			return null;
		} finally {
			if (c != null)
				c.close();
		}
	}

	private static String stringWithCases(String word, Boolean[] ucs) {
		char[] chars = word.toCharArray();
		for (int j = 0, k = 0; j < chars.length; j++) {
			char ch = chars[j];
			if (Character.isLetter(ch)) {
				if (k >= ucs.length)
					break;
				if (ucs[k])
					chars[j] = Character.toUpperCase(ch);
				else
					chars[j] = Character.toLowerCase(ch);
				k++;
			}
		}
		return new String(chars);
	}

	protected List<String> queryPrefix(String prefix,
	                                   TKInputState inputState) {
		int prefixLen = prefix.length();
		boolean isLatin = TKInputMethodService.latin.matcher(prefix.substring(0, 1)).matches();
		String lookup = isLatin ? "word" : "han";
		Cursor c = null;
		try {
			// query table
			int subtype = inputState.getInteger(Subtype);
			String wordsTable = (subtype == SUBTYPE_POJ ? "words_poj" : "words");
			// query exact prefix
			String query1 = "select word, han from " + wordsTable
			                +" where " + lookup + " like ? order by weight desc, word limit "
			                + CANDIDATE_MAX_NUM;
			c = db.rawQuery(query1, new String[] {prefix + "_%"});
			if (queued.get() > 1)
				return null;
			HashSet<String> words = new HashSet<>();
			List<String> candidates = new ArrayList<>();
			int count = 0;
			while (count < CANDIDATE_MAX_NUM) {
				if (!c.moveToNext())
					break;
				String word = c.getString(isLatin ? 0 : 1);
				if (!words.contains(word)) {
					candidates.add(word.substring(prefixLen));
					words.add(word);
				}
				count++;
			}

			words.clear();
			if (queued.get() > 1) {
				candidates.clear();
				return null;
			}
			return candidates;
		} catch (Exception ex) {
			return null;
		} finally {
			if (c != null)
				c.close();
		}
	}

	@Override
	protected void onPostExecute(List<String> candidates) {
		int count = queued.decrementAndGet();
		if (count > 0 || candidates == null || candidates.size() <= 0)
			return;
		TKViewState viewState = svc.getViewState();
		if (viewState == null)
			return;
		Object[] cdds = (Object[])viewState.get(Candidates);
		List<Object> cddsNew = new ArrayList<>();
		cddsNew.addAll(Arrays.asList(cdds));
		cddsNew.addAll(candidates);
		cdds = cddsNew.toArray(new Object[0]);
		viewState = viewState.set(Candidates, cdds);
		if (queued.get() > 0)
			return;
		svc.setViewState(viewState);
		svc.updateView(null);
	}
}
