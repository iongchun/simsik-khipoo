/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

import static tw.iongchun.taigikbd.TKInputState.Afters;
import static tw.iongchun.taigikbd.TKInputState.Boot;
import static tw.iongchun.taigikbd.TKInputState.Glottal;
import static tw.iongchun.taigikbd.TKInputState.Hat;
import static tw.iongchun.taigikbd.TKInputState.Leads;
import static tw.iongchun.taigikbd.TKInputState.PojNN;
import static tw.iongchun.taigikbd.TKInputState.Tail;
import static tw.iongchun.taigikbd.TKInputState.Tone;
import static tw.iongchun.taigikbd.TKInputState.Vowels;

/**
 * Created by iongchun on 6/28/17.
 */

public class TKComposingUtils {
	protected static final String LEADS_NULL = "(null)";
	protected static final String VOWELS_NULL = "(null)";
	protected static final String AFTERS_NULL = "(null)";
	protected static final Character GLOTTAL_NULL = 0;

	public static final TKInputState COMPOSING_EMPTY = TKInputState.EMPTY
		.setString(Hat, "")
		.setString(Leads, TKComposingUtils.LEADS_NULL)
		.setString(Vowels, TKComposingUtils.VOWELS_NULL)
		.setInteger(Tone, 0)
		.setString(Afters, TKComposingUtils.AFTERS_NULL)
		.setCharacter(Glottal, TKComposingUtils.GLOTTAL_NULL)
		.setBoolean(PojNN, false)
		.setString(Boot, "");

	protected static final String TONE_MARK_POJ = "poj";
	protected static final String TONE_MARK_TL = "tailo";
	protected static final String TONE_MARK_SUBTYPE = "subtype";
	static final HashSet<String> leads = new HashSet<>();
	private static final TwoWayMap<String, String> combinedChars = new TwoWayMap<>();
	private static final HashMap<String, Integer> vowelLevels = new HashMap<>();

	static {
		leads.add("m");
		leads.add("n");
		leads.add("ng");
		leads.add("p");
		leads.add("ph");
		leads.add("b");
		leads.add("t");
		leads.add("th");
		leads.add("k");
		leads.add("kh");
		leads.add("g");
		leads.add("ch");
		leads.add("chh");
		leads.add("ts");
		leads.add("tsh");
		leads.add("j");
		leads.add("s");
		leads.add("h");
		leads.add("l");
		// reserved
		//leads.add("dz");
		//leads.add("d");
	}

	static {
		combinedChars.put("A\u0301", "Á"); // tone2
		combinedChars.put("A\u0300", "À"); // tone3
		combinedChars.put("A\u0302", "Â"); // tone5
		combinedChars.put("A\u030c", "Ǎ"); // tone6
		combinedChars.put("A\u0304", "Ā"); // tone7
		combinedChars.put("a\u0301", "á"); // tone2
		combinedChars.put("a\u0300", "à"); // tone3
		combinedChars.put("a\u0302", "â"); // tone5
		combinedChars.put("a\u030c", "ǎ"); // tone6
		combinedChars.put("a\u0304", "ā"); // tone7
		combinedChars.put("E\u0301", "É"); // tone2
		combinedChars.put("E\u0300", "È"); // tone3
		combinedChars.put("E\u0302", "Ê"); // tone5
		combinedChars.put("E\u030c", "Ě"); // tone6
		combinedChars.put("E\u0304", "Ē"); // tone7
		combinedChars.put("e\u0301", "é"); // tone2
		combinedChars.put("e\u0300", "è"); // tone3
		combinedChars.put("e\u0302", "ê"); // tone5
		combinedChars.put("e\u030c", "ě"); // tone6
		combinedChars.put("e\u0304", "ē"); // tone7
		combinedChars.put("I\u0301", "Í"); // tone2
		combinedChars.put("I\u0300", "Ì"); // tone3
		combinedChars.put("I\u0302", "Î"); // tone5
		combinedChars.put("I\u030c", "Ǐ"); // tone6
		combinedChars.put("I\u0304", "Ī"); // tone7
		combinedChars.put("i\u0301", "í"); // tone2
		combinedChars.put("i\u0300", "ì"); // tone3
		combinedChars.put("i\u0302", "î"); // tone5
		combinedChars.put("i\u030c", "ǐ"); // tone6
		combinedChars.put("i\u0304", "ī"); // tone7
		combinedChars.put("O\u0301", "Ó"); // tone2
		combinedChars.put("O\u0300", "Ò"); // tone3
		combinedChars.put("O\u0302", "Ô"); // tone5
		combinedChars.put("O\u030c", "Ǒ"); // tone6
		combinedChars.put("O\u0304", "Ō"); // tone7
		combinedChars.put("O\u030b", "Ő"); // tone9
		combinedChars.put("o\u0301", "ó"); // tone2
		combinedChars.put("o\u0300", "ò"); // tone3
		combinedChars.put("o\u0302", "ô"); // tone5
		combinedChars.put("o\u030c", "ǒ"); // tone6
		combinedChars.put("o\u0304", "ō"); // tone7
		combinedChars.put("o\u030b", "ő"); // tone9
		combinedChars.put("U\u0301", "Ú"); // tone2
		combinedChars.put("U\u0300", "Ù"); // tone3
		combinedChars.put("U\u0302", "Û"); // tone5
		combinedChars.put("U\u030c", "Ǔ"); // tone6
		combinedChars.put("U\u0304", "Ū"); // tone7
		combinedChars.put("U\u030b", "Ű"); // tone9
		combinedChars.put("u\u0301", "ú"); // tone2
		combinedChars.put("u\u0300", "ù"); // tone3
		combinedChars.put("u\u0302", "û"); // tone5
		combinedChars.put("u\u030c", "ǔ"); // tone6
		combinedChars.put("u\u0304", "ū"); // tone7
		combinedChars.put("u\u030b", "ű"); // tone9
		combinedChars.put("M\u0301", "Ḿ"); // tone2
		combinedChars.put("m\u0301", "ḿ"); // tone2
		combinedChars.put("N\u0301", "Ń"); // tone2
		combinedChars.put("N\u0300", "Ǹ"); // tone3
		combinedChars.put("N\u030c", "Ň"); // tone6
		combinedChars.put("n\u0301", "ń"); // tone2
		combinedChars.put("n\u0300", "ǹ"); // tone3
		combinedChars.put("n\u030c", "ň"); // tone6
	}

	static {
		vowelLevels.put("i", 1);
		vowelLevels.put("u", 1);
		vowelLevels.put("ir", 1);
		vowelLevels.put("e", 2);
		vowelLevels.put("o", 2);
		vowelLevels.put("er", 2);
		vowelLevels.put("oo", 3);
		vowelLevels.put("o͘", 3);
		vowelLevels.put("ee", 3);
		vowelLevels.put("a", 4);
	}

	public static boolean isGlottal(char ch) {
		ch = Character.toLowerCase(ch);
		switch (ch) {
		default:
			return false;
		case 'h':
		case 't':
		case 'p':
		case 'k':
			return true;
		}
	}

	public static boolean isVowel(char ch) {
		ch = Character.toLowerCase(ch);
		switch (ch) {
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case '\u0358': // dot above right
		case 'u':
		case 'r':
			return true;
		}
		return false;
	}

	public static boolean isTone(char ch) {
		ch = Character.toLowerCase(ch);
		switch (ch) {
		case '\u0301': // acute accent
		case '\u0300': // grave accent
		case '\u0302': // circumflex accent
		case '\u030c': // caron
		case '\u0304': // macron
		case '\u030d': // vertical line above
		case '\u030b': // double acute accent
			return true;
		}
		return false;
	}

	public static int getToneNumber(char ch) {
		switch (ch) {
		case '\u0301': // acute accent
			return 2;
		case '\u0300': // grave accent
			return 3;
		case '\u0302': // circumflex accent
			return 5;
		case '\u030c': // caron
			return 6;
		case '\u0304': // macron
			return 7;
		case '\u030d': // vertical line above
			return 8;
		case '\u030b': // double acute accent
			return 9;
		}
		return 0;
	}

	public static boolean isNasal(String str) {
		str = str.toLowerCase(Locale.ROOT);
		return str.equals("m") || str.equals("n") || str.equals("ng");
	}

	public static boolean isValidAfters(String str) {
		if (isNasal(str))
			return true;
		str = str.toLowerCase(Locale.ROOT);
		return str.equals("nn") || str.equals("ⁿ");
	}

	public static boolean isValidAfterForLeads(String str) {
		// hh: for parsing of chh
		// sh: for parsing of tsh
		if (str.equalsIgnoreCase("hh") || str.equalsIgnoreCase("sh"))
			return true;
		return isValidLeads(str);
	}

	public static boolean isValidLeadForLeads(String str) {
		// for composing of ch and chh
		if (str.equalsIgnoreCase("c"))
			return true;
		return isValidLeads(str);
	}

	public static boolean isValidLeads(String str) {
		return leads.contains(str.toLowerCase(Locale.ROOT));
	}

	public static TKInputState parseState(CharSequence chars) {
		TKInputState state = COMPOSING_EMPTY;

		Object[] dcp = decompose(chars);
		char[] after = (char[])dcp[0];
		int idx = 0;
		char ch = after[idx++];

		// parse leading consonants
		boolean checkMore = true;
		String leads = LEADS_NULL;
		int tone = 0;
		do {
			if (isTone(ch)) {
				tone = getToneNumber(ch);
			} else {
				String check;
				if (leads == LEADS_NULL)
					check = String.valueOf(ch);
				else
					check = leads + ch;
				if (!isValidLeadForLeads(check))
					break;
				leads = check;
			}

			if (idx >= after.length)
				checkMore = false;
			else
				ch = after[idx++];
		} while (checkMore);
		if (leads != LEADS_NULL) {
			state = state.setString(Leads, leads).setInteger(Tone, tone);
		}
		if (!checkMore) {
			// move nasal to afters
			if (isNasal(leads)) {
				state = state.setString(Leads, "").setString(Vowels, "").setString(Afters, leads);
				if (tone == 0)
					state = state.setInteger(Tone, 1);
			}
			return state;
		}

		// parse vowels
		String vowels = VOWELS_NULL;
		do {
			if (isTone(ch)) {
				tone = getToneNumber(ch);
			} else {
				if (!isVowel(ch))
					break;
				if (vowels == VOWELS_NULL)
					vowels = String.valueOf(ch);
				else
					vowels += ch;
			}

			if (idx >= after.length)
				checkMore = false;
			else
				ch = after[idx++];
		} while (checkMore);
		if (vowels != VOWELS_NULL) {
			if (leads == LEADS_NULL) {
				leads = "";
				state = state.setString(Leads, leads);
			} else if (!leads.isEmpty() && !isValidLeads(leads)) {
				return state;
			}
			state = state.setString(Vowels, vowels);
			if (tone == 0)
				tone = 1;
			state = state.setInteger(Tone, tone);
		}
		if (!checkMore) {
			// move nasal without vowel from leads to afters
			if (vowels.isEmpty() && isNasal(leads))
				state = state.setString(Leads, "").setString(Afters, leads);
			return state;
		}

		// parse tailing consonants
		String afters = AFTERS_NULL;
		do {
			if (isTone(ch)) {
				tone = getToneNumber(ch);
			} else {
				String check;
				if (afters == AFTERS_NULL)
					check = String.valueOf(ch);
				else
					check = afters + ch;
				if (!isValidAfters(check))
					break;
				// nn must be accompanied with vowel(s)
				if (!isNasal(check) && (vowels == VOWELS_NULL || vowels.isEmpty()))
					break;
				afters = check;
			}

			if (idx >= after.length)
				checkMore = false;
			else
				ch = after[idx++];
		} while (checkMore);
		if (afters != AFTERS_NULL) {
			if (vowels == VOWELS_NULL) {
				if (!isNasal(afters) // nn without vowel
				    || !isValidLeads(leads)) {
					return state;
				}
				vowels = "";
				state = state.setString(Vowels, vowels);
				if (tone == 0) {
					tone = 1;
					state = state.setInteger(Tone, tone);
				}
				state = state.setString(Afters, afters);
			} else if (afters.equals("ⁿ")) {
				state = state.setString(Afters, "").setBoolean(PojNN, true);
			} else {
				state = state.setString(Afters, afters);
			}
			state = state.setInteger(Tone, tone);
		} else if (vowels == VOWELS_NULL) {
			if (leads != LEADS_NULL && isNasal(leads)) {
				// move leads to afters
				afters = leads;
				leads = vowels = "";
				state = state.setString(Leads, leads)
				             .setString(Vowels, vowels)
				             .setString(Afters, afters);
				if (tone == 0)
					tone = 1;
				state = state.setInteger(Tone, tone);
			} else {
				return state;
			}
		}
		if (!checkMore) {
			state = state.setString(Tail, chars.toString());
			return state;
		}

		// parse glottal
		if (isGlottal(ch)) {
			if (afters == AFTERS_NULL) {
				afters = "";
				state = state.setString(Afters, afters);
			}
			char glottal = ch;
			state = state.setCharacter(Glottal, glottal);
			if (tone != 8) {
				if (tone == 2)
					tone = 8;
				else
					tone = 4;
				state = state.setInteger(Tone, tone);
			}

			if (idx >= after.length)
				return state;
			ch = after[idx++];
		}

		if (afters == AFTERS_NULL || afters.isEmpty()) {
			if (ch == 'ⁿ') {
				if (afters == AFTERS_NULL) {
					afters = "";
					state = state.setString(Afters, afters);
				}
				state = state.setBoolean(PojNN, true);
			}
		}

		return state;
	}

	public static TKInputState tailoToPoj(TKInputState state) {
		// ts(h) to ch(h)
		String leads = state.getString(Leads);
		if (leads != LEADS_NULL && leads.length() >= 2) {
			char ch0 = leads.charAt(0);
			if (ch0 == 't') {
				String post = leads.substring(2);
				char ch1 = leads.charAt(1);
				if (ch1 == 's')
					state = state.setString(Leads, "ch" + post);
				else if (ch1 == 'S')
					state = state.setString(Leads, "cH" + post);
			} else if (ch0 == 'T') {
				String post = leads.substring(2);
				char ch1 = leads.charAt(1);
				if (ch1 == 's')
					state = state.setString(Leads, "Ch" + post);
				else if (ch1 == 'H')
					state = state.setString(Leads, "CH" + post);
			}
		}

		String vowels = state.getString(Vowels);
		if (vowels != VOWELS_NULL && vowels.length() >= 2) {
			// ua/e to oa/e
			char ch0 = vowels.charAt(0);
			if (ch0 == 'u') {
				char ch1 = vowels.charAt(1);
				if (ch1 == 'a' || ch1 == 'e'
				    || ch1 == 'A' || ch1 == 'E') {
					String post = vowels.substring(1);
					state = state.setString(Vowels, "o" + post);
				}
			} else if (ch0 == 'U') {
				char ch1 = vowels.charAt(1);
				if (ch1 == 'a' || ch1 == 'e'
				    || ch1 == 'A' || ch1 == 'E') {
					String post = vowels.substring(1);
					state = state.setString(Vowels, "O" + post);
				}
			}

			// oo to o͘
			int vlen = vowels.length();
			char chl1 = vowels.charAt(vlen - 1);
			if (chl1 == 'o' || chl1 == 'O') {
				char chl2 = vowels.charAt(vlen - 2);
				if (chl2 == 'o' || chl2 == 'O') {
					String pre = vowels.substring(0, vlen - 2);
					state = state.setString(Vowels, pre + chl2 + '\u0358');
				}
			}
		}

		// move nn to PojNN
		String afters = state.getString(Afters);
		if (afters.equalsIgnoreCase("nn")) {
			state = state.setString(Afters, "").setBoolean(PojNN, true);
		}

		// ing to eng, ik to ek
		if (vowels.equalsIgnoreCase("i")) {
			if (afters.equalsIgnoreCase("ng") || state.getCharacter(Glottal) == 'k') {
				char vowel = vowels.charAt(0);
				state = state.setString(Vowels, vowel == 'i' ? "e" : "E");
			}
		}

		return state;
	}

	public static String getComposingTextTL(TKInputState compoState) {
		String leads = compoState.getString(Leads, LEADS_NULL);
		if (leads == LEADS_NULL)
			return "";
		String textComposing = leads;

		String vowels = compoState.getString(Vowels, VOWELS_NULL);
		if (vowels == VOWELS_NULL)
			return textComposing;

		int tone = compoState.getInteger(Tone, 0);
		char toneChar = 0;
		switch (tone) {
		case 2: // acute accent
			toneChar = '\u0301';
			break;
		case 3: // grave accent
			toneChar = '\u0300';
			break;
		case 5: // circumflex accent
			toneChar = '\u0302';
			break;
		case 6: // caron
			toneChar = '\u030c';
			break;
		case 7: // macron
			toneChar = '\u0304';
			break;
		case 8: // vertical line above
			toneChar = '\u030d';
			break;
		case 9: // double acute accent
			toneChar = '\u030b';
			break;
		}

		String afters = compoState.getString(Afters, AFTERS_NULL);
		if (toneChar == 0) {
			textComposing += vowels;
			if (afters != AFTERS_NULL)
				textComposing += afters;
		} else {
			if (vowels.isEmpty()) {
				if (afters.length() == 1) {
					textComposing += getCombinedCharacter(afters + toneChar);
				} else {
					textComposing += getCombinedCharacter(afters.substring(0, 1) + toneChar) + afters.substring(1);
				}
			} else {
				int vidx = vowels.length() - 1;
				char chLast1a = vowels.charAt(vidx--);
				char chLast1b = 0;
				String vlast1;
				if (isR(chLast1a) && vidx >= 0) {
					// ir or er
					chLast1b = chLast1a;
					chLast1a = vowels.charAt(vidx--);
					vlast1 = String.valueOf(chLast1a) + chLast1b;
				} else if (isO(chLast1a) && vidx >= 0 && isO(vowels.charAt(vidx))) {
					// oo
					chLast1b = chLast1a;
					chLast1a = vowels.charAt(vidx--);
					vlast1 = String.valueOf(chLast1a) + chLast1b;
				} else if (chLast1a == '\u0358' && vidx >= 0 && isO(vowels.charAt(vidx))) {
					// o͘
					chLast1a = vowels.charAt(vidx--);
					chLast1b = '\u0358';
					vlast1 = String.valueOf(chLast1a) + chLast1b;
				} else if (isE(chLast1a) && vidx >= 0 && isE(vowels.charAt(vidx))) {
					// ee
					chLast1b = chLast1a;
					chLast1a = vowels.charAt(vidx--);
					vlast1 = String.valueOf(chLast1a) + chLast1b;
				} else {
					vlast1 = String.valueOf(chLast1a);
				}
				int vidx1 = vidx + 1;

				if (vidx < 0) {
					textComposing += vowels.substring(0, vidx1)
					                 + getCombinedCharacter(String.valueOf(chLast1a) + toneChar);
					if (chLast1b != 0)
						textComposing += chLast1b;
				} else {
					char chLast2a = vowels.charAt(vidx--);
					char chLast2b = 0;
					String vlast2;
					if (isR(chLast2a) && vidx >= 0) {
						// ir or er
						chLast2b = chLast2a;
						chLast2a = vowels.charAt(vidx--);
						vlast2 = String.valueOf(chLast2a) + chLast2b;
					} else if (isO(chLast2a) && vidx >= 0 && isO(vowels.charAt(vidx))) {
						// oo
						chLast2b = chLast2a;
						chLast2a = vowels.charAt(vidx--);
						vlast2 = String.valueOf(chLast2a) + chLast2b;
					} else if (chLast2a == '\u0358' && vidx >= 0 && isO(vowels.charAt(vidx))) {
						// o͘
						chLast2a = vowels.charAt(vidx--);
						chLast2b = '\u0358';
						vlast2 = String.valueOf(chLast2a) + chLast2b;
					} else if (isE(chLast2a) && vidx >= 0 && isE(vowels.charAt(vidx))) {
						// ee
						chLast2b = chLast2a;
						chLast2a = vowels.charAt(vidx--);
						vlast2 = String.valueOf(chLast2a) + chLast2b;
					} else {
						vlast2 = String.valueOf(chLast2a);
					}
					int vidx2 = vidx + 1;

					if (getVowelLevel(vlast1) >= getVowelLevel(vlast2)) {
						textComposing += vowels.substring(0, vidx1)
						                 + getCombinedCharacter(String.valueOf(chLast1a) + toneChar);
						if (chLast1b != 0)
							textComposing += chLast1b;
					} else {
						textComposing += vowels.substring(0, vidx2)
						                 + getCombinedCharacter(String.valueOf(chLast2a) + toneChar);
						if (chLast2b != 0)
							textComposing += chLast2b;
						textComposing += vlast1;
					}
				}
				if (afters != AFTERS_NULL)
					textComposing += afters;
			}
		}

		char glottal = compoState.getCharacter(Glottal, GLOTTAL_NULL);
		if (glottal != GLOTTAL_NULL)
			textComposing += glottal;
		if (compoState.getBoolean(PojNN, false))
			textComposing += 'ⁿ';

		return textComposing;
	}

	public static boolean isO(char ch) {
		return ch == 'o' || ch == 'O';
	}

	public static boolean isE(char ch) {
		return ch == 'e' || ch == 'E';
	}

	public static boolean isA(char ch) {
		return ch == 'a' || ch == 'A';
	}

	public static boolean isI(char ch) {
		return ch == 'i' || ch == 'I';
	}

	public static boolean isR(char ch) {
		return ch == 'r' || ch == 'R';
	}

	public static String getComposingTextPOJ(TKInputState compoState) {
		String leads = compoState.getString(Leads, LEADS_NULL);
		if (leads == LEADS_NULL)
			return "";
		String textComposing = leads;

		String vowels = compoState.getString(Vowels, VOWELS_NULL);
		if (vowels == VOWELS_NULL)
			return textComposing;

		int tone = compoState.getInteger(Tone, 0);
		char toneChar = 0;
		switch (tone) {
		case 2: // acute accent
			toneChar = '\u0301';
			break;
		case 3: // grave accent
			toneChar = '\u0300';
			break;
		case 5: // circumflex accent
			toneChar = '\u0302';
			break;
		case 6: // caron
			toneChar = '\u030c';
			break;
		case 7: // macron
			toneChar = '\u0304';
			break;
		case 8: // vertical line above
			toneChar = '\u030d';
			break;
		case 9: // double acute accent
			toneChar = '\u030b';
			break;
		}

		String afters = compoState.getString(Afters, AFTERS_NULL);
		char glottal = compoState.getCharacter(Glottal, GLOTTAL_NULL);
		if (toneChar == 0) {
			textComposing += vowels;
			if (afters != AFTERS_NULL)
				textComposing += afters;
		} else {
			if (vowels.isEmpty()) {
				if (afters.length() == 1) {
					textComposing += getCombinedCharacter(afters + toneChar);
				} else {
					textComposing += getCombinedCharacter(afters.substring(0, 1) + toneChar) + afters.substring(1);
				}
			} else {
				int vidx = vowels.length() - 1;
				char chLast1a = vowels.charAt(vidx--);
				char chLast1b = 0;
				String vlast1;
				if (isR(chLast1a) && vidx >= 0) {
					// ir or er
					chLast1b = chLast1a;
					chLast1a = vowels.charAt(vidx--);
					vlast1 = String.valueOf(chLast1a) + chLast1b;
				} else if (isO(chLast1a) && vidx >= 0 && isO(vowels.charAt(vidx))) {
					// oo
					chLast1b = chLast1a;
					chLast1a = vowels.charAt(vidx--);
					vlast1 = String.valueOf(chLast1a) + chLast1b;
				} else if (chLast1a == '\u0358' && vidx >= 0 && isO(vowels.charAt(vidx))) {
					// o͘
					chLast1a = vowels.charAt(vidx--);
					chLast1b = '\u0358';
					vlast1 = String.valueOf(chLast1a) + chLast1b;
				} else if (isE(chLast1a) && vidx >= 0 && isE(vowels.charAt(vidx))) {
					// ee
					chLast1b = chLast1a;
					chLast1a = vowels.charAt(vidx--);
					vlast1 = String.valueOf(chLast1a) + chLast1b;
				} else {
					vlast1 = String.valueOf(chLast1a);
				}
				int vidx1 = vidx + 1;

				if (vidx < 0) {
					textComposing += vowels.substring(0, vidx1)
					                 + getCombinedCharacter(String.valueOf(chLast1a) + toneChar);
					if (chLast1b != 0)
						textComposing += chLast1b;
				} else {
					char chLast2a = vowels.charAt(vidx--);
					char chLast2b = 0;
					String vlast2;
					if (isR(chLast2a) && vidx >= 0) {
						// ir or er
						chLast2b = chLast2a;
						chLast2a = vowels.charAt(vidx--);
						vlast2 = String.valueOf(chLast2a) + chLast2b;
					} else if (isO(chLast2a) && vidx >= 0 && isO(vowels.charAt(vidx))) {
						// oo
						chLast2b = chLast2a;
						chLast2a = vowels.charAt(vidx--);
						vlast2 = String.valueOf(chLast2a) + chLast2b;
					} else if (chLast2a == '\u0358' && vidx >= 0 && isO(vowels.charAt(vidx))) {
						// o͘
						chLast2a = vowels.charAt(vidx--);
						chLast2b = '\u0358';
						vlast2 = String.valueOf(chLast2a) + chLast2b;
					} else if (isE(chLast2a) && vidx >= 0 && isE(vowels.charAt(vidx))) {
						// ee
						chLast2b = chLast2a;
						chLast2a = vowels.charAt(vidx--);
						vlast2 = String.valueOf(chLast2a) + chLast2b;
					} else {
						vlast2 = String.valueOf(chLast2a);
					}
					int vidx2 = vidx + 1;

					boolean mark1 = false;
					if (vidx < 0) {
						boolean checkTail = false;
						if (vlast1.equalsIgnoreCase("i")) {
							if (vlast2.equalsIgnoreCase("i"))
								checkTail = true;
						} else {
							if (vlast2.equalsIgnoreCase("i"))
								mark1 = true;
							else
								checkTail = true;
						}
						if (checkTail)
							mark1 = (glottal != GLOTTAL_NULL || (afters != AFTERS_NULL && !afters.isEmpty() && !afters.equals("ⁿ")));
					}
					if (mark1) {
						textComposing += vowels.substring(0, vidx1)
						                 + getCombinedCharacter(String.valueOf(chLast1a) + toneChar);
						if (chLast1b != 0)
							textComposing += chLast1b;
					} else {
						textComposing += vowels.substring(0, vidx2)
						                 + getCombinedCharacter(String.valueOf(chLast2a) + toneChar);
						if (chLast2b != 0)
							textComposing += chLast2b;
						textComposing += vlast1;
					}
				}
				if (afters != AFTERS_NULL)
					textComposing += afters;
			}
		}

		if (glottal != GLOTTAL_NULL)
			textComposing += glottal;
		if (compoState.getBoolean(PojNN, false))
			textComposing += 'ⁿ';

		return textComposing;
	}

	private static String getCombinedCharacter(String str) {
		if (combinedChars.containsKey(str))
			return combinedChars.get(str);
		return str;
	}

	public static String getSeparatedCharacter(String str) {
		if (combinedChars.containsInvKey(str))
			return combinedChars.getInv(str);
		return str;
	}

	private static int getVowelLevel(String str) {
		str = str.toLowerCase(Locale.ROOT);
		if (vowelLevels.containsKey(str)) {
			Integer val = vowelLevels.get(str);
			if (val != null)
				return val;
		}
		return 1;
	}

	public static Object[] decompose(CharSequence cseq) {
		if (cseq == null)
			return null;
		int cseqLen = cseq.length();
		char[] chars = new char[cseqLen * 2];
		int[] csidx = new int[cseqLen * 2];
		int cidx = chars.length - 1;
		for (int i = cseqLen - 1; i >= 0; i--) {
			char ch = cseq.charAt(i);
			String cstr = String.valueOf(ch);
			String sstr = getSeparatedCharacter(cstr);
			if (sstr.equals(cstr)) {
				csidx[cidx] = i;
				chars[cidx--] = ch;
			} else {
				csidx[cidx] = i;
				chars[cidx--] = sstr.charAt(1);
				csidx[cidx] = i;
				chars[cidx--] = sstr.charAt(0);
			}
		}
		char[] cret = new char[chars.length - cidx - 1];
		System.arraycopy(chars, cidx + 1, cret, 0, cret.length);
		int[] iret = new int[cret.length];
		System.arraycopy(csidx, cidx + 1, iret, 0, iret.length);
		return new Object[] {cret, iret};
	}

	public static String composed(CharSequence cseq) {
		if (cseq == null)
			return null;
		int cseqLen = cseq.length();
		StringBuilder sbuf = new StringBuilder();
		for (int i = 0; i < cseqLen; i++) {
			char ch = cseq.charAt(i);
			int sbufSize = sbuf.length();
			if (isTone(ch) && sbufSize > 0) {
				int idx = sbufSize - 1;
				String str = String.valueOf(sbuf.charAt(idx));
				sbuf.deleteCharAt(idx);
				sbuf.append(getCombinedCharacter(str + ch));
			} else {
				sbuf.append(ch);
			}
		}
		return sbuf.toString();
	}

	public static char[] removeTone(CharSequence cseq) {
		if (cseq == null)
			return null;
		int cseqLen = cseq.length();
		char[] chars = new char[cseqLen];
		int cidx = chars.length - 1;
		for (int i = cseqLen - 1; i >= 0; i--) {
			char ch = cseq.charAt(i);
			if (ch >= 0x0300 && ch <= 0x03ff)
				continue;
			String cstr = String.valueOf(ch);
			String sstr = getSeparatedCharacter(cstr);
			if (sstr.equals(cstr)) {
				chars[cidx--] = ch;
			} else {
				chars[cidx--] = sstr.charAt(0);
			}
		}
		char[] cret = new char[chars.length - cidx - 1];
		System.arraycopy(chars, cidx + 1, cret, 0, cret.length);
		return cret;
	}

	private static class TwoWayMap<K, V> extends HashMap<K, V> {
		private final HashMap<V, K> invs = new HashMap<>();

		@Override
		public V put(K key, V value) {
			invs.put(value, key);
			return super.put(key, value);
		}

		public K getInv(Object key) {
			return invs.get(key);
		}

		public boolean containsInvKey(Object key) {
			return invs.containsKey(key);
		}
	}
}
