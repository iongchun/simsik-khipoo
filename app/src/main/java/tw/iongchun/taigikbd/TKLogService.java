/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.app.IntentService;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import androidx.annotation.Nullable;

import java.util.Vector;

/**
 * Created by iongchun on 6/5/17.
 */

public class TKLogService extends IntentService {
	private final Vector<String> logs = new Vector<>();
	private final ServiceBinder binder = new ServiceBinder();

	public TKLogService() {
		super("LogServiceHandler");
	}

	@Override
	protected void onHandleIntent(@Nullable Intent intent) {
		if (intent != null) {
			String text = intent.getStringExtra(TKLogIntent.EXTRA_LOGTXT);
			logs.add(text);
		}
	}

	public String[] getLogs() {
		String[] value = logs.toArray(new String[0]);
		logs.clear();
		return value;
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	public class ServiceBinder extends Binder {
		TKLogService getService() {
			return TKLogService.this;
		}
	}
}
