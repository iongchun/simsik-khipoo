/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Build;
import android.preference.PreferenceManager;

import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.core.graphics.drawable.DrawableCompat;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.StringBuilderPrinter;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CorrectionInfo;
import android.view.inputmethod.CursorAnchorInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

import static tw.iongchun.taigikbd.TKComposingUtils.AFTERS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.COMPOSING_EMPTY;
import static tw.iongchun.taigikbd.TKComposingUtils.GLOTTAL_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.LEADS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.TONE_MARK_POJ;
import static tw.iongchun.taigikbd.TKComposingUtils.TONE_MARK_SUBTYPE;
import static tw.iongchun.taigikbd.TKComposingUtils.TONE_MARK_TL;
import static tw.iongchun.taigikbd.TKComposingUtils.VOWELS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.composed;
import static tw.iongchun.taigikbd.TKComposingUtils.decompose;
import static tw.iongchun.taigikbd.TKComposingUtils.getComposingTextPOJ;
import static tw.iongchun.taigikbd.TKComposingUtils.getComposingTextTL;
import static tw.iongchun.taigikbd.TKComposingUtils.getSeparatedCharacter;
import static tw.iongchun.taigikbd.TKComposingUtils.getToneNumber;
import static tw.iongchun.taigikbd.TKComposingUtils.isA;
import static tw.iongchun.taigikbd.TKComposingUtils.isE;
import static tw.iongchun.taigikbd.TKComposingUtils.isGlottal;
import static tw.iongchun.taigikbd.TKComposingUtils.isNasal;
import static tw.iongchun.taigikbd.TKComposingUtils.isO;
import static tw.iongchun.taigikbd.TKComposingUtils.isR;
import static tw.iongchun.taigikbd.TKComposingUtils.isTone;
import static tw.iongchun.taigikbd.TKComposingUtils.isValidAfterForLeads;
import static tw.iongchun.taigikbd.TKComposingUtils.isValidAfters;
import static tw.iongchun.taigikbd.TKComposingUtils.isValidLeadForLeads;
import static tw.iongchun.taigikbd.TKComposingUtils.isValidLeads;
import static tw.iongchun.taigikbd.TKComposingUtils.isVowel;
import static tw.iongchun.taigikbd.TKInputState.Afters;
import static tw.iongchun.taigikbd.TKInputState.AspirationToggle;
import static tw.iongchun.taigikbd.TKInputState.AutoDash;
import static tw.iongchun.taigikbd.TKInputState.Boot;
import static tw.iongchun.taigikbd.TKInputState.CapMode;
import static tw.iongchun.taigikbd.TKInputState.CapState;
import static tw.iongchun.taigikbd.TKInputState.ComposingMode;
import static tw.iongchun.taigikbd.TKInputState.ComposingRegion;
import static tw.iongchun.taigikbd.TKInputState.ComposingStarted;
import static tw.iongchun.taigikbd.TKInputState.ComposingState;
import static tw.iongchun.taigikbd.TKInputState.DeleteAfterCluster;
import static tw.iongchun.taigikbd.TKInputState.DeleteLeadingsMode;
import static tw.iongchun.taigikbd.TKInputState.DeleteVowelsMode;
import static tw.iongchun.taigikbd.TKInputState.DictHanji;
import static tw.iongchun.taigikbd.TKInputState.DictSuggest;
import static tw.iongchun.taigikbd.TKInputState.Glottal;
import static tw.iongchun.taigikbd.TKInputState.GlottalReplaceGlottal;
import static tw.iongchun.taigikbd.TKInputState.GlottalToggleTone;
import static tw.iongchun.taigikbd.TKInputState.Hat;
import static tw.iongchun.taigikbd.TKInputState.InputStarted;
import static tw.iongchun.taigikbd.TKInputState.LanguageKey;
import static tw.iongchun.taigikbd.TKInputState.LeadReplaceLead;
import static tw.iongchun.taigikbd.TKInputState.Leads;
import static tw.iongchun.taigikbd.TKInputState.PhoneticInput;
import static tw.iongchun.taigikbd.TKInputState.PhoneticInputTempDisabled;
import static tw.iongchun.taigikbd.TKInputState.PojNN;
import static tw.iongchun.taigikbd.TKInputState.Subtype;
import static tw.iongchun.taigikbd.TKInputState.Tail;
import static tw.iongchun.taigikbd.TKInputState.TextMode;
import static tw.iongchun.taigikbd.TKInputState.Tone;
import static tw.iongchun.taigikbd.TKInputState.ToneMarkStyle;
import static tw.iongchun.taigikbd.TKInputState.VoicelessToggle;
import static tw.iongchun.taigikbd.TKInputState.Vowels;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_ASPIRATION_TOGGLE;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_AUTO_DASH;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_AFTER_CLUSTER;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_LEADINGS;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_MULTICHAR_VOWEL;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_VOWELS;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_VOWEL_CLUSTER;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DICT_HANJI;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DICT_SUGGEST;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_GLOTTAL_REPLACE;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_GLOTTAL_TOGGLE;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_IM_SWITCH;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_KEYBOARD_THEME;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_LEAD_REPLACE_LEAD;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_PHONETIC_INPUT;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_POJ_TONE_MARK;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_TONE_MARK_STYLE;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_VOICELESS_TOGGLE;
import static tw.iongchun.taigikbd.TKViewState.ActionID;
import static tw.iongchun.taigikbd.TKViewState.ActionKeyIconID;
import static tw.iongchun.taigikbd.TKViewState.ActionKeyIconType;
import static tw.iongchun.taigikbd.TKViewState.ActionKeyLabel;
import static tw.iongchun.taigikbd.TKViewState.ActionKeyType;
import static tw.iongchun.taigikbd.TKViewState.AsciiPeriodAlt;
import static tw.iongchun.taigikbd.TKViewState.AutoCorrects;
import static tw.iongchun.taigikbd.TKViewState.CandidatePrefix;
import static tw.iongchun.taigikbd.TKViewState.Candidates;
import static tw.iongchun.taigikbd.TKViewState.CommitText;
import static tw.iongchun.taigikbd.TKViewState.ComposingCorrected;
import static tw.iongchun.taigikbd.TKViewState.ComposingCorrectedCached;
import static tw.iongchun.taigikbd.TKViewState.ComposingCorrection;
import static tw.iongchun.taigikbd.TKViewState.ComposingCursor;
import static tw.iongchun.taigikbd.TKViewState.ComposingEnd;
import static tw.iongchun.taigikbd.TKViewState.ComposingStart;
import static tw.iongchun.taigikbd.TKViewState.ComposingStateDisplay;
import static tw.iongchun.taigikbd.TKViewState.ComposingText;
import static tw.iongchun.taigikbd.TKViewState.CurrentKeyboard;
import static tw.iongchun.taigikbd.TKViewState.DeleteAfter;
import static tw.iongchun.taigikbd.TKViewState.DeleteBefore;
import static tw.iongchun.taigikbd.TKViewState.HasAutoCorrection;
import static tw.iongchun.taigikbd.TKViewState.HasSuggestions;
import static tw.iongchun.taigikbd.TKViewState.InputTitle;
import static tw.iongchun.taigikbd.TKViewState.InputTypeDisplay;
import static tw.iongchun.taigikbd.TKViewState.MainKeyboard;
import static tw.iongchun.taigikbd.TKViewState.MultiLine;
import static tw.iongchun.taigikbd.TKViewState.NumSym1Primes;
import static tw.iongchun.taigikbd.TKViewState.SelectionEnd;
import static tw.iongchun.taigikbd.TKViewState.SelectionStart;
import static tw.iongchun.taigikbd.TKViewState.ShiftState;
import static tw.iongchun.taigikbd.TKViewState.ShowCompletions;
import static tw.iongchun.taigikbd.TKViewState.TextBeforeCached;
import static tw.iongchun.taigikbd.TKViewState.TextBeforeCursor;
import static tw.iongchun.taigikbd.TKViewState.TextKeyboard;
import static tw.iongchun.taigikbd.TKViewState.ToneNumHigh;
import static tw.iongchun.taigikbd.TKViewState.ToneNumLow;
import static tw.iongchun.taigikbd.TKViewState.ToneNumMid;
import static tw.iongchun.taigikbd.TKViewState.ToneNumRise;

public class TKInputMethodService extends InputMethodService
	implements KeyboardView.OnKeyboardActionListener,
	SharedPreferences.OnSharedPreferenceChangeListener {
	// for TKInputState.Subtype
	public static final int SUBTYPE_TL = 0;
	public static final int SUBTYPE_POJ = 1;
	// for TKInputState.TextType
	public static final String TEXT_TYPE_TL = "TL";
	public static final String TEXT_TYPE_POJ = "POJ";
	public static final String TEXT_TYPE_ASCII = "ASCII";
	// for TKViewState.MainKeyboard, TextKeyboard, CurrentKeyboard
	public static final int KBD_TL = 0;
	public static final int KBD_POJ = 1;
	public static final int KBD_ASCII = 2;
	public static final int KBD_NUMSYM1 = 3;
	public static final int KBD_NUMSYM2 = 4;
	public static final int KEYCODE_LANGUAGE = -101;
	// key codes for text
	public static final int KEYCODE_TONE_HIGH = 121; // y
	private static final String KEYLABEL_TONE1 = "\u25CC";
	private static final String KEYLABEL_TONE2 = "\u00B4";
	private static final String KEYLABEL_TONE8 = " \u030D";
	public static final int KEYCODE_TONE_LOW = 118; // v
	private static final String KEYLABEL_TONE3 = "\u0060";
	private static final String KEYLABEL_TONE4 = "\u25CC";
	public static final int KEYCODE_TONE_RISE = 119; // w
	private static final String KEYLABEL_TONE5 = "\u005E";
	private static final String KEYLABEL_TONE6 = "\u02C7";
	public static final int KEYCODE_TONE_MID = 102; // f
	private static final String KEYLABEL_TONE7 = "\u00AF";
	private static final String KEYLABEL_TONE9 = "\u02DD";
	public static final String DEL_LEADINGS_CHAR = "char";
	public static final String DEL_LEADINGS_VALID = "valid";
	public static final String DEL_LEADINGS_WHOLE = "whole";
	public static final String DEL_VOWELS_CHAR = "char";
	public static final String DEL_VOWELS_SINGLE = "single";
	public static final String DEL_VOWELS_WHOLE = "whole";
	public static final String[] AUTO_CORRECTS_NULL = new String[0];
	public static final Object[] CANDIDATES_NULL = new Object[0];
	public static final String LANGKEY_NEXT_IM = "imNext";
	public static final String LANGKEY_LAST_IM = "imLast";
	public static final String LANGKEY_PICK_IM = "imPick";
	public static final String LANGKEY_ASCII = "ascii";
	private static final int KEY_ACTION = 0;
	private static final int KEY_ENTER = 1;
	private static final int KEY_SPACE = 2;
	private static final int KEYCODE_PRIME = '\u2032';
	private static final String KEYLABEL_PRIME = "\u2032";
	private static final int KEYCODE_DPRIME = '\u2033';
	private static final String KEYLABEL_DPRIME = "\u2033";
	private static final int KEYCODE_NUMSYM1 = -2;
	private static final int KEYCODE_TEXT = -4;
	private static final int KEYCODE_NUMSYM2 = -6;
	private static final int KEYCODE_ACTION = -3;
	private static final int KEYCODE_SHIFT = -1;
	private static final int KEYCODE_DELETE = -5;
	private static final int KEYCODE_ENTER = 10;
	private static final int KEYCODE_APOST = 39;
	private static final int KEYCODE_QUOTE = 34;
	private static final int KEYCODE_SPACE = 32;
	private static final int KEYCODE_PERIOD = 46;
	private static final int KEYCODE_COMMA = 44;
	private static final int KEYCODE_AT = 64;
	private static final int KEYCODE_SLASH = 47;
	// key codes for symbol
	private static final int KEYCODE_DIVISION = -200; // ÷
	private static final int KEYCODE_DEGREE = -201; // °
	private static final int KEYCODE_MU = -202; // µ
	private static final int KEYCODE_ELLIPSIS = -203; // …

	// theme
	private static final int KEYCODE_SUPERSCRIPT2 = -204; // ²

	// view state
	private static final int KEYCODE_SUPERSCRIPT3 = -205; // ³
	// values for TKInputState.CapMode
	private static final String TEXT_CAP_NONE = "none";
	private static final String TEXT_CAP_SENTENCES = "sentence";
	private static final String TEXT_CAP_WORDS = "word";
	private static final String TEXT_CAP_CHARACTERS = "character";
	// for TKViewState.ActionKeyType
	private static final int KEY_TYPE_LABEL = 0;
	private static final int KEY_TYPE_ICON = 1;
	private static final Map<String, TKKeyboardView.Theme> themeMap = new HashMap<>();
	private boolean appDebug = false;
	private TKInputState inputState;
	private final Object inputStateLock = new Object();

	// keyboards and keys
	private InputMethodManager imMgr;
	private SQLiteDatabase db = null;
	private boolean testMode = true;
	private String kbdTheme = "default";
	private final AtomicBoolean dictReady = new AtomicBoolean(false);
	// test only
	private boolean subtypePojEnabledMock = false;
	private boolean subtypeTailoEnabledMock = false;
	private ViewGroup inputView;
	private TextView inputTitle;
	private ViewGroup autoCorrects;
	private TextView inputType;
	private TextView composingState;
	private TKKeyboardView kbdView;
	private TKViewState viewState = null;
	private final Keyboard[] kbds = new Keyboard[5];
	private final Keyboard.Key[][] kbdKeys = new Keyboard.Key[3][];
	// action keys
	private final int[] keyIdxActions = new int[kbds.length];
	// Enter keys
	private final int[] keyIdxEnters = new int[kbds.length];
	private final int[] keyWidthEnters = new int[kbds.length];
	private final int[] keyGapEnters = new int[kbds.length];
	private final String[] keyLabelEnters = new String[kbds.length];
	// Space keys
	private final int[] keyIdxSpaces = new int[kbds.length];
	private final int[] keyWidthSpaces = new int[kbds.length];
	// period (".") key for ASCII
	private int keyIdxPeriodASCII;
	private Keyboard.Key keyPeriodASCII;
	// apostrophe (') keys for numsym1
	private int keyIdxApostNS1;
	private Keyboard.Key keyApostNS1;
	// quotation (") keys for numsym1
	private int keyIdxQuoteNS1;
	private Keyboard.Key keyQuoteNS1;
	// rise tone key
	private int keyIdxTlToneRise;
	private Keyboard.Key keyTlToneRise;
	private int keyIdxPojToneRise;
	private Keyboard.Key keyPojToneRise;
	// rise tone key
	private int keyIdxTlToneHigh;
	private Keyboard.Key keyTlToneHigh;
	private int keyIdxPojToneHigh;
	private Keyboard.Key keyPojToneHigh;
	// middle tone key
	private int keyIdxTlToneMid;
	private Keyboard.Key keyTlToneMid;
	private int keyIdxPojToneMid;
	private Keyboard.Key keyPojToneMid;
	// rise tone key
	private int keyIdxTlToneLow;
	private Keyboard.Key keyTlToneLow;
	private int keyIdxPojToneLow;
	private Keyboard.Key keyPojToneLow;

	private ViewGroup candidatesView;
	private ViewGroup candidates = null;
	private TextView candidatePlaceHolder = null;
	private final AtomicBoolean candidatesShown = new AtomicBoolean(false);
	private boolean zeroCursorWorkAround = false;
	private boolean kbdSwitcher = true;
	private boolean textEditorWillAutoComplete = false;
	private boolean textMultiLine = false;

	static {
		themeMap.put("dark", TKKeyboardView.dark);
		themeMap.put("light", TKKeyboardView.light);
		themeMap.put("blue", TKKeyboardView.blue);
		themeMap.put("skyBlue", TKKeyboardView.skyBlue);
		themeMap.put("reddishPurple", TKKeyboardView.reddishPurple);
		themeMap.put("bluishGreen", TKKeyboardView.bluishGreen);
		themeMap.put("vermillion", TKKeyboardView.vermillion);
		themeMap.put("orange", TKKeyboardView.orange);
		themeMap.put("yellow", TKKeyboardView.yellow);
	}

	public TKInputMethodService() {
	}

	private static boolean isOoNasal(String str) {
		str = str.toLowerCase(Locale.ROOT);
		return str.equals("ng") || str.equals("nn") || str.equals("m") || str.equals("ⁿ");
	}

	static final Pattern latin = Pattern.compile("\\p{Latin}");

	private static boolean isPhraseCharacter(int ch) {
		if (ch == '-' || latin.matcher(new String(new int[] {ch}, 0, 1)).matches())
			return true;
		int cat = Character.getType(ch);
		switch (cat) {
		case Character.COMBINING_SPACING_MARK:
		case Character.NON_SPACING_MARK:
		case Character.ENCLOSING_MARK:
		case Character.MODIFIER_LETTER:
			return true;
		}
		return false;
	}

	protected void log(String text) {
		if (appDebug) {
			String pfx = String.valueOf(System.currentTimeMillis() % (60 * 60 * 1000)) + ' ';
			pfx += "[" + Thread.currentThread().getName() + "] ";
			Intent intent = new TKLogIntent(this, pfx + text);
			startService(intent);
		}
	}

	protected void log(String text, Object... args) {
		if (args != null && args.length >= 1) {
			StringBuilder sb = new StringBuilder(text);
			for (Object arg : args) {
				if (arg == null)
					sb.append("(null)");
				else
					sb.append(arg.toString());
			}
			text = sb.toString();
		}
		log(text);
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(TKLocaleHelper.overrideContext(newBase));
	}

	protected void initInputState() {
		inputState = new TKInputState().setBoolean(DictSuggest, true)
		                               .setBoolean(AutoDash, true)
		                               .setString(ToneMarkStyle, TONE_MARK_SUBTYPE)
		                               .setBoolean(GlottalReplaceGlottal, false)
		                               .setBoolean(GlottalToggleTone, false)
		                               .setBoolean(PhoneticInput, false)
		                               .setBoolean(AspirationToggle, true)
		                               .setBoolean(VoicelessToggle, false)
		                               .setBoolean(LeadReplaceLead, false)
		                               .setString(DeleteLeadingsMode, DEL_LEADINGS_VALID)
		                               .setString(LanguageKey, LANGKEY_NEXT_IM)
		                               .setString(DeleteVowelsMode, DEL_VOWELS_SINGLE)
		                               .setBoolean(DeleteAfterCluster, false)
		                               .setBoolean(InputStarted, false);
	}

	protected void fineInputState() {
		inputState = null;
	}

	public TKInputState getInputState() {
		synchronized (inputStateLock) {
			return inputState;
		}
	}

	protected void setInputState(TKInputState newState) {
		synchronized (inputStateLock) {
			inputState = newState;
		}
	}

	public TKInputState getComposingState() {
		return inputState.getState(ComposingState);
	}

	public TKInputState getComposingRegion() {
		return inputState.getState(ComposingRegion);
	}

	protected void startInputState() {
		inputState = inputState.setBoolean(InputStarted, true).setBoolean(TextMode, false);
	}

	protected void finishInputState() {
		inputState = inputState.setBoolean(InputStarted, false);
	}

	@Override
	public void onCreate() {
		log("onCreate");
		testMode = false;

		if (getPackageName().endsWith("debug"))
			appDebug = true;

		TKNotification.init(this);
		imMgr = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

		// setup preference defaults
		PreferenceManager.setDefaultValues(this, R.xml.pref_intro, false);
		PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
		PreferenceManager.setDefaultValues(this, R.xml.pref_leadings, false);
		PreferenceManager.setDefaultValues(this, R.xml.pref_delete_key, false);
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		migrateProperties(sharedPref);
		// make sure the fallback values here match preference default,
		// otherwise the value of new preference is unexpected
		boolean dictSuggest = sharedPref.getBoolean(PREF_DICT_SUGGEST, true);
		boolean dictHanji = sharedPref.getBoolean(PREF_DICT_HANJI, true);
		boolean autoDash = sharedPref.getBoolean(PREF_AUTO_DASH, true);
		String toneMarkStyle = sharedPref.getString(PREF_TONE_MARK_STYLE, TONE_MARK_SUBTYPE);
		boolean aspirationToggle = sharedPref.getBoolean(PREF_ASPIRATION_TOGGLE, true);
		boolean voicelessToggle = sharedPref.getBoolean(PREF_VOICELESS_TOGGLE, false);
		boolean phoneticInput = sharedPref.getBoolean(PREF_PHONETIC_INPUT, false);
		boolean leadReplaceLead = sharedPref.getBoolean(PREF_LEAD_REPLACE_LEAD, false);
		boolean glottalReplaceGlottal = sharedPref.getBoolean(PREF_GLOTTAL_REPLACE, false);
		boolean glottalToggleTone = sharedPref.getBoolean(PREF_GLOTTAL_TOGGLE, false);
		String deleteVowelsMode = sharedPref.getString(PREF_DEL_VOWELS, DEL_VOWELS_SINGLE);
		String deleteLeadingsMode = sharedPref.getString(PREF_DEL_LEADINGS, DEL_LEADINGS_VALID);
		boolean deleteAfterCluster = sharedPref.getBoolean(PREF_DEL_AFTER_CLUSTER, false);
		String languageKey = sharedPref.getString(PREF_IM_SWITCH, LANGKEY_NEXT_IM);
		kbdTheme = sharedPref.getString(PREF_KEYBOARD_THEME, kbdTheme);
		log("Read preferences:\n dictSuggest: ", dictSuggest,
		    "\n dictHanji: ", dictHanji,
		    "\n autoDash: ", autoDash,
		    "\n toneMarkStyle: ", toneMarkStyle,
		    "\n aspirationToggle: ", aspirationToggle,
		    "\n voicelessToggle: ", voicelessToggle,
		    "\n voicelessToggle: ", voicelessToggle,
		    "\n phoneticInput: ", phoneticInput,
		    "\n leadReplaceLead: ", leadReplaceLead,
		    "\n glottalToggleTone: ", glottalToggleTone,
		    "\n deleteLeadingsMode: ", deleteLeadingsMode,
		    "\n deleteVowelsMode: ", deleteVowelsMode,
		    "\n deleteAfterCluster: ", deleteAfterCluster,
		    "\n languageKey: ", languageKey,
		    "\n keyboardTheme: ", kbdTheme
		);

		LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				dictReady.set(true);
			}
		}, new IntentFilter(TKBackgroundService.INTENT_DB_READY));
		if (dictSuggest) {
			dictReady.set(false);
			//deleteDatabase(TKDatabase.DATABASE_NAME);
			TKBackgroundService.updateDatabase(this);
			db = new TKDatabase(this).getReadableDatabase();
		}

		initInputState();
		inputState = inputState.setBoolean(DictSuggest, dictSuggest)
		                       .setBoolean(DictHanji, dictHanji)
		                       .setBoolean(AutoDash, autoDash)
		                       .setString(ToneMarkStyle, toneMarkStyle)
		                       .setBoolean(AspirationToggle, aspirationToggle)
		                       .setBoolean(VoicelessToggle, voicelessToggle)
		                       .setBoolean(PhoneticInput, phoneticInput)
		                       .setBoolean(LeadReplaceLead, leadReplaceLead)
		                       .setBoolean(GlottalReplaceGlottal, glottalReplaceGlottal)
		                       .setBoolean(GlottalToggleTone, glottalToggleTone)
		                       .setString(DeleteLeadingsMode, deleteLeadingsMode)
		                       .setString(DeleteVowelsMode, deleteVowelsMode)
		                       .setBoolean(DeleteAfterCluster, deleteAfterCluster)
		                       .setString(LanguageKey, languageKey);
		sharedPref.registerOnSharedPreferenceChangeListener(this);

		TKLocaleHelper.setLocale(this);

		super.onCreate();
	}

	protected void migrateProperties(SharedPreferences sharedPref) {
		migrateDelVowels(sharedPref);
		migrateToneMarkStyle(sharedPref);
	}

	// test only
	protected void setSubtypePojEnabledMock(boolean value) {
		subtypePojEnabledMock = value;
	}

	// test only
	protected void setSubtypeTailoEnabledMock(boolean value) {
		subtypeTailoEnabledMock = value;
	}

	protected void migrateToneMarkStyle(SharedPreferences sharedPref) {
		SharedPreferences.Editor editor = sharedPref.edit();
		if (sharedPref.contains(PREF_POJ_TONE_MARK)) {
			boolean hasPOJ = false;
			boolean hasTL = false;
			if (imMgr == null) {
				// test only
				hasPOJ = subtypePojEnabledMock;
				hasTL = subtypeTailoEnabledMock;
			} else {
				String pkgName = getPackageName();
				for (InputMethodInfo imi : imMgr.getInputMethodList()) {
					if (pkgName.equals(imi.getPackageName())) {
						for (InputMethodSubtype subtype : imMgr.getEnabledInputMethodSubtypeList(imi, true)) {
							String extra = subtype.getExtraValue();
							if (extra.equals("orthography=POJ"))
								hasPOJ = true;
							else if (extra.equals("orthography=TL"))
								hasTL = true;
						}
						break;
					}
				}
			}
			boolean pojToneMark = sharedPref.getBoolean(PREF_POJ_TONE_MARK, false);

			String style = TONE_MARK_SUBTYPE;
			if (pojToneMark) {
				if (hasTL)
					style = TONE_MARK_POJ;
			} else {
				if (hasPOJ && !hasTL)
					style = TONE_MARK_TL;
			}
			log("migrate deprecated property ", PREF_POJ_TONE_MARK,
			    " value ", pojToneMark,
			    " to new property ", PREF_TONE_MARK_STYLE,
			    " value ", style);
			editor.putString(PREF_TONE_MARK_STYLE, style);
			editor.remove(PREF_POJ_TONE_MARK);
		}
		editor.apply();
	}

	protected void migrateDelVowels(SharedPreferences sharedPref) {
		SharedPreferences.Editor editor = sharedPref.edit();
		if (sharedPref.contains(PREF_DEL_MULTICHAR_VOWEL)) {
			boolean deleteMultiCharVowel = sharedPref.getBoolean(PREF_DEL_MULTICHAR_VOWEL, false);
			if (deleteMultiCharVowel) {
				String delVowels = DEL_VOWELS_SINGLE;
				log("migrate deprecated property ", PREF_DEL_MULTICHAR_VOWEL,
				    " value ", deleteMultiCharVowel,
				    " to new property ", PREF_DEL_VOWELS,
				    " value ", delVowels);
				editor.putString(PREF_DEL_VOWELS, delVowels);
			}
			editor.remove(PREF_DEL_MULTICHAR_VOWEL);
		}
		if (sharedPref.contains(PREF_DEL_VOWEL_CLUSTER)) {
			boolean deleteVowelCluster = sharedPref.getBoolean(PREF_DEL_VOWEL_CLUSTER, false);
			if (deleteVowelCluster) {
				String delVowels = DEL_VOWELS_WHOLE;
				log("migrate deprecated property ", PREF_DEL_VOWEL_CLUSTER,
				    " value ", deleteVowelCluster,
				    " to new property ", PREF_DEL_VOWELS,
				    " value ", delVowels);
				editor.putString(PREF_DEL_VOWELS, delVowels);
			}
			editor.remove(PREF_DEL_VOWEL_CLUSTER);
		}
		editor.apply();
	}

	@Override
	public void onDestroy() {
		log("onDestroy");

		// will call onFinishInput
		super.onDestroy();

		fineCandidatesView();
		fineInputView();
		if (db != null) {
			db.close();
			db = null;
		}
		imMgr = null;
		inputState = null;
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPref, String key) {
		log("onSharedPreferenceChanged: ", key);
		TKInputState inputState = getInputState();
		if (inputState == null) // FIXME: why null?
			return;
		switch (key) {
		case PREF_AUTO_DASH: {
				boolean value = sharedPref.getBoolean(PREF_AUTO_DASH, false);
				log("change AutoDash to ", value);
				inputState = inputState.setBoolean(AutoDash, value);
			}
			break;
		case PREF_DICT_SUGGEST: {
				boolean value = sharedPref.getBoolean(PREF_DICT_SUGGEST, false);
				log("change DictSuggest to ", value);
				inputState = inputState.setBoolean(DictSuggest, value);
				if (value) {
					dictReady.set(false);
					TKBackgroundService.updateDatabase(this);
					if (db == null)
						db = new TKDatabase(this).getReadableDatabase();
				} else if (db != null) {
					db.close();
					db = null;
					TKBackgroundService.terminateDatabase();
				}
			}
			break;
		case PREF_DICT_HANJI: {
				boolean value = sharedPref.getBoolean(PREF_DICT_HANJI, false);
				log("change DictHanji to ", value);
				inputState = inputState.setBoolean(DictHanji, value);
			}
			break;
		case PREF_TONE_MARK_STYLE: {
				String value = sharedPref.getString(PREF_TONE_MARK_STYLE, TONE_MARK_SUBTYPE);
				log("change ToneMarkStyle to ", value);
				inputState = inputState.setString(ToneMarkStyle, value);
			}
			break;
		case PREF_ASPIRATION_TOGGLE: {
				boolean value = sharedPref.getBoolean(PREF_ASPIRATION_TOGGLE, false);
				log("change AspirationToggle to ", value);
				inputState = inputState.setBoolean(AspirationToggle, value);
			}
			break;
		case PREF_VOICELESS_TOGGLE: {
				boolean value = sharedPref.getBoolean(PREF_VOICELESS_TOGGLE, false);
				log("change VoicelessToggle to ", value);
				inputState = inputState.setBoolean(VoicelessToggle, value);
			}
			break;
		case PREF_PHONETIC_INPUT: {
				boolean value = sharedPref.getBoolean(PREF_PHONETIC_INPUT, false);
				log("change PhoneticInput to ", value);
				inputState = inputState.setBoolean(PhoneticInput, value);
			}
			break;
		case PREF_LEAD_REPLACE_LEAD: {
				boolean value = sharedPref.getBoolean(PREF_LEAD_REPLACE_LEAD, false);
				log("change LeadReplaceLead to ", value);
				inputState = inputState.setBoolean(LeadReplaceLead, value);
			}
			break;
		case PREF_GLOTTAL_REPLACE: {
				boolean value = sharedPref.getBoolean(PREF_GLOTTAL_REPLACE, false);
				log("change GlottalReplaceGlottal to ", value);
				inputState = inputState.setBoolean(GlottalReplaceGlottal, value);
			}
			break;
		case PREF_GLOTTAL_TOGGLE: {
				boolean value = sharedPref.getBoolean(PREF_GLOTTAL_TOGGLE, false);
				log("change GlottalToggleTone to ", value);
				inputState = inputState.setBoolean(GlottalToggleTone, value);
			}
			break;
		case PREF_DEL_VOWELS: {
				String value = sharedPref.getString(PREF_DEL_VOWELS, DEL_VOWELS_CHAR);
				log("change DeleteVowelsMode to ", value);
				inputState = inputState.setString(DeleteVowelsMode, value);
			}
			break;
		case PREF_DEL_LEADINGS: {
				String value = sharedPref.getString(PREF_DEL_LEADINGS, DEL_LEADINGS_CHAR);
				log("change DeleteLeadingsMode to ", value);
				inputState = inputState.setString(DeleteLeadingsMode, value);
			}
			break;
		case PREF_DEL_AFTER_CLUSTER: {
				boolean value = sharedPref.getBoolean(PREF_DEL_AFTER_CLUSTER, false);
				log("change DeleteAfterCluster to ", value);
				inputState = inputState.setBoolean(DeleteAfterCluster, value);
			}
			break;
		case PREF_IM_SWITCH: {
				String value = sharedPref.getString(PREF_IM_SWITCH, LANGKEY_NEXT_IM);
				log("change LanguageKey to ", value);
				inputState = inputState.setString(LanguageKey, value);
			}
			break;
		case PREF_KEYBOARD_THEME: {
				String value = sharedPref.getString(PREF_KEYBOARD_THEME, kbdTheme);
				log("change KeyboardTheme to ", value);
				kbdTheme = value;
				if (kbdView != null)
					updateTheme();
			}
			break;
		}
		setInputState(inputState);
	}

	// app got focus
	@Override
	public void onBindInput() {
		log("onBindInput");
		super.onBindInput();
	}

	// app unfocused
	@Override
	public void onUnbindInput() {
		log("onUnbindInput");
		super.onUnbindInput();
	}

	// will be called between onBindInput and onUnbindInput,
	// even when input view is not started
	// this callback is not always called when not editing,
	// should always check initial subtype in onStartInputView
	@Override
	protected void onCurrentInputMethodSubtypeChanged(InputMethodSubtype subType) {
		log("onCurrentInputMethodSubtypeChanged: ", subType);
		if (inputState == null) // FIXME: why null?
			return;
		switchSubtype(subType);
		if (inputState.getBoolean(InputStarted)) {
			updateViewStateForSubtype();
			if (inputState.getBoolean(TextMode) && inputState.getBoolean(ComposingMode)
			    && inputState.getBoolean(ComposingStarted)) {
				// change tone mark and candidates
				updateComposingText();
			}
			InputConnection ic = getCurrentInputConnection();
			ic.beginBatchEdit();
			updateView(ic);
			ic.endBatchEdit();
		}
		super.onCurrentInputMethodSubtypeChanged(subType);
	}

	private void switchSubtype(InputMethodSubtype subType) {
		if (appDebug) {
			StringBuilder sb = new StringBuilder("switch to subtype: ");
			if (subType == null) {
				sb.append("(null)");
			} else {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
					sb.append("\n language tag: ");
					sb.append(subType.getLanguageTag());
				}
				sb.append("\n locale: ");
				sb.append(subType.getLocale());
				sb.append("\n mode: ");
				sb.append(subType.getMode());
				sb.append("\n extra value: ");
				sb.append(subType.getExtraValue());
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
					sb.append("\n overrides implicitly enabled subtype: ");
					sb.append(subType.overridesImplicitlyEnabledSubtype());
					sb.append("\n is auxiliary: ");
					sb.append(subType.isAuxiliary());
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
						sb.append("\n is ASCII capable: ");
						sb.append(subType.isAsciiCapable());
					}
				}
			}
			log(sb.toString());
		}
		if (subType == null)
			return;

		// default subtype is TL
		int targetSubtype = SUBTYPE_TL;
		String[] extras = subType.getExtraValue().split(",");
		for (String extra : extras) {
			if (extra.equals("orthography=POJ")) {
				targetSubtype = SUBTYPE_POJ;
				break;
			}
		}
		switchSubtype(targetSubtype);
	}

	protected void switchSubtype(int subType) {
		String textType;
		switch (subType) {
		// default subtype is TL
		default:
		case SUBTYPE_TL:
			textType = TEXT_TYPE_TL;
			break;
		case SUBTYPE_POJ:
			textType = TEXT_TYPE_POJ;
			break;
		}
		inputState = inputState.setInteger(Subtype, subType);
	}

	private int lastOrientHeight = -1;
	private int lastOrientValue = -1;

	private static int getOrientValue(int rotation) {
		if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180)
			return 0;
		else
			return 1;
	}

	// before input view created
	@Override
	public boolean onEvaluateFullscreenMode() {
		log("onEvaluateFullscreenMode");
		boolean value = super.onEvaluateFullscreenMode();
		log(" default value: " + value);

		WindowManager windowManager = (WindowManager)getSystemService(WINDOW_SERVICE);
		Display display = windowManager.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		log("display metrics: width=" + metrics.widthPixels + " height=" + metrics.heightPixels);
		int heightPx = metrics.heightPixels;
		int rotation = display.getRotation();
		log("display: rotation=" + rotation);
		if (metrics.heightPixels != metrics.widthPixels) {
			if (lastOrientValue == -1) {
				lastOrientValue = getOrientValue(rotation);
				lastOrientHeight = metrics.heightPixels;
			} else {
				int thisOrientValue = getOrientValue(rotation);
				if (thisOrientValue != lastOrientValue) {
					if (lastOrientHeight == metrics.heightPixels) {
						log("bug: metrics does not change with rotation");
						if (thisOrientValue == 1)
							heightPx = metrics.widthPixels;
					}
				}
			}
		}
		log("display height in pixels: " + heightPx);

		float keyHeight = getResources().getDimension(R.dimen.key_height);
		log("key height: " + keyHeight);
		value = (heightPx < keyHeight * 8);
		log("onEvaluateFullscreenMode: " + value);
		return value;
	}

	@Override
	public View onCreateInputView() {
		log("onCreateInputView");

		if (viewState != null)
			fineInputView();
		initInputView();

		if (appDebug) {
			ViewGroup debugHeader = inputView.findViewById(R.id.debug_header);
			debugHeader.setVisibility(View.VISIBLE);
		}
		return inputView;
	}

	@SuppressLint("InflateParams")
	private void initInputView() {
		inputView = (ViewGroup)getLayoutInflater().inflate(R.layout.input_view, null);
		inputView.setOnClickListener(v -> toggleCandidates());
		inputTitle = inputView.findViewById(R.id.input_title);
		autoCorrects = inputView.findViewById(R.id.autocorrects);
		inputType = inputView.findViewById(R.id.input_mode);
		composingState = inputView.findViewById(R.id.input_state);
		kbdView = inputView.findViewById(R.id.keyboard);
		updateTheme();
		kbdView.setOnKeyboardActionListener(this);

		initViewState();
		viewState = viewState.setString(InputTitle, inputTitle.getText().toString())
		                     .setString(InputTypeDisplay, inputType.getText().toString())
		                     .setString(ComposingStateDisplay, composingState.getText().toString())
		                     .setBoolean(ShiftState, kbdView.isShifted())
		                     .resetChanged();

		initKeyboard();
	}

	private void fineInputView() {
		fineKeyboard();
		fineViewState();
		kbdView = null;
		composingState = null;
		inputType = null;
		inputTitle = null;
		inputView = null;
	}

	// text: capitalization mode

	private void updateTheme() {
		TKKeyboardView.Theme theme = themeMap.get(kbdTheme);
		kbdView.setTheme(theme);
		if (theme == null) {
			int bgDark = getResources().getColor(android.R.color.background_dark);
			int textColor = getResources().getColor(R.color.colorAccent);
			theme = new TKKeyboardView.Theme(bgDark, bgDark, 0, 0, null, textColor, null, 0);
		}
		inputView.setBackgroundColor(theme.bgColor);
		inputTitle.setBackgroundColor(theme.keyColor);
		inputTitle.setTextColor(theme.textColor);
		if (appDebug) {
			TextView inputMode = inputView.findViewById(R.id.input_mode);
			inputMode.setBackgroundColor(theme.keyColor);
			inputMode.setTextColor(theme.textColor);
			TextView inputState = inputView.findViewById(R.id.input_state);
			inputState.setBackgroundColor(theme.keyColor);
			inputState.setTextColor(theme.textColor);
		}
		if (candidatesView != null) {
			updateThemeForCandidatesView(theme);
		}
	}

	protected TKViewState getViewState() {
		return viewState;
	}

	protected void setViewState(TKViewState newState) {
		if (viewState != null)
			viewState = newState;
	}

	protected void initViewState() {
		viewState = new TKViewState();
	}

	protected void fineViewState() {
		viewState = null;
	}

	protected void startViewState(EditorInfo info) {
		if (info.initialSelStart <= info.initialSelEnd) {
			viewState = viewState.setInteger(SelectionStart, info.initialSelStart)
			                     .setInteger(SelectionEnd, info.initialSelEnd);
		} else {
			viewState = viewState.setInteger(SelectionStart, info.initialSelEnd)
			                     .setInteger(SelectionEnd, info.initialSelStart);
		}
		viewState = viewState.setInteger(ComposingStart, -1)
		                     .setInteger(ComposingEnd, -1)
		                     .setString(ComposingText, "")
		                     .resetChanged();
	}

	protected void finishViewState() {
		viewState = viewState.resetChanged();
	}

	private CharSequence getTextBeforeCursor(InputConnection ic, int count) {
		int cursor = viewState.getInteger(SelectionStart);
		int delLen = 0;
		if (viewState.contains(DeleteBefore)) {
			delLen = viewState.getInteger(DeleteBefore);
			cursor -= delLen;
		}

		if (viewState.contains(TextBeforeCursor)) {
			int cursorOrg = viewState.getInteger(TextBeforeCursor);
			CharSequence cache = (CharSequence)viewState.get(TextBeforeCached);
			int beforeOrg = cursorOrg - cache.length();
			int before = cursor - count;
			if (before < 0)
				before = 0;
			if (cursorOrg >= cursor && beforeOrg <= before) {
				// return content from cache
				int begin = before - beforeOrg;
				int end = cursor - beforeOrg;
				if (end >= begin) {
					CharSequence cs = cache.subSequence(begin, end);
					log("cached text before ", cursor, ": ", cs);
					return cs;
				}
			}
		}

		// text before: composing text
		StringBuilder sb = new StringBuilder();
		String compText = viewState.getString(ComposingText);
		int compLen;
		if (viewState.contains(ComposingCursor)) {
			compLen = viewState.getInteger(ComposingCursor);
			compText = compText.substring(0, compLen);
		} else {
			compLen = compText.length();
		}
		if (compLen > 0) {
			if (compLen <= delLen) {
				delLen -= compLen;
				compLen = 0;
			} else {
				compLen -= delLen;
				delLen = 0;
				sb.append(compText, 0, compLen);
				if (compLen >= count) {
					viewState = viewState.setInteger(TextBeforeCursor, cursor)
					                     .set(TextBeforeCached, sb);
					return sb.subSequence(compLen - count, compLen);
				}
			}
		}
		int beforeLen = compLen;

		// text before: commit text
		String commitText = viewState.getString(CommitText);
		if (commitText != null) {
			int commitLen = commitText.length();
			if (commitLen <= delLen) {
				delLen -= commitLen;
			} else {
				commitLen -= delLen;
				delLen = 0;
				sb.insert(0, commitText, 0, commitLen);
				beforeLen += commitLen;
				if (beforeLen >= count) {
					viewState = viewState.setInteger(TextBeforeCursor, cursor)
					                     .set(TextBeforeCached, sb);
					return sb.subSequence(beforeLen - count, beforeLen);
				}
			}
		}

		TKViewState orgState = viewState.getChangedFrom();
		int orgCompLen;
		if (orgState.contains(ComposingCursor))
			orgCompLen = orgState.getInteger(ComposingCursor);
		else
			orgCompLen = orgState.getString(ComposingText).length();
		int depreLen = delLen + orgCompLen;
		int leftLen = count - beforeLen + depreLen;
		CharSequence cs = ic.getTextBeforeCursor(leftLen, 0);
		log("view: get text before of length ", leftLen, ": [", cs, ']');
		if (cs != null && cs.length() > 0) {
			int csLen = cs.length();
			if (csLen > depreLen) {
				csLen -= depreLen;
				sb.insert(0, cs, 0, csLen);
				beforeLen += csLen;
			}
		}
		viewState = viewState.setInteger(TextBeforeCursor, cursor)
		                     .set(TextBeforeCached, sb);
		if (beforeLen > count)
			return sb.subSequence(beforeLen - count, beforeLen);
		else
			return sb;
	}

	// input inputState: composing text

	// count is number of code-points
	private void addDeleteCodePointsBefore(InputConnection ic, int count) {
		if (count <= 0)
			return;
		int numChars = 0;
		CharSequence before = getTextBeforeCursor(ic, count * 2);
		for (int i = before.length() - 1; i >= 0; i--) {
			if (count <= 0)
				break;
			numChars++;
			if (Character.isLowSurrogate(before.charAt(i)))
				continue;
			count--;
		}
		if (numChars > 0) {
			viewState = viewState.setInteger(DeleteBefore, numChars);
			CharSequence cache = (CharSequence)viewState.get(TextBeforeCached);
			if (cache != null) {
				if (numChars >= cache.length()) {
					viewState = viewState.remove(TextBeforeCursor)
					                     .remove(TextBeforeCached);
				} else {
					viewState = viewState.set(TextBeforeCached,
					                          cache.subSequence(0, cache.length() - numChars));
				}
			}
		}
	}

	private void updateViewStateForSubtype() {
		final int subType = inputState.getInteger(Subtype);
		viewState = viewState.setString(InputTitle, getInputTitleDisplay(subType));

		final int orgMain = viewState.getInteger(MainKeyboard);
		final int newMain;
		switch (subType) {
		default:
		case SUBTYPE_TL:
			newMain = KBD_TL;
			break;
		case SUBTYPE_POJ:
			newMain = KBD_POJ;
			break;
		}
		viewState = viewState.setInteger(MainKeyboard, newMain);

		final int kbdText = viewState.getInteger(TextKeyboard);
		if (kbdText == orgMain)
			viewState = viewState.setInteger(TextKeyboard, newMain);

		final int kbdNow = viewState.getInteger(CurrentKeyboard);
		if (kbdNow == orgMain)
			viewState = viewState.setInteger(CurrentKeyboard, newMain);
	}

	protected String getInputTitleDisplay(int subtype) {
		int titleStringID;
		switch (subtype) {
		default:
		case SUBTYPE_TL:
			titleStringID = R.string.input_title_tl;
			break;
		case SUBTYPE_POJ:
			titleStringID = R.string.input_title_poj;
			break;
		}
		return getResources().getString(titleStringID);
	}

	private void initKeyboard() {
		kbds[KBD_TL] = new Keyboard(this, R.xml.kbd_tailo);
		kbds[KBD_POJ] = new Keyboard(this, R.xml.kbd_poj);
		kbds[KBD_NUMSYM1] = new Keyboard(this, R.xml.kbd_numsym1);
		kbds[KBD_NUMSYM2] = new Keyboard(this, R.xml.kbd_numsym2);
		kbds[KBD_ASCII] = new Keyboard(this, R.xml.kbd_ascii);

		int idx;
		if (appDebug) {
			idx = 0;
			for (Keyboard.Key key : kbds[KBD_TL].getKeys()) {
				StringBuilder sb = new StringBuilder("key#");
				sb.append(idx);
				sb.append(": label=");
				sb.append(key.label);
				sb.append(" text=");
				sb.append(key.text);
				sb.append(" codes=");
				if (key.codes == null) {
					sb.append("(null)");
				} else {
					sb.append('[');
					for (int kcode : key.codes) {
						sb.append(kcode);
						sb.append(' ');
					}
					sb.append(']');
				}
				log(sb.toString());
				idx++;
			}
		}

		Keyboard.Key[] keyActions = new Keyboard.Key[kbds.length];
		kbdKeys[KEY_ACTION] = keyActions;
		Keyboard.Key[] keyEnters = new Keyboard.Key[kbds.length];
		kbdKeys[KEY_ENTER] = keyEnters;
		Keyboard.Key[] keySpaces = new Keyboard.Key[kbds.length];
		kbdKeys[KEY_SPACE] = keySpaces;
		for (int i = 0; i < kbds.length; i++) {
			Keyboard kbd = kbds[i];
			idx = 0;
			for (Keyboard.Key key : kbd.getKeys()) {
				// compatible with KitKat (4.4) or lower
				if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
					int iconId = 0;
					switch (key.codes[0]) {
					case -1:
						iconId = R.drawable.ic_keyboard_capslock_24dp;
						break;
					case -5:
						iconId = R.drawable.ic_keyboard_backspace_24dp;
						break;
					case -101:
						iconId = R.drawable.ic_language_24dp;
						break;
					case 32:
						iconId = R.drawable.ic_space_bar_24dp;
						break;
					case 10:
						iconId = R.drawable.ic_keyboard_return_24dp;
						break;
					}
					if (iconId != 0) {
						key.icon = VectorDrawableCompat.create(getResources(), iconId, null);
						key.label = null;
					}
				}
				if (key.icon != null) {
					key.icon = DrawableCompat.wrap(key.icon.mutate());
					DrawableCompat.setTint(key.icon, Color.WHITE);
				}
				switch (key.codes[0]) {
				case KEYCODE_ACTION:
					keyActions[i] = key;
					keyIdxActions[i] = idx;
					break;
				case KEYCODE_ENTER:
					keyEnters[i] = key;
					keyIdxEnters[i] = idx;
					keyWidthEnters[i] = key.width;
					keyGapEnters[i] = key.gap;
					if (key.label != null)
						keyLabelEnters[i] = key.label.toString();
					break;
				case KEYCODE_SPACE:
					keySpaces[i] = key;
					keyIdxSpaces[i] = idx;
					keyWidthSpaces[i] = key.width;
					break;
				case KEYCODE_APOST:
					if (i == KBD_NUMSYM1) {
						keyApostNS1 = key;
						keyIdxApostNS1 = idx;
					}
					break;
				case KEYCODE_QUOTE:
					if (i == KBD_NUMSYM1) {
						keyQuoteNS1 = key;
						keyIdxQuoteNS1 = idx;
					}
					break;
				case KEYCODE_PERIOD:
					if (i == KBD_ASCII) {
						keyPeriodASCII = key;
						keyIdxPeriodASCII = idx;
					}
					break;
				case KEYCODE_TONE_RISE:
					if (i == KBD_TL) {
						keyTlToneRise = key;
						keyIdxTlToneRise = idx;
					} else if (i == KBD_POJ) {
						keyPojToneRise = key;
						keyIdxPojToneRise = idx;
					}
					break;
				case KEYCODE_TONE_HIGH:
					if (i == KBD_TL) {
						keyTlToneHigh = key;
						keyIdxTlToneHigh = idx;
					} else if (i == KBD_POJ) {
						keyPojToneHigh = key;
						keyIdxPojToneHigh = idx;
					}
					break;
				case KEYCODE_TONE_MID:
					if (i == KBD_TL) {
						keyTlToneMid = key;
						keyIdxTlToneMid = idx;
					} else if (i == KBD_POJ) {
						keyPojToneMid = key;
						keyIdxPojToneMid = idx;
					}
					break;
				case KEYCODE_TONE_LOW:
					if (i == KBD_TL) {
						keyTlToneLow = key;
						keyIdxTlToneLow = idx;
					} else if (i == KBD_POJ) {
						keyPojToneLow = key;
						keyIdxPojToneLow = idx;
					}
					break;
				}
				idx++;
			}
		}
		toneKeysTl = new ToneKeys(
			keyIdxTlToneRise, keyTlToneRise,
			keyIdxTlToneHigh, keyTlToneHigh,
			keyIdxTlToneMid, keyTlToneMid,
			keyIdxTlToneLow, keyTlToneLow);
		toneKeysPoj = new ToneKeys(
			keyIdxPojToneRise, keyPojToneRise,
			keyIdxPojToneHigh, keyPojToneHigh,
			keyIdxPojToneMid, keyPojToneMid,
			keyIdxPojToneLow, keyPojToneLow);
	}

	private void fineKeyboard() {
	}

	@Override
	public View onCreateCandidatesView() {
		log("onCreateCandidatesView");
		initCandidatesView();
		return candidatesView;
	}

	@SuppressLint("InflateParams")
	private void initCandidatesView() {
		candidatesView = (ViewGroup)getLayoutInflater().inflate(R.layout.candidates_view, null);
		initCandidates();
		TKKeyboardView.Theme theme = (kbdView == null ? null : kbdView.getTheme());
		if (theme == null) {
			int bgDark = getResources().getColor(android.R.color.background_dark);
			theme = new TKKeyboardView.Theme(bgDark, 0, 0, 0, null, 0, null, 0);
		}
		updateThemeForCandidatesView(theme);
	}

	private void updateThemeForCandidatesView(TKKeyboardView.Theme theme) {
		candidatesView.setBackgroundColor(theme.bgColor);
		candidatePlaceHolder.setTextColor(theme.bgColor);
	}

	private void fineCandidatesView() {
		fineCandidates();
		candidates = null;
		candidatesView = null;
	}

	private void initCandidates() {
		candidates = candidatesView.findViewById(R.id.candidates);
		candidatePlaceHolder = candidatesView.findViewById(R.id.candidatePlaceHolder);
	}

	private void fineCandidates() {
		candidates = null;
	}

	private void showCandidates() {
		if (candidatesShown.compareAndSet(false, true))
			setCandidatesViewShown(true);
	}

	private void hideCandidates() {
		if (candidatesShown.compareAndSet(true, false))
			setCandidatesViewShown(false);
	}

	private void toggleCandidates() {
		boolean old = candidatesShown.get();
		if (candidatesShown.compareAndSet(old, !old))
			setCandidatesViewShown(!old);
	}

	private void clearCandidates() {
		for (int i = candidates.getChildCount() - 1; i >= 0; i--) {
			View view = candidates.getChildAt(i);
			if (view != candidatePlaceHolder)
				candidates.removeView(view);
		}
	}

	private void startCandidates() {
	}

	private void finishCandidates() {
		if (candidates != null) {
			hideCandidates();
			clearCandidates();
		}
	}

	private void initTextCapMode(InputConnection ic) {
		boolean willCap = false;
		CharSequence before;
		switch (inputState.getString(CapMode)) {
		default:
			break;
		case TEXT_CAP_CHARACTERS:
			willCap = true;
			break;
		case TEXT_CAP_WORDS:
			before = getTextBeforeCursor(ic, 2);
			if (before == null) {
				willCap = true;
			} else {
				int blen = before.length();
				if (blen <= 0) {
					willCap = true;
				} else {
					char ch = before.charAt(blen - 1);
					if (blen <= 1) {
						if (Character.isWhitespace(ch))
							willCap = true;
					} else {
						char ch2 = before.charAt(blen - 2);
						if (Character.isSurrogatePair(ch2, ch)) {
							if (Character.isWhitespace(Character.toCodePoint(ch2, ch)))
								willCap = true;
						} else if (Character.isWhitespace(ch)) {
							willCap = true;
						}
					}
				}
			}
			break;
		case TEXT_CAP_SENTENCES:
			willCap = checkSentenceCap(ic, false);
			break;
		}
		log("text will be capitalized: ", willCap);
		inputState = inputState.setBoolean(CapState, willCap);

		if (viewState.getInteger(CurrentKeyboard).equals(viewState.getInteger(TextKeyboard)))
			viewState = viewState.setBoolean(ShiftState, willCap);
	}

	private boolean checkSentenceCap(InputConnection ic, boolean hasWhiteSpace) {
		CharSequence before = getTextBeforeCursor(ic, 11);
		if (before == null)
			return true;
		char lsrg = 0;
		for (int i = before.length() - 1; i >= 0; i--) {
			char ch = before.charAt(i);
			int codePoint;
			if (Character.isLowSurrogate(ch)) {
				lsrg = ch;
				continue;
			} else if (Character.isHighSurrogate(ch)) {
				if (lsrg == 0)
					continue;
				codePoint = Character.toCodePoint(ch, lsrg);
			} else {
				codePoint = ch;
				lsrg = 0;
			}
			if (Character.isWhitespace(codePoint)) {
				hasWhiteSpace = true;
				continue;
			}
			if (codePoint == '.' || codePoint == '?' || codePoint == '!')
				return hasWhiteSpace;
			else
				return false;
		}
		return true;
	}

	private void fineTextCapMode() {
	}

	private void updateTextCapModeAfterKey(InputConnection ic, int code) {
		if (isKeyTone(code))
			return;

		boolean nowCap = inputState.getBoolean(CapState);
		boolean nextCap = nowCap;
		boolean updated = false;
		switch (inputState.getString(CapMode)) {
		case TEXT_CAP_CHARACTERS:
			if (!nowCap) {
				nextCap = true;
				updated = true;
			}
			break;
		case TEXT_CAP_WORDS:
			if (nowCap) {
				if (!Character.isWhitespace(code)) {
					nextCap = false;
					updated = true;
				}
			} else {
				if (Character.isWhitespace(code)) {
					nextCap = true;
					updated = true;
				}
			}
			break;
		case TEXT_CAP_SENTENCES:
			if (nowCap) {
				if (!Character.isWhitespace(code)) {
					nextCap = false;
					updated = true;
				}
			} else {
				if (Character.isWhitespace(code) && checkSentenceCap(ic, true)) {
					nextCap = true;
					updated = true;
				}
			}
			break;
		}
		if (updated) {
			inputState = inputState.setBoolean(CapState, nextCap);
			if (viewState.getInteger(CurrentKeyboard).equals(viewState.getInteger(TextKeyboard)))
				viewState = viewState.setBoolean(ShiftState, nextCap);
		}
	}

	private void updateComposingTextVisual() {
		if (inputState.getBoolean(PhoneticInput, false)) {
			inputState = inputState.setState(ComposingState,
			                                 toPhonetic(inputState.getState(ComposingState)));
		}
		updateComposingText(false);
		updateComposingStateDebug();
	}

	private void updateComposingText() {
		updateComposingText(false);
		updateComposingStateDebug();
	}

	private void updateComposingStateDebug() {
		if (appDebug) {
			TKInputState compoRegion = inputState.getState(ComposingRegion);
			TKInputState compoState = inputState.getState(ComposingState);
			StringBuilder sb = new StringBuilder();
			sb.append(compoState.getString(Leads));
			sb.append(',');
			sb.append(compoState.getString(Vowels));
			sb.append(',');
			sb.append(compoState.getInteger(Tone));
			sb.append(',');
			sb.append(compoState.getString(Afters));
			sb.append(',');
			sb.append(compoState.getCharacter(Glottal));
			sb.append(',');
			sb.append(compoState.getBoolean(PojNN));
			sb.append('/');
			sb.append(compoRegion.getString(Leads));
			sb.append(',');
			sb.append(compoRegion.getString(Vowels));
			sb.append(',');
			sb.append(compoRegion.getInteger(Tone));
			sb.append(',');
			sb.append(compoRegion.getString(Afters));
			sb.append(',');
			sb.append(compoRegion.getCharacter(Glottal));
			sb.append(',');
			sb.append(compoRegion.getBoolean(PojNN));
			String desc = sb.toString();
			composingState.setText(desc);
			log("update composing text: ", desc);
		}
	}

	private void updateComposingText(boolean progressing) {
		TKInputState compoState = inputState.getState(ComposingState);
		boolean phoneticInput = inputState.getBoolean(PhoneticInput, false);
		boolean phoneticDisabled = inputState.getBoolean(PhoneticInputTempDisabled, false);
		if (phoneticDisabled)
			inputState = inputState.remove(PhoneticInputTempDisabled);
		TKInputState phonetic, visual;
		if (phoneticInput && !phoneticDisabled) {
			if (progressing) {
				phonetic = compoState;
			} else {
				phonetic = toPhonetic(compoState);
				if (phonetic != compoState) {
					compoState = phonetic;
					inputState = inputState.setState(ComposingState, compoState);
				}
			}
			visual = toVisual(phonetic);
		} else {
			visual = compoState;
		}
		TKInputState compoRegion = inputState.getState(ComposingRegion);

		String textPrefix = getComposingText(visual);
		log("textPrefix: ", textPrefix);
		if (viewState.contains(TextBeforeCached)) {
			CharSequence cache = (CharSequence)viewState.get(TextBeforeCached);
			int cacheLen = cache.length();
			String orgText = viewState.getString(ComposingText);
			int orgLen = orgText.length();
			StringBuilder sb = new StringBuilder();
			if (cacheLen > orgLen)
				sb.append(cache, 0, cacheLen - orgLen);
			sb.append(textPrefix);
			viewState.set(TextBeforeCached, sb);
		} else {
			viewState.set(TextBeforeCursor, viewState.getInteger(SelectionStart))
			         .set(TextBeforeCached, textPrefix);
		}

		if (phoneticInput && !phoneticDisabled) {
			if (progressing) {
				phonetic = compoRegion;
			} else {
				phonetic = toPhonetic(compoRegion);
				if (phonetic != compoRegion) {
					compoRegion = phonetic;
					inputState = inputState.setState(ComposingRegion, compoRegion);
					updateToneViewState();
				}
			}
			visual = toVisual(phonetic);
		} else {
			phonetic = visual = compoRegion;
		}
		String textComposing = getComposingText(visual);
		String hat = visual.getString(Hat);
		String boot = visual.getString(Boot);
		log("hat: ", hat, ", textComposing: ", textComposing, ", boot: ", boot);
		viewState = viewState.setString(ComposingText, hat + textComposing + boot).remove(ComposingCursor);

		int lenComposing = textComposing.length();
		if (lenComposing <= 0) {
			viewState = viewState.remove(ComposingCorrectedCached)
			                     .set(AutoCorrects, AUTO_CORRECTS_NULL)
			                     .set(Candidates, CANDIDATES_NULL)
			                     .remove(CandidatePrefix);
			if (inputState.getBoolean(DictSuggest)
			    && (viewState.getBoolean(HasSuggestions, false)
			        || inputState.getBoolean(DictHanji, false))
			    && !viewState.getBoolean(ShowCompletions, false)) {
				if (dictReady.get() && !hat.isEmpty()) {
					new TKQueryCandidatesTask(this, db).execute(
						new TKQueryParameter(inputState, viewState));
				}
			}
			// set composing cursor when boot is available
			if (boot.isEmpty())
				return;
		}

		// set composing cursor
		int cursor = 0;
		int lenPrefix = textPrefix.length();
		for (int i = 0; i < lenPrefix; i++) {
			char ch2 = textPrefix.charAt(i);
			if (!Character.isLetter(ch2))
				continue;
			cursor++;
			for (; cursor < lenComposing; cursor++) {
				char ch = textComposing.charAt(cursor);
				if (Character.isLetter(ch))
					break;
			}
		}
		if (cursor < lenComposing || !boot.isEmpty()) {
			log("composing cursor: ", cursor);
			int cursorWithHat = hat.length() + cursor;
			log("composing cursor with hat: ", cursorWithHat);
			viewState = viewState.setInteger(ComposingCursor, cursorWithHat);

			// update tone for composing state here
			if (!textComposing.startsWith(textPrefix)) {
				textPrefix = textComposing.substring(0, cursor);
				char[] chars = (char[])decompose(textPrefix)[0];
				int tone = 1;
				for (char aChar : chars) {
					if (isTone(aChar)) {
						tone = getToneNumber(aChar);
						break;
					}
				}
				if (tone == 1 && compoState.getCharacter(Glottal) != GLOTTAL_NULL)
					tone = 4;
				compoState = compoState.setInteger(Tone, tone);
				inputState = inputState.setState(ComposingState, compoState);
				updateToneViewState();
			}
		}
		// composing cursor is set when composing text is empty and boot is available
		if (lenComposing <= 0)
			return;

		final int subtype = inputState.getInteger(Subtype);
		final boolean pojNN = visual.getBoolean(PojNN);
		final char glottal = visual.getCharacter(Glottal);
		final String afters = visual.getString(Afters);
		String vowels = visual.getString(Vowels);
		int vlen = (vowels == VOWELS_NULL ? -1 : vowels.length());
		boolean fromEE = false;

		// correction
		viewState = viewState.remove(ComposingCorrectedCached).set(AutoCorrects, AUTO_CORRECTS_NULL);
		if (phoneticInput) {
			if (phonetic != visual) {
				String textPhonetic = getComposingText(phonetic);
				viewState = viewState.set(ComposingCorrected, phonetic)
				                     .set(AutoCorrects, new String[] {textPhonetic, textComposing});
			}
		} else {
			TKInputState corrected = visual;
			switch (subtype) {
			case SUBTYPE_TL: {
				// o-e/a should be u-e/a
				if (vlen >= 2) {
					char check = vowels.charAt(0);
					if (check == 'o') {
						char check2 = vowels.charAt(1);
						if (isE(check2) || isA(check2)) {
							vowels = "u" + vowels.substring(1);
							corrected = corrected.setString(Vowels, vowels);
						}
					} else if (check == 'O') {
						char check2 = vowels.charAt(1);
						if (isE(check2) || isA(check2)) {
							vowels = "U" + vowels.substring(1);
							corrected = corrected.setString(Vowels, vowels);
						}
					}
				}
				// ee-k/ng should be e-k/ng
				if (vlen >= 2 && (glottal == 'k' || afters.equalsIgnoreCase("ng"))) {
					String check2 = vowels.substring(vlen - 2);
					if (check2.equalsIgnoreCase("ee")) {
						vowels = vowels.substring(0, vlen - 1);
						vlen--;
						corrected = corrected.setString(Vowels, vowels);
						fromEE = true;
					}
				}
				break;
			}
			case SUBTYPE_POJ:
				// u-e/a should be o-e/a
				if (vlen >= 2) {
					char check = vowels.charAt(0);
					if (check == 'u') {
						char check2 = vowels.charAt(1);
						if (isE(check2) || isA(check2)) {
							vowels = "o" + vowels.substring(1);
							corrected = corrected.setString(Vowels, vowels);
						}
					} else if (check == 'U') {
						char check2 = vowels.charAt(1);
						if (isE(check2) || isA(check2)) {
							vowels = "O" + vowels.substring(1);
							corrected = corrected.setString(Vowels, vowels);
						}
					}
				}
				// i-k/ng should be e-k/ng
				if (vlen == 1 && (glottal == 'k' || afters.equalsIgnoreCase("ng"))) {
					char check = vowels.charAt(0);
					if (check == 'i') {
						vowels = "e";
						corrected = corrected.setString(Vowels, vowels);
					} else if (check == 'I') {
						vowels = "E";
						corrected = corrected.setString(Vowels, vowels);
					}
				}
				break;
			}
			// oo-k/p/m/ng/nn should be o-k/p/m/ng/nn
			if ((vlen >= 2) && (glottal == 'k' || glottal == 'p' || pojNN || isOoNasal(afters))) {
				String check = vowels.substring(vlen - 2);
				if (check.equalsIgnoreCase("oo") || check.equalsIgnoreCase("o͘")) {
					vowels = vowels.substring(0, vlen - 1);
					vlen--;
					corrected = corrected.setString(Vowels, vowels);
				}
			}
			// e-n/et should be ia-n/iat
			if (vlen == 1 && (glottal == 't' || afters.equalsIgnoreCase("n"))) {
				char check = vowels.charAt(0);
				if (check == 'e') {
					vowels = "ia";
					vlen++;
					corrected = corrected.setString(Vowels, vowels);
				} else if (check == 'E') {
					if (inputState.getString(CapMode) == TEXT_CAP_CHARACTERS)
						vowels = "IA";
					else
						vowels = "Ia";
					vlen++;
					corrected = corrected.setString(Vowels, vowels);
				}
			}
			if (corrected != visual) {
				String textCorrected = getComposingText(corrected);
				viewState = viewState.set(ComposingCorrected, corrected.remove(Tail))
				                     .setString(ComposingCorrectedCached, textCorrected)
				                     .set(AutoCorrects, new String[] {textComposing, textCorrected});
				visual = corrected;
			}
		}

		if ((!viewState.getBoolean(HasSuggestions, false)
		     && !inputState.getBoolean(DictHanji, false))
		    || viewState.getBoolean(ShowCompletions, false)) {
			viewState = viewState.set(Candidates, CANDIDATES_NULL).remove(CandidatePrefix);
		} else {
			viewState = viewState.set(Candidates, CANDIDATES_NULL)
			                     .remove(CandidatePrefix);
			if (inputState.getBoolean(DictSuggest)) {
				if (dictReady.get()) {
					new TKQueryCandidatesTask(this, db).execute(
						new TKQueryParameter(inputState, viewState));
				}
			} else {
				TKInputState suggested = visual;
				// TL: although e-k/ng is valid for ee-k/ng,
				//     it is seldom used and is often POJ-typo of i-k/ng
				if (!fromEE && subtype == SUBTYPE_TL && vlen == 1 && (glottal == 'k' || afters.equalsIgnoreCase("ng"))) {
					char check = vowels.charAt(0);
					if (check == 'e')
						suggested = suggested.setString(Vowels, "i");
					else if (check == 'E')
						suggested = suggested.setString(Vowels, "I");
				}
				if (suggested != visual) {
					String textSuggested = getComposingText(suggested);
					viewState = viewState.set(Candidates, new Object[] {hat + textSuggested + boot});
				}
			}
		}
	}

	protected String getComposingText(TKInputState compoState) {
		String style = inputState.getString(ToneMarkStyle);
		if (style.equals(TONE_MARK_TL) ||
		    (inputState.getInteger(Subtype) == SUBTYPE_TL &&
		     style.equals(TONE_MARK_SUBTYPE))) {
			return getComposingTextTL(compoState);
		} else {
			return getComposingTextPOJ(compoState);
		}
	}

	protected TKInputState toVisual(TKInputState phonetic) {
		final boolean pojNN = phonetic.getBoolean(PojNN);
		final char glottal = phonetic.getCharacter(Glottal);
		final String afters = phonetic.getString(Afters);
		String vowels = phonetic.getString(Vowels);
		int vlen = (vowels == VOWELS_NULL ? -1 : vowels.length());
		TKInputState visual = phonetic;
		switch (inputState.getInteger(Subtype)) {
		case SUBTYPE_TL: {
			// ee-k/ng to e-k/ng
			if (vlen >= 2 && (glottal == 'k' || afters.equalsIgnoreCase("ng"))) {
				String check2 = vowels.substring(vlen - 2);
				if (check2.equalsIgnoreCase("ee")) {
					vowels = vowels.substring(0, vlen - 1);
					vlen--;
					visual = visual.setString(Vowels, vowels);
				}
			}
			break;
		}
		case SUBTYPE_POJ:
			// u-e/a to o-e/a
			if (vlen >= 2) {
				char check = vowels.charAt(0);
				if (check == 'u') {
					char check2 = vowels.charAt(1);
					if (isE(check2) || isA(check2)) {
						vowels = "o" + vowels.substring(1);
						visual = visual.setString(Vowels, vowels);
					}
				} else if (check == 'U') {
					char check2 = vowels.charAt(1);
					if (isE(check2) || isA(check2)) {
						vowels = "O" + vowels.substring(1);
						visual = visual.setString(Vowels, vowels);
					}
				}
			}
			// i-k/ng to e-k/ng
			if (vlen == 1 && (glottal == 'k' || afters.equalsIgnoreCase("ng"))) {
				char check = vowels.charAt(0);
				if (check == 'i') {
					vowels = "e";
					visual = visual.setString(Vowels, vowels);
				} else if (check == 'I') {
					vowels = "E";
					visual = visual.setString(Vowels, vowels);
				}
			}
			break;
		}
		// oo-k/p/m/ng/nn to o-k/p/m/ng/nn
		if (vlen >= 2 && (glottal == 'k' || glottal == 'p' || pojNN || isOoNasal(afters))) {
			String check = vowels.substring(vlen - 2);
			if (check.equalsIgnoreCase("oo") || check.equalsIgnoreCase("o͘")) {
				vowels = vowels.substring(0, vlen - 1);
				vlen--;
				visual = visual.setString(Vowels, vowels);
			}
		}
		// e-n/et to ia-n/iat
		if (vlen == 1 && (glottal == 't' || afters.equalsIgnoreCase("n"))) {
			char check = vowels.charAt(0);
			if (check == 'e') {
				vowels = "ia";
				vlen++;
				visual = visual.setString(Vowels, vowels);
			} else if (check == 'E') {
				if (inputState.getString(CapMode) == TEXT_CAP_CHARACTERS)
					vowels = "IA";
				else
					vowels = "Ia";
				vlen++;
				visual = visual.setString(Vowels, vowels);
			}
		}
		return visual;
	}

	protected TKInputState toPhonetic(TKInputState visual) {
		final boolean pojNN = visual.getBoolean(PojNN);
		final char glottal = visual.getCharacter(Glottal);
		final String afters = visual.getString(Afters);
		String vowels = visual.getString(Vowels);
		int vlen = (vowels == VOWELS_NULL ? -1 : vowels.length());
		TKInputState phonetic = visual;

		// o-e/a to u-e/a
		if (vlen >= 2) {
			char check = vowels.charAt(0);
			// o but not oo
			if (check == 'o') {
				char check2 = vowels.charAt(1);
				if (isE(check2) || isA(check2)) {
					vowels = "u" + vowels.substring(1);
					phonetic = phonetic.setString(Vowels, vowels);
				}
			} else if (check == 'O') {
				char check2 = vowels.charAt(1);
				if (isE(check2) || isA(check2)) {
					vowels = "U" + vowels.substring(1);
					phonetic = phonetic.setString(Vowels, vowels);
				}
			}
		}

		switch (inputState.getInteger(Subtype)) {
		case SUBTYPE_TL: {
			// e-k/ng to ee-k/ng
			if (vlen == 1 && (glottal == 'k' || afters.equalsIgnoreCase("ng"))) {
				char check1 = vowels.charAt(0);
				// e but not ee
				if (isE(check1)) {
					if (inputState.getString(CapMode) == TEXT_CAP_CHARACTERS)
						vowels += 'E';
					else
						vowels += 'e';
					vlen++;
					phonetic = phonetic.setString(Vowels, vowels);
				}
			}
			break;
		}
		case SUBTYPE_POJ:
			// e-k/ng to i-k/ng
			if (vlen == 1 && (glottal == 'k' || afters.equalsIgnoreCase("ng"))) {
				char check = vowels.charAt(0);
				if (check == 'e') {
					vowels = "i";
					phonetic = phonetic.setString(Vowels, vowels);
				} else if (check == 'E') {
					vowels = "I";
					phonetic = phonetic.setString(Vowels, vowels);
				}
			}
			break;
		}

		// o-k/p/m/ng/nn to oo-k/p/m/ng/nn
		if (vlen >= 1 && (glottal == 'k' || glottal == 'p' || pojNN || isOoNasal(afters))) {
			char check = vowels.charAt(vlen - 1);
			// o but not oo
			if (isO(check)) {
				boolean notOO = true;
				if (vlen >= 2) {
					char check2 = vowels.charAt(vlen - 2);
					if (check2 == '\u0358' || isO(check2))
						notOO = false;
				}
				if (notOO) {
					if (inputState.getInteger(Subtype) == SUBTYPE_POJ) {
						vowels += '\u0358';
					} else {
						if (inputState.getString(CapMode) == TEXT_CAP_CHARACTERS)
							vowels += 'O';
						else
							vowels += 'o';
					}
					vlen++;
					phonetic = phonetic.setString(Vowels, vowels);
				}
			}
		}

		// ia-n/t to e-n/t
		if (vlen == 2 && (glottal == 't' || afters.equalsIgnoreCase("n"))) {
			if (vowels.equalsIgnoreCase("ia")) {
				char check = vowels.charAt(0);
				if (check == 'i')
					vowels = "e";
				else
					vowels = "E";
				vlen--;
				phonetic = phonetic.setString(Vowels, vowels);
			}
		}

		return phonetic;
	}

	private void initComposing(InputConnection ic, int cursor) {
		// reset composing text
		resetComposing();
		if (ic == null)
			return;

		viewState = viewState.setInteger(ComposingStart, -1)
		                     .setInteger(ComposingEnd, -1);
		parseBeforeText(ic, cursor, 0);
		if (zeroCursorWorkAround)
			return;
		parseAfterText(ic);
		if (viewState.getInteger(ComposingStart, -1) >= 0)
			updateComposingTextVisual();
	}

	private void parseBeforeText(InputConnection ic, int cursor, int skip) {
		log("check syllable: cursor=", cursor, " skip=", skip);
		CharSequence obefore = ic.getTextBeforeCursor(100, 0);
		log("view: get text before: [", obefore, ']');
		if (obefore == null)
			return;

		int olen = obefore.length();
		if (viewState.contains(DeleteBefore)) {
			int delcs = viewState.getInteger(DeleteBefore);
			if (olen <= delcs)
				return;
			olen -= delcs;
			obefore = obefore.subSequence(0, olen);
			cursor -= delcs;
			log("with ", delcs, " characters deleted: cursor=", cursor, " before=[", obefore, "]");
		}

		// sometimes after screen rotate, editor reports wrong cursor = 0, but with text before available
		// after that onUpdateSelection will report with correct cursor
		if (cursor < olen) {
			log("PROBLEM: cursor is ", cursor, " but before is at least ", olen);
			zeroCursorWorkAround = true;
			viewState = viewState.setInteger(ComposingStart, cursor)
			                     .setInteger(ComposingEnd, cursor);
			return;
		}
		if (olen <= 0)
			return;

		Object[] dcp = decompose(obefore);
		char[] before = (char[])dcp[0];
		int[] beforeIndex = (int[])dcp[1];
		int len = before.length;
		log("length: ", len);
		if (len <= 0)
			return;

		int idx = len - 1;
		if (skip > 0) {
			int olast = olen - 1 - skip;
			while (beforeIndex[idx] != olast) {
				idx--;
				if (idx < 0)
					return;
			}
		}

		char ch = before[idx--];
		log("before[", idx + 1, "]=", ch, ',', (int)ch);
		if (Character.isWhitespace(ch))
			return;

		// parse PojNN
		boolean iPojNN = false;
		log("check POJ nn: ", ch);
		if (ch == 'ⁿ') {
			if (idx < 0)
				return;
			iPojNN = true;
			ch = before[idx--];
			log("before[", idx + 1, "]=", ch, ',', (int)ch);
			if (Character.isWhitespace(ch))
				return;
		}

		TKInputState compoState = inputState.getState(ComposingState);
		// parse Glottal
		char iglottal = GLOTTAL_NULL;
		log("check glottal: ", ch);
		if (isGlottal(ch)) {
			if (idx < 0) {
				inputState = inputState.setBoolean(ComposingStarted, true)
				                       .setState(ComposingState, compoState.setString(Hat, "").setString(Leads, String.valueOf(ch)));
				int crEnd = cursor;
				int ostart = beforeIndex[idx + 1];
				int crStart = ostart + cursor - olen;
				log("set composing region: ", crStart, '~', crEnd);
				viewState = viewState.setInteger(ComposingStart, crStart)
				                     .setInteger(ComposingEnd, crEnd)
				                     .setString(ComposingText, obefore.subSequence(ostart, olen).toString())
				                     .resetChanged(ComposingText);
				return;
			}
			iglottal = ch;
			ch = before[idx--];
			log("before[", idx + 1, "]=", ch, ',', (int)ch);
			if (Character.isWhitespace(ch)) {
				inputState = inputState.setBoolean(ComposingStarted, true)
				                       .setState(ComposingState, compoState.setString(Hat, "").setString(Leads, String.valueOf(iglottal)));
				int crEnd = cursor;
				int ostart = beforeIndex[idx + 2];
				int crStart = ostart + cursor - olen;
				log("set composing region: ", crStart, '~', crEnd);
				viewState = viewState.setInteger(ComposingStart, crStart)
				                     .setInteger(ComposingEnd, crEnd)
				                     .setString(ComposingText, obefore.subSequence(ostart, olen).toString())
				                     .resetChanged(ComposingText);
				return;
			}
		}

		// parse Afters
		int itone = 0;
		String iafters;
		if (iPojNN) {
			iafters = "";
		} else {
			iafters = AFTERS_NULL;
			if (isTone(ch)) {
				itone = getToneNumber(ch);
				if (idx < 0)
					return;
				ch = before[idx--];
				log("before[", idx + 1, "]=", ch, ',', (int)ch);
				if (Character.isWhitespace(ch))
					return;
			}
			String check = String.valueOf(ch);

			char ch2 = 0;
			String check2 = null;
			int itone2 = 0;
			if (idx >= 0) {
				ch2 = before[idx--];
				log("before[", idx + 1, "]=", ch2, ',', (int)ch2);
				if (!Character.isWhitespace(ch2)) {
					if (isTone(ch2)) {
						itone2 = getToneNumber(ch2);
						if (idx >= 0) {
							ch2 = before[idx--];
							log("before[", idx + 1, "]=", ch2, ',', (int)ch2);
							if (!Character.isWhitespace(ch2))
								check2 = String.valueOf(ch2) + ch;
						}
					} else {
						check2 = String.valueOf(ch2) + ch;
					}
				}
			}

			log("check afters: ", check2, " or ", check);
			if (check2 != null && isValidAfters(check2)) {
				if (itone2 != 0)
					itone = itone2;
				if (idx < 0 || Character.isWhitespace(before[idx])) {
					if (isNasal(check2)) { // ng
						if (itone == 0)
							itone = (iglottal == GLOTTAL_NULL ? 1 : 4);
						inputState = inputState.setBoolean(ComposingStarted, true)
						                       .setState(ComposingState, compoState.setString(Hat, "")
						                                                           .setString(Leads, "").setString(Vowels, "")
						                                                           .setInteger(Tone, itone).setString(Afters, check2)
						                                                           .setCharacter(Glottal, iglottal).setBoolean(PojNN, iPojNN));
						int crEnd = cursor;
						int ostart = beforeIndex[idx + 1];
						int crStart = ostart + cursor - olen;
						log("set composing region: ", crStart, '~', crEnd);
						viewState = viewState.setInteger(ComposingStart, crStart)
						                     .setInteger(ComposingEnd, crEnd)
						                     .setString(ComposingText, obefore.subSequence(ostart, olen).toString())
						                     .resetChanged(ComposingText);
						return;
					} else if (isValidAfters(check)) { // nn
						if (itone == 0)
							itone = (iglottal == GLOTTAL_NULL ? 1 : 4);
						inputState = inputState.setBoolean(ComposingStarted, true)
						                       .setState(ComposingState, compoState.setString(Hat, "")
						                                                           .setString(Leads, check2.substring(0, 1))
						                                                           .setString(Vowels, "").setInteger(Tone, itone)
						                                                           .setString(Afters, check).setCharacter(Glottal, iglottal)
						                                                           .setBoolean(PojNN, iPojNN));
						int crEnd = cursor;
						int ostart = beforeIndex[idx + 1];
						int crStart = ostart + cursor - olen;
						log("set composing region: ", crStart, '~', crEnd);
						viewState = viewState.setInteger(ComposingStart, crStart)
						                     .setInteger(ComposingEnd, crEnd)
						                     .setString(ComposingText, obefore.subSequence(ostart, olen).toString())
						                     .resetChanged(ComposingText);
						return;
					} else {
						return;
					}
				}
				iafters = check2;
				ch = before[idx--];
			} else if (isValidAfters(check)) {
				if (check2 == null) {
					if (itone == 0)
						itone = (iglottal == GLOTTAL_NULL ? 1 : 4);
					inputState = inputState.setBoolean(ComposingStarted, true)
					                       .setState(ComposingState, compoState.setString(Hat, "")
					                                                           .setString(Leads, "").setString(Vowels, "")
					                                                           .setInteger(Tone, itone).setString(Afters, check)
					                                                           .setCharacter(Glottal, iglottal).setBoolean(PojNN, iPojNN));
					int crEnd = cursor;
					int ostart;
					if (ch2 == 0)
						ostart = beforeIndex[idx + 1];
					else
						ostart = beforeIndex[idx + 2];
					int crStart = ostart + cursor - olen;
					log("set composing region: ", crStart, '~', crEnd);
					viewState = viewState.setInteger(ComposingStart, crStart)
					                     .setInteger(ComposingEnd, crEnd)
					                     .setString(ComposingText, obefore.subSequence(ostart, olen).toString())
					                     .resetChanged(ComposingText);
					return;
				}
				iafters = check;
				ch = ch2;
				if (itone2 != 0)
					itone = itone2;
			} else {
				if (iglottal != GLOTTAL_NULL)
					iafters = "";
				if (ch2 != 0) {
					idx++;
					if (itone2 != 0)
						idx++;
				}
			}
		}

		// parse Vowels
		String ivowels = VOWELS_NULL;
		String mleads = "", ileads = "";
		int idxLeads = -1;
		while (true) {
			log("check vowel: ", ch);
			String check = String.valueOf(ch);
			String seps = getSeparatedCharacter(check);
			if (!seps.equals(check)) {
				char[] sepcs = seps.toCharArray();
				log("vowel decomposed: ");
				for (int i = 0; i < sepcs.length; i++) {
					log(" #", i, ": ", sepcs[i]);
					if (isTone(sepcs[i])) {
						itone = getToneNumber(sepcs[i]);
					} else {
						if (ivowels == VOWELS_NULL) {
							ivowels = String.valueOf(sepcs[i]);
							if (itone == 0)
								itone = (iglottal == GLOTTAL_NULL ? 1 : 4);
						} else {
							ivowels = sepcs[i] + ivowels;
						}
						log("ivowels=", ivowels);
					}
				}
                /*
                if (itone != 0)
                    break;
                    */
			} else if (isVowel(ch)) {
				if (ivowels == VOWELS_NULL) {
					ivowels = check;
					if (itone == 0)
						itone = (iglottal == GLOTTAL_NULL ? 1 : 4);
				} else {
					ivowels = check + ivowels;
				}
				log("ivowels=", ivowels);
			} else if (isTone(ch)) {
				itone = getToneNumber(ch);
			} else {
				if (ivowels == VOWELS_NULL) {
					if (iafters == AFTERS_NULL) {
					} else if (isNasal(iafters)) {
						// no vowel, and afters is m/n/ng, implicit vowel
						ivowels = "";
						if (itone == 0)
							itone = (iglottal == GLOTTAL_NULL ? 1 : 4);
					} else if (iafters.isEmpty()) {
						// no vowel and empty afters; has glottal and/or PojNN
						if (iPojNN) // PojNN must has a vowel
							return;
						// move glottal to leads; all glottals (h/k/p/t) are valid leads
						ileads = mleads = String.valueOf(iglottal);
						idxLeads = idx + 2;
						itone = 0;
						iafters = AFTERS_NULL;
						iglottal = GLOTTAL_NULL;
					} else if (iafters.equalsIgnoreCase("nn")) {
						// split -nn to n-n
						ileads = mleads = iafters.substring(0, 1);
						idxLeads = idx + 2;
						ivowels = "";
						if (itone == 0)
							itone = (iglottal == GLOTTAL_NULL ? 1 : 4);
						iafters = iafters.substring(1);
					} else {
						return;
					}
				}
				break;
			}
			log("ivowels=", ivowels);

			if (idx < 0) {
				inputState = inputState.setBoolean(ComposingStarted, true)
				                       .setState(ComposingState, compoState.setString(Hat, "")
				                                                           .setString(Leads, "").setString(Vowels, ivowels)
				                                                           .setInteger(Tone, itone).setString(Afters, iafters)
				                                                           .setCharacter(Glottal, iglottal).setBoolean(PojNN, iPojNN));
				int crEnd = cursor;
				int ostart = beforeIndex[idx + 1];
				int crStart = ostart + cursor - olen;
				log("set composing region: ", crStart, '~', crEnd);
				viewState = viewState.setInteger(ComposingStart, crStart)
				                     .setInteger(ComposingEnd, crEnd)
				                     .setString(ComposingText, obefore.subSequence(ostart, olen).toString())
				                     .resetChanged(ComposingText);
				return;
			}

			ch = before[idx--];
			log("before[", idx + 1, "]=", ch, ',', (int)ch);
			if (Character.isWhitespace(ch)) {
				inputState = inputState.setBoolean(ComposingStarted, true)
				                       .setState(ComposingState, compoState.setString(Hat, "")
				                                                           .setString(Leads, "").setString(Vowels, ivowels)
				                                                           .setInteger(Tone, itone).setString(Afters, iafters)
				                                                           .setCharacter(Glottal, iglottal).setBoolean(PojNN, iPojNN));
				int crEnd = cursor;
				int ostart = beforeIndex[idx + 2];
				int crStart = ostart + cursor - olen;
				log("set composing region: ", crStart, '~', crEnd);
				viewState = viewState.setInteger(ComposingStart, crStart)
				                     .setInteger(ComposingEnd, crEnd)
				                     .setString(ComposingText, obefore.subSequence(ostart, olen).toString())
				                     .resetChanged(ComposingText);
				return;
			}
		}

		// parse Leads
		while (true) {
			String check = ch + mleads;
			log("check leads: ", check);
			if (ivowels != VOWELS_NULL) {
				if (isValidLeads(check)) {
					ileads = check;
					idxLeads = idx + 1;
					log("ileads=", ileads);
				}
			} else {
				if (isValidLeadForLeads(check)) {
					ileads = check;
					idxLeads = idx + 1;
					log("ileads=", ileads);
				}
			}
			if (!isValidAfterForLeads(check)) {
				idx++;
				break;
			}
			mleads = check;
			log("mleads=", mleads);

			if (idx < 0)
				break;
			ch = before[idx--];
			log("before[", idx + 1, "]=", ch, ',', (int)ch);
			if (Character.isWhitespace(ch)) {
				idx++;
				break;
			}
		}
		if (idxLeads >= 0) {
			idx = idxLeads - 1;
		} else {
			if (ivowels == VOWELS_NULL) {
				if (idx < 0 || before[idx] != '-')
					return;
				ileads = LEADS_NULL;
			}
			idx += mleads.length();
		}

		// parse Hat
		String hat = "";
		if (idx >= 0 && before[idx] == '-') {
			StringBuilder buf = new StringBuilder();
			char low = 0;
			while (idx >= 0) {
				ch = before[idx];
				log("before[", idx, "]=", ch, ',', (int)ch);
				if (Character.isLowSurrogate(ch)) {
					if (low != 0) {
						idx++;
						break;
					}
					low = ch;
					idx--;
					continue;
				}
				int cp;
				if (Character.isHighSurrogate(ch)) {
					if (low == 0)
						break;
					cp = Character.toCodePoint(ch, low);
				} else {
					cp = ch;
				}
				if (!isPhraseCharacter(cp)) {
					if (low != 0)
						idx++;
					break;
				}
				idx--;
				if (low != 0) {
					buf.insert(0, low);
					low = 0;
				}
				buf.insert(0, ch);
			}
			int hatBegin;
			for (hatBegin = 0; hatBegin < buf.length(); hatBegin++) {
				if (buf.charAt(hatBegin) != '-')
					break;
				idx++;
			}
			hat = composed(buf.substring(hatBegin));
		}

		inputState = inputState.setBoolean(ComposingStarted, true)
		                       .setState(ComposingState, compoState.setString(Hat, hat)
		                                                           .setString(Leads, ileads).setString(Vowels, ivowels)
		                                                           .setInteger(Tone, itone).setString(Afters, iafters)
		                                                           .setCharacter(Glottal, iglottal).setBoolean(PojNN, iPojNN));

		int crEnd = cursor;
		int ostart;
		if (idx == beforeIndex.length - 1)
			ostart = beforeIndex[idx] + 1;
		else
			ostart = beforeIndex[idx + 1];
		log("before: olen=", olen, " idx=", idx, " oidx=", ostart);
		int crStart = ostart + cursor - olen;
		log("set composing region: ", crStart, '~', crEnd);
		viewState = viewState.setInteger(ComposingStart, crStart)
		                     .setInteger(ComposingEnd, crEnd)
		                     .setString(ComposingText, obefore.subSequence(ostart, olen).toString())
		                     .resetChanged(ComposingText);
	}

	private void parseAfterText(InputConnection ic) {
		TKInputState compoState = inputState.getState(ComposingState);
		CharSequence oafter = ic.getTextAfterCursor(100, 0);
		log("view: get text after: [", oafter, ']');
		if (oafter == null || oafter.length() <= 0) {
			inputState = inputState.setState(ComposingRegion, compoState);
			updateToneViewState();
			return;
		}
		TKInputState compoRegion = parseAfterText(oafter, compoState);
		if (compoRegion.contains(Tail) || compoRegion.contains(Boot)) {
			int end = viewState.getInteger(ComposingEnd);
			if (end < 0) {
				end = viewState.getInteger(SelectionStart);
				if (viewState.contains(DeleteBefore))
					end -= viewState.getInteger(DeleteBefore);
				viewState = viewState.setInteger(ComposingStart, end);
			}
			String text = viewState.getString(ComposingText);

			if (compoRegion.contains(Tail)) {
				String tail = compoRegion.getString(Tail);
				int tailLen = tail.length();
				log("composing tail: [", tail, "](", tailLen, ')');
				text += tail;
				end += tailLen;
				log("extend composing region: ~", end, ", text [", text, ']');
			}

			if (compoRegion.contains(Boot)) {
				String boot = compoRegion.getString(Boot);
				int bootLen = boot.length();
				log("composing boot: [", boot, "](", bootLen, ')');
				text += boot;
				end += bootLen;
				log("extend composing region: ~", end, ", text [", text, ']');
			}
			viewState = viewState.setInteger(ComposingEnd, end)
			                     .setString(ComposingText, text).resetChanged(ComposingText);
		}
		inputState = inputState.setState(ComposingRegion, compoRegion);
		updateToneViewState();
	}

	private TKInputState parseAfterText(CharSequence oafter, TKInputState region) {
		String vowels = region.getString(Vowels);
		int tone = region.getInteger(Tone);
		boolean toneChangeable = (vowels == VOWELS_NULL || tone <= 1);
		return parseAfterText(oafter, region, toneChangeable);
	}

	private TKInputState parseAfterText(CharSequence oafter, TKInputState region, boolean toneChangeable) {
		String leads = region.getString(Leads);
		String vowels = region.getString(Vowels);
		int tone = region.getInteger(Tone);
		String afters = region.getString(Afters);
		char glottal = region.getCharacter(Glottal);
		boolean pojNN = region.getBoolean(PojNN);
		if (leads.isEmpty() && vowels.isEmpty() && isNasal(afters)
		    && glottal == GLOTTAL_NULL && !pojNN) {
			// nasal only, move nasal from afters to leads
			region = region.setString(Leads, afters).setString(Vowels, VOWELS_NULL).setString(Afters, AFTERS_NULL);
			return parseAfterText(oafter, region, toneChangeable);
		}

		Object[] dcp = decompose(oafter);
		char[] after = (char[])dcp[0];
		int[] afterIndex = (int[])dcp[1];
		int idx = 0;
		int idxTail = idx;
		char ch = after[idx++];

		if (vowels == VOWELS_NULL) {
			// parse (more) leading consonants
			boolean checkMore = true;
			do {
				if (isTone(ch)) {
					if (toneChangeable)
						tone = getToneNumber(ch);
				} else {
					String check;
					if (leads == LEADS_NULL)
						check = String.valueOf(ch);
					else
						check = leads + ch;
					if (!isValidLeadForLeads(check))
						break;
					leads = check;
				}

				if (idx >= after.length)
					checkMore = false;
				else
					ch = after[idx++];
			} while (checkMore);
			if (leads != LEADS_NULL) {
				region = region.setString(Leads, leads).setInteger(Tone, tone);
				idxTail = idx - 1;
			}
			if (!checkMore) {
				// move nasal to afters
				if (isNasal(leads)) {
					region = region.setString(Leads, "").setString(Vowels, "").setString(Afters, leads);
					if (tone == 0)
						region = region.setInteger(Tone, 1);
				}
				region = region.setString(Tail, oafter.toString());
				return region;
			}
		}

		if (afters == AFTERS_NULL) {
			// parse (more) vowels
			boolean checkMore = true;
			do {
				if (isTone(ch)) {
					if (toneChangeable)
						tone = getToneNumber(ch);
				} else {
					if (!isVowel(ch))
						break;
					if (vowels == VOWELS_NULL)
						vowels = String.valueOf(ch);
					else
						vowels += ch;
				}

				if (idx >= after.length)
					checkMore = false;
				else
					ch = after[idx++];
			} while (checkMore);
			if (vowels != VOWELS_NULL) {
				if (leads == LEADS_NULL) {
					leads = "";
					region = region.setString(Leads, leads);
				} else if (!leads.isEmpty() && !isValidLeads(leads)) {
					if (idxTail > 0) {
						int tailEnd = afterIndex[idxTail - 1] + 1;
						String tail = oafter.subSequence(0, tailEnd).toString();
						region = region.setString(Tail, tail);
						region = parseAfterTextBoot(oafter, tailEnd, region);
					} else {
						region = parseAfterTextBoot(oafter, 0, region);
					}
					return region;
				}
				region = region.setString(Vowels, vowels);
				if (tone == 0)
					tone = 1;
				region = region.setInteger(Tone, tone);
				idxTail = idx - 1;
			}
			if (!checkMore) {
				// move nasal without vowel from leads to afters
				if (vowels.isEmpty() && isNasal(leads))
					region = region.setString(Leads, "").setString(Afters, leads);
				region = region.setString(Tail, oafter.toString());
				return region;
			}
		}

		if (glottal == GLOTTAL_NULL) {
			// parse (more) tailing consonants
			if (!pojNN) {
				boolean checkMore = true;
				do {
					if (isTone(ch)) {
						if (toneChangeable)
							tone = getToneNumber(ch);
					} else {
						String check;
						if (afters == AFTERS_NULL)
							check = String.valueOf(ch);
						else
							check = afters + ch;
						if (!isValidAfters(check))
							break;
						// nn must be accompanied with vowel(s)
						if (!isNasal(check) && (vowels == VOWELS_NULL || vowels.isEmpty()))
							break;
						afters = check;
					}

					if (idx >= after.length)
						checkMore = false;
					else
						ch = after[idx++];
				} while (checkMore);
				if (afters != AFTERS_NULL) {
					if (vowels == VOWELS_NULL) {
						if (!isNasal(afters) // nn without vowel
						    || !isValidLeads(leads)) {
							if (idxTail > 0) {
								int tailEnd = afterIndex[idxTail - 1] + 1;
								String tail = oafter.subSequence(0, tailEnd).toString();
								region = region.setString(Tail, tail);
								region = parseAfterTextBoot(oafter, tailEnd, region);
							} else {
								region = parseAfterTextBoot(oafter, 0, region);
							}
							return region;
						}
						vowels = "";
						region = region.setString(Vowels, vowels);
						if (tone == 0) {
							tone = 1;
							region = region.setInteger(Tone, tone);
						}
						region = region.setString(Afters, afters);
					} else if (afters.equals("ⁿ")) {
						region = region.setString(Afters, "").setBoolean(PojNN, true);
					} else {
						region = region.setString(Afters, afters);
					}
					region = region.setInteger(Tone, tone);
					idxTail = idx - 1;
				} else if (vowels == VOWELS_NULL) {
					if (leads != LEADS_NULL && isNasal(leads)) {
						// move leads to afters
						afters = leads;
						leads = vowels = "";
						region = region.setString(Leads, leads)
						               .setString(Vowels, vowels)
						               .setString(Afters, afters);
						if (tone == 0)
							tone = 1;
						region = region.setInteger(Tone, tone);
					} else {
						if (idxTail > 0) {
							int tailEnd = afterIndex[idxTail - 1] + 1;
							String tail = oafter.subSequence(0, tailEnd).toString();
							region = region.setString(Tail, tail);
							region = parseAfterTextBoot(oafter, tailEnd, region);
						} else {
							region = parseAfterTextBoot(oafter, 0, region);
						}
						return region;
					}
				}
				if (!checkMore) {
					region = region.setString(Tail, oafter.toString());
					return region;
				}
			}

			// parse glottal
			if (isGlottal(ch)) {
				if (afters == AFTERS_NULL) {
					afters = "";
					region = region.setString(Afters, afters);
				} else if (pojNN) {
					// move POJ nn to afters
					afters = "ⁿ";
					pojNN = false;
					region = region.setString(Afters, afters).setBoolean(PojNN, pojNN);
				}
				glottal = ch;
				region = region.setCharacter(Glottal, glottal);
				if (tone != 8) {
					if (tone == 2)
						tone = 8;
					else
						tone = 4;
					region = region.setInteger(Tone, tone);
				}

				if (idx >= after.length) {
					region = region.setString(Tail, oafter.toString());
					return region;
				}
				idxTail = idx;
				ch = after[idx++];
			}
		}

		if (!pojNN && (afters == AFTERS_NULL || afters.isEmpty())) {
			if (ch == 'ⁿ') {
				if (afters == AFTERS_NULL) {
					afters = "";
					region = region.setString(Afters, afters);
				}
				region = region.setBoolean(PojNN, true);
				idxTail = idx;
			}
		}

		if (idxTail > 0) {
			int tailEnd = afterIndex[idxTail - 1] + 1;
			String tail = oafter.subSequence(0, tailEnd).toString();
			region = region.setString(Tail, tail);
			region = parseAfterTextBoot(oafter, tailEnd, region);
		} else {
			region = parseAfterTextBoot(oafter, 0, region);
		}
		return region;
	}

	private TKInputState parseAfterTextBoot(CharSequence oafter, int begin, TKInputState region) {
		int end = oafter.length();
		if (begin < end && oafter.charAt(begin) == '-') {
			int bootEnd = begin;
			char hi = 0;
			for (int i = begin; i < end; i++) {
				char ch = oafter.charAt(i);
				if (Character.isHighSurrogate(ch)) {
					if (hi != 0)
						break;
					hi = ch;
					continue;
				}
				int cp;
				if (Character.isLowSurrogate(ch)) {
					if (hi == 0)
						break;
					cp = Character.toCodePoint(hi, ch);
				} else {
					cp = ch;
				}
				hi = 0;
				if (!isPhraseCharacter(cp))
					break;
				bootEnd = i;
			}
			String boot = oafter.subSequence(begin, bootEnd + 1).toString();
			region = region.setString(Boot, boot);
		}
		return region;
	}

	// input inputState

	private void fineComposing() {
		resetComposing();
	}

	private void resetComposing() {
		inputState = inputState.setBoolean(ComposingStarted, false)
		                       .setState(ComposingState, COMPOSING_EMPTY)
		                       .setState(ComposingRegion, COMPOSING_EMPTY);
		viewState = viewState.setString(ComposingText, "").remove(ComposingCursor)
		                     .remove(ComposingCorrectedCached).set(AutoCorrects, AUTO_CORRECTS_NULL);
		updateToneViewState();
	}

	private void updateToneViewState() {
		int toneNumRise = 5;
		int toneNumHigh = 2;
		int toneNumMiddle = 7;
		int toneNumLow = 3;
		if (inputState.getBoolean(TextMode)
		    && inputState.getBoolean(ComposingMode)) {
			TKInputState compoState = inputState.getState(ComposingRegion);
			final int tone = compoState.getInteger(Tone, 0);
			switch (tone) {
			default:
				break;
			case 2:
				toneNumHigh = 1;
				break;
			case 3:
				toneNumLow = 1;
				break;
			case 4:
				toneNumHigh = 8;
				toneNumLow = 4;
				break;
			case 5:
				toneNumRise = 6;
				break;
			case 6:
				toneNumRise = 1;
				break;
			case 7:
				toneNumMiddle = 9;
				break;
			case 8:
				toneNumHigh = 4;
				toneNumLow = 4;
				break;
			case 9:
				toneNumMiddle = 1;
				break;
			}
		}
		viewState = viewState.setInteger(ToneNumRise, toneNumRise)
		                     .setInteger(ToneNumHigh, toneNumHigh)
		                     .setInteger(ToneNumMid, toneNumMiddle)
		                     .setInteger(ToneNumLow, toneNumLow);
	}

	private void updateToneByKey(int code) {
		TKInputState region = inputState.getState(ComposingRegion);
		int tone = region.getInteger(Tone);
		final int toneOrg = tone;
		if (toneOrg == 4 || toneOrg == 8) {
			// for 4th and 8th tones
			switch (code) {
			case KEYCODE_TONE_LOW: // always 4th tone
				tone = 4;
				break;
			case KEYCODE_TONE_HIGH: // toggle between 4th & 8th tone
				if (toneOrg == 4)
					tone = 8;
				else
					tone = 4;
				break;
			default: // no effect
				break;
			}
		} else {
			// for 1st, 2nd, 3rd, 5th, 6th, 7th, and 9th tones
			switch (code) {
			case KEYCODE_TONE_HIGH:// toggle between 2nd and 1st
				if (toneOrg == 2)
					tone = 1;
				else
					tone = 2;
				break;
			case KEYCODE_TONE_RISE: // toggle between 5th, 6th, and 1st
				switch (toneOrg) {
				default:
					tone = 5;
					break;
				case 5:
					tone = 6;
					break;
				case 6:
					tone = 1;
					break;
				}
				break;
			case KEYCODE_TONE_LOW: // toggle between 3rd and 1st
				if (toneOrg == 3)
					tone = 1;
				else
					tone = 3;
				break;
			case KEYCODE_TONE_MID: // toggle between 7th, 9th, and 1st
				switch (toneOrg) {
				default:
					tone = 7;
					break;
				case 7:
					tone = 9;
					break;
				case 9:
					tone = 1;
					break;
				}
				break;
			}
		}
		if (tone != toneOrg) {
			log("update tone to ", tone);
			inputState = inputState.setState(ComposingRegion, region.setInteger(Tone, tone));
			TKInputState state = inputState.getState(ComposingState);
			int toneState = state.getInteger(Tone);
			if (toneState == toneOrg || (toneOrg == 4 && toneState == 1)) {
				if (tone == 4 && state.getCharacter(Glottal) == GLOTTAL_NULL)
					tone = 1;
				inputState = inputState.setState(ComposingState, state.setInteger(Tone, tone));
			}
			updateToneViewState();
		}
	}

	private void inputComposingByKey(int code, boolean gwPrevious, boolean gwNext) {
		char ch = (char)code;
		if (inputState.getBoolean(CapState))
			ch = Character.toUpperCase(ch);

		// non-alphabet commits composing text and itself
		log("check code: ", code);
		if ((code < 'a' || code > 'z') && code != '\u0358' && code != 'ⁿ') {
			if (code == '-') {
				commitComposingText(true);
				TKInputState compoState = inputState.getState(ComposingState);
				String hat = compoState.getString(Hat, "");
				if (hat.isEmpty()) {
					log("commit text: -");
					addCommitText("-");
				} else {
					inputState = inputState.setBoolean(ComposingStarted, true)
					                       .setState(ComposingState, compoState.setString(Hat, hat + '-'));
				}
			} else {
				commitComposingText();
				String text = String.valueOf(ch);
				log("commit text: ", text);
				addCommitText(text);
			}
			return;
		}

		// tone mark; do not start or end composing mode
		if (isKeyTone(code)) {
			updateToneByKey(code);
			return;
		}

		TKInputState compoState = inputState.getState(ComposingState);
		String leads = compoState.getString(Leads);
		log("check leads: ", leads);
		if (leads == LEADS_NULL) {
			if (isKeyVowel(code)) {
				String vowels = String.valueOf(ch);
				log("update leads to [], vowels to [", vowels, ']');
				compoState = compoState.setString(Leads, "")
				                       .setString(Vowels, vowels);
				if (compoState.getInteger(Tone) == 0) {
					log("update tone to 1");
					compoState = compoState.setInteger(Tone, 1);
				}
				inputState = inputState.setBoolean(ComposingStarted, true)
				                       .setState(ComposingState, compoState);
			} else if (code == 'n' || code == 'm') {
				// for n, m, and ng with implicit vowel
				String afters = String.valueOf(ch);
				log("update leads to [], vowels to [], afters to [", afters, ']');
				compoState = compoState.setString(Leads, "")
				                       .setString(Vowels, "")
				                       .setString(Afters, afters);
				if (compoState.getInteger(Tone) == 0) {
					log("update tone to 1");
					compoState = compoState.setInteger(Tone, 1);
				}
				inputState = inputState.setBoolean(ComposingStarted, true)
				                       .setState(ComposingState, compoState);
			} else {
				String text = String.valueOf(ch);
				if (isValidLeadForLeads(text)) {
					log("update leads to [", text, ']');
					compoState = compoState.setString(Leads, text);
					inputState = inputState.setBoolean(ComposingStarted, true)
					                       .setState(ComposingState, compoState);
				} else {
					String hat = compoState.getString(Hat);
					text = hat + text;
					log("commit text: ", text);
					addCommitText(text);
					inputState = inputState.setState(ComposingState, compoState.setString(Hat, ""));
					return;
				}
			}
			return;
		}

		String vowels = compoState.getString(Vowels);
		log("check vowels: ", vowels);
		if (vowels == VOWELS_NULL) {
			if (isKeyVowel(code)) {
				if (!isValidLeads(leads)) {
					commitComposingText();
					// auto dash? bad for typo
					inputState = inputState.setBoolean(ComposingStarted, true);
					log("update leads to []");
					compoState = compoState.setString(Leads, "");
				}
				vowels = String.valueOf(ch);
				log("update vowels to [", vowels, ']');
				compoState = compoState.setString(Vowels, vowels);
				if (compoState.getInteger(Tone) == 0) {
					log("update tone to 1");
					compoState = compoState.setInteger(Tone, 1);
				}
			} else if (code == 'n' || code == 'm') {
				// for n, m, and ng with implicit vowel
				if (!isValidLeads(leads)) {
					commitComposingText();
					// auto dash? bad for typo
					inputState = inputState.setBoolean(ComposingStarted, true);
					log("update leads to []");
					compoState = compoState.setString(Leads, "");
				}
				String afters = String.valueOf(ch);
				log("update vowels to [], afters to [", afters, ']');
				compoState = compoState.setString(Vowels, "").setString(Afters, afters);
				if (compoState.getInteger(Tone) == 0) {
					log("update tone to 1");
					compoState = compoState.setInteger(Tone, 1);
				}
			} else {
				if (inputState.getBoolean(AspirationToggle) &&
				    doAspirationToggle(code, gwPrevious, gwNext, leads)) {
					return;
				}
				if (inputState.getBoolean(VoicelessToggle) &&
				    doVoicelessToggle(code, leads)) {
					return;
				}

				leads = leads + ch;
				if (!isValidLeadForLeads(leads)) {
					if (inputState.getBoolean(LeadReplaceLead)) {
						String check = String.valueOf(ch);
						if (isValidLeadForLeads(check)) {
							ch = (char)code;
							if (Character.isUpperCase(leads.codePointAt(0)))
								ch = Character.toUpperCase((char)code);
							leads = String.valueOf(ch);
							log("update leads to [", leads, ']');
							compoState = compoState.setString(Leads, leads);
							inputState = inputState.setState(ComposingState, compoState);
							return;
						}
					}

					StringBuilder preBdr = new StringBuilder();
					do {
						char[] chars = Character.toChars(leads.codePointAt(0));
						for (char aChar : chars) preBdr.append(aChar);
						leads = leads.substring(chars.length);
						if (leads.isEmpty()) {
							String hat = compoState.getString(Hat);
							//pre = hat + pre;
							preBdr.insert(0, hat);
							String pre = preBdr.toString();
							log("commit text: ", pre);
							addCommitText(pre);
							resetComposing();
							return;
						}
					} while (!isValidLeadForLeads(leads));
					String hat = compoState.getString(Hat);
					//pre = hat + pre;
					preBdr.insert(0, hat);
					String pre = preBdr.toString();
					log("commit text: ", pre);
					addCommitText(pre);
					compoState = compoState.setString(Hat, "");
					// auto dash? bad for typo
				}
				log("update leads to [", leads, ']');
				compoState = compoState.setString(Leads, leads);
			}
			inputState = inputState.setState(ComposingState, compoState);
			return;
		}

		String afters = compoState.getString(Afters);
		log("check afters: ", afters);
		if (afters == AFTERS_NULL) {
			if (isKeyVowel(code)) {
				vowels += ch;
				log("update vowels to [", vowels, ']');
				compoState = compoState.setString(Vowels, vowels);
			} else if (isKeyGlottal(code) && !gwNext) {
				log("update afters to [], glottal to ", ch);
				compoState = compoState.setString(Afters, "").setCharacter(Glottal, ch);
				int tone = compoState.getInteger(Tone);
				if (tone != 8) {
					if (tone == 2)
						tone = 8;
					else
						tone = 4;
				}
				log("update tone to ", tone);
				compoState = compoState.setInteger(Tone, tone);
			} else if (code == 'ⁿ') {
				log("update afters to [], glottal to N/A, pojNN to true");
				compoState = compoState.setString(Afters, "")
				                       .setCharacter(Glottal, GLOTTAL_NULL)
				                       .setBoolean(PojNN, true);
			} else {
				afters = String.valueOf(ch);
				if (!isValidAfters(afters)) {
					inputState = inputState.setState(ComposingState, compoState);
					// commit composing text, begin new syllable
					commitComposingText();
					if (inputState.getBoolean(AutoDash))
						addCommitText("-");
					inputComposingByKey(code, gwPrevious, gwNext);
					return;
				}
				log("update afters to [", afters, ']');
				compoState = compoState.setString(Afters, afters);
			}
			inputState = inputState.setState(ComposingState, compoState);
			return;
		}

		// vowels != null && afters != null

		// POJ nn
		if (code == 'ⁿ') {
			if (compoState.getBoolean(PojNN) || !afters.isEmpty()) {
				// commit composing text and the invalid nn
				commitComposingText();
				// no auto dash, just typo
				addCommitText(String.valueOf(ch));
			} else {
				log("update pojNN to true");
				compoState = compoState.setBoolean(PojNN, true);
				inputState = inputState.setState(ComposingState, compoState);
			}
			return;
		}

		// glottals

		if (isKeyGlottal(code) && !gwNext) {
			char glottal = compoState.getCharacter(Glottal);
			if (glottal == GLOTTAL_NULL) {
				if (compoState.getBoolean(PojNN)) {
					// move POJ nn to afters
					afters = "ⁿ";
					log("update afters to [", afters, "], pojNN to false");
					compoState = compoState.setString(Afters, afters).setBoolean(PojNN, false);
				}
				log("update glottal to ", ch);
				compoState = compoState.setCharacter(Glottal, ch);
				int tone = compoState.getInteger(Tone);
				if (tone == 2) {
					log("update tone to 8");
					compoState = compoState.setInteger(Tone, 8);
				} else if (tone != 4 && tone != 8) {
					log("update tone to 4");
					compoState = compoState.setInteger(Tone, 4);
				}
				inputState = inputState.setState(ComposingState, compoState);
			} else if (inputState.getBoolean(GlottalToggleTone) && Character.toLowerCase(glottal) == code) {
				if (glottal != ch) {
					// change case
					log("update glottal to ", ch);
					compoState = compoState.setCharacter(Glottal, ch);
				} else {
					// toggle tone
					if (compoState.getInteger(Tone) == 4) {
						log("update tone to 8");
						compoState = compoState.setInteger(Tone, 8);
					} else {
						log("update tone to 4");
						compoState = compoState.setInteger(Tone, 4);
					}
				}
				inputState = inputState.setState(ComposingState, compoState);
			} else if (inputState.getBoolean(GlottalReplaceGlottal)) {
				log("update glottal to ", ch);
				compoState = compoState.setCharacter(Glottal, ch);
				inputState = inputState.setState(ComposingState, compoState);
			} else {
				// commit composing text, begin new syllable
				commitComposingText();
				if (inputState.getBoolean(AutoDash))
					addCommitText("-");
				inputComposingByKey(code, gwPrevious, gwNext);
			}
			return;
		}

		// vowels

		if (isKeyVowel(code)) {
			if (leads.isEmpty() && vowels.isEmpty() && compoState.getCharacter(Glottal) == GLOTTAL_NULL) {
				// treat previous nasal (m, n, or ng) as consonant instead of afters
				leads = afters;
				vowels = String.valueOf(ch);
				log("update leads to [", leads, "], vowels to [", vowels, "], afters to N/A");
				compoState = compoState.setString(Leads, leads)
				                       .setString(Vowels, vowels)
				                       .setString(Afters, AFTERS_NULL);
				inputState = inputState.setState(ComposingState, compoState);
			} else {
				// commit composing text, begin new syllable
				commitComposingText();
				if (inputState.getBoolean(AutoDash))
					addCommitText("-");
				inputComposingByKey(code, gwPrevious, gwNext);
			}
			return;
		}

		// consonants

		if (compoState.getBoolean(PojNN) || compoState.getCharacter(Glottal) != GLOTTAL_NULL) {
			// commit composing text, begin new syllable
			commitComposingText();
			if (inputState.getBoolean(AutoDash))
				addCommitText("-");
			inputComposingByKey(code, gwPrevious, gwNext);
			return;
		}

		if (vowels.isEmpty() && !isNasal(afters + ch)) {
			String check = String.valueOf(ch);
			if (isValidAfters(check)) {
				// treat previous nasal (m, n, or ng) as consonant instead of afters
				if (!leads.isEmpty()) {
					String hat = compoState.getString(Hat);
					leads = hat + leads;
					log("commit text: ", leads);
					addCommitText(leads);
					compoState = compoState.setString(Hat, "");
					// auto dash? bad for typo
				}
				leads = afters;
				afters = check;
				log("update leads to [", leads, "], afters to [", afters, ']');
				compoState = compoState.setString(Leads, leads).setString(Afters, afters);
				inputState = inputState.setState(ComposingState, compoState);
			} else {
				// commit composing text, begin new syllable
				commitComposingText();
				if (inputState.getBoolean(AutoDash))
					addCommitText("-");
				inputComposingByKey(code, gwPrevious, gwNext);
			}
			return;
		}

		if (!gwNext) {
			afters += ch;
			if (isValidAfters(afters)) {
				log("update afters to [", afters, ']');
				compoState = compoState.setString(Afters, afters);
				inputState = inputState.setState(ComposingState, compoState);
				return;
			}
		}
		// commit composing text, begin new syllable
		commitComposingText();
		if (inputState.getBoolean(AutoDash))
			addCommitText("-");
		inputComposingByKey(code, gwPrevious, gwNext);
	}

	private void updateComposingRegion(InputConnection ic) {
		parseAfterText(ic);
		updateComposingText();
	}

	// input mode

	private boolean doAspirationToggle(int code, boolean gwPrevious, boolean gwNext, String leads) {
		boolean toggled = false;
		switch (leads) {
		case "k":
		case "K":
			if (code == 'k' && !gwNext) {
				leads += inputState.getBoolean(CapState) ? 'H' : 'h';
				toggled = true;
			}
			break;
		case "kh":
		case "Kh":
		case "kH":
		case "KH":
			if (code == 'k' && !gwNext) {
				leads = leads.substring(0, 1);
				toggled = true;
			}
			break;
		case "t":
		case "T":
			if (code == 't' && !gwNext) {
				leads += inputState.getBoolean(CapState) ? 'H' : 'h';
				toggled = true;
			}
			break;
		case "th":
		case "Th":
		case "tH":
		case "TH":
			if (code == 't' && !gwNext) {
				leads = leads.substring(0, 1);
				toggled = true;
			}
			break;
		case "p":
		case "P":
			if (code == 'p' && !gwNext) {
				leads += inputState.getBoolean(CapState) ? 'H' : 'h';
				toggled = true;
			}
			break;
		case "ph":
		case "Ph":
		case "pH":
		case "PH":
			if (code == 'p' && !gwNext) {
				leads = leads.substring(0, 1);
				toggled = true;
			}
			break;
		case "ts":
		case "Ts":
		case "tS":
		case "TS":
			if (code == 't' && gwNext) {
				leads += inputState.getBoolean(CapState) ? 'T' : 't';
				toggled = true;
			}
			break;
		case "tst":
		case "Tst":
		case "tSt":
		case "TSt":
			if (code == 's' && gwPrevious) {
				leads = leads.substring(0, 2) + 'h';
				toggled = true;
			}
			break;
		case "tsT":
		case "TsT":
		case "tST":
		case "TST":
			if (code == 's' && gwPrevious) {
				leads = leads.substring(0, 2) + 'H';
				toggled = true;
			}
			break;
		case "tsh":
		case "Tsh":
		case "tSh":
		case "TSh":
		case "tsH":
		case "TsH":
		case "tSH":
		case "TSH":
			if (code == 't' && gwNext) {
				leads += 't';
				toggled = true;
			}
			break;
		case "tsht":
		case "Tsht":
		case "tSht":
		case "TSht":
		case "tsHt":
		case "TsHt":
		case "tSHt":
		case "TSHt":
			if (code == 's' && gwPrevious) {
				leads = leads.substring(0, 2);
				toggled = true;
			}
			break;
		case "ch":
		case "Ch":
		case "cH":
		case "CH":
			if (code == 'c' && gwNext) {
				leads += inputState.getBoolean(CapState) ? 'C' : 'c';
				toggled = true;
			}
			break;
		case "chc":
		case "Chc":
		case "cHc":
		case "CHc":
			if (code == 'h' && gwPrevious) {
				leads = leads.substring(0, 2) + 'h';
				toggled = true;
			}
			break;
		case "chC":
		case "ChC":
		case "cHC":
		case "CHC":
			if (code == 'h' && gwPrevious) {
				leads = leads.substring(0, 2) + 'H';
				toggled = true;
			}
			break;
		case "chh":
		case "cHh":
		case "Chh":
		case "CHh":
		case "chH":
		case "cHH":
		case "ChH":
		case "CHH":
			if (code == 'c' && gwNext) {
				leads += 'c';
				toggled = true;
			}
			break;
		case "chhc":
		case "cHhc":
		case "Chhc":
		case "CHhc":
		case "chHc":
		case "cHHc":
		case "ChHc":
		case "CHHc":
			if (code == 'h' && gwPrevious) {
				leads = leads.substring(0, 2);
				toggled = true;
			}
			break;
		}
		if (toggled) {
			log("update leads to ", leads);
			inputState = inputState.setState(ComposingState,
			                                 inputState.getState(ComposingState).setString(Leads, leads));
		}
		return toggled;
	}

	private boolean doVoicelessToggle(int code, String leads) {
		boolean toggled = false;
		switch (leads) {
		// g and k
		case "g":
			if (code == 'g') {
				leads = "k";
				toggled = true;
			}
			break;
		case "G":
			if (code == 'g') {
				leads = "K";
				toggled = true;
			}
			break;
		case "k":
			if (code == 'g') {
				leads = "g";
				toggled = true;
			}
			break;
		case "K":
			if (code == 'g') {
				leads = "G";
				toggled = true;
			}
			break;
		// b and p
		case "b":
			if (code == 'b') {
				leads = "p";
				toggled = true;
			}
			break;
		case "B":
			if (code == 'b') {
				leads = "P";
				toggled = true;
			}
			break;
		case "p":
			if (code == 'b') {
				leads = "b";
				toggled = true;
			}
			break;
		case "P":
			if (code == 'b') {
				leads = "B";
				toggled = true;
			}
			break;
		// j and ch/ts
		case "j":
			if (code == 'j') {
				if (inputState.getInteger(Subtype) == SUBTYPE_POJ)
					leads = "ch";
				else
					leads = "ts";
				toggled = true;
			}
			break;
		case "J":
			if (code == 'j') {
				if (inputState.getInteger(Subtype) == SUBTYPE_POJ) {
					if (inputState.getString(CapMode) == TEXT_CAP_CHARACTERS)
						leads = "CH";
					else
						leads = "Ch";
				} else {
					if (inputState.getString(CapMode) == TEXT_CAP_CHARACTERS)
						leads = "TS";
					else
						leads = "Ts";
				}
				toggled = true;
			}
			break;
		case "ch":
		case "cH":
		case "ts":
		case "tS":
			if (code == 'j') {
				leads = "j";
				toggled = true;
			}
			break;
		case "Ch":
		case "CH":
		case "Ts":
		case "TS":
			if (code == 'j') {
				leads = "J";
				toggled = true;
			}
			break;
		// l and d (should be t and d, though)
		case "l":
			if (code == 'l') {
				leads = "d";
				toggled = true;
			}
			break;
		case "L":
			if (code == 'l') {
				leads = "D";
				toggled = true;
			}
			break;
		case "d":
			if (code == 'l') {
				leads = "l";
				toggled = true;
			}
			break;
		case "D":
			if (code == 'l') {
				leads = "L";
				toggled = true;
			}
			break;
		}
		if (toggled) {
			log("update leads to ", leads);
			inputState = inputState.setState(ComposingState,
			                                 inputState.getState(ComposingState).setString(Leads, leads));
		}
		return toggled;
	}

	private void addCommitText(String text) {
		if (viewState.contains(TextBeforeCached)) {
			CharSequence cache = (CharSequence)viewState.get(TextBeforeCached);
			String compText = viewState.getString(ComposingText);
			int cacheLen = cache.length();
			int compLen = compText.length();
			if (cacheLen > compLen) {
				StringBuilder sb = new StringBuilder();
				int boundary = cacheLen - compLen;
				sb.append(cache, 0, boundary);
				sb.append(text);
				sb.append(cache, boundary, cacheLen);
				viewState.set(TextBeforeCached, sb);
			}
		}
		if (viewState.contains(CommitText)) {
			String orgText = viewState.getString(CommitText);
			text = orgText + text;
		}
		viewState = viewState.setString(CommitText, text);
	}

	private void commitComposingText() {
		commitComposingText(false);
	}

	private void commitComposingText(boolean hatten) {
		String text = viewState.getString(ComposingText);
		if (!text.isEmpty()) {
			if (viewState.contains(ComposingCursor)) {
				TKInputState state = inputState.getState(ComposingState);
				int tone = state.getInteger(Tone);
				char glottal = state.getCharacter(Glottal);
				if (glottal == GLOTTAL_NULL && tone == 8) {
					state = state.setInteger(Tone, 2);
					text = state.getString(Hat) + getComposingText(state);
				} else {
					int cursor = viewState.getInteger(ComposingCursor);
					text = text.substring(0, cursor);
					int start = viewState.getInteger(ComposingStart);
					viewState = viewState.setInteger(ComposingStart, start)
					                     .setInteger(ComposingEnd, start + cursor)
					                     .setString(ComposingText, text);
				}
			} else if (viewState.getBoolean(HasAutoCorrection) && viewState.contains(ComposingCorrectedCached)) {
                /* commitCorrection seems not work
                String textCorrected = viewState.getString(ComposingCorrectedCached);
                TKViewState from = viewState.getChangedFrom();
                int start = from.getInteger(SelectionStart);
                if (viewState.contains(CommitText))
                    start += viewState.getString(CommitText).length();
                start -= text.length();
                CorrectionInfo info = new CorrectionInfo(start, text, textCorrected);
                viewState = viewState.set(ComposingCorrection, info);
                */
				text = inputState.getState(ComposingState).getString(Hat) + viewState.getString(ComposingCorrectedCached);
			}

			if (!hatten)
				addCommitText(text);
		}
		resetComposing();
		if (hatten) {
			TKInputState compoState = inputState.getState(ComposingState);
			compoState = compoState.setString(Hat, text);
			inputState = inputState.setState(ComposingState, compoState);
		}
	}

	private void deleteComposing(InputConnection ic) {
		excludeComposingTail(ic);

		if (!inputState.getBoolean(ComposingStarted)) {
			addDeleteCodePointsBefore(ic, 1);
			initComposing(ic, viewState.getInteger(SelectionStart));
			return;
		}

		TKInputState compoState = inputState.getState(ComposingState);
		final TKInputState compoStateOrg = compoState;
		boolean checkAftersEmpty = false;
		boolean checkVowelEmpty = false;
		boolean checkLeadsEmpty = false;
		boolean checkHatEmpty = false;
		String hat = compoState.getString(Hat);
		String leads = compoState.getString(Leads);
		String vowels = compoState.getString(Vowels);
		String afters = compoState.getString(Afters);
		if (compoState.getBoolean(PojNN)) {
			compoState = compoState.setBoolean(PojNN, false);
			if (compoState.getCharacter(Glottal) == GLOTTAL_NULL) {
				checkAftersEmpty = true;
			}
		} else if (compoState.getCharacter(Glottal) != GLOTTAL_NULL) {
			compoState = compoState.setCharacter(Glottal, GLOTTAL_NULL);
			if (compoState.getInteger(Tone) == 8)
				compoState = compoState.setInteger(Tone, 2);
			else
				compoState = compoState.setInteger(Tone, 1);
			if (afters.equals("ⁿ"))
				compoState = compoState.setString(Afters, "").setBoolean(PojNN, true);
			else
				checkAftersEmpty = true;
		} else if (afters != AFTERS_NULL) {
			if (inputState.getBoolean(DeleteAfterCluster)) {
				afters = AFTERS_NULL;
				compoState = compoState.setString(Afters, AFTERS_NULL);
				checkVowelEmpty = true;
			} else {
				int len = afters.length();
				if (len <= 1) {
					compoState = compoState.setString(Afters, AFTERS_NULL);
					checkVowelEmpty = true;
				} else {
					afters = afters.substring(0, len - 1);
					compoState = compoState.setString(Afters, afters);
				}
			}
		} else if (vowels != VOWELS_NULL) {
			String mode = inputState.getString(DeleteVowelsMode);
			int delSize = 1;
			int len = vowels.length();
			switch (mode) {
			default:
			case DEL_VOWELS_CHAR:
				if (len >= 2 && vowels.charAt(len - 1) == '\u0358')
					delSize = 2;
				break;
			case DEL_VOWELS_SINGLE:
				if (len >= 2) {
					char lastCh = vowels.charAt(len - 1);
					if (lastCh == '\u0358') {
						delSize = 2;
					} else if (isO(lastCh)) { // oo
						char lastCh2 = vowels.charAt(len - 2);
						if (isO(lastCh2))
							delSize = 2;
					} else if (isE(lastCh)) { // ee
						char lastCh2 = vowels.charAt(len - 2);
						if (isE(lastCh2))
							delSize = 2;
					} else if (isR(lastCh)) { // ir or er
						delSize = 2;
					}
				}
				break;
			case DEL_VOWELS_WHOLE:
				delSize = len;
				break;
			}
			len -= delSize;
			if (len <= 0) {
				compoState = compoState.setString(Vowels, VOWELS_NULL).setInteger(Tone, 0);
				checkLeadsEmpty = true;
			} else {
				vowels = vowels.substring(0, len);
				compoState = compoState.setString(Vowels, vowels);
			}
		} else if (leads != LEADS_NULL) {
			String mode = inputState.getString(DeleteLeadingsMode);
			switch (mode) {
			case DEL_LEADINGS_CHAR: {
				int len = leads.length();
				if (len <= 1) {
					compoState = compoState.setString(Leads, LEADS_NULL).setInteger(Tone, 0);
					checkHatEmpty = true;
				} else {
					leads = leads.substring(0, len - 1);
					compoState = compoState.setString(Leads, leads);
				}
				break;
			}
			default:
			case DEL_LEADINGS_VALID:
				do {
					int len = leads.length();
					if (len <= 1) {
						compoState = compoState.setString(Leads, LEADS_NULL).setInteger(Tone, 0);
						checkHatEmpty = true;
						break;
					}
					leads = leads.substring(0, len - 1);
					compoState = compoState.setString(Leads, leads);
				} while (!isValidLeads(leads));
				break;
			case DEL_LEADINGS_WHOLE:
				compoState = compoState.setString(Leads, LEADS_NULL).setInteger(Tone, 0);
				checkHatEmpty = true;
				break;
			}
		} else if (!"".equals(hat)) {
			hat = hat.substring(0, hat.length() - 1);
			compoState = compoState.setString(Hat, hat);
			checkHatEmpty = true;
		} else {
			addDeleteCodePointsBefore(ic, 1);
		}
		if (checkAftersEmpty) {
			if ("".equals(afters)) {
				compoState = compoState.setString(Afters, AFTERS_NULL);
				checkVowelEmpty = true;
			}
		}
		if (checkVowelEmpty) {
			if ("".equals(vowels)) {
				compoState = compoState.setString(Vowels, VOWELS_NULL).setInteger(Tone, 0);
				checkLeadsEmpty = true;
			}
		}
		if (checkLeadsEmpty) {
			if ("".equals(leads)) {
				inputState = inputState.setBoolean(ComposingStarted, false);
				compoState = compoState.setString(Leads, LEADS_NULL)
				                       .setInteger(Tone, 0);
			} else if (isNasal(leads)) {
				// move nasal to afters
				compoState = compoState.setString(Leads, "")
				                       .setString(Vowels, "")
				                       .setString(Afters, leads);
				if (compoState.getInteger(Tone) == 0)
					compoState = compoState.setInteger(Tone, 1);
			}
		}
		if (checkHatEmpty) {
			if ("".equals(hat))
				inputState = inputState.setBoolean(ComposingStarted, false);
		}
		if (compoState != compoStateOrg)
			inputState = inputState.setState(ComposingState, compoState);

		if (compoState.getString(Leads) == LEADS_NULL) {
			int cursor = viewState.getInteger(SelectionStart);
			int skip = viewState.getString(ComposingText).length() - hat.length();
			parseBeforeText(ic, cursor, skip);
		}
		updateComposingRegion(ic);
	}

	private void initTextState(InputConnection ic, int start, int end) {
		if (inputState.getBoolean(ComposingMode)) {
			if (start != end) {
				// selection
				commitComposingText();
			} else {
				// cursor only
				initComposing(ic, start);
			}
		}
		initTextCapMode(ic);
	}

	private void fineTextState() {
		if (inputState.getBoolean(TextMode)) {
			if (inputState.getBoolean(ComposingMode)) {
				fineComposing();
				inputState = inputState.setBoolean(ComposingMode, false);
			}
			fineTextCapMode();
			inputState = inputState.set(TextMode, false);
		}
	}

	private void inputTextComposing(InputConnection ic, int code,
	                                boolean gluedWithPrevious,
	                                boolean gluedWithNext) {
		inputComposingByKey(code, gluedWithPrevious, gluedWithNext);
		// Capitalization for next
		updateTextCapModeAfterKey(ic, code);
	}

	private void inputText(InputConnection ic, int code) {
		char ch = (char)code;
		if (inputState.getBoolean(CapState))
			ch = Character.toUpperCase(ch);
		addCommitText(String.valueOf(ch));
		// Capitalization for next
		updateTextCapModeAfterKey(ic, code);
	}

	private void deleteText(InputConnection ic) {
		if (inputState.getBoolean(ComposingMode)) {
			deleteComposing(ic);
			if (inputState.getBoolean(ComposingStarted)) {
				boolean willCap = inputState.getBoolean(CapState);
				switch (inputState.getString(CapMode)) {
				case TEXT_CAP_CHARACTERS:
					willCap = true;
					break;
				case TEXT_CAP_WORDS:
				case TEXT_CAP_SENTENCES:
					willCap = false;
					break;
				}
				log("text will be capitalized: ", willCap);
				inputState = inputState.setBoolean(CapState, willCap);
				if (viewState.getInteger(CurrentKeyboard).equals(viewState.getInteger(TextKeyboard)))
					viewState = viewState.setBoolean(ShiftState, willCap);
				return;
			}
		} else {
			addDeleteCodePointsBefore(ic, 1);
		}
		initTextCapMode(ic);
	}

	protected void switchInputType(InputConnection ic, EditorInfo info) {
		viewState = viewState.setBoolean(NumSym1Primes, false)
		                     .setInteger(ActionID, -1)
		                     .setInteger(ActionKeyType, KEY_TYPE_LABEL)
		                     .setString(ActionKeyLabel, " ");
		boolean fullScreen = isFullscreenMode();
		log("Full screen: ", fullScreen);

		log("SDK version: ", Build.VERSION.SDK_INT);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			kbdSwitcher = false;
		} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			if (imMgr == null) { // for test only
				kbdSwitcher = true;
			} else {
				Window window = getWindow().getWindow();
				if (window != null)
					kbdSwitcher = imMgr.shouldOfferSwitchingToNextInputMethod(window.getAttributes().token);
			}
		} else {
			kbdSwitcher = true;
		}
		log("Keyboard switcher: ", kbdSwitcher);

		// action keys
		int actionID;
		String actionLabel = null;
		int actionIconID = 0;
		int actionIconType = 0;
		if (info.actionLabel != null) {
			log("customized action: label", info.actionLabel, " id=", info.actionId);
			actionLabel = info.actionLabel.toString();
			if (actionLabel.isEmpty())
				actionLabel = " ";
			actionID = info.actionId;
		} else {
			actionID = info.imeOptions & EditorInfo.IME_MASK_ACTION;
			log("built-in action: id=", actionID);
			switch (actionID) {
			default:
			case EditorInfo.IME_ACTION_UNSPECIFIED:
				break;
			case EditorInfo.IME_ACTION_DONE:
				actionIconID = R.drawable.ic_done_24dp;
				actionIconType = 1;
				break;
			case EditorInfo.IME_ACTION_GO:
				actionIconID = R.drawable.ic_navigate_next_24dp;
				actionIconType = 1;
				break;
			case EditorInfo.IME_ACTION_NEXT:
				actionIconID = R.drawable.ic_keyboard_arrow_down_24dp;
				actionIconType = 1;
				break;
			case EditorInfo.IME_ACTION_PREVIOUS:
				actionIconID = R.drawable.ic_keyboard_arrow_up_24dp;
				actionIconType = 1;
				break;
			case EditorInfo.IME_ACTION_SEARCH:
				actionIconID = android.R.drawable.ic_menu_search;
				break;
			case EditorInfo.IME_ACTION_SEND:
				actionIconID = android.R.drawable.ic_menu_send;
				break;
			}
		}
		log("action ID: ", actionID);
		viewState = viewState.setInteger(ActionID, actionID);
		if (actionID != EditorInfo.IME_ACTION_UNSPECIFIED && actionID != EditorInfo.IME_ACTION_NONE) {
			log("action label: ", actionLabel);
			log("action icon ID: ", actionIconID);
			log("action icon type: ", actionIconType);
			if (actionIconID != 0) {
				viewState = viewState.setInteger(ActionKeyType, KEY_TYPE_ICON)
				                     .setInteger(ActionKeyIconID, actionIconID)
				                     .setInteger(ActionKeyIconType, actionIconType);
			} else {
				viewState = viewState.setInteger(ActionKeyType, KEY_TYPE_LABEL)
				                     .setString(ActionKeyLabel, actionLabel);
			}
		}

		if (info.inputType == InputType.TYPE_NULL) {
			// use ASCII keyboard without any text processing
			inputState = inputState.setBoolean(CapState, info.initialCapsMode != 0);
			viewState = viewState.setInteger(TextKeyboard, KBD_ASCII)
			                     .setInteger(CurrentKeyboard, KBD_ASCII)
			                     .setBoolean(ShiftState, inputState.getBoolean(CapState))
			                     .setString(InputTypeDisplay, "(null)");
			return;
		}

		int itClass = info.inputType & InputType.TYPE_MASK_CLASS;
		int itVariation = info.inputType & InputType.TYPE_MASK_VARIATION;
		int itFlags = info.inputType & InputType.TYPE_MASK_FLAGS;
		StringBuilder sb = new StringBuilder();
		switch (itClass) {
		default:
			sb.append(Integer.toHexString(itClass));
			sb.append(':');
			sb.append(Integer.toHexString(itVariation));
			sb.append('(');
			sb.append(Integer.toHexString(itFlags));
			sb.append(')');

			// use ASCII keyboard without any text processing
			inputState = inputState.setBoolean(CapState, info.initialCapsMode != 0);
			viewState = viewState.setInteger(TextKeyboard, KBD_ASCII)
			                     .setInteger(CurrentKeyboard, KBD_ASCII)
			                     .setBoolean(ShiftState, inputState.getBoolean(CapState));
			break;
		case InputType.TYPE_CLASS_PHONE:
			sb.append("phone");
			if (itVariation != 0 || itFlags != 0) {
				if (itVariation != 0) {
					sb.append(':');
					sb.append(Integer.toHexString(itVariation));
				}
				if (itFlags != 0) {
					sb.append('(');
					sb.append(Integer.toHexString(itFlags));
					sb.append(')');
				}
			}

			inputState = inputState.setBoolean(CapState, false);
			viewState = viewState.setInteger(CurrentKeyboard, KBD_NUMSYM1)
			                     .setBoolean(ShiftState, false)
			                     .setInteger(TextKeyboard, KBD_ASCII);
			break;
		case InputType.TYPE_CLASS_DATETIME:
			sb.append("dt");
			switch (itVariation) {
			case InputType.TYPE_DATETIME_VARIATION_NORMAL:
				sb.append(":datetime");
				break;
			default:
				sb.append(':');
				sb.append(Integer.toHexString(itVariation));
				break;
			case InputType.TYPE_DATETIME_VARIATION_DATE:
				sb.append("dateOnly");
				break;
			case InputType.TYPE_DATETIME_VARIATION_TIME:
				sb.append("timeOnly");
				break;
			}
			if (itFlags != 0) {
				sb.append('(');
				sb.append(Integer.toHexString(itFlags));
				sb.append(')');
			}

			inputState = inputState.setBoolean(CapState, false);
			viewState = viewState.setInteger(CurrentKeyboard, KBD_NUMSYM1)
			                     .setBoolean(ShiftState, false)
			                     .setBoolean(NumSym1Primes, true)
			                     .setInteger(TextKeyboard, KBD_ASCII);
			break;
		case InputType.TYPE_CLASS_NUMBER:
			sb.append("number");
			switch (itVariation) {
			case InputType.TYPE_NUMBER_VARIATION_NORMAL:
				//sb.append(":normal");
				break;
			default:
				sb.append(':');
				sb.append(Integer.toHexString(itVariation));
				break;
			case InputType.TYPE_NUMBER_VARIATION_PASSWORD:
				sb.append(":passwd");
				viewState = viewState.setInteger(TextKeyboard, KBD_ASCII);
				break;
			}
			if (itFlags != 0) {
				sb.append('(');
				boolean firstFlag = true;
				if ((itFlags & InputType.TYPE_NUMBER_FLAG_SIGNED) != 0) {
					firstFlag = false;
					sb.append("signed");
					itFlags &= ~InputType.TYPE_NUMBER_FLAG_SIGNED;
				}
				if ((itFlags & InputType.TYPE_NUMBER_FLAG_DECIMAL) != 0) {
					if (!firstFlag)
						sb.append(',');
					else
						firstFlag = false;
					sb.append("decimal");
					itFlags &= ~InputType.TYPE_NUMBER_FLAG_DECIMAL;
				}
				if (itFlags != 0) {
					if (!firstFlag)
						sb.append(',');
					sb.append(Integer.toHexString(itFlags));
				}
				sb.append(')');
			}

			inputState = inputState.setBoolean(CapState, false);
			viewState = viewState.setInteger(CurrentKeyboard, KBD_NUMSYM1)
			                     .setBoolean(ShiftState, false)
			                     .setBoolean(NumSym1Primes, true)
			                     .setInteger(TextKeyboard, KBD_ASCII);
			break;
		case InputType.TYPE_CLASS_TEXT:
			sb.append("text");
			boolean useASCII = false;
			int kcodePeriodAlt = KEYCODE_COMMA;
			switch (itVariation) {
			case InputType.TYPE_TEXT_VARIATION_NORMAL:
				//sb.append("normal");
				break;
			default:
				sb.append(':');
				sb.append(Integer.toHexString(itVariation));
				break;
			// technical
			case InputType.TYPE_TEXT_VARIATION_URI:
				sb.append(":uri");
				useASCII = true;
				kcodePeriodAlt = KEYCODE_SLASH;
				break;
			case InputType.TYPE_TEXT_VARIATION_PASSWORD:
				sb.append(":passwd");
				useASCII = true;
				break;
			case InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD:
				sb.append(":vPasswd");
				useASCII = true;
				break;
			case InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS:
				sb.append(":emAddr");
				useASCII = true;
				kcodePeriodAlt = KEYCODE_AT;
				break;
			case InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT:
				sb.append(":emSbj");
				break;
			// daily usage
			case InputType.TYPE_TEXT_VARIATION_PERSON_NAME:
				sb.append(":pName");
				break;
			case InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS:
				sb.append(":postAddr");
				break;
			// situation
			case InputType.TYPE_TEXT_VARIATION_SHORT_MESSAGE:
				sb.append(":shortMsg");
				break;
			case InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE:
				sb.append(":longMsg");
				break;
			case InputType.TYPE_TEXT_VARIATION_FILTER:
				// entering text to filter contents of a list etc.
				sb.append(":filter");
				break;
			case InputType.TYPE_TEXT_VARIATION_PHONETIC:
				// Entering text for phonetic pronunciation,
				// such as a phonetic name field in contacts.
				// This is mostly useful for languages where one spelling may have
				// several phonetic readings, like Japanese.
				sb.append(":phonetic");
				break;
			case InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT:
				sb.append(":webText");
				break;
			case InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS:
				sb.append(":webEmAddr");
				useASCII = true;
				kcodePeriodAlt = KEYCODE_AT;
				break;
			case InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD:
				sb.append(":webPasswd");
				useASCII = true;
				break;
			}
			boolean hasSuggestions = true;
			textEditorWillAutoComplete = false;
			String textCapMode = TEXT_CAP_NONE;
			textMultiLine = false;
			boolean autoCorrect = false;
			if (itFlags != 0) {
				sb.append('(');
				boolean firstFlag = true;

				if ((itFlags & InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE) != 0) {
					// The editor should show an interface for displaying suggestions,
					// the input method should not be showing candidates itself,
					// but can expect the editor to supply its own completions/candidates
					// from onDisplayCompletions(), i.e. in fullscreen mode
					firstFlag = false;
					sb.append("autoComp");
					itFlags &= ~InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE;

					textEditorWillAutoComplete = true;
				}
				if ((itFlags & InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS) != 0) {
					// The IME should never show an interface to display suggestions.
					// Most IMEs will also take this to mean they should not try to auto-correct.
					// It overrides the TYPE_TEXT_FLAG_AUTO_CORRECT value when set.
					if (!firstFlag)
						sb.append(',');
					else
						firstFlag = false;
					sb.append("noSugg");
					itFlags &= ~InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;

					hasSuggestions = false;
				}
				if ((itFlags & InputType.TYPE_TEXT_FLAG_AUTO_CORRECT) != 0) {
					// The IME will try to auto-correct typos as the user is typing,
					// but does not define whether the IME offers an interface to show suggestions.
					// Should always set this flag unless really expect users to type non-words
					if (!firstFlag)
						sb.append(',');
					else
						firstFlag = false;
					sb.append("autoCorr");
					itFlags &= ~InputType.TYPE_TEXT_FLAG_AUTO_CORRECT;

					autoCorrect = true;
				}

				if ((itFlags & InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS) != 0) {
					// Capitalize all characters.
					// Overrides TYPE_TEXT_FLAG_CAP_WORDS and TYPE_TEXT_FLAG_CAP_SENTENCES.
					if (!firstFlag)
						sb.append(',');
					else
						firstFlag = false;
					sb.append("capChars");
					itFlags &= ~InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS;

					textCapMode = TEXT_CAP_CHARACTERS;
				} else if ((itFlags & InputType.TYPE_TEXT_FLAG_CAP_WORDS) != 0) {
					// Capitalize the first character of every word.
					// Overrides TYPE_TEXT_FLAG_CAP_SENTENCES.
					if (!firstFlag)
						sb.append(',');
					else
						firstFlag = false;
					sb.append("capWords");
					itFlags &= ~InputType.TYPE_TEXT_FLAG_CAP_WORDS;

					textCapMode = TEXT_CAP_WORDS;
				} else if ((itFlags & InputType.TYPE_TEXT_FLAG_CAP_SENTENCES) != 0) {
					// Capitalize the first character of each sentence.
					if (!firstFlag)
						sb.append(',');
					else
						firstFlag = false;
					sb.append("capSents");
					itFlags &= ~InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;

					textCapMode = TEXT_CAP_SENTENCES;
				}

				// TYPE_TEXT_FLAG_MULTI_LINE:
				// Multiple lines of text can be entered into the field.
				// The IME may choose not to display an enter key when this flag is not set.
				if ((itFlags & InputType.TYPE_TEXT_FLAG_MULTI_LINE) != 0) {
					if (!firstFlag)
						sb.append(',');
					else
						firstFlag = false;
					sb.append("multiLine");
					itFlags &= ~InputType.TYPE_TEXT_FLAG_MULTI_LINE;

					textMultiLine = true;
				}

				// TYPE_TEXT_FLAG_IME_MULTI_LINE:
				// The regular text view associated with this should not be multi-line,
				// but when a fullscreen input method is providing text,
				// it should use multiple lines if it can.
				if ((itFlags & InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE) != 0) {
					if (!firstFlag)
						sb.append(',');
					else
						firstFlag = false;
					sb.append("imeMultiLine");
					itFlags &= ~InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE;

					if (fullScreen)
						textMultiLine = true;
				}

				if (itFlags != 0) {
					if (!firstFlag)
						sb.append(',');
					sb.append(Integer.toHexString(itFlags));
				}
				sb.append(')');
			}

			inputState = inputState.setBoolean(TextMode, true).setString(CapMode, textCapMode)
			                       .setBoolean(CapState, info.initialCapsMode != 0);
			if (useASCII) {
				inputState = inputState.setBoolean(ComposingMode, false);
			} else {
				inputState = inputState.setBoolean(ComposingMode, true);
				initComposing(ic, info.initialSelStart);
			}
			log("Set input state to:\n  text mode:", inputState.getBoolean(TextMode),
			    "\n  capitalization mode: ", inputState.getString(CapMode),
			    "\n  initial capitalization: ", inputState.getBoolean(CapState),
			    "\n  composing mode: ", inputState.getBoolean(ComposingMode)
			);

			viewState = viewState.setBoolean(ShowCompletions, textEditorWillAutoComplete && fullScreen)
			                     .setBoolean(ShiftState, inputState.getBoolean(CapState))
			                     .setBoolean(HasAutoCorrection, autoCorrect)
			                     .setBoolean(HasSuggestions, hasSuggestions)
			                     .setBoolean(MultiLine, textMultiLine);
			if (useASCII) {
				viewState = viewState.setInteger(TextKeyboard, KBD_ASCII)
				                     .setInteger(CurrentKeyboard, KBD_ASCII)
				                     .setInteger(AsciiPeriodAlt, kcodePeriodAlt);
			} else {
				int kbdIdxMain = viewState.getInteger(MainKeyboard);
				viewState = viewState.setInteger(TextKeyboard, kbdIdxMain)
				                     .setInteger(CurrentKeyboard, kbdIdxMain);
			}

			log("Set view state to:\n  show completions for editor: ", viewState.getBoolean(ShowCompletions),
			    "\n  main keyboard: ", viewState.getInteger(MainKeyboard),
			    "\n  text keyboard: ", viewState.getInteger(TextKeyboard),
			    "\n  current keyboard: ", viewState.getInteger(CurrentKeyboard),
			    "\n  shift state: ", viewState.getBoolean(ShiftState),
			    "\n  auto correction: ", viewState.getBoolean(HasAutoCorrection),
			    "\n  suggestions: ", viewState.getBoolean(HasSuggestions),
			    "\n  multi-line: ", viewState.getBoolean(MultiLine),
			    "\n  period alt for ASCII: ", viewState.getInteger(AsciiPeriodAlt, KEYCODE_COMMA)
			);
			break;
		}
		viewState = viewState.setString(InputTypeDisplay, sb.toString());
	}

	protected void updateView(InputConnection ic) {
		if (viewState == null)
			return;

		// cursor/selection
		int expStart = viewState.getInteger(SelectionStart);
		int expEnd = viewState.getInteger(SelectionEnd);
		if (viewState.isAnyChanged(SelectionStart, SelectionEnd)) {
			log("view: set selection to ", expStart, '~', expEnd);
			ic.setSelection(expStart, expEnd);
		}

		// delete
		if (viewState.contains(DeleteAfter)) {
			// DeleteAfter is number of chars
			int numChars = viewState.getInteger(DeleteAfter);
			if (numChars > 0) {
				log("view: delete ", numChars, " characters after");
				boolean rz = ic.deleteSurroundingText(0, numChars);
				log("view: delete ", rz ? "succeeds" : "fails");
			}
			viewState = viewState.remove(DeleteAfter);
		}
		if (viewState.contains(DeleteBefore)) {
			int numChars = viewState.getInteger(DeleteBefore);
			if (numChars > 0) {
				// TODO: tested on API level 15 without workaround
				// TODO: if followed by setting composing region, delete return true without effect
				log("view: delete ", numChars, " characters before");
				boolean rz = ic.deleteSurroundingText(numChars, 0);
				log("view: delete ", rz ? "succeeds" : "fails");
				// update cursor/selection
				expStart -= numChars;
				expEnd -= numChars;
			}
			viewState = viewState.remove(DeleteBefore);
		}

		// update composing region
		int compStart = viewState.getInteger(ComposingStart, -1);
		int compEnd = viewState.getInteger(ComposingEnd, -1);
		boolean composingRegionUpdated = false;
		if (viewState.isAnyChanged(ComposingStart, ComposingEnd)) {
			log("view: set composing region to ", compStart, '~', compEnd);
			ic.setComposingRegion(compStart, compEnd);
			composingRegionUpdated = true;
		}

		// commit text
		boolean composingFinished = false;
		if (viewState.contains(CommitText)) {
			String text = viewState.getString(CommitText);
			// XXX: commitText() does not clear composing text as documented
			// so we use finishComposingText() here
			if (compStart >= 0) {
				log("view: finish text: [", text, ']');
				ic.setComposingText(text, 1);
				ic.finishComposingText();
			} else {
				log("view: commit text: [", text, ']');
				ic.commitText(text, 1);
			}

			// update cursor/selection
			int textLen = text.length();
			if (compStart < 0) {
				expStart += textLen;
				expEnd = expStart;
			} else {
				expStart = compStart + textLen;
				expEnd = expStart;
				composingFinished = true;
			}
			// update composing region
			compStart = compEnd = -1;

			viewState = viewState.remove(CommitText);
		}

		if (viewState.contains(ComposingCorrection)) {
			CorrectionInfo info = (CorrectionInfo)viewState.get(ComposingCorrection);
			log("view: commit correction ", info);
			ic.commitCorrection(info);
			viewState = viewState.remove(ComposingCorrection);
		}

		// composing text
		boolean composingUpdated = false;
		if (viewState.isChanged(ComposingText)) {
			String text = viewState.getString(ComposingText);
			int lenText = text.length();
			if (lenText <= 0 && (composingRegionUpdated || composingFinished)) {
				// do nothing
			} else {
				log("view: set composing text: [", text, ']');
				ic.setComposingText(text, 1);
				if (lenText <= 0) {
					log("view: finish composing text");
					ic.finishComposingText();
				} else {
					composingUpdated = true;
				}

				// update composing region and cursor/selection
				if (lenText <= 0) {
					expStart = expEnd = compStart;
					compStart = compEnd = -1;
				} else {
					if (compStart < 0)
						compStart = expStart;
					compEnd = compStart + lenText;
					expStart = expEnd = compEnd;
				}
			}
		} else if (composingFinished) {
			String text = viewState.getString(ComposingText);
			if (text != null && !text.isEmpty()) {
				log("view: set composing text: [", text, ']');
				ic.setComposingText(text, 1);
				// update composing region
				int textLen = text.length();
				compStart = expStart;
				compEnd = compStart + textLen;
				expStart = expEnd = compEnd;
				composingUpdated = true;
			}
		}

		if (viewState.contains(ComposingCursor)) {
			int cursor = compStart + viewState.getInteger(ComposingCursor);
			if (composingUpdated || expStart != cursor) {
				expStart = expEnd = cursor;
				if (ic != null) {
					log("view: set selection to ", expStart, '~', expEnd);
					ic.setSelection(expStart, expEnd);
				}
			}
		}

		viewState = viewState.setInteger(SelectionStart, expStart);
		viewState = viewState.setInteger(SelectionEnd, expEnd);
		log("expected selection: ", expStart, '~', expEnd);
		viewState = viewState.setInteger(ComposingStart, compStart);
		viewState = viewState.setInteger(ComposingEnd, compEnd);
		log("expected composing: ", compStart, '~', compEnd);

		if (!testMode) {
			updateAutoCorrectsView();
			updateCandidatesView();
			updateKeyboardView();
		}
		viewState = viewState.remove(TextBeforeCursor)
		                     .remove(TextBeforeCached)
		                     .resetChanged();
	}

	private void updateAutoCorrectsView() {
		if (!viewState.isChanged(AutoCorrects))
			return;
		String[] items = (String[])viewState.get(AutoCorrects);

		// nothing to show
		autoCorrects.removeAllViews();
		if (items.length <= 0) {
			viewState = viewState.set(AutoCorrects, AUTO_CORRECTS_NULL);
			return;
		}

		final TKInputState corrected = (TKInputState)viewState.get(ComposingCorrected);
		final View.OnClickListener listener = v -> {
			log("update composing to correction: ", corrected);
			InputConnection ic = getCurrentInputConnection();
			ic.beginBatchEdit();
			inputState = inputState.setState(ComposingState, corrected)
			                       .setState(ComposingRegion, corrected)
			                       .setBoolean(PhoneticInputTempDisabled, true);
			updateComposingText();
			updateToneViewState();
			updateView(ic);
			ic.endBatchEdit();
		};
		String correctedText = null;
		if (viewState.getBoolean(HasAutoCorrection))
			correctedText = viewState.getString(ComposingCorrectedCached);
		boolean odd = true;
		int cnt = 0;
		for (final String item : items) {
			if (item == null)
				continue;
			TextView view = new TextView(this);
			view.setText(item);
			view.setOnClickListener(listener);
			TKKeyboardView.Theme theme = kbdView.getTheme();
			if (theme != null) {
				if (item.equals(correctedText))
					view.setTextColor(theme.ledColor);
				else if (odd)
					view.setTextColor(theme.textColor);
				else
					view.setTextColor(theme.textColor2);
			} else {
				if (item.equals(correctedText))
					view.setTextColor(Color.RED);
				else if (odd)
					view.setTextColor(Color.LTGRAY);
				else
					view.setTextColor(Color.WHITE);
			}
			view.setPadding(16, 0, 16, 0);
			view.setTextSize(16.0f);
			odd = !odd;
			view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
			autoCorrects.addView(view);
			cnt++;
		}
		if (cnt <= 0)
			viewState = viewState.set(AutoCorrects, AUTO_CORRECTS_NULL);
	}

	private void updateCandidatesView() {
		// not changed
		if (!viewState.isChanged(Candidates))
			return;
		Object[] cdds = (Object[])viewState.get(Candidates);
		if (cdds instanceof CompletionInfo[]) {
			// not in the mode
			if (!viewState.getBoolean(ShowCompletions, false)) {
				viewState = viewState.set(Candidates, CANDIDATES_NULL);
				return;
			}
		}
		// view is not initialized yet
		if (candidates == null) {
			viewState = viewState.set(Candidates, CANDIDATES_NULL);
			return;
		}
		// nothing to show
		if (cdds.length <= 0) {
			// avoid twinkle
			if (viewState.getString(ComposingText).isEmpty())
				hideCandidates();
			clearCandidates();
			viewState = viewState.set(Candidates, CANDIDATES_NULL);
			return;
		}

		clearCandidates();
		boolean odd = true;
		int cnt = 0;
		for (final Object cdd : cdds) {
			if (cdd == null)
				continue;

			final String text;
			final View.OnClickListener listener;
			if (cdd instanceof CompletionInfo) {
				final CompletionInfo cinfo = (CompletionInfo)cdd;
				CharSequence clabel = cinfo.getLabel();
				if (clabel != null) {
					text = clabel.toString();
				} else {
					CharSequence ctext = cinfo.getText();
					if (ctext == null)
						continue;
					text = ctext.toString();
				}
				listener = v -> {
					viewState = viewState.set(Candidates, CANDIDATES_NULL)
					                     .remove(CandidatePrefix)
					                     .set(AutoCorrects, AUTO_CORRECTS_NULL);
					InputConnection ic = getCurrentInputConnection();
					ic.beginBatchEdit();
					log("view: commit completion: ", cinfo);
					ic.commitCompletion(cinfo);
					updateView(ic);
					ic.endBatchEdit();
				};
			} else {
				text = cdd.toString();
				listener = v -> {
					log("commit candidate: ", text);
					resetComposing();
					addCommitText(text);
					viewState = viewState.set(Candidates, CANDIDATES_NULL)
					                     .set(AutoCorrects, AUTO_CORRECTS_NULL);
					InputConnection ic = getCurrentInputConnection();
					ic.beginBatchEdit();
					updateView(ic);
					ic.endBatchEdit();

					// prefix propagation
					final String prefix = viewState.getString(CandidatePrefix, null);
					String full;
					if (prefix == null)
						full = text;
					else
						full = prefix + text;
					viewState = viewState.setString(CandidatePrefix, full);
					TKBackgroundService.incrementWordWeight(TKInputMethodService.this, full);
					new TKQueryCandidatesTask(this, db).execute(
						new TKQueryParameter(full, inputState, viewState));
				};
			}
			TextView cdv = new TextView(this);
			cdv.setText(text);
			cdv.setOnClickListener(listener);
			TKKeyboardView.Theme theme = kbdView.getTheme();
			if (theme != null) {
				if (odd)
					cdv.setTextColor(theme.textColor);
				else
					cdv.setTextColor(theme.textColor2);
			} else {
				if (odd)
					cdv.setTextColor(Color.LTGRAY);
				else
					cdv.setTextColor(Color.WHITE);
			}
			cdv.setTextSize(16.0f);
			final float density = getResources().getDisplayMetrics().scaledDensity;
			cdv.setPadding((int)(4 * density), 0, (int)(4 * density), 0);
			float minWidth = 32 * density;
			cdv.setMinWidth((int)minWidth);
			cdv.setGravity(Gravity.CENTER);
			odd = !odd;
			cdv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
			candidates.addView(cdv);
			cnt++;
		}
		if (cnt > 0)
			showCandidates();
		else
			viewState = viewState.set(Candidates, CANDIDATES_NULL).remove(CandidatePrefix);
	}

	private void updateKeyboardView() {
		// keyboard
		if (viewState.isChanged(InputTitle))
			inputTitle.setText(viewState.getString(InputTitle));
		if (viewState.isChanged(InputTypeDisplay))
			inputType.setText(viewState.getString(InputTypeDisplay));

		int kbdIdx = viewState.getInteger(CurrentKeyboard);
		boolean currChanged = viewState.isChanged(CurrentKeyboard);
		boolean allKeysChanged = false;
		if (currChanged) {
			log("switch to keyboard: ", kbdIdx);
			kbdView.setKeyboard(kbds[kbdIdx]);
			// shifted inputState was reset by setKeyboard()
			kbdView.setShifted(viewState.getBoolean(ShiftState));
			allKeysChanged = true;
		} else if (viewState.isChanged(ShiftState)) {
			kbdView.setShifted(viewState.getBoolean(ShiftState));
			allKeysChanged = true;
		}

		// action key
		if (viewState.getInteger(ActionKeyType) == KEY_TYPE_LABEL) {
			if (currChanged || viewState.isAnyChanged(ActionKeyType, ActionKeyLabel)) {
				String label = viewState.getString(ActionKeyLabel);
				Keyboard.Key key = kbdKeys[KEY_ACTION][kbdIdx];
				key.label = label;
				key.icon = null;
				if (!allKeysChanged)
					kbdView.invalidateKey(keyIdxActions[kbdIdx]);
			}
		} else {
			if (currChanged || viewState.isAnyChanged(ActionKeyType, ActionKeyIconID)) {
				int iconID = viewState.getInteger(ActionKeyIconID);
				int iconType = viewState.getInteger(ActionKeyIconType);
				// compatible with KitKat (4.4) or lower
				Drawable icon;
				if (iconType == 0) {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
						icon = getResources().getDrawable(iconID, null);
					else
						icon = getResources().getDrawable(iconID);
				} else {
					icon = VectorDrawableCompat.create(getResources(), iconID, null);
				}
				if (icon != null) {
					icon = DrawableCompat.wrap(icon.mutate());
					DrawableCompat.setTint(icon, Color.WHITE);
					log("key icon: ", icon);
					Keyboard.Key key = kbdKeys[KEY_ACTION][kbdIdx];
					key.label = null;
					key.icon = icon;
					if (!allKeysChanged)
						kbdView.invalidateKey(keyIdxActions[kbdIdx]);
				}
			}
		}

		if (inputState.getBoolean(TextMode)) {
			// multi-line: space and enter key
			if (currChanged || viewState.isChanged(MultiLine)) {
				Keyboard.Key keyEnter = kbdKeys[KEY_ENTER][kbdIdx];
				Keyboard.Key keySpace = kbdKeys[KEY_SPACE][kbdIdx];
				if (viewState.getBoolean(MultiLine)) {
					keySpace.width = keyWidthSpaces[kbdIdx];
					keyEnter.width = keyWidthEnters[kbdIdx];
					keyEnter.label = keyLabelEnters[kbdIdx];
				} else {
					keySpace.width = keyWidthSpaces[kbdIdx] + keyGapEnters[kbdIdx] + keyWidthEnters[kbdIdx];
					keyEnter.width = 0;
					keyEnter.label = " ";
				}
				if (!allKeysChanged) {
					kbdView.invalidateKey(keyIdxEnters[kbdIdx]);
					kbdView.invalidateKey(keyIdxSpaces[kbdIdx]);
				}
			}
		}

		switch (kbdIdx) {
		case KBD_ASCII: // period key
			if (currChanged || viewState.isChanged(AsciiPeriodAlt)) {
				int periodAlt = viewState.getInteger(AsciiPeriodAlt, KEYCODE_COMMA);
				keyPeriodASCII.codes[1] = periodAlt;
				keyPeriodASCII.label = ". " + (char)periodAlt;
				if (!allKeysChanged)
					kbdView.invalidateKey(keyIdxPeriodASCII);
			}
			break;
		case KBD_NUMSYM1: // apostrophe/prime and quotation/double-prime key
			if (currChanged || viewState.isChanged(NumSym1Primes)) {
				if (viewState.getBoolean(NumSym1Primes, false)) {
					// prime and double prime
					keyApostNS1.codes[0] = KEYCODE_PRIME;
					keyApostNS1.label = KEYLABEL_PRIME;
					keyQuoteNS1.codes[0] = KEYCODE_DPRIME;
					keyQuoteNS1.label = KEYLABEL_DPRIME;
				} else {
					// apostrophe and quotation
					keyApostNS1.codes[0] = '\'';
					keyApostNS1.label = "'";
					keyQuoteNS1.codes[0] = '"';
					keyQuoteNS1.label = "\"";
				}
				if (!allKeysChanged) {
					kbdView.invalidateKey(keyIdxApostNS1);
					kbdView.invalidateKey(keyIdxQuoteNS1);
				}
			}
			break;
		case KBD_TL:
			updateToneKeys(toneKeysTl, allKeysChanged);
			break;
		case KBD_POJ:
			updateToneKeys(toneKeysPoj, allKeysChanged);
			break;
		}

		if (allKeysChanged)
			kbdView.invalidateAllKeys();
	}

	private static class ToneKeys {
		public final int riseIdx;
		public final Keyboard.Key rise;
		public final int highIdx;
		public final Keyboard.Key high;
		public final int midIdx;
		public final Keyboard.Key mid;
		public final int lowIdx;
		public final Keyboard.Key low;

		public ToneKeys(
			int riseIdx, Keyboard.Key rise,
			int highIdx, Keyboard.Key high,
			int midIdx, Keyboard.Key mid,
			int lowIdx, Keyboard.Key low) {
			this.riseIdx = riseIdx;
			this.rise = rise;
			this.highIdx = highIdx;
			this.high = high;
			this.midIdx = midIdx;
			this.mid = mid;
			this.lowIdx = lowIdx;
			this.low = low;
		}
	}

	private ToneKeys toneKeysTl;
	private ToneKeys toneKeysPoj;

	private void updateToneKeys(ToneKeys keys, boolean allKeysChanged) {
		if (viewState.isChanged(ToneNumRise)) {
			switch (viewState.getInteger(ToneNumRise)) {
			default:
				keys.rise.label = KEYLABEL_TONE5;
				break;
			case 6:
				keys.rise.label = KEYLABEL_TONE6;
				break;
			case 1:
				keys.rise.label = KEYLABEL_TONE1;
				break;
			}
			if (!allKeysChanged)
				kbdView.invalidateKey(keys.riseIdx);
		}
		if (viewState.isChanged(ToneNumMid)) {
			switch (viewState.getInteger(ToneNumMid)) {
			default:
				keys.mid.label = KEYLABEL_TONE7;
				break;
			case 9:
				keys.mid.label = KEYLABEL_TONE9;
				break;
			case 1:
				keys.mid.label = KEYLABEL_TONE1;
				break;
			}
			if (!allKeysChanged)
				kbdView.invalidateKey(keys.midIdx);
		}
		if (viewState.isChanged(ToneNumHigh)) {
			switch (viewState.getInteger(ToneNumHigh)) {
			default:
				keys.high.label = KEYLABEL_TONE2;
				break;
			case 4:
				keys.high.label = KEYLABEL_TONE4;
				break;
			case 8:
				keys.high.label = KEYLABEL_TONE8;
				break;
			case 1:
				keys.high.label = KEYLABEL_TONE1;
				break;
			}
			if (!allKeysChanged)
				kbdView.invalidateKey(keys.highIdx);
		}
		if (viewState.isChanged(ToneNumLow)) {
			switch (viewState.getInteger(ToneNumLow)) {
			default:
				keys.low.label = KEYLABEL_TONE3;
				break;
			case 4:
				keys.low.label = KEYLABEL_TONE4;
				break;
			case 1:
				keys.low.label = KEYLABEL_TONE1;
				break;
			}
			if (!allKeysChanged)
				kbdView.invalidateKey(keys.lowIdx);
		}
	}

	@Override
	public void onStartInputView(EditorInfo info, boolean restarting) {
		if (appDebug) {
			StringBuilder sb = new StringBuilder("onStartInputView: restarting=");
			sb.append(restarting);
			sb.append(" EditorInfo=\n");
			info.dump(new StringBuilderPrinter(sb), "  ");
			log(sb.toString());
		}
		startInputState();
		// because onCurrentInputMethodSubtypeChanged() is not always called
		startViewState(info);
		switchCurrentSubtype();

		startCandidates();

		InputConnection ic = getCurrentInputConnection();
		log("ic: ", ic);
		if (ic == null) { // FIXME: why null?
			log("InputConnection IS NULL!");
			switchInputType(null, info);
			updateKeyboardView();
		} else {
			switchInputType(ic, info);
			ic.beginBatchEdit();
			updateView(ic);
			ic.endBatchEdit();
		}
		super.onStartInputView(info, restarting);
	}

	// for hardware keyboard

	protected void switchCurrentSubtype() {
		InputMethodSubtype subType = imMgr.getCurrentInputMethodSubtype();
		log("current subtype: ", subType);
		switchSubtype(subType);
		updateMainKeyboard();
	}

	protected void updateMainKeyboard() {
		final int subType = inputState.getInteger(Subtype);
		viewState = viewState.setString(InputTitle, getInputTitleDisplay(subType));

		final int kbdMain;
		switch (subType) {
		default:
		case SUBTYPE_TL:
			kbdMain = KBD_TL;
			break;
		case SUBTYPE_POJ:
			kbdMain = KBD_POJ;
			break;
		}
		viewState = viewState.setInteger(MainKeyboard, kbdMain);
	}

	// OnKeyboardActionListener

	// editor switched
	@Override
	public void onFinishInput() {
		log("onFinishInput");
		// onFinishInput will be called even when onStartInputView not called
		if (inputState.getBoolean(InputStarted)) {
			fineTextState();
			finishCandidates();
			finishViewState();
			finishInputState();
		}
		super.onFinishInput();
	}

	@Override
	public void onDisplayCompletions(CompletionInfo[] completions) {
		if (appDebug) {
			StringBuilder sb = new StringBuilder("onDisplayCompletions:");
			if (completions == null) {
				sb.append("(null)");
			} else {
				for (CompletionInfo info : completions) {
					sb.append(' ');
					sb.append(info.getText());
				}
			}
			log(sb.toString());
		}
		if (viewState == null)
			return;
		if (completions == null)
			viewState = viewState.set(Candidates, CANDIDATES_NULL).remove(CandidatePrefix);
		else
			viewState = viewState.set(Candidates, completions);
		InputConnection ic = getCurrentInputConnection();
		ic.beginBatchEdit();
		updateView(ic);
		ic.endBatchEdit();
	}

	@Override
	public void onStartCandidatesView(EditorInfo info, boolean restarting) {
		if (appDebug) {
			StringBuilder sb = new StringBuilder("onStartCandidatesView: restarting=");
			sb.append(restarting);
			sb.append(" EditorInfo=\n");
			info.dump(new StringBuilderPrinter(sb), "  ");
			log(sb.toString());
		}

		InputConnection ic = getCurrentInputConnection();
		if (ic != null) { // FIXME: why null?
			ic.beginBatchEdit();
			switchInputType(ic, info);
			ic.endBatchEdit();
		}
		super.onStartCandidatesView(info, restarting);
	}

	@Override
	public void onFinishCandidatesView(boolean finishingInput) {
		log("onFinishCandidatesView: ", finishingInput);
		super.onFinishCandidatesView(finishingInput);
	}

	@Override
	public void onKey(int code, int[] nearCodes) {
		if (appDebug) {
			StringBuilder sbuf = new StringBuilder("onKey(");
			sbuf.append(code);
			sbuf.append(",[");
			boolean rest = false;
			for (int near1 : nearCodes) {
				if (near1 == -1) {
					if (rest) {
						sbuf.append("...");
						break;
					}
					rest = true;
				}
				sbuf.append(near1);
				sbuf.append(',');
			}
			if (rest)
				sbuf.append(']');
			else
				sbuf.setCharAt(sbuf.length() - 1, ']');
			sbuf.append(')');
			log(sbuf.toString());
		}

		InputConnection ic = getCurrentInputConnection();

		// special keys
		if (code < 0) {
			// symbols
			String sym = null;
			switch (code) {
			case KEYCODE_DIVISION:
				sym = "\u00f7";
				break;
			case KEYCODE_DEGREE:
				sym = "\u00b0";
				break;
			case KEYCODE_MU:
				sym = "\u00b5";
				break;
			case KEYCODE_ELLIPSIS:
				sym = "\u2026";
				break;
			case KEYCODE_SUPERSCRIPT2:
			case KEYCODE_SUPERSCRIPT3:
				sym = "\u00b2";
				break;
			}
			if (sym != null) {
				if (inputState.getBoolean(TextMode) && inputState.getBoolean(ComposingMode)
				    && inputState.getBoolean(ComposingStarted)) {
					commitComposingText();
				}
				addCommitText(sym);
				ic.beginBatchEdit();
				updateView(ic);
				ic.endBatchEdit();
				return;
			}

			switch (code) {
			default:
				return;
			case KEYCODE_SHIFT: // Shift
				boolean shifted = !inputState.getBoolean(CapState);
				inputState = inputState.setBoolean(CapState, shifted);
				viewState = viewState.setBoolean(ShiftState, shifted);
				ic.beginBatchEdit();
				updateView(ic);
				ic.endBatchEdit();
				return;
			case KEYCODE_DELETE: // Delete
				ic.beginBatchEdit();
				updateStateOnDelete(ic);
				updateView(ic);
				ic.endBatchEdit();
				return;
			case KEYCODE_LANGUAGE: // Language
				ic.beginBatchEdit();
				boolean viewUpdated = switchInputMethod(ic);
				if (viewUpdated)
					updateView(ic);
				ic.endBatchEdit();
				return;
			case KEYCODE_ACTION:
				int actionID = viewState.getInteger(ActionID, -1);
				if (actionID != -1) {
					log("view: perform editor action ", actionID);
					ic.performEditorAction(actionID);
				}
				return;
			case KEYCODE_NUMSYM1: // number/symbol first layer
				log("switch to keyboard: numsym1");
				viewState = viewState.setInteger(CurrentKeyboard, KBD_NUMSYM1)
				                     .setBoolean(ShiftState, false);
				ic.beginBatchEdit();
				updateView(ic);
				ic.endBatchEdit();
				return;
			case KEYCODE_TEXT:
				int kbdIdxText = viewState.getInteger(TextKeyboard);
				log("switch to keyboard: text: ", kbdIdxText);
				viewState = viewState.setInteger(CurrentKeyboard, kbdIdxText)
				                     .setBoolean(ShiftState, inputState.getBoolean(CapState));
				ic.beginBatchEdit();
				updateView(ic);
				ic.endBatchEdit();
				return;
			case KEYCODE_NUMSYM2: // number/symbol second layer
				log("switch to keyboard: numsym2");
				viewState = viewState.setInteger(CurrentKeyboard, KBD_NUMSYM2)
				                     .setBoolean(ShiftState, false);
				ic.beginBatchEdit();
				updateView(ic);
				ic.endBatchEdit();
				return;
			}
		}

		ic.beginBatchEdit();
		updateStateOnKey(ic, code);
		updateView(ic);
		ic.endBatchEdit();
	}

	protected void updateStateOnDelete(InputConnection ic) {
		final int start = viewState.getInteger(SelectionStart);
		final int end = viewState.getInteger(SelectionEnd);
		if (start != end) {
			// delete selection
			viewState = viewState.setInteger(SelectionEnd, start)
			                     .incInteger(DeleteAfter, end - start);
		} else {
			if (inputState.getBoolean(TextMode)) {
				deleteText(ic);
			} else {
				addDeleteCodePointsBefore(ic, 1);
			}
		}
	}

	protected void updateStateOnKey(InputConnection ic, int code) {
		if (inputState.getBoolean(TextMode)) {
			if (inputState.getBoolean(ComposingMode)) {
				if (isKeyTone(code)) {
					updateToneByKey(code);
					updateComposingText();
				} else {
					excludeComposingTail(ic);
					inputTextComposing(ic, code, false, false);
					updateComposingRegion(ic);
				}
			} else {
				inputText(ic, code);
			}
		} else {
			addCommitText(String.valueOf((char)code));
		}
	}

	private void excludeComposingTail(InputConnection ic) {
		if (viewState.contains(ComposingCursor)) {
			int cursor = viewState.getInteger(ComposingCursor);
			int start, end;
			String text;
			if (cursor == 0) {
				start = end = -1;
				text = "";
			} else {
				start = viewState.getInteger(ComposingStart);
				end = start + cursor;
				text = viewState.getString(ComposingText).substring(0, cursor);
			}
			log("exclude tail from composing text: [", text, ']');
			log("view: set composing region to ", start, '~', end);
			ic.setComposingRegion(start, end);
			viewState = viewState.setInteger(ComposingStart, start).resetChanged(ComposingStart)
			                     .setInteger(ComposingEnd, end).resetChanged(ComposingEnd)
			                     .setString(ComposingText, text).resetChanged(ComposingText);
		}
	}

	protected boolean switchInputMethod(InputConnection ic) {
		String langKey = inputState.getString(LanguageKey, LANGKEY_NEXT_IM);
		WindowManager.LayoutParams params = null;
		if (langKey.equals(LANGKEY_NEXT_IM) || langKey.equals(LANGKEY_LAST_IM)) {
			Window window = getWindow().getWindow();
			if (window != null)
				params = window.getAttributes();
			if (params == null) {
				langKey = LANGKEY_ASCII;
			} else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
				langKey = LANGKEY_ASCII;
			} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
			           && !imMgr.shouldOfferSwitchingToNextInputMethod(params.token)) {
				langKey = LANGKEY_ASCII;
			}
		}
		switch (langKey) {
		default:
		case LANGKEY_NEXT_IM:
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && params != null)
				imMgr.switchToNextInputMethod(params.token, false);
			break;
		case LANGKEY_LAST_IM:
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && params != null)
				imMgr.switchToLastInputMethod(params.token);
			break;
		case LANGKEY_PICK_IM:
			imMgr.showInputMethodPicker();
			break;
		case LANGKEY_ASCII:
			if (viewState.getInteger(TextKeyboard) == KBD_ASCII) {
				// already ASCII, switch back
				inputState = inputState.setBoolean(ComposingMode, true);
				initComposing(ic, viewState.getInteger(SelectionStart));
				int kbdMain = viewState.getInteger(MainKeyboard);
				viewState = viewState.setInteger(TextKeyboard, kbdMain)
				                     .setInteger(CurrentKeyboard, kbdMain);
			} else {
				// switch to ASCII
				commitComposingText();
				inputState = inputState.setBoolean(ComposingMode, false);
				viewState = viewState.setInteger(TextKeyboard, KBD_ASCII)
				                     .setInteger(CurrentKeyboard, KBD_ASCII);
			}
			viewState = viewState.setBoolean(ShiftState, inputState.getBoolean(CapState));
			return true;
		}
		return false;
	}

	@Override
	public void onText(CharSequence text) {
		log("onText(", text, ')');

		InputConnection ic = getCurrentInputConnection();
		ic.beginBatchEdit();
		updateStateOnText(ic, text);
		updateView(ic);
		ic.endBatchEdit();
	}

	protected void updateStateOnText(InputConnection ic, CharSequence text) {
		if (inputState.getBoolean(TextMode)) {
			if (inputState.getBoolean(ComposingMode)) {
				excludeComposingTail(ic);

				int last = text.length() - 1;
				for (int i = 0; i <= last; i++) {
					inputTextComposing(ic, text.charAt(i), i != 0, i != last);
				}

				updateComposingRegion(ic);
			} else {
				int textLen = text.length();
				for (int i = 0; i < textLen; i++) {
					inputText(ic, text.charAt(i));
				}
			}
		} else {
			addCommitText(text.toString());
		}
	}

	// low level

	@Override
	public void onPress(int code) {
		log("onPress(", code, ')');
	}

	@Override
	public void onRelease(int code) {
		log("onRelease(", code, ')');
	}

	// swiping

	@Override
	public void swipeRight() {
		log("swipeRight()");
	}

	@Override
	public void swipeUp() {
		log("swipeUp()");

	}

	@Override
	public void swipeDown() {
		log("swipeDown()");

	}

	@Override
	public void swipeLeft() {
		log("swipeLeft()");

	}

	// hardware key events

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		log("onKeyDown: code=", keyCode, " event=", event);
        /*
        Override this to intercept key down events before they are processed by the application.
        If you return true, the application will not process the event itself.
        If you return false, the normal application processing will occur as if the IME had not seen the event at all.

        The default implementation intercepts KeyEvent.KEYCODE_BACK if the IME is currently shown,
        to possibly hide it when the key goes up (if not canceled or long pressed).
        In addition, in fullscreen mode only, it will consume DPAD movement events to move the cursor
        in the extracted text view, not allowing them to perform navigation in the underlying application.
         */
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		log("onKeyUp: code=", keyCode, " event=", event);
        /*
        Override this to intercept key up events before they are processed by the application.
        If you return true, the application will not itself process the event.
        If you return false, the normal application processing will occur as if the IME had not seen the event at all.

        The default implementation intercepts KeyEvent.KEYCODE_BACK to hide the current IME UI if it is shown.
        In addition, in fullscreen mode only, it will consume DPAD movement events to move the cursor
        in the extracted text view, not allowing them to perform navigation in the underlying application.
         */
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		log("onKeyLongPress: code=", keyCode, " event=", event);
        /*
        Default implementation of KeyEvent.Callback.onKeyLongPress(): always returns false (doesn't handle the event).
         */
		return super.onKeyLongPress(keyCode, event);
	}

	@Override
	public boolean onKeyMultiple(int keyCode, int count, KeyEvent event) {
		log("onKeyMultiple: code=", keyCode, " count=", count, " event=", event);
        /*
        Override this to intercept special key multiple events before they are processed by the application.
        If you return true, the application will not itself process the event.
        If you return false, the normal application processing will occur as if the IME had not seen the event at all.

        The default implementation always returns false, except when in fullscreen mode,
        where it will consume DPAD movement events to move the cursor in the extracted text view,
        not allowing them to perform navigation in the underlying application.
         */
		return super.onKeyMultiple(keyCode, count, event);
	}

	// semantic switch

	private boolean isKeyVowel(int code) {
		switch (code) {
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case '\u0358':
		case 'u':
		case 'r':
			return true;
		}
		return false;
	}

	private boolean isKeyTone(int code) {
		switch (code) {
		case KEYCODE_TONE_HIGH:
		case KEYCODE_TONE_LOW:
		case KEYCODE_TONE_RISE:
		case KEYCODE_TONE_MID:
			return true;
		}
		return false;
	}

	private boolean isKeyConsonant(int code) {
		if (code >= 'b' && code <= 'z')
			return !isKeyVowel(code) && !isKeyTone(code);
		return false;
	}

	private boolean isKeyGlottal(int code) {
		switch (code) {
		case 'h':
		case 'k':
		case 'p':
		case 't':
			return true;
		}
		return false;
	}

	private boolean isTextVowel(String text) {
		if (text.equals("o͘"))
			return true;
		return false;
	}

	// cursor

	@Override
	public void onUpdateCursor(Rect newCursor) {
		log("cursor moved to: ", newCursor);
		super.onUpdateCursor(newCursor);
	}

	@Override
	public void onUpdateCursorAnchorInfo(CursorAnchorInfo cursorAnchorInfo) {
		log("cursor moved to: ", cursorAnchorInfo);
		super.onUpdateCursorAnchorInfo(cursorAnchorInfo);
	}

	@Override
	public void onUpdateSelection(int oldSelStart, int oldSelEnd, int newSelStart, int newSelEnd, int candidatesStart, int candidatesEnd) {
		log("selection changed from ", oldSelStart, ',', oldSelEnd, " to ", newSelStart, ',', newSelEnd,
		    " (candidates: ", candidatesStart, ',', candidatesEnd, ')');
		super.onUpdateSelection(oldSelStart, oldSelEnd, newSelStart, newSelEnd, candidatesStart, candidatesEnd);
		// FIXME: why null?
		if (viewState == null)
			return;
		InputConnection ic = getCurrentInputConnection();
		ic.beginBatchEdit();
		boolean viewUpdated = updateStateOnSelection(ic, newSelStart, newSelEnd, candidatesStart, candidatesEnd);
		if (viewUpdated)
			updateView(ic);
		ic.endBatchEdit();
	}

	// returns: need to updateView()
	protected boolean updateStateOnSelection(InputConnection ic, int newSelStart, int newSelEnd, int candidatesStart, int candidatesEnd) {
		int expStart = viewState.getInteger(SelectionStart);
		int expEnd = viewState.getInteger(SelectionEnd);
		boolean moved;
		if (newSelStart <= newSelEnd) {
			viewState = viewState.setInteger(SelectionStart, newSelStart)
			                     .setInteger(SelectionEnd, newSelEnd);
			moved = (newSelStart != expStart || newSelEnd != expEnd);
		} else {
			viewState = viewState.setInteger(SelectionStart, newSelEnd)
			                     .setInteger(SelectionEnd, newSelStart);
			moved = (newSelEnd != expStart || newSelStart != expEnd);
		}

		if (candidatesStart == candidatesEnd) {
			viewState = viewState.setInteger(ComposingStart, -1)
			                     .setInteger(ComposingEnd, -1);
		} else if (candidatesStart < candidatesEnd) {
			viewState = viewState.setInteger(ComposingStart, candidatesStart)
			                     .setInteger(ComposingEnd, candidatesEnd);
		} else {
			viewState = viewState.setInteger(ComposingStart, candidatesEnd)
			                     .setInteger(ComposingEnd, candidatesStart);
		}
		viewState = viewState.resetChanged();
		// this is necessary to be compatible with table editing in Google Document app
		if (candidatesStart != candidatesEnd && newSelEnd != newSelStart) {
			if ((candidatesStart < newSelStart && candidatesEnd < newSelStart)
			    || (candidatesStart > newSelEnd && candidatesEnd > newSelEnd)) {
				log("WORKAROUND: reset due to cursor moved with invalid candidate state");
				resetComposing();
				return false;
			}
		}

		if (inputState.getBoolean(TextMode)) {
			boolean updateState = true;
			if (zeroCursorWorkAround) {
				log("WORKAROUND: force to check text inputState");
				zeroCursorWorkAround = false;
			} else if (!moved) {
				updateState = false;
			}
			if (updateState) {
				initTextState(ic, newSelStart, newSelEnd);
				return true;
			}
		}
		return false;
	}
}
