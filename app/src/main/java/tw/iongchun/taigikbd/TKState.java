/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import org.pcollections.HashTreePSet;
import org.pcollections.IntTreePMap;
import org.pcollections.MapPSet;

/**
 * Created by iongchun on 6/22/17.
 */

public class TKState {
	// Parugo requires Java8
	//private ImMap<String,Object> vmap = map();
	private final IntTreePMap<Object> vmap;
	private final MapPSet<Integer> cset;
	private final TKState from;

	public TKState() {
		vmap = IntTreePMap.empty();
		cset = HashTreePSet.empty();
		from = null;
	}

	private TKState(IntTreePMap<Object> vmap, MapPSet<Integer> cset, TKState from) {
		this.vmap = vmap;
		this.cset = cset;
		this.from = from;
	}

	public TKState getChangedFrom() {
		return from;
	}

	public TKState set(Key key, Object value) {
		if (vmap.containsKey(key.value)) {
			Object orgValue = vmap.get(key.value);
			if (orgValue == null) {
				if (value == null)
					return this;
			} else if (orgValue.equals(value)) {
				return this;
			}
		}

		boolean changed;
		if (from == null) {
			changed = true;
		} else if (!from.vmap.containsKey(key.value)) {
			changed = true;
		} else {
			Object fromValue = from.vmap.get(key.value);
			if (fromValue == null)
				changed = (value != null);
			else
				changed = !fromValue.equals(value);
		}
		return new TKState(vmap.plus(key.value, value),
		                   changed ? cset.plus(key.value) : cset.minus(key.value),
		                   from);
	}

	public Object get(Key key) {
		return vmap.get(key.value);
	}

	public TKState remove(Key key) {
		if (!vmap.containsKey(key.value))
			return this;
		boolean changed = (from == null || from.vmap.containsKey(key.value));
		return new TKState(vmap.minus(key.value),
		                   changed ? cset.plus(key.value) : cset.minus(key.value),
		                   from);
	}

	public boolean contains(Key key) {
		return vmap.containsKey(key.value);
	}

	public boolean isChanged(Key key) {
		return cset.contains(key.value);
	}

	public TKState resetChanged() {
		return new TKState(vmap, HashTreePSet.empty(), this);
	}

	public TKState resetChanged(Key key) {
		if (!cset.contains(key.value))
			return this;
		return new TKState(vmap, cset.minus(key.value),
		                   from == null ? null : from.set(key, vmap.get(key.value)));
	}

	public static class Key {
		private final Integer value;

		private Key(Integer value) {
			this.value = value;
		}

		public static Key valueOf(int value) {
			return new Key(value);
		}

		@Override
		public int hashCode() {
			return value.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof Key))
				return false;
			return value.equals(((Key)obj).value);
		}
	}
}