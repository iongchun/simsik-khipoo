/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import android.util.DisplayMetrics;

import java.util.Locale;

import androidx.preference.PreferenceManager;

import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_UI_LOCALE;

/**
 * Created by iongchun on 6/21/17.
 */

public class TKLocaleHelper {
	private static LocaleList localeList = null;

	public static void setLocale(Context context) {
		// use context override for N
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
			return;

		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		String uiLocale = sharedPref.getString(PREF_UI_LOCALE, null);
		if (uiLocale == null || !uiLocale.startsWith("m:"))
			return;
		uiLocale = uiLocale.substring(2);
		Locale prefLocale;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			prefLocale = new Locale.Builder().setLanguageTag(uiLocale).build();
		} else {
			String[] toks = uiLocale.split("-");
			String lang = toks[0];
			String country;
			if (toks.length <= 2)
				country = toks[1];
			else
				country = toks[2];
			prefLocale = new Locale(lang, country);
		}
		Resources res = context.getResources();
		Configuration conf = res.getConfiguration();
		conf.locale = prefLocale;
		DisplayMetrics dm = res.getDisplayMetrics();
		res.updateConfiguration(conf, dm);
	}

	public static Context overrideContext(Context base) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N)
			return base;

		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(base);
		String uiLocale = sharedPref.getString(PREF_UI_LOCALE, null);
		// for test only
		if (uiLocale == null)
			return base;

		Configuration conf = base.getResources().getConfiguration();
		if (localeList == null)
			localeList = conf.getLocales();

		if (!uiLocale.startsWith("m:")) {
			conf.setLocales(localeList);
			return base.createConfigurationContext(conf);
		}
		uiLocale = uiLocale.substring(2);

		Locale prefLocale = Locale.forLanguageTag(uiLocale);
		boolean hasLocale = false;
		for (int i = 0; i < localeList.size(); i++) {
			Locale locale = localeList.get(i);
			if (locale.equals(prefLocale)) {
				hasLocale = true;
				break;
			}
		}
		Locale[] locales = new Locale[localeList.size() + (hasLocale ? 0 : 1)];
		locales[0] = prefLocale;
		for (int i = 1, j = 0; i < locales.length; j++) {
			Locale locale = localeList.get(j);
			if (locale.equals(prefLocale))
				continue;
			locales[i++] = locale;
		}
		conf.setLocales(new LocaleList(locales));
		return base.createConfigurationContext(conf);
	}
}