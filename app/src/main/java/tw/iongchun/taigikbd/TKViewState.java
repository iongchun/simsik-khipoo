/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

/**
 * Created by iongchun on 6/22/17.
 */

public class TKViewState {
	private static int keyIndex = 0;
	public static final TKState.Key InputTitle = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key InputTypeDisplay = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingStateDisplay = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key MainKeyboard = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key TextKeyboard = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key CurrentKeyboard = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ShiftState = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ShowCompletions = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ActionID = TKState.Key.valueOf(keyIndex++);
	// tone keys
	//  rise tone key: for 5th, 6th
	public static final TKState.Key ToneNumRise = TKState.Key.valueOf(keyIndex++);
	//  high tone key: for 2th, 8th
	public static final TKState.Key ToneNumHigh = TKState.Key.valueOf(keyIndex++);
	//  middle tone key: for 7th, 9th
	public static final TKState.Key ToneNumMid = TKState.Key.valueOf(keyIndex++);
	//  low tone key: for 3th, 4th
	public static final TKState.Key ToneNumLow = TKState.Key.valueOf(keyIndex++);
	// action key
	public static final TKState.Key ActionKeyType = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ActionKeyLabel = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ActionKeyIconID = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ActionKeyIconType = TKState.Key.valueOf(keyIndex++);
	// multi-line: enter and space key
	public static final TKState.Key MultiLine = TKState.Key.valueOf(keyIndex++);
	// alternate character of period (.) for ASCII
	public static final TKState.Key AsciiPeriodAlt = TKState.Key.valueOf(keyIndex++);
	// show primes for numsym1
	public static final TKState.Key NumSym1Primes = TKState.Key.valueOf(keyIndex++);
	// commit text and composing text
	public static final TKState.Key CommitText = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingText = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingStart = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingEnd = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingCursor = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key HasAutoCorrection = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingCorrected = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingCorrectedCached = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key ComposingCorrection = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key AutoCorrects = TKState.Key.valueOf(keyIndex++);
	// cursor/selection
	public static final TKState.Key SelectionStart = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key SelectionEnd = TKState.Key.valueOf(keyIndex++);
	// delete text
	public static final TKState.Key DeleteBefore = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key DeleteAfter = TKState.Key.valueOf(keyIndex++);
	// candidates
	public static final TKState.Key Candidates = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key CandidatePrefix = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key HasSuggestions = TKState.Key.valueOf(keyIndex++);
	// cache for text before
	public static final TKState.Key TextBeforeCursor = TKState.Key.valueOf(keyIndex++);
	public static final TKState.Key TextBeforeCached = TKState.Key.valueOf(keyIndex++);
	private final TKState state;
	private final TKViewState from;

	public TKViewState() {
		state = new TKState();
		from = null;
	}

	private TKViewState(TKState state, TKViewState from) {
		this.state = state;
		this.from = from;
	}

	public TKViewState getChangedFrom() {
		return from;
	}

	public TKViewState set(TKState.Key key, Object value) {
		TKState newState = state.set(key, value);
		if (newState == state)
			return this;
		return new TKViewState(newState, from);
	}

	public Object get(TKState.Key key) {
		return state.get(key);
	}

	public TKViewState remove(TKState.Key key) {
		TKState newState = state.remove(key);
		if (newState == state)
			return this;
		return new TKViewState(newState, from);
	}

	public boolean contains(TKState.Key key) {
		return state.contains(key);
	}

	public boolean containsAll(TKState.Key... keys) {
		for (TKState.Key key : keys) {
			if (!state.contains(key))
				return false;
		}
		return true;
	}

	public boolean isChanged(TKState.Key key) {
		return state.isChanged(key);
	}

	public boolean isAnyChanged(TKState.Key... keys) {
		for (TKState.Key key : keys) {
			if (state.isChanged(key))
				return true;
		}
		return false;
	}

	public TKViewState resetChanged() {
		TKState newState = state.resetChanged();
		if (newState == state)
			return this;
		return new TKViewState(newState, this);
	}

	public TKViewState resetChanged(TKState.Key key) {
		TKState newState = state.resetChanged(key);
		if (newState == state)
			return this;

		TKState newFromState = newState.getChangedFrom();
		TKViewState newFrom;
		if (newFromState == null)
			newFrom = null;
		else
			newFrom = new TKViewState(newFromState, null);
		return new TKViewState(newState, newFrom);
	}

	public TKViewState setString(TKState.Key key, String value) {
		return set(key, value);
	}

	public String getString(TKState.Key key) {
		return (String)get(key);
	}

	public String getString(TKState.Key key, String defaultValue) {
		if (state.contains(key))
			return (String)get(key);
		else
			return defaultValue;
	}

	public TKViewState setInteger(TKState.Key key, Integer value) {
		return set(key, value);
	}

	public Integer getInteger(TKState.Key key) {
		return (Integer)get(key);
	}

	public TKViewState incInteger(TKState.Key key, Integer value) {
		if (value == 0)
			return this;
		if (state.contains(key))
			value = getInteger(key) + value;
		return set(key, value);
	}

	public Integer getInteger(TKState.Key key, Integer defaultValue) {
		if (state.contains(key))
			return (Integer)state.get(key);
		else
			return defaultValue;
	}

	public TKViewState setBoolean(TKState.Key key, Boolean value) {
		return set(key, value);
	}

	public Boolean getBoolean(TKState.Key key) {
		return (Boolean)get(key);
	}

	public Boolean getBoolean(TKState.Key key, Boolean defaultValue) {
		if (state.contains(key))
			return (Boolean)state.get(key);
		else
			return defaultValue;
	}
}
