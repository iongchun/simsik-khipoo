/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.inputmethod.InputMethodInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class TKLogActivity extends AppCompatActivity implements ServiceConnection {

	private final static AtomicReference<TKLogService> logSvc = new AtomicReference<>(null);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		FloatingActionButton fab = findViewById(R.id.fab);
		fab.setOnClickListener(view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                                       .setAction("Action", null).show());

		Button poj = findViewById(R.id.poj);
		poj.setOnClickListener(v -> {
			InputMethodManager imMgr = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			List<InputMethodInfo> imis = imMgr.getInputMethodList();
			InputMethodInfo info = null;
			for (InputMethodInfo imi : imis) {
				if (imi.getPackageName().equals(getPackageName())) {
					info = imi;
					break;
				}
			}
			if (info == null)
				return;
			final Intent intent =
				new Intent(Settings.ACTION_INPUT_METHOD_SUBTYPE_SETTINGS);
			intent.putExtra(Settings.EXTRA_INPUT_METHOD_ID, info.getId());
			intent.putExtra(Intent.EXTRA_TITLE, "Select Enabled Subtypes");
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
			                | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
			                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		});

		Intent intent = new Intent(this, TKLogService.class);
		bindService(intent, this, Context.BIND_AUTO_CREATE);
	}

	@Override
	protected void onDestroy() {
		if (logSvc.get() != null)
			unbindService(this);
		super.onDestroy();
	}

	// ServiceConnection: begin

	@Override
	public void onServiceConnected(ComponentName name, IBinder binder) {
		logSvc.set(((TKLogService.ServiceBinder)binder).getService());
	}

	@Override
	public void onServiceDisconnected(ComponentName name) {
		logSvc.set(null);
	}

	// ServiceConnection: end

	@Override
	protected void onResume() {
		super.onResume();

		TKLogService svc = logSvc.get();
		if (svc == null)
			return;
		String[] logs = svc.getLogs();

		// Capture the layout's TextView and set the string as its text
		TextView logView = findViewById(R.id.logs);
		for (String log : logs)
			logView.append(log + '\n');
	}
}