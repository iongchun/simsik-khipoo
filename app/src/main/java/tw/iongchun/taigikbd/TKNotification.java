package tw.iongchun.taigikbd;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;

/**
 * Created by iongchun on 10/31/17.
 */

public class TKNotification {
	public static String CHANNEL_INFO = "info";
	public static int NOTIF_DICT_DB = 0;

	public static void init(Context context) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationManager notifManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
			NotificationChannel info = new NotificationChannel(CHANNEL_INFO,
			                                                   context.getString(R.string.notif_channel_info_name),
			                                                   NotificationManager.IMPORTANCE_DEFAULT);
			info.setDescription(context.getString(R.string.notif_channel_info_desc));
			notifManager.createNotificationChannel(info);
		}
	}
}
