/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static tw.iongchun.taigikbd.TKComposingUtils.AFTERS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.GLOTTAL_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.LEADS_NULL;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_POJ;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_TL;
import static tw.iongchun.taigikbd.TKInputState.Afters;
import static tw.iongchun.taigikbd.TKInputState.ComposingMode;
import static tw.iongchun.taigikbd.TKInputState.ComposingStarted;
import static tw.iongchun.taigikbd.TKInputState.ComposingState;
import static tw.iongchun.taigikbd.TKInputState.Glottal;
import static tw.iongchun.taigikbd.TKInputState.InputStarted;
import static tw.iongchun.taigikbd.TKInputState.Leads;
import static tw.iongchun.taigikbd.TKInputState.PhoneticInput;
import static tw.iongchun.taigikbd.TKInputState.PojNN;
import static tw.iongchun.taigikbd.TKInputState.TextMode;
import static tw.iongchun.taigikbd.TKInputState.Tone;
import static tw.iongchun.taigikbd.TKInputState.Vowels;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_UI_LOCALE;
import static tw.iongchun.taigikbd.TKViewState.CommitText;
import static tw.iongchun.taigikbd.TKViewState.ComposingEnd;
import static tw.iongchun.taigikbd.TKViewState.ComposingStart;
import static tw.iongchun.taigikbd.TKViewState.ComposingText;
import static tw.iongchun.taigikbd.TKViewState.DeleteAfter;
import static tw.iongchun.taigikbd.TKViewState.DeleteBefore;
import static tw.iongchun.taigikbd.TKViewState.SelectionEnd;
import static tw.iongchun.taigikbd.TKViewState.SelectionStart;

/**
 * Created by iongchun on 7/7/17.
 */

@RunWith(AndroidJUnit4.class)
public class PhoneticInputTest {
	private static final String INPUT_TITLE_TL = "SimSik:TL";
	private static final String INPUT_TITLE_POJ = "SimSik:POJ";
	private TKInputMethodService ims;
	@Mock
	private Context context;
	@Mock
	private SharedPreferences preferences;
	@Mock
	private Resources resources;
	@Mock
	private InputConnection inputConnection;
	@Mock
	private EditorInfo editorInfo;

	@Before
	public void init() {
		initMockito();
		initIMS();
	}

	@After
	public void fine() {
		fineIMS();
	}

	private void initMockito() {
		MockitoAnnotations.initMocks(this);
	}

	private void initIMS() {
		ims = new TKInputMethodService();
		when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(preferences);
		when(preferences.getString(eq(PREF_UI_LOCALE), anyString())).thenReturn(null);
		when(context.getResources()).thenReturn(resources);
		when(resources.getString(eq(R.string.input_title_tl))).thenReturn(INPUT_TITLE_TL);
		when(resources.getString(eq(R.string.input_title_poj))).thenReturn(INPUT_TITLE_POJ);
		ims.attachBaseContext(context);

		ims.initInputState();
		ims.initViewState();

		ims.startInputState();
		ims.setInputState(ims.getInputState().setBoolean(PhoneticInput, true));

		editorInfo.initialSelStart = 0;
		editorInfo.initialSelEnd = 0;
		editorInfo.inputType = InputType.TYPE_CLASS_TEXT
		                       | InputType.TYPE_TEXT_VARIATION_NORMAL
		                       | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
		                       | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT;
		editorInfo.initialCapsMode = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
		ims.startViewState(editorInfo);
		ims.switchSubtype(SUBTYPE_TL);
		ims.updateMainKeyboard();
		ims.switchInputType(inputConnection, editorInfo);
	}

	private void fineIMS() {
		ims.finishInputState();
		ims.fineViewState();
		ims.fineInputState();
	}

	private void checkComposingState(
		String leads, String vowels, int tone,
		String afters, char glotal, boolean pojNN,
		int start, int end,
		int deleteBefore, int deleteAfter,
		String commitText,
		String composingText) {
		TKInputState inputState = ims.getInputState();
		TKViewState viewState = ims.getViewState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));
		TKInputState compoState = inputState.getState(ComposingState);
		if (leads != null) {
			assertEquals(leads, compoState.getString(Leads));
			if (leads == LEADS_NULL)
				assertFalse(inputState.getBoolean(ComposingStarted));
			else
				assertTrue(inputState.getBoolean(ComposingStarted));
		}
		if (inputState.getBoolean(ComposingStarted)) {
			assertEquals(vowels, compoState.getString(Vowels));
			assertEquals(tone, (int)compoState.getInteger(Tone));
			assertEquals(afters, compoState.getString(Afters));
			assertEquals(glotal, (char)compoState.getCharacter(Glottal));
			assertEquals(pojNN, (boolean)compoState.getBoolean(PojNN));

			assertEquals(start, (int)viewState.getInteger(ComposingStart));
			assertEquals(end, (int)viewState.getInteger(ComposingEnd));
			assertEquals(composingText, viewState.getString(ComposingText));
		}
		assertFalse(viewState.isAnyChanged(SelectionStart, SelectionEnd));
		if (deleteBefore > 0) {
			assertTrue(viewState.contains(DeleteBefore));
			assertEquals(deleteBefore, (int)viewState.getInteger(DeleteBefore));
		} else {
			assertFalse(viewState.contains(DeleteBefore));
		}
		if (deleteAfter > 0) {
			assertTrue(viewState.contains(DeleteAfter));
			assertEquals(deleteAfter, (int)viewState.getInteger(DeleteAfter));
		} else {
			assertFalse(viewState.contains(DeleteAfter));
		}
		if (commitText != null) {
			assertTrue(viewState.contains(CommitText));
			assertEquals(commitText, viewState.getString(CommitText));
		} else {
			assertFalse(viewState.contains(CommitText));
		}
	}

	@Test
	public void oongToOng() {
		// Ông
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ôong");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Oo", 5, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Ông");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Liông
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Liôong");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "ioo", 5, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Liông");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Só͘ng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Só͘ng");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "o͘", 2, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Sóng");
	}

	@Test
	public void oonnToOnn() {
		// ionn
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ioonn");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "ioo", 1, "nn", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "ionn");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// honnh
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ho͘hⁿ");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("H", "o͘", 4, "", 'h', true, 0, 4, 0, 0, null, "Hohⁿ");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// kônn
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kôonn");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", "oo", 5, "nn", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "kônn");
	}

	@Test
	public void oomToOm() {
		// Ōm
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ōom");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Oo", 7, "m", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ōm");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Som
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("So͘m");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "o͘", 1, "m", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Som");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// tôm
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tôom");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "oo", 5, "m", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "tôm");
	}

	@Test
	public void ookToOk() {
		// Ok
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("O͘k");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "O͘", 4, "", 'k', false, 0, 2, 0, 0, null, "Ok");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);

		// Sok
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Sook");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "oo", 4, "", 'k', false, 0, 3, 0, 0, null, "Sok");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Bo̍k
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Bo̍ok");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "oo", 8, "", 'k', false, 0, 4, 0, 0, null, "Bo̍k");
	}

	@Test
	public void oopToOp() {
		// Hop
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Hoop");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("H", "oo", 4, "", 'p', false, 0, 3, 0, 0, null, "Hop");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Ko̍p
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ko̍op");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("K", "oo", 8, "", 'p', false, 0, 4, 0, 0, null, "Ko̍p");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Chhop
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chho͘p");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "o͘", 4, "", 'p', false, 0, 5, 0, 0, null, "Chhop");
	}

	@Test
	public void eekToEk() {
		// Ek
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Eek");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Ee", 4, "", 'k', false, 0, 2, 0, 0, null, "Ek");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Lek
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Leek");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "ee", 4, "", 'k', false, 0, 3, 0, 0, null, "Lek");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Tshe̍k
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tshe̍ek");
		ims.updateStateOnSelection(inputConnection, 7, 7, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Tsh", "ee", 8, "", 'k', false, 0, 6, 0, 0, null, "Tshe̍k");
	}

	@Test
	public void eengToEng() {
		// Peng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Peeng");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("P", "ee", 1, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Peng");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Léng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Léeng");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "ee", 2, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Léng");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Bêng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Bêeng");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "ee", 5, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Bêng");
	}

	@Test
	public void pojUaToOa() {
		ims.switchSubtype(SUBTYPE_POJ);

		// Joa̍h
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Jua̍h");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		checkComposingState("J", "ua", 8, "", 'h', false, 0, 5, 0, 0, null, "Joa̍h");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Hoâiⁿ
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Huâiⁿ");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("H", "uai", 5, "", GLOTTAL_NULL, true, 0, 5, 0, 0, null, "Hoâiⁿ");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Bôa
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Buâ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "ua", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Bôa");
	}

	@Test
	public void pojUeToOe() {
		ims.switchSubtype(SUBTYPE_POJ);

		// Boe̍h
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Bue̍h");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "ue", 8, "", 'h', false, 0, 5, 0, 0, null, "Boe̍h");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);

		// Jôe
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Juê");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("J", "ue", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Jôe");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// ōe
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("uē");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "ue", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ōe");
	}

	@Test
	public void pojIkToEk() {
		ims.switchSubtype(SUBTYPE_POJ);

		// Be̍k
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Bi̍k");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "i", 8, "", 'k', false, 0, 4, 0, 0, null, "Be̍k");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// hek
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hik");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "i", 4, "", 'k', false, 0, 3, 0, 0, null, "hek");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Chhe̍k
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chhi̍k");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "i", 8, "", 'k', false, 0, 6, 0, 0, null, "Chhe̍k");
	}

	@Test
	public void pojIngToEng() {
		ims.switchSubtype(SUBTYPE_POJ);

		// Gêng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Gîng");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("G", "i", 5, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Gêng");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Éng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Íng");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "I", 2, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Éng");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Khèng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Khìng");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Kh", "i", 3, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Khèng");
	}

	@Test
	public void notIannPlease() {
		// Siânn
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Siâ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "ia", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Siâ");

		ims.updateStateOnText(inputConnection, "nn");
		checkComposingState("S", "ia", 5, "nn", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Siânn");
		ims.updateView(inputConnection);
		checkComposingState("S", "ia", 5, "nn", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Siânn");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Siânn");
		ims.updateStateOnSelection(inputConnection, 5, 5, 0, 5);
		ims.updateView(inputConnection);
		checkComposingState("S", "ia", 5, "nn", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Siânn");
	}
}
