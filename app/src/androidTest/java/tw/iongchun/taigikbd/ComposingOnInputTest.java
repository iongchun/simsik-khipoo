/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static tw.iongchun.taigikbd.TKComposingUtils.AFTERS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.GLOTTAL_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.LEADS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.VOWELS_NULL;
import static tw.iongchun.taigikbd.TKInputMethodService.KEYCODE_TONE_HIGH;
import static tw.iongchun.taigikbd.TKInputMethodService.KEYCODE_TONE_LOW;
import static tw.iongchun.taigikbd.TKInputMethodService.KEYCODE_TONE_RISE;
import static tw.iongchun.taigikbd.TKInputMethodService.KEYCODE_TONE_MID;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_POJ;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_TL;
import static tw.iongchun.taigikbd.TKInputState.Afters;
import static tw.iongchun.taigikbd.TKInputState.AspirationToggle;
import static tw.iongchun.taigikbd.TKInputState.AutoDash;
import static tw.iongchun.taigikbd.TKInputState.Boot;
import static tw.iongchun.taigikbd.TKInputState.CapState;
import static tw.iongchun.taigikbd.TKInputState.ComposingMode;
import static tw.iongchun.taigikbd.TKInputState.ComposingStarted;
import static tw.iongchun.taigikbd.TKInputState.Glottal;
import static tw.iongchun.taigikbd.TKInputState.GlottalReplaceGlottal;
import static tw.iongchun.taigikbd.TKInputState.Hat;
import static tw.iongchun.taigikbd.TKInputState.InputStarted;
import static tw.iongchun.taigikbd.TKInputState.LeadReplaceLead;
import static tw.iongchun.taigikbd.TKInputState.Leads;
import static tw.iongchun.taigikbd.TKInputState.PojNN;
import static tw.iongchun.taigikbd.TKInputState.Subtype;
import static tw.iongchun.taigikbd.TKInputState.TextMode;
import static tw.iongchun.taigikbd.TKInputState.Tone;
import static tw.iongchun.taigikbd.TKInputState.VoicelessToggle;
import static tw.iongchun.taigikbd.TKInputState.Vowels;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_UI_LOCALE;
import static tw.iongchun.taigikbd.TKViewState.CommitText;
import static tw.iongchun.taigikbd.TKViewState.ComposingCursor;
import static tw.iongchun.taigikbd.TKViewState.ComposingEnd;
import static tw.iongchun.taigikbd.TKViewState.ComposingStart;
import static tw.iongchun.taigikbd.TKViewState.ComposingText;
import static tw.iongchun.taigikbd.TKViewState.DeleteAfter;
import static tw.iongchun.taigikbd.TKViewState.DeleteBefore;
import static tw.iongchun.taigikbd.TKViewState.SelectionEnd;
import static tw.iongchun.taigikbd.TKViewState.SelectionStart;

/**
 * Created by iongchun on 6/28/17.
 */

@RunWith(AndroidJUnit4.class)
public class ComposingOnInputTest {
	private static final String INPUT_TITLE_TL = "SimSik:TL";
	private static final String INPUT_TITLE_POJ = "SimSik:POJ";
	private TKInputMethodService ims;
	@Mock
	private Context context;
	@Mock
	private SharedPreferences preferences;
	@Mock
	private Resources resources;
	@Mock
	private InputConnection inputConnection;
	@Mock
	private EditorInfo editorInfo;

	@Before
	public void init() {
		initMockito();
		initIMS();
	}

	@After
	public void fine() {
		fineIMS();
	}

	private void initMockito() {
		MockitoAnnotations.initMocks(this);
	}

	private void initIMS() {
		ims = new TKInputMethodService();
		when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(preferences);
		when(preferences.getString(eq(PREF_UI_LOCALE), anyString())).thenReturn(null);
		when(context.getResources()).thenReturn(resources);
		when(resources.getString(eq(R.string.input_title_tl))).thenReturn(INPUT_TITLE_TL);
		when(resources.getString(eq(R.string.input_title_poj))).thenReturn(INPUT_TITLE_POJ);
		ims.attachBaseContext(context);

		ims.initInputState();
		ims.initViewState();

		ims.startInputState();

		editorInfo.initialSelStart = 0;
		editorInfo.initialSelEnd = 0;
		editorInfo.inputType = InputType.TYPE_CLASS_TEXT
		                       | InputType.TYPE_TEXT_VARIATION_NORMAL
		                       | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
		editorInfo.initialCapsMode = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
		ims.startViewState(editorInfo);
		ims.switchSubtype(SUBTYPE_TL);
		ims.updateMainKeyboard();
		ims.switchInputType(inputConnection, editorInfo);
		ims.setInputState(ims.getInputState()
		                     .setBoolean(AutoDash, false)
		                     .setBoolean(AspirationToggle, false)
		                     .setBoolean(VoicelessToggle, false)
		                     .setBoolean(LeadReplaceLead, false)
		                     .setBoolean(GlottalReplaceGlottal, false)
		);
	}

	private void fineIMS() {
		ims.finishInputState();
		ims.fineViewState();
		ims.fineInputState();
	}

	private void checkComposingState(
		String hat,
		String leads, String vowels, int tone,
		String afters, char glotal, boolean pojNN,
		String boot,
		int start, int end,
		int deleteBefore, int deleteAfter,
		String commitText,
		String composingText) {
		TKInputState inputState = ims.getInputState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));

		TKInputState state = ims.getComposingState();
		if (leads != null) {
			if (leads == LEADS_NULL)
				assertFalse(inputState.getBoolean(ComposingStarted));
			else
				assertTrue(inputState.getBoolean(ComposingStarted));
		}
		checkState(state, hat, leads, vowels, tone, afters, glotal, pojNN, boot);

		TKViewState viewState = ims.getViewState();
		assertEquals(start, (int)viewState.getInteger(ComposingStart));
		assertEquals(end, (int)viewState.getInteger(ComposingEnd));
		assertEquals(composingText, viewState.getString(ComposingText));

		assertFalse(viewState.isAnyChanged(SelectionStart, SelectionEnd));
		if (deleteBefore > 0) {
			assertTrue(viewState.contains(DeleteBefore));
			assertEquals(deleteBefore, (int)viewState.getInteger(DeleteBefore));
		} else {
			assertFalse(viewState.contains(DeleteBefore));
		}
		if (deleteAfter > 0) {
			assertTrue(viewState.contains(DeleteAfter));
			assertEquals(deleteAfter, (int)viewState.getInteger(DeleteAfter));
		} else {
			assertFalse(viewState.contains(DeleteAfter));
		}
		if (commitText != null) {
			assertTrue(viewState.contains(CommitText));
			assertEquals(commitText, viewState.getString(CommitText));
		} else {
			assertFalse(viewState.contains(CommitText));
		}
	}

	private void checkComposingState(
		String leads, String vowels, int tone,
		String afters, char glotal, boolean pojNN,
		int start, int end,
		int deleteBefore, int deleteAfter,
		String commitText,
		String composingText) {
		checkComposingState(null, leads, vowels, tone, afters, glotal, pojNN, null,
		                    start, end, deleteBefore, deleteAfter, commitText, composingText);
	}

	private void checkComposingRegion(int cursor,
	                                  String leads, String vowels, int tone,
	                                  String afters, char glotal, boolean pojNN) {
		checkState(ims.getComposingRegion(), leads, vowels, tone, afters, glotal, pojNN);
		TKViewState viewState = ims.getViewState();
		if (cursor >= 0) {
			assertEquals(cursor, (int)viewState.getInteger(ComposingCursor));
			int start = viewState.getInteger(ComposingStart);
			assertEquals(start + cursor, (int)viewState.getInteger(SelectionStart));
		} else {
			assertFalse(viewState.contains(ComposingCursor));
		}
	}

	private void checkState(TKInputState state,
	                        String hat,
	                        String leads, String vowels, int tone,
	                        String afters, char glotal, boolean pojNN,
	                        String boot) {
		if (hat != null)
			assertEquals(hat, state.getString(Hat));
		if (leads != null)
			assertEquals(leads, state.getString(Leads));
		assertEquals(vowels, state.getString(Vowels));
		assertEquals(tone, (int)state.getInteger(Tone));
		assertEquals(afters, state.getString(Afters));
		assertEquals(glotal, (char)state.getCharacter(Glottal));
		assertEquals(pojNN, (boolean)state.getBoolean(PojNN));
		if (boot != null)
			assertEquals(boot, state.getString(Boot));
	}

	private void checkState(TKInputState state,
	                        String leads, String vowels, int tone,
	                        String afters, char glotal, boolean pojNN) {
		checkState(state, null, leads, vowels, tone, afters, glotal, pojNN, null);
	}

	@Test
	public void validLeadsToInvalidLeads() {
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");

		ims.updateStateOnKey(inputConnection, 'l');
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "L");
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "L");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("L");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "L");

		ims.updateStateOnKey(inputConnection, 'ⁿ');
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, "Lⁿ", "");
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Lⁿ");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
	}

	@Test
	public void nasalWithoutLeadsFromAftersToLeadsWithInputVowel() {
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("N̄g");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 7, "Ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "N̄g");

		ims.updateStateOnText(inputConnection, "oo");
		checkComposingState("Ng", "oo", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Ngōo");
		ims.updateView(inputConnection);
		checkComposingState("Ng", "oo", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Ngōo");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ngōo");
		ims.updateStateOnSelection(inputConnection, 4, 4, 0, 4);
		ims.updateView(inputConnection);
		checkComposingState("Ng", "oo", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Ngōo");
	}

	@Test
	public void nasalWithLeadsFromAftersToLeadsWithInputConsonant() {
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Pn̂");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("P", "", 5, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Pn̂");

		ims.updateStateOnKey(inputConnection, 'n');
		checkComposingState("n", "", 5, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, "P", "nn̂");
		ims.updateView(inputConnection);
		checkComposingState("n", "", 5, "n", GLOTTAL_NULL, false, 1, 4, 0, 0, null, "nn̂");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Pnn̂");
		ims.updateStateOnSelection(inputConnection, 4, 4, 1, 4);
		ims.updateView(inputConnection);
		checkComposingState("n", "", 5, "n", GLOTTAL_NULL, false, 1, 4, 0, 0, null, "nn̂");
	}

	@Test
	public void aspirationToggle() {
		ims.setInputState(ims.getInputState().setBoolean(AspirationToggle, true));
		ims.setInputState(ims.getInputState().setBoolean(LeadReplaceLead, true));
		ims.setInputState(ims.getInputState().setBoolean(VoicelessToggle, true));

		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		ims.updateStateOnKey(inputConnection, 't');
		checkComposingState("Th", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Th");
		ims.updateView(inputConnection);
		checkComposingState("Th", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Th");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Th");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Th", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Th");

		ims.updateStateOnKey(inputConnection, 't');
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "T");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		ims.setInputState(ims.getInputState().setBoolean(CapState, true));
		ims.updateStateOnKey(inputConnection, 't');
		checkComposingState("TH", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "TH");
		ims.updateView(inputConnection);
		checkComposingState("TH", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "TH");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("TH");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("TH", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "TH");

		ims.setInputState(ims.getInputState().setBoolean(CapState, true));
		ims.updateStateOnKey(inputConnection, 't');
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "T");
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		ims.updateStateOnKey(inputConnection, 's');
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Ts");
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ts");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");

		ims.updateStateOnKey(inputConnection, 'j');
		checkComposingState("J", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "J");
		ims.updateView(inputConnection);
		checkComposingState("J", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "J");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("J");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("J", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "J");

		ims.setInputState(ims.getInputState().setInteger(Subtype, SUBTYPE_POJ));
		ims.updateStateOnKey(inputConnection, 'j');
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Ch");
		ims.updateView(inputConnection);
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ch");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ch");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ch");

		ims.updateStateOnKey(inputConnection, 'h');
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Chh");
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chh");
		ims.updateStateOnSelection(inputConnection, 3, 3, 0, 3);
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");
	}

	@Test
	public void voicelessToggle() {
		ims.setInputState(ims.getInputState().setBoolean(VoicelessToggle, true));
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("G");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("G", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "G");

		ims.updateStateOnKey(inputConnection, 'g');
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("K");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");

		ims.updateStateOnKey(inputConnection, 'g');
		checkComposingState("G", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "G");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("G");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("G", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "G");
	}

	@Test
	public void leadsReplaceLeads() {
		ims.setInputState(ims.getInputState().setBoolean(LeadReplaceLead, true));

		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		ims.updateStateOnKey(inputConnection, 'p');
		checkComposingState("P", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "P");
		ims.updateView(inputConnection);
		checkComposingState("P", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "P");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("P");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("P", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "P");

		ims.updateStateOnKey(inputConnection, 'k');
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("K");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");

		ims.updateStateOnKey(inputConnection, 'h');
		checkComposingState("Kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Kh");
		ims.updateView(inputConnection);
		checkComposingState("Kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Kh");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Kh");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Kh");

		ims.updateStateOnText(inputConnection, "b");
		checkComposingState("B", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "B");
		ims.updateView(inputConnection);
		checkComposingState("B", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "B");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("B");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("B", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "B");

		ims.updateStateOnText(inputConnection, "ts");
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Ts");
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ts");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");

		ims.updateStateOnKey(inputConnection, 'h');
		checkComposingState("Tsh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Tsh");
		ims.updateView(inputConnection);
		checkComposingState("Tsh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Tsh");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tsh");
		ims.updateStateOnSelection(inputConnection, 3, 3, 0, 3);
		ims.updateView(inputConnection);
		checkComposingState("Tsh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Tsh");

		ims.updateStateOnText(inputConnection, "ch");
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Ch");
		ims.updateView(inputConnection);
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ch");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ch");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ch");

		ims.updateStateOnText(inputConnection, "ts");
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ts");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");

		ims.updateStateOnKey(inputConnection, 'h');
		checkComposingState("Tsh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Tsh");
		ims.updateView(inputConnection);
		checkComposingState("Tsh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Tsh");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tsh");
		ims.updateStateOnSelection(inputConnection, 3, 3, 0, 3);
		ims.updateView(inputConnection);
		checkComposingState("Tsh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Tsh");

		ims.updateStateOnKey(inputConnection, 's');
		checkComposingState("S", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "S");
		ims.updateView(inputConnection);
		checkComposingState("S", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "S");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("S");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("S", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "S");

		ims.updateStateOnKey(inputConnection, 't');
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		ims.updateStateOnText(inputConnection, "ts");
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Ts");
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ts");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");

		ims.updateStateOnKey(inputConnection, 'j');
		checkComposingState("J", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "J");
		ims.updateView(inputConnection);
		checkComposingState("J", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "J");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("J");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("J", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "J");

		// ng is a valid after, will not replace
		ims.updateStateOnText(inputConnection, "ng");
		checkComposingState("J", "", 1, "ng", GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Jng");
		ims.updateView(inputConnection);
		checkComposingState("J", "", 1, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Jng");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Jng");
		ims.updateStateOnSelection(inputConnection, 3, 3, 0, 3);
		ims.updateView(inputConnection);
		checkComposingState("J", "", 1, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Jng");
	}

	@Test
	public void inputLeadAfterNothingBeforeLeads() {
		// _[hāng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hāng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "hāng");
		checkComposingRegion(0, "h", "a", 7, "ng", GLOTTAL_NULL, false);

		// [p_hāng]
		ims.updateStateOnKey(inputConnection, 'p');
		ims.updateView(inputConnection);
		checkComposingState("P", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "Phāng");
		checkComposingRegion(1, "Ph", "a", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputLeadsAfterNothingBeforeLeads() {
		// _[hāng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hāng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "hāng");
		checkComposingRegion(0, "h", "a", 7, "ng", GLOTTAL_NULL, false);

		// [ch_hāng]
		ims.updateStateOnText(inputConnection, "ch");
		ims.updateView(inputConnection);
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 7, 0, 0, null, "Chhāng");
		checkComposingRegion(2, "Chh", "a", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputInvalidLeadAfterNothingBeforeLeads() {
		// _[hāng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hāng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "hāng");
		checkComposingRegion(0, "h", "a", 7, "ng", GLOTTAL_NULL, false);

		// [s]_hāng
		ims.updateStateOnKey(inputConnection, 's');
		ims.updateView(inputConnection);
		checkComposingState("S", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "S");
		checkComposingRegion(-1, "S", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterNothingBeforeLeads() {
		// _[lāng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("lāng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "lāng");
		checkComposingRegion(0, "l", "a", 7, "ng", GLOTTAL_NULL, false);

		// [a]_lāng
		ims.updateStateOnKey(inputConnection, 'a');
		ims.updateView(inputConnection);
		checkComposingState("", "A", 1, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "A");
		checkComposingRegion(-1, "", "A", 1, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterNothingBeforeNasalLeads() {
		// _[nāng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nāng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "nāng");
		checkComposingRegion(0, "n", "a", 7, "ng", GLOTTAL_NULL, false);

		// [a_n]āng
		ims.updateStateOnKey(inputConnection, 'a');
		ims.updateView(inputConnection);
		checkComposingState("", "A", 1, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "An");
		checkComposingRegion(1, "", "A", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterNothingBeforeGlotalLeads() {
		// _[hāng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hāng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "hāng");
		checkComposingRegion(0, "h", "a", 7, "ng", GLOTTAL_NULL, false);

		// [a_h]āng
		ims.updateStateOnKey(inputConnection, 'a');
		ims.updateView(inputConnection);
		checkComposingState("", "A", 1, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "Ah");
		checkComposingRegion(1, "", "A", 4, "", 'h', false);
	}

	@Test
	public void inputAfterAfterNothingBeforeLeads() {
		// _[bat]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("bat");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "bat");
		checkComposingRegion(0, "b", "a", 4, "", 't', false);
		// [m]_bat
		ims.updateStateOnKey(inputConnection, 'm');
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "M", GLOTTAL_NULL, false, 1, 2, 0, 0, null, "M");
		checkComposingRegion(-1, "", "", 1, "M", GLOTTAL_NULL, false);
	}

	@Test
	public void inputAfterAfterNothingBeforeNasalLeads() {
		// _[ngôo]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ngôo");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "ngôo");
		checkComposingRegion(0, "ng", "oo", 5, AFTERS_NULL, GLOTTAL_NULL, false);
		// [n_ng]ôo
		ims.updateStateOnKey(inputConnection, 'n');
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "N", GLOTTAL_NULL, false, 1, 4, 0, 0, null, "Nng");
		checkComposingRegion(1, "N", "", 1, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputAftersAfterNothingBeforeLeads() {
		// _[sàng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("sàng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "sàng");
		checkComposingRegion(0, "s", "a", 3, "ng", GLOTTAL_NULL, false);
		// [ng]_sàng
		ims.updateStateOnText(inputConnection, "ng");
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "Ng", GLOTTAL_NULL, false, 1, 3, 0, 0, null, "Ng");
		checkComposingRegion(-1, "", "", 1, "Ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputAftersAfterNothingBeforeNasalLeads() {
		// _[mi̍h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("mi̍h");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "mi̍h");
		checkComposingRegion(0, "m", "i", 8, "", 'h', false);
		// [ngm]_i̍h
		ims.updateStateOnText(inputConnection, "ng");
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "Ng", GLOTTAL_NULL, false, 1, 4, 0, 0, null, "Ngm");
		checkComposingRegion(2, "Ng", "", 1, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void inputNNAfterNothingBeforeLeads() {
		// _[phīn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("phīn");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "phīn");
		checkComposingRegion(0, "ph", "i", 7, "n", GLOTTAL_NULL, false);
		// [nn]_phīn
		ims.updateStateOnText(inputConnection, "nn");
		ims.updateView(inputConnection);
		checkComposingState("N", "", 1, "n", GLOTTAL_NULL, false, 1, 4, 0, 0, null, "Nnp");
		checkComposingRegion(2, "N", "", 4, "n", 'p', false);
	}

	@Test
	public void inputNNAfterNothingBeforeNasalLeads() {
		// _[nôo]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nôo");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "nôo");
		checkComposingRegion(0, "n", "oo", 5, AFTERS_NULL, GLOTTAL_NULL, false);
		// [nn]_nôo
		ims.updateStateOnText(inputConnection, "nn");
		ims.updateView(inputConnection);
		checkComposingState("N", "", 1, "n", GLOTTAL_NULL, false, 1, 3, 0, 0, null, "Nn");
		checkComposingRegion(-1, "N", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputPojNNAfterNothingBeforeLeads() {
		// _[phīn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("phīn");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "phīn");
		checkComposingRegion(0, "ph", "i", 7, "n", GLOTTAL_NULL, false);
		// ⁿ_[phīn]
		ims.updateStateOnText(inputConnection, "ⁿ");
		assertEquals("ⁿ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "phīn");
		checkComposingRegion(0, "ph", "i", 7, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputPojNNAfterNothingBeforeNasalLeads() {
		// _[nôo]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nôo");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "nôo");
		checkComposingRegion(0, "n", "oo", 5, AFTERS_NULL, GLOTTAL_NULL, false);
		// ⁿ_[nôo]
		ims.updateStateOnText(inputConnection, "ⁿ");
		assertEquals("ⁿ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 5, 0, 0, null, "nôo");
		checkComposingRegion(0, "n", "oo", 5, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputToneAfterNothingBeforeLeads() {
		// _[pi]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("pi");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "pi");
		checkComposingRegion(0, "p", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false);
		// _[pí]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "pí");
		checkComposingRegion(0, "p", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// _[pì]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "pì");
		checkComposingRegion(0, "p", "i", 3, AFTERS_NULL, GLOTTAL_NULL, false);
		// _[pi]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "pi");
		checkComposingRegion(0, "p", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputToneAfterNothingBeforeLeads2() {
		// _[pih]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("pih");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "pih");
		checkComposingRegion(0, "p", "i", 4, "", 'h', false);
		// _[pi̍h]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "pi̍h");
		checkComposingRegion(0, "p", "i", 8, "", 'h', false);
		// _[pih]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "pih");
		checkComposingRegion(0, "p", "i", 4, "", 'h', false);
	}

	@Test
	public void inputLeadAfterNothingBeforeVowels() {
		// _[é]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("é");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "é");
		checkComposingRegion(0, "", "e", 2, AFTERS_NULL, GLOTTAL_NULL, false);

		// [l_é]
		ims.updateStateOnKey(inputConnection, 'l');
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "Lé");
		checkComposingRegion(1, "L", "e", 2, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterNothingBeforeVowels() {
		// _[é]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("é");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "é");
		checkComposingRegion(0, "", "e", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// [u_é]
		ims.updateStateOnKey(inputConnection, 'u');
		ims.updateView(inputConnection);
		checkComposingState("", "U", 1, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "Ué");
		checkComposingRegion(1, "", "Ue", 2, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterNothingBeforeVowelsToneChanged() {
		// _[ū]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ū");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "ū");
		checkComposingRegion(0, "", "u", 7, AFTERS_NULL, GLOTTAL_NULL, false);
		// [ā_u]
		ims.updateStateOnKey(inputConnection, 'a');
		ims.updateView(inputConnection);
		checkComposingState("", "A", 7, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "Āu");
		checkComposingRegion(1, "", "Au", 7, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputNNAfterNothingBeforeVowels() {
		// _[ū]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ū");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "ū");
		checkComposingRegion(0, "", "u", 7, AFTERS_NULL, GLOTTAL_NULL, false);
		// [nn]_ū
		ims.updateStateOnText(inputConnection, "nn");
		ims.updateView(inputConnection);
		checkComposingState("N", "", 1, "n", GLOTTAL_NULL, false, 1, 3, 0, 0, null, "Nn");
		checkComposingRegion(-1, "N", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputPojNNAfterNothingBeforeVowels() {
		// _[ū]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ū");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "ū");
		checkComposingRegion(0, "", "u", 7, AFTERS_NULL, GLOTTAL_NULL, false);
		// ⁿ_[ū]
		ims.updateStateOnText(inputConnection, "ⁿ");
		assertEquals("ⁿ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 3, 0, 0, null, "ū");
		checkComposingRegion(0, "", "u", 7, AFTERS_NULL, GLOTTAL_NULL, false);
	}


	@Test
	public void inputToneAfterNothingBeforeVowels() {
		// _[e]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("e");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "e");
		checkComposingRegion(0, "", "e", 1, AFTERS_NULL, GLOTTAL_NULL, false);
		// _[ê]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_RISE);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "ê");
		checkComposingRegion(0, "", "e", 5, AFTERS_NULL, GLOTTAL_NULL, false);
		// _[ē]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_MID);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "ē");
		checkComposingRegion(0, "", "e", 7, AFTERS_NULL, GLOTTAL_NULL, false);
		// _[e̋]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_MID);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "e̋");
		checkComposingRegion(0, "", "e", 9, AFTERS_NULL, GLOTTAL_NULL, false);
		// _[e]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_MID);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "e");
		checkComposingRegion(0, "", "e", 1, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputToneAfterNothingBeforeVowels2() {
		// _[eh]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("eh");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "eh");
		checkComposingRegion(0, "", "e", 4, "", 'h', false);
		// _[e̍h]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "e̍h");
		checkComposingRegion(0, "", "e", 8, "", 'h', false);
		// _[eh]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "eh");
		checkComposingRegion(0, "", "e", 4, "", 'h', false);
	}

	@Test
	public void inputLeadAfterNothingBeforeNasal() {
		// _[n̄g]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n̄g");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "n̄g");
		checkComposingRegion(0, "", "", 7, "ng", GLOTTAL_NULL, false);

		// [n_n̄g]
		ims.updateStateOnKey(inputConnection, 'n');
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "N", GLOTTAL_NULL, false, 1, 5, 0, 0, null, "Nn̄g");
		checkComposingRegion(1, "N", "", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterNothingBeforeNasal() {
		// _[m̄]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m̄");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "m̄");
		checkComposingRegion(0, "", "", 7, "m", GLOTTAL_NULL, false);
		// [ō_m]
		ims.updateStateOnKey(inputConnection, 'o');
		ims.updateView(inputConnection);
		checkComposingState("", "O", 7, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "Ōm");
		checkComposingRegion(1, "", "O", 7, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void inputNNAfterNothingBeforeNasal() {
		// _[m̄]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m̄");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "m̄");
		checkComposingRegion(0, "", "", 7, "m", GLOTTAL_NULL, false);
		// [nn]_m̄
		ims.updateStateOnText(inputConnection, "nn");
		ims.updateView(inputConnection);
		checkComposingState("N", "", 1, "n", GLOTTAL_NULL, false, 1, 3, 0, 0, null, "Nn");
		checkComposingRegion(-1, "N", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputPojNNAfterNothingBeforeNasal() {
		// _[m̄]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m̄");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "m̄");
		checkComposingRegion(0, "", "", 7, "m", GLOTTAL_NULL, false);
		// ⁿ_[m̄]
		ims.updateStateOnText(inputConnection, "ⁿ");
		assertEquals("ⁿ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 4, 0, 0, null, "m̄");
		checkComposingRegion(0, "", "", 7, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void inputLeadAfterLeadsBeforeVowels() {
		// [Ch_á]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("á");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chá");
		checkComposingRegion(2, "Ch", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// [Chh_á]
		ims.updateStateOnKey(inputConnection, 'h');
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Chhá");
		checkComposingRegion(3, "Chh", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputInvalidLeadAfterLeadsBeforeVowels() {
		// [h_ōo]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("h");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ōo");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "hōo");
		checkComposingRegion(1, "h", "oo", 7, AFTERS_NULL, GLOTTAL_NULL, false);
		// h[s_ōo]
		ims.updateStateOnKey(inputConnection, 's');
		assertEquals("h", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "sōo");
		checkComposingRegion(1, "s", "oo", 7, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputInvalidLeadsAfterLeadsBeforeVowels() {
		// [B_ah]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("B");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ah");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Bah");
		checkComposingRegion(1, "B", "a", 4, "", 'h', false);
		// B[ts_ah]
		ims.updateStateOnText(inputConnection, "ts");
		assertEquals("B", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "tsah");
		checkComposingRegion(2, "ts", "a", 4, "", 'h', false);
	}

	@Test
	public void inputInvalidLeadsAfterLeadsBeforeVowelsWithHat() {
		// {Sóo-[j_ī]}
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Sóo-j");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ī");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Sóo-", "j", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, "", 0, 6, 0, 0, null, "Sóo-jī");
		checkComposingRegion(5, "j", "i", 7, AFTERS_NULL, GLOTTAL_NULL, false);
		// Sóo-j{[h_ī]}
		ims.updateStateOnKey(inputConnection, 'h');
		assertEquals("Sóo-j", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("", "h", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, "", 5, 7, 0, 0, null, "hī");
		checkComposingRegion(1, "h", "i", 7, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterLeadsBeforeVowels() {
		// [Ts_ā]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ā");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Tsā");
		checkComposingRegion(2, "Ts", "a", 7, AFTERS_NULL, GLOTTAL_NULL, false);

		// [Tsi_ā]
		ims.updateStateOnKey(inputConnection, 'i');
		ims.updateView(inputConnection);
		checkComposingState("Ts", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Tsiā");
		checkComposingRegion(3, "Ts", "ia", 7, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputNNAfterLeadsBeforeVowels() {
		// [n_uā]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("uā");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "nuā");
		checkComposingRegion(1, "n", "ua", 7, AFTERS_NULL, GLOTTAL_NULL, false);
		// n[nn]_uā
		ims.updateStateOnText(inputConnection, "nn");
		assertEquals("n", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 1, 3, 0, 0, null, "nn");
		checkComposingRegion(-1, "n", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputPojNNAfterLeadsBeforeVowels() {
		// [n_uā]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("uā");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "nuā");
		checkComposingRegion(1, "n", "ua", 7, AFTERS_NULL, GLOTTAL_NULL, false);
		// nⁿ_[uā]
		ims.updateStateOnText(inputConnection, "ⁿ");
		assertEquals("nⁿ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 4, 0, 0, null, "uā");
		checkComposingRegion(0, "", "ua", 7, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputToneAfterLeadsBeforeVowels() {
		// [p_ue]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("p");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ue");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "pue");
		checkComposingRegion(1, "p", "ue", 1, AFTERS_NULL, GLOTTAL_NULL, false);
		// [p_uē]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_MID);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "puē");
		checkComposingRegion(1, "p", "ue", 7, AFTERS_NULL, GLOTTAL_NULL, false);
		// [p_uè]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "puè");
		checkComposingRegion(1, "p", "ue", 3, AFTERS_NULL, GLOTTAL_NULL, false);
		// [p_ue]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "pue");
		checkComposingRegion(1, "p", "ue", 1, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputToneAfterLeadsBeforeVowels2() {
		// [p_eh]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("p");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("eh");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "peh");
		checkComposingRegion(1, "p", "e", 4, "", 'h', false);
		// [p_e̍h]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "pe̍h");
		checkComposingRegion(1, "p", "e", 8, "", 'h', false);
		// [p_eh]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "peh");
		checkComposingRegion(1, "p", "e", 4, "", 'h', false);
	}

	@Test
	public void inputLeadAfterLeadsBeforeNasal() {
		// [n_ńg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ńg");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "nńg");
		checkComposingRegion(1, "n", "", 2, "ng", GLOTTAL_NULL, false);
		// [ng_ńg]
		ims.updateStateOnKey(inputConnection, 'g');
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "ngńg");
		checkComposingRegion(2, "ng", "", 2, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputInvaidLeadAfterLeadsBeforeNasal() {
		// [t_ńg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("t");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ńg");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "tńg");
		checkComposingRegion(1, "t", "", 2, "ng", GLOTTAL_NULL, false);
		// t[k_ńg]
		ims.updateStateOnKey(inputConnection, 'k');
		assertEquals("t", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "kńg");
		checkComposingRegion(1, "k", "", 2, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputInvalidLeadsAfterLeadsBeforeNasal() {
		// [t_ńg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("t");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ńg");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "tńg");
		checkComposingRegion(1, "t", "", 2, "ng", GLOTTAL_NULL, false);
		// t[ch_ńg]
		ims.updateStateOnText(inputConnection, "ch");
		assertEquals("t", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "chńg");
		checkComposingRegion(2, "ch", "", 2, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputInvalidNasalAfterLeadsBeforeNasal() {
		// [t_ńg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("t");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ńg");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "tńg");
		checkComposingRegion(1, "t", "", 2, "ng", GLOTTAL_NULL, false);
		// [tng]_ńg
		ims.updateStateOnText(inputConnection, "ng");
		ims.updateView(inputConnection);
		checkComposingState("t", "", 1, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "tng");
		checkComposingRegion(-1, "t", "", 1, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputNNAfterLeadsBeforeNasal() {
		// [t_ńg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("t");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ńg");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "tńg");
		checkComposingRegion(1, "t", "", 2, "ng", GLOTTAL_NULL, false);
		// t[nn]_ńg
		ims.updateStateOnText(inputConnection, "nn");
		assertEquals("t", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 1, 3, 0, 0, null, "nn");
		checkComposingRegion(-1, "n", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputPojNNAfterLeadsBeforeNasal() {
		// [t_ńg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("t");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ńg");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "tńg");
		checkComposingRegion(1, "t", "", 2, "ng", GLOTTAL_NULL, false);
		// tⁿ_[ńg]
		ims.updateStateOnText(inputConnection, "ⁿ");
		assertEquals("tⁿ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 4, 0, 0, null, "ńg");
		checkComposingRegion(0, "", "", 2, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterLeadsBeforeNasal() {
		// [L_n̂g]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("L");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n̂g");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Ln̂g");
		checkComposingRegion(1, "L", "", 5, "ng", GLOTTAL_NULL, false);

		// [Lâ_ng]
		ims.updateStateOnKey(inputConnection, 'a');
		ims.updateView(inputConnection);
		checkComposingState("L", "a", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Lâng");
		checkComposingRegion(2, "L", "a", 5, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputToneAfterLeadsBeforeNasal() {
		// [k_ng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("k");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "kng");
		checkComposingRegion(1, "k", "", 1, "ng", GLOTTAL_NULL, false);
		// [k_n̄g]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_MID);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "kn̄g");
		checkComposingRegion(1, "k", "", 7, "ng", GLOTTAL_NULL, false);
		// [k_ńg]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "kńg");
		checkComposingRegion(1, "k", "", 2, "ng", GLOTTAL_NULL, false);
		// [k_ng]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "kng");
		checkComposingRegion(1, "k", "", 1, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputToneAfterLeadsBeforeNasal2() {
		// [ph_n̍gh]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ph");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n̍gh");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "phn̍gh");
		checkComposingRegion(2, "ph", "", 8, "ng", 'h', false);
		// [ph_ngh]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState("ph", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "phngh");
		checkComposingRegion(2, "ph", "", 4, "ng", 'h', false);
		// [ph_n̍gh]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState("ph", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "phn̍gh");
		checkComposingRegion(2, "ph", "", 8, "ng", 'h', false);
	}

	@Test
	public void inputLeadAfterVowelsBeforeNasal() {
		// [ō_m]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ō");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ōm");
		checkComposingRegion(1, "", "o", 7, "m", GLOTTAL_NULL, false);
		// ō[s_m]
		ims.updateStateOnKey(inputConnection, 's');
		assertEquals("ō", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 3, 0, 0, null, "sm");
		checkComposingRegion(1, "s", "", 1, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void inputLeadsAfterVowelsBeforeNasal() {
		// [ō_m]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ō");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ōm");
		checkComposingRegion(1, "", "o", 7, "m", GLOTTAL_NULL, false);
		// ō[ts_m]
		ims.updateStateOnText(inputConnection, "ts");
		assertEquals("ō", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "tsm");
		checkComposingRegion(2, "ts", "", 1, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void inputGlotalAfterVowelsBeforeNasal() {
		// [ō_m]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ō");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ōm");
		checkComposingRegion(1, "", "o", 7, "m", GLOTTAL_NULL, false);
		// [ok]_m
		ims.updateStateOnKey(inputConnection, 'k');
		ims.updateView(inputConnection);
		checkComposingState("", "o", 4, "", 'k', false, 0, 2, 0, 0, null, "ok");
		checkComposingRegion(-1, "", "o", 4, "", 'k', false);
	}

	@Test
	public void inputNasalAfterVowelsBeforeNasal() {
		// [ō_m]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ō");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ōm");
		checkComposingRegion(1, "", "o", 7, "m", GLOTTAL_NULL, false);
		// [ōng]_m
		ims.updateStateOnText(inputConnection, "ng");
		ims.updateView(inputConnection);
		checkComposingState("", "o", 7, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ōng");
		checkComposingRegion(-1, "", "o", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterVowelsBeforeNasal() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [ō_m]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ō");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ōm");
		checkComposingRegion(1, "", "o", 7, "m", GLOTTAL_NULL, false);
		// [oā_m]
		ims.updateStateOnKey(inputConnection, 'a');
		ims.updateView(inputConnection);
		checkComposingState("", "oa", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "oām");
		checkComposingRegion(2, "", "oa", 7, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void inputNNAfterVowelsBeforeNasal() {
		// [ō_m]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ō");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ōm");
		checkComposingRegion(1, "", "o", 7, "m", GLOTTAL_NULL, false);
		// [ōnn]_m
		ims.updateStateOnText(inputConnection, "nn");
		ims.updateView(inputConnection);
		checkComposingState("", "o", 7, "nn", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ōnn");
		checkComposingRegion(-1, "", "o", 7, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void inputPojNNAfterVowelsBeforeNasal() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [ō_m]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ō");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ōm");
		checkComposingRegion(1, "", "o", 7, "m", GLOTTAL_NULL, false);
		// [ōⁿ]_m
		ims.updateStateOnText(inputConnection, "ⁿ");
		ims.updateView(inputConnection);
		checkComposingState("", "o", 7, "", GLOTTAL_NULL, true, 0, 2, 0, 0, null, "ōⁿ");
		checkComposingRegion(-1, "", "o", 7, "", GLOTTAL_NULL, true);
	}

	@Test
	public void inputToneAfterVowelsBeforeNasal() {
		// [o_ng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("o");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ong");
		checkComposingRegion(1, "", "o", 1, "ng", GLOTTAL_NULL, false);
		// [ô_ng]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_RISE);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ông");
		checkComposingRegion(1, "", "o", 5, "ng", GLOTTAL_NULL, false);
		// [ò_ng]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "òng");
		checkComposingRegion(1, "", "o", 3, "ng", GLOTTAL_NULL, false);
		// [o_ng]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ong");
		checkComposingRegion(1, "", "o", 1, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void inputToneAfterVowelsBeforeNasal2() {
		// [o_ngh]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("o");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ngh");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "ongh");
		checkComposingRegion(1, "", "o", 4, "ng", 'h', false);
		// [o̍_ngh]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "o̍ngh");
		checkComposingRegion(2, "", "o", 8, "ng", 'h', false);
		// [o_ngh]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "ongh");
		checkComposingRegion(1, "", "o", 4, "ng", 'h', false);
	}

	@Test
	public void inputLeadAfterVowelsBeforeNN() {
		// [tá_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tánn");
		checkComposingRegion(2, "t", "a", 2, "nn", GLOTTAL_NULL, false);
		// tá[b_n]n
		ims.updateStateOnKey(inputConnection, 'b');
		assertEquals("tá", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("b", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 4, 0, 0, null, "bn");
		checkComposingRegion(1, "b", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputLeadsAfterVowelsBeforeNN() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [tá_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tánn");
		checkComposingRegion(2, "t", "a", 2, "nn", GLOTTAL_NULL, false);
		// tá[ch_n]n
		ims.updateStateOnText(inputConnection, "ch");
		assertEquals("tá", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 5, 0, 0, null, "chn");
		checkComposingRegion(2, "ch", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputNasalAfterVowelsBeforeNN() {
		// [tá_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tánn");
		checkComposingRegion(2, "t", "a", 2, "nn", GLOTTAL_NULL, false);
		// [tám]_nn
		ims.updateStateOnKey(inputConnection, 'm');
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 2, "m", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "tám");
		checkComposingRegion(-1, "t", "a", 2, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void inputGlotalAfterVowelsBeforeNN() {
		// [tá_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tánn");
		checkComposingRegion(2, "t", "a", 2, "nn", GLOTTAL_NULL, false);
		// [ta̍p]_nn
		ims.updateStateOnKey(inputConnection, 'p');
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 8, "", 'p', false, 0, 4, 0, 0, null, "ta̍p");
		checkComposingRegion(-1, "t", "a", 8, "", 'p', false);
	}

	@Test
	public void inputNNAfterVowelsBeforeNN() {
		// [tá_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tánn");
		checkComposingRegion(2, "t", "a", 2, "nn", GLOTTAL_NULL, false);
		// [tánn]_nn
		ims.updateStateOnText(inputConnection, "nn");
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 2, "nn", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tánn");
		checkComposingRegion(-1, "t", "a", 2, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void inputPojNNAfterVowelsBeforeNN() {
		// [tá_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tánn");
		checkComposingRegion(2, "t", "a", 2, "nn", GLOTTAL_NULL, false);
		// [táⁿ]_nn
		ims.updateStateOnText(inputConnection, "ⁿ");
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 2, "", GLOTTAL_NULL, true, 0, 3, 0, 0, null, "táⁿ");
		checkComposingRegion(-1, "t", "a", 2, "", GLOTTAL_NULL, true);
	}

	@Test
	public void inputVowelAfterVowelsBeforeNN() {
		// [tá_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tánn");
		checkComposingRegion(2, "t", "a", 2, "nn", GLOTTAL_NULL, false);
		// [tái_nn]
		ims.updateStateOnKey(inputConnection, 'i');
		ims.updateView(inputConnection);
		checkComposingState("t", "ai", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "táinn");
		checkComposingRegion(3, "t", "ai", 2, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void inputToneAfterVowelsBeforeNN() {
		// [i_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("i");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "inn");
		checkComposingRegion(1, "", "i", 1, "nn", GLOTTAL_NULL, false);
		// [ì_nn]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ìnn");
		checkComposingRegion(1, "", "i", 3, "nn", GLOTTAL_NULL, false);
		// [í_nn]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ínn");
		checkComposingRegion(1, "", "i", 2, "nn", GLOTTAL_NULL, false);
		// [i_nn]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "inn");
		checkComposingRegion(1, "", "i", 1, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void inputToneAfterVowelsBeforeNN2() {
		// [khe̍_nnh]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nnh");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 7, 0, 0, null, "khe̍nnh");
		checkComposingRegion(4, "kh", "e", 8, "nn", 'h', false);
		// [khe_nnh]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_LOW);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "khennh");
		checkComposingRegion(3, "kh", "e", 4, "nn", 'h', false);
		// [khe̍_nnh]
		ims.updateStateOnKey(inputConnection, KEYCODE_TONE_HIGH);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 7, 0, 0, null, "khe̍nnh");
		checkComposingRegion(4, "kh", "e", 8, "nn", 'h', false);
	}

	@Test
	public void inputLeadAfterVowelsBeforeGlotal() {
		// [tho̍_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tho̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "tho̍h");
		checkComposingRegion(4, "th", "o", 8, "", 'h', false);
		// thó[l]_h
		ims.updateStateOnKey(inputConnection, 'l');
		assertEquals("thó", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("l", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 3, 4, 0, 0, null, "l");
		checkComposingRegion(-1, "l", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputLeadsAfterVowelsBeforeGlotal() {
		// [tho̍_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tho̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "tho̍h");
		checkComposingRegion(4, "th", "o", 8, "", 'h', false);
		// thó[ts_h]
		ims.updateStateOnText(inputConnection, "ts");
		assertEquals("thó", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 3, 6, 0, 0, null, "tsh");
		checkComposingRegion(2, "tsh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputGlotalAfterVowelsBeforeGlotal() {
		// [tho̍_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tho̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "tho̍h");
		checkComposingRegion(4, "th", "o", 8, "", 'h', false);
		// [tho̍k]_h
		ims.updateStateOnKey(inputConnection, 'k');
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, "", 'k', false, 0, 5, 0, 0, null, "tho̍k");
		checkComposingRegion(-1, "th", "o", 8, "", 'k', false);
	}

	@Test
	public void inputNasalVowelsBeforeGlotal() {
		// [tho̍_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tho̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "tho̍h");
		checkComposingRegion(4, "th", "o", 8, "", 'h', false);
		// [tho̍ng_h]
		ims.updateStateOnText(inputConnection, "ng");
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, "ng", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "tho̍ngh");
		checkComposingRegion(6, "th", "o", 8, "ng", 'h', false);
	}

	@Test
	public void inputNNAfterVowelsBeforeGlotal() {
		// [tho̍_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tho̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "tho̍h");
		checkComposingRegion(4, "th", "o", 8, "", 'h', false);
		// [tho̍nn_h]
		ims.updateStateOnText(inputConnection, "nn");
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "tho̍nnh");
		checkComposingRegion(6, "th", "o", 8, "nn", 'h', false);
	}

	@Test
	public void inputPojNNAfterVowelsBeforeGlotal() {
		// [tho̍_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tho̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "tho̍h");
		checkComposingRegion(4, "th", "o", 8, "", 'h', false);
		// [tho̍ⁿ_h]
		ims.updateStateOnText(inputConnection, "ⁿ");
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, "", GLOTTAL_NULL, true, 0, 6, 0, 0, null, "tho̍ⁿh");
		checkComposingRegion(5, "th", "o", 8, "ⁿ", 'h', false);
	}

	@Test
	public void inputVowelAfterVowelsBeforeGlotal() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [tho̍_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tho̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("th", "o", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "tho̍h");
		checkComposingRegion(4, "th", "o", 8, "", 'h', false);
		// [thoa̍_h]
		ims.updateStateOnKey(inputConnection, 'a');
		ims.updateView(inputConnection);
		checkComposingState("th", "oa", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "thoa̍h");
		checkComposingRegion(5, "th", "oa", 8, "", 'h', false);
	}

	@Test
	public void inputLeadAfterVowelsBeforePojNN() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [tsî_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tsî");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tsîⁿ");
		checkComposingRegion(3, "ts", "i", 5, "", GLOTTAL_NULL, true);
		// tsî[s]_ⁿ
		ims.updateStateOnKey(inputConnection, 's');
		assertEquals("tsî", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 3, 4, 0, 0, null, "s");
		checkComposingRegion(-1, "s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputLeadsAfterVowelsBeforePojNN() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [tsî_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tsî");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tsîⁿ");
		checkComposingRegion(3, "ts", "i", 5, "", GLOTTAL_NULL, true);
		// tsî[ch]_ⁿ
		ims.updateStateOnText(inputConnection, "ch");
		assertEquals("tsî", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 3, 5, 0, 0, null, "ch");
		checkComposingRegion(-1, "ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputNasalAfterVowelsBeforePojNN() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [tsî_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tsî");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tsîⁿ");
		checkComposingRegion(3, "ts", "i", 5, "", GLOTTAL_NULL, true);
		// [tsîn]_ⁿ
		ims.updateStateOnKey(inputConnection, 'n');
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 5, "n", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tsîn");
		checkComposingRegion(-1, "ts", "i", 5, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputGlotalAfterVowelsBeforePojNN() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [tsî_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tsî");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tsîⁿ");
		checkComposingRegion(3, "ts", "i", 5, "", GLOTTAL_NULL, true);
		// [tsik_ⁿ]
		ims.updateStateOnKey(inputConnection, 'k');
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 4, "", 'k', false, 0, 5, 0, 0, null, "tsikⁿ");
		checkComposingRegion(4, "ts", "i", 4, "", 'k', true);
	}

	@Test
	public void inputNNAfterVowelsBeforePojNN() {
		// [tsî_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tsî");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tsîⁿ");
		checkComposingRegion(3, "ts", "i", 5, "", GLOTTAL_NULL, true);
		// [tsînn]_ⁿ
		ims.updateStateOnText(inputConnection, "nn");
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 5, "nn", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "tsînn");
		checkComposingRegion(-1, "ts", "i", 5, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void inputPojNNAfterVowelsBeforePojNN() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [tsî_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tsî");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tsîⁿ");
		checkComposingRegion(3, "ts", "i", 5, "", GLOTTAL_NULL, true);
		// [tsîⁿ]_ⁿ
		ims.updateStateOnText(inputConnection, "ⁿ");
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 5, "", GLOTTAL_NULL, true, 0, 4, 0, 0, null, "tsîⁿ");
		checkComposingRegion(-1, "ts", "i", 5, "", GLOTTAL_NULL, true);
	}

	@Test
	public void inputVowelAfterVowelsBeforePojNN() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [tsî_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tsî");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tsîⁿ");
		checkComposingRegion(3, "ts", "i", 5, "", GLOTTAL_NULL, true);
		// [tsiû_ⁿ]
		ims.updateStateOnKey(inputConnection, 'u');
		ims.updateView(inputConnection);
		checkComposingState("ts", "iu", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "tsiûⁿ");
		checkComposingRegion(4, "ts", "iu", 5, "", GLOTTAL_NULL, true);
	}

	@Test
	public void inputLeadAfterNasalBeforeGlotal() {
		// [phng_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("phng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", "", 1, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "phngh");
		checkComposingRegion(4, "ph", "", 4, "ng", 'h', false);
		// phng[j]_h
		ims.updateStateOnKey(inputConnection, 'j');
		assertEquals("phng", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("j", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 4, 5, 0, 0, null, "j");
		checkComposingRegion(-1, "j", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputLeadsAfterNasalBeforeGlotal() {
		// [phng_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("phng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", "", 1, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "phngh");
		checkComposingRegion(4, "ph", "", 4, "ng", 'h', false);
		// phng[ts_h]
		ims.updateStateOnText(inputConnection, "ts");
		assertEquals("phng", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 4, 7, 0, 0, null, "tsh");
		checkComposingRegion(2, "tsh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputGlotalAfterNasalBeforeGlotal() {
		// [phng_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("phng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", "", 1, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "phngh");
		checkComposingRegion(4, "ph", "", 4, "ng", 'h', false);
		// [phngp]_h
		ims.updateStateOnKey(inputConnection, 'p');
		ims.updateView(inputConnection);
		checkComposingState("ph", "", 4, "ng", 'p', false, 0, 5, 0, 0, null, "phngp");
		checkComposingRegion(-1, "ph", "", 4, "ng", 'p', false);
	}

	@Test
	public void inputNasalAfterNasalBeforeGlotal() {
		// [phng_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("phng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", "", 1, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "phngh");
		checkComposingRegion(4, "ph", "", 4, "ng", 'h', false);
		// ph[ngm_h]
		ims.updateStateOnKey(inputConnection, 'm');
		assertEquals("ph", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ng", "", 1, "m", GLOTTAL_NULL, false, 2, 6, 0, 0, null, "ngmh");
		checkComposingRegion(3, "ng", "", 4, "m", 'h', false);
	}

	@Test
	public void inputNNAfterNasalBeforeGlotal() {
		// [phng_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("phng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", "", 1, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "phngh");
		checkComposingRegion(4, "ph", "", 4, "ng", 'h', false);
		// phng[nn_h]
		ims.updateStateOnText(inputConnection, "nn");
		assertEquals("phng", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 4, 7, 0, 0, null, "nnh");
		checkComposingRegion(2, "n", "", 4, "n", 'h', false);
	}

	@Test
	public void inputPojNNAfterNasalBeforeGlotal() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [phng_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("phng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", "", 1, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "phngh");
		checkComposingRegion(4, "ph", "", 4, "ng", 'h', false);
		// phngⁿ_[h]
		ims.updateStateOnText(inputConnection, "ⁿ");
		assertEquals("phngⁿ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 5, 6, 0, 0, null, "h");
		checkComposingRegion(0, "h", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterNasalBeforeGlotal() {
		// [phng_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("phng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", "", 1, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "phngh");
		checkComposingRegion(4, "ph", "", 4, "ng", 'h', false);
		// phng[a_h]
		ims.updateStateOnKey(inputConnection, 'a');
		assertEquals("phng", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("", "a", 1, AFTERS_NULL, GLOTTAL_NULL, false, 4, 6, 0, 0, null, "ah");
		checkComposingRegion(1, "", "a", 4, "", 'h', false);
	}

	@Test
	public void inputLeadAfterNNBeforeGlotal() {
		// [khe̍nn_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍nn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "khe̍nnh");
		checkComposingRegion(6, "kh", "e", 8, "nn", 'h', false);
		// khénn[b]_h
		ims.updateStateOnKey(inputConnection, 'b');
		assertEquals("khénn", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("b", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 5, 6, 0, 0, null, "b");
		checkComposingRegion(-1, "b", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputLeadsAfterNNBeforeGlotal() {
		// [khe̍nn_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍nn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "khe̍nnh");
		checkComposingRegion(6, "kh", "e", 8, "nn", 'h', false);
		// khénn-[ts_h]
		ims.updateStateOnText(inputConnection, "ts");
		assertEquals("khénn", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 5, 8, 0, 0, null, "tsh");
		checkComposingRegion(2, "tsh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputGlotalAfterNNBeforeGlotal() {
		// [khe̍nn_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍nn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "khe̍nnh");
		checkComposingRegion(6, "kh", "e", 8, "nn", 'h', false);
		// [khe̍nnh]_h
		ims.updateStateOnKey(inputConnection, 'h');
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "nn", 'h', false, 0, 7, 0, 0, null, "khe̍nnh");
		checkComposingRegion(-1, "kh", "e", 8, "nn", 'h', false);
	}

	@Test
	public void inputNasalAfterNNBeforeGlotal() {
		// [khe̍nn_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍nn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "khe̍nnh");
		checkComposingRegion(6, "kh", "e", 8, "nn", 'h', false);
		// khénn[ng_h]
		ims.updateStateOnText(inputConnection, "ng");
		assertEquals("khénn", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "ng", GLOTTAL_NULL, false, 5, 8, 0, 0, null, "ngh");
		checkComposingRegion(2, "", "", 4, "ng", 'h', false);
	}

	@Test
	public void inputNNAfterNNBeforeGlotal() {
		// [khe̍nn_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍nn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "khe̍nnh");
		checkComposingRegion(6, "kh", "e", 8, "nn", 'h', false);
		// khénn[nn_h]
		ims.updateStateOnText(inputConnection, "nn");
		assertEquals("khénn", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 5, 8, 0, 0, null, "nnh");
		checkComposingRegion(2, "n", "", 4, "n", 'h', false);
	}

	@Test
	public void inputPojNNAfterNNBeforeGlotal() {
		// [khe̍nn_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍nn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "khe̍nnh");
		checkComposingRegion(6, "kh", "e", 8, "nn", 'h', false);
		// khénnⁿ_[h]
		ims.updateStateOnText(inputConnection, "ⁿ");
		assertEquals("khénnⁿ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 6, 7, 0, 0, null, "h");
		checkComposingRegion(0, "h", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputVowelAfterNNBeforeGlotal() {
		// [khe̍nn_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍nn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "khe̍nnh");
		checkComposingRegion(6, "kh", "e", 8, "nn", 'h', false);
		// khénn[oo_h]
		ims.updateStateOnText(inputConnection, "oo");
		assertEquals("khénn", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("", "oo", 1, AFTERS_NULL, GLOTTAL_NULL, false, 5, 8, 0, 0, null, "ooh");
		checkComposingRegion(2, "", "oo", 4, "", 'h', false);
	}

	@Test
	public void inputLeadAfterGlotalBeforePojNN() {
		// [hah_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hah");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "a", 4, "", 'h', false, 0, 4, 0, 0, null, "hahⁿ");
		checkComposingRegion(3, "h", "a", 4, "", 'h', true);
		// hah[s]_ⁿ
		ims.updateStateOnKey(inputConnection, 's');
		assertEquals("hah", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 3, 4, 0, 0, null, "s");
		checkComposingRegion(-1, "s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputLeadsAfterGlotalBeforePojNN() {
		// [hah_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hah");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "a", 4, "", 'h', false, 0, 4, 0, 0, null, "hahⁿ");
		checkComposingRegion(3, "h", "a", 4, "", 'h', true);
		// hah[ch]_ⁿ
		ims.updateStateOnText(inputConnection, "ch");
		assertEquals("hah", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 3, 5, 0, 0, null, "ch");
		checkComposingRegion(-1, "ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputGlotalAfterGlotalBeforePojNN() {
		// [hah_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hah");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "a", 4, "", 'h', false, 0, 4, 0, 0, null, "hahⁿ");
		checkComposingRegion(3, "h", "a", 4, "", 'h', true);
		// hah[p]_ⁿ
		ims.updateStateOnKey(inputConnection, 'p');
		assertEquals("hah", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 3, 4, 0, 0, null, "p");
		checkComposingRegion(-1, "p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void inputNasalAfterGlotalBeforePojNN() {
		// [hah_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hah");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "a", 4, "", 'h', false, 0, 4, 0, 0, null, "hahⁿ");
		checkComposingRegion(3, "h", "a", 4, "", 'h', true);
		// hah[n]ⁿ
		ims.updateStateOnKey(inputConnection, 'n');
		assertEquals("hah", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "n", GLOTTAL_NULL, false, 3, 4, 0, 0, null, "n");
		checkComposingRegion(-1, "", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputNNAfterGlotalBeforePojNN() {
		// [hah_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hah");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "a", 4, "", 'h', false, 0, 4, 0, 0, null, "hahⁿ");
		checkComposingRegion(3, "h", "a", 4, "", 'h', true);
		// hah[nn]_ⁿ
		ims.updateStateOnText(inputConnection, "nn");
		assertEquals("hah", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 3, 5, 0, 0, null, "nn");
		checkComposingRegion(-1, "n", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void inputPojNNAfterGlotalBeforePojNN() {
		// [hah_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hah");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "a", 4, "", 'h', false, 0, 4, 0, 0, null, "hahⁿ");
		checkComposingRegion(3, "h", "a", 4, "", 'h', true);
		// [hahⁿ]_ⁿ
		ims.updateStateOnText(inputConnection, "ⁿ");
		ims.updateView(inputConnection);
		checkComposingState("h", "a", 4, "", 'h', true, 0, 4, 0, 0, null, "hahⁿ");
		checkComposingRegion(-1, "h", "a", 4, "", 'h', true);
	}

	@Test
	public void inputVowelAfterGlotalBeforePojNN() {
		// [hah_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hah");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "a", 4, "", 'h', false, 0, 4, 0, 0, null, "hahⁿ");
		checkComposingRegion(3, "h", "a", 4, "", 'h', true);
		// hah[u_ⁿ]
		ims.updateStateOnKey(inputConnection, 'u');
		assertEquals("hah", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("", "u", 1, AFTERS_NULL, GLOTTAL_NULL, false, 3, 5, 0, 0, null, "uⁿ");
		checkComposingRegion(1, "", "u", 1, "", GLOTTAL_NULL, true);
	}
}
