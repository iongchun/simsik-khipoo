/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotSame;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static tw.iongchun.taigikbd.TKComposingUtils.AFTERS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.GLOTTAL_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.LEADS_NULL;
import static tw.iongchun.taigikbd.TKInputMethodService.KBD_ASCII;
import static tw.iongchun.taigikbd.TKInputMethodService.LANGKEY_ASCII;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_TL;
import static tw.iongchun.taigikbd.TKInputState.Afters;
import static tw.iongchun.taigikbd.TKInputState.ComposingMode;
import static tw.iongchun.taigikbd.TKInputState.ComposingStarted;
import static tw.iongchun.taigikbd.TKInputState.ComposingState;
import static tw.iongchun.taigikbd.TKInputState.Glottal;
import static tw.iongchun.taigikbd.TKInputState.InputStarted;
import static tw.iongchun.taigikbd.TKInputState.LanguageKey;
import static tw.iongchun.taigikbd.TKInputState.Leads;
import static tw.iongchun.taigikbd.TKInputState.PojNN;
import static tw.iongchun.taigikbd.TKInputState.TextMode;
import static tw.iongchun.taigikbd.TKInputState.Tone;
import static tw.iongchun.taigikbd.TKInputState.Vowels;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_UI_LOCALE;
import static tw.iongchun.taigikbd.TKViewState.CommitText;
import static tw.iongchun.taigikbd.TKViewState.ComposingEnd;
import static tw.iongchun.taigikbd.TKViewState.ComposingStart;
import static tw.iongchun.taigikbd.TKViewState.ComposingText;
import static tw.iongchun.taigikbd.TKViewState.CurrentKeyboard;
import static tw.iongchun.taigikbd.TKViewState.DeleteAfter;
import static tw.iongchun.taigikbd.TKViewState.DeleteBefore;
import static tw.iongchun.taigikbd.TKViewState.MainKeyboard;
import static tw.iongchun.taigikbd.TKViewState.SelectionEnd;
import static tw.iongchun.taigikbd.TKViewState.SelectionStart;

/**
 * Created by iongchun on 7/7/17.
 */

@RunWith(AndroidJUnit4.class)
public class InputMethodSwitchTest {
	private static final String INPUT_TITLE_TL = "SimSik:TL";
	private static final String INPUT_TITLE_POJ = "SimSik:POJ";
	private TKInputMethodService ims;
	@Mock
	private Context context;
	@Mock
	private SharedPreferences preferences;
	@Mock
	private Resources resources;
	@Mock
	private InputConnection inputConnection;
	@Mock
	private EditorInfo editorInfo;

	@Before
	public void init() {
		initMockito();
		initIMS();
	}

	@After
	public void fine() {
		fineIMS();
	}

	private void initMockito() {
		MockitoAnnotations.initMocks(this);
	}

	private void initIMS() {
		ims = new TKInputMethodService();
		when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(preferences);
		when(preferences.getString(eq(PREF_UI_LOCALE), anyString())).thenReturn(null);
		when(context.getResources()).thenReturn(resources);
		when(resources.getString(eq(R.string.input_title_tl))).thenReturn(INPUT_TITLE_TL);
		when(resources.getString(eq(R.string.input_title_poj))).thenReturn(INPUT_TITLE_POJ);
		ims.attachBaseContext(context);

		ims.initInputState();
		ims.initViewState();

		ims.startInputState();

		editorInfo.initialSelStart = 0;
		editorInfo.initialSelEnd = 0;
		editorInfo.inputType = InputType.TYPE_CLASS_TEXT
		                       | InputType.TYPE_TEXT_VARIATION_NORMAL
		                       | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
		                       | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT;
		editorInfo.initialCapsMode = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
		ims.startViewState(editorInfo);
		ims.switchSubtype(SUBTYPE_TL);
		ims.updateMainKeyboard();
		ims.switchInputType(inputConnection, editorInfo);
	}

	private void fineIMS() {
		ims.finishInputState();
		ims.fineViewState();
		ims.fineInputState();
	}

	private void checkComposingState(
		String leads, String vowels, int tone,
		String afters, char glotal, boolean pojNN,
		int start, int end,
		int deleteBefore, int deleteAfter,
		String commitText,
		String composingText) {
		TKInputState inputState = ims.getInputState();
		TKViewState viewState = ims.getViewState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));
		TKInputState compoState = inputState.getState(ComposingState);
		if (leads != null) {
			assertEquals(leads, compoState.getString(Leads));
			if (leads == LEADS_NULL)
				assertFalse(inputState.getBoolean(ComposingStarted));
			else
				assertTrue(inputState.getBoolean(ComposingStarted));
		}
		if (inputState.getBoolean(ComposingStarted)) {
			assertEquals(vowels, compoState.getString(Vowels));
			assertEquals(tone, (int)compoState.getInteger(Tone));
			assertEquals(afters, compoState.getString(Afters));
			assertEquals(glotal, (char)compoState.getCharacter(Glottal));
			assertEquals(pojNN, (boolean)compoState.getBoolean(PojNN));

			assertEquals(start, (int)viewState.getInteger(ComposingStart));
			assertEquals(end, (int)viewState.getInteger(ComposingEnd));
			assertEquals(composingText, viewState.getString(ComposingText));
		}
		assertFalse(viewState.isAnyChanged(SelectionStart, SelectionEnd));
		if (deleteBefore > 0) {
			assertTrue(viewState.contains(DeleteBefore));
			assertEquals(deleteBefore, (int)viewState.getInteger(DeleteBefore));
		} else {
			assertFalse(viewState.contains(DeleteBefore));
		}
		if (deleteAfter > 0) {
			assertTrue(viewState.contains(DeleteAfter));
			assertEquals(deleteAfter, (int)viewState.getInteger(DeleteAfter));
		} else {
			assertFalse(viewState.contains(DeleteAfter));
		}
		if (commitText != null) {
			assertTrue(viewState.contains(CommitText));
			assertEquals(commitText, viewState.getString(CommitText));
		} else {
			assertFalse(viewState.contains(CommitText));
		}
	}

	@Test
	public void toASCII() {
		ims.setInputState(ims.getInputState().setString(LanguageKey, LANGKEY_ASCII));
		int kbdMain = ims.getViewState().getInteger(MainKeyboard);
		assertNotSame(KBD_ASCII, kbdMain);
		assertTrue(ims.getInputState().getBoolean(ComposingMode));

		// switch to ASCII
		ims.switchInputMethod(inputConnection);
		assertEquals(KBD_ASCII, (int)ims.getViewState().getInteger(CurrentKeyboard));
		assertFalse(ims.getInputState().getBoolean(ComposingMode));

		// switch back
		ims.switchInputMethod(inputConnection);
		assertEquals(kbdMain, (int)ims.getViewState().getInteger(CurrentKeyboard));
		assertTrue(ims.getInputState().getBoolean(ComposingMode));

		// switch to ASCII with composing text
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Gû");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("G", "u", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Gû");
		ims.switchInputMethod(inputConnection);
		assertEquals(KBD_ASCII, (int)ims.getViewState().getInteger(CurrentKeyboard));
		assertFalse(ims.getInputState().getBoolean(ComposingStarted));
		assertFalse(ims.getInputState().getBoolean(ComposingMode));
		assertEquals("Gû", ims.getViewState().getString(CommitText));

		ims.setViewState(ims.getViewState().remove(CommitText));
		// switch back with composing text
		ims.switchInputMethod(inputConnection);
		assertEquals(kbdMain, (int)ims.getViewState().getInteger(CurrentKeyboard));
		checkComposingState("G", "u", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Gû");
	}
}
