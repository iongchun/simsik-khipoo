/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static tw.iongchun.taigikbd.TKComposingUtils.AFTERS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.GLOTTAL_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.LEADS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.VOWELS_NULL;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_POJ;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_TL;
import static tw.iongchun.taigikbd.TKInputState.Afters;
import static tw.iongchun.taigikbd.TKInputState.AutoDash;
import static tw.iongchun.taigikbd.TKInputState.ComposingMode;
import static tw.iongchun.taigikbd.TKInputState.ComposingStarted;
import static tw.iongchun.taigikbd.TKInputState.ComposingState;
import static tw.iongchun.taigikbd.TKInputState.Glottal;
import static tw.iongchun.taigikbd.TKInputState.GlottalReplaceGlottal;
import static tw.iongchun.taigikbd.TKInputState.GlottalToggleTone;
import static tw.iongchun.taigikbd.TKInputState.InputStarted;
import static tw.iongchun.taigikbd.TKInputState.Leads;
import static tw.iongchun.taigikbd.TKInputState.PojNN;
import static tw.iongchun.taigikbd.TKInputState.Subtype;
import static tw.iongchun.taigikbd.TKInputState.TextMode;
import static tw.iongchun.taigikbd.TKInputState.Tone;
import static tw.iongchun.taigikbd.TKInputState.Vowels;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_UI_LOCALE;
import static tw.iongchun.taigikbd.TKViewState.CommitText;
import static tw.iongchun.taigikbd.TKViewState.ComposingEnd;
import static tw.iongchun.taigikbd.TKViewState.ComposingStart;
import static tw.iongchun.taigikbd.TKViewState.ComposingText;
import static tw.iongchun.taigikbd.TKViewState.DeleteAfter;
import static tw.iongchun.taigikbd.TKViewState.DeleteBefore;
import static tw.iongchun.taigikbd.TKViewState.SelectionEnd;
import static tw.iongchun.taigikbd.TKViewState.SelectionStart;

/**
 * Created by iongchun on 7/4/17.
 */

@RunWith(AndroidJUnit4.class)
public class AutoDashTest {
	private static final String INPUT_TITLE_TL = "SimSik:TL";
	private static final String INPUT_TITLE_POJ = "SimSik:POJ";
	private TKInputMethodService ims;
	@Mock
	private Context context;
	@Mock
	private SharedPreferences preferences;
	@Mock
	private Resources resources;
	@Mock
	private InputConnection inputConnection;
	@Mock
	private EditorInfo editorInfo;

	@Before
	public void init() {
		initMockito();
		initIMS();
	}

	@After
	public void fine() {
		fineIMS();
	}

	private void initMockito() {
		MockitoAnnotations.initMocks(this);
	}

	private void initIMS() {
		ims = new TKInputMethodService();
		when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(preferences);
		when(preferences.getString(eq(PREF_UI_LOCALE), anyString())).thenReturn(null);
		when(context.getResources()).thenReturn(resources);
		when(resources.getString(eq(R.string.input_title_tl))).thenReturn(INPUT_TITLE_TL);
		when(resources.getString(eq(R.string.input_title_poj))).thenReturn(INPUT_TITLE_POJ);
		ims.attachBaseContext(context);

		ims.initInputState();
		ims.initViewState();

		ims.startInputState();

		editorInfo.initialSelStart = 0;
		editorInfo.initialSelEnd = 0;
		editorInfo.inputType = InputType.TYPE_CLASS_TEXT
		                       | InputType.TYPE_TEXT_VARIATION_NORMAL
		                       | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
		editorInfo.initialCapsMode = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
		ims.startViewState(editorInfo);
		ims.switchSubtype(SUBTYPE_TL);
		ims.updateMainKeyboard();
		ims.switchInputType(inputConnection, editorInfo);
	}

	private void fineIMS() {
		ims.finishInputState();
		ims.fineViewState();
		ims.fineInputState();
	}

	private void checkComposingState(
		String leads, String vowels, int tone,
		String afters, char glotal, boolean pojNN,
		int start, int end,
		int deleteBefore, int deleteAfter,
		String commitText,
		String composingText) {
		TKInputState inputState = ims.getInputState();
		TKInputState compoState = inputState.getState(ComposingState);
		TKViewState viewState = ims.getViewState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));
		if (leads != null) {
			assertEquals(leads, compoState.getString(Leads));
			if (leads == LEADS_NULL)
				assertFalse(inputState.getBoolean(ComposingStarted));
			else
				assertTrue(inputState.getBoolean(ComposingStarted));
		}
		if (inputState.getBoolean(ComposingStarted)) {
			assertEquals(vowels, compoState.getString(Vowels));
			assertEquals(tone, (int)compoState.getInteger(Tone));
			assertEquals(afters, compoState.getString(Afters));
			assertEquals(glotal, (char)compoState.getCharacter(Glottal));
			assertEquals(pojNN, (boolean)compoState.getBoolean(PojNN));

			assertEquals(start, (int)viewState.getInteger(ComposingStart));
			assertEquals(end, (int)viewState.getInteger(ComposingEnd));
			assertEquals(composingText, viewState.getString(ComposingText));
		}
		assertFalse(viewState.isAnyChanged(SelectionStart, SelectionEnd));
		if (deleteBefore > 0) {
			assertTrue(viewState.contains(DeleteBefore));
			assertEquals(deleteBefore, (int)viewState.getInteger(DeleteBefore));
		} else {
			assertFalse(viewState.contains(DeleteBefore));
		}
		if (deleteAfter > 0) {
			assertTrue(viewState.contains(DeleteAfter));
			assertEquals(deleteAfter, (int)viewState.getInteger(DeleteAfter));
		} else {
			assertFalse(viewState.contains(DeleteAfter));
		}
		if (commitText != null) {
			assertTrue(viewState.contains(CommitText));
			assertEquals(commitText, viewState.getString(CommitText));
		} else {
			assertFalse(viewState.contains(CommitText));
		}
	}

	@Test
	public void consonantAfterVowels() {
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, true));
		// Sù
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Sù");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "u", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Sù");
		// to Sù-b
		ims.updateStateOnKey(inputConnection, 'b');
		checkComposingState("b", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, "Sù-", "b");
	}

	@Test
	public void vowelAfterAfters() {
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, true));
		// Lâng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Lâng");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "a", 5, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Lâng");
		// to Lâng-u
		ims.updateStateOnKey(inputConnection, 'u');
		checkComposingState("", "u", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, "Lâng-", "u");
	}

	@Test
	public void vowelAfterNasalWithEmptyVowel() {
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, true));
		// Ln̄g
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ln̄g");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "", 7, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Ln̄g");
		// to Ln̄g-oo
		ims.updateStateOnText(inputConnection, "oo");
		checkComposingState("", "oo", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, "Ln̄g-", "oo");
	}

	@Test
	public void consonantAfterAfters() {
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, true));
		// Sam
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Sam");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "a", 1, "m", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Sam");
		// to Sam-s
		ims.updateStateOnKey(inputConnection, 's');
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, "Sam-", "s");
	}

	@Test
	public void consonantAfterNasalWithEmptyVowel() {
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, true));
		// N̂g
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("N̂g");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 5, "Ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "N̂g");
		// to N̂g-j
		ims.updateStateOnKey(inputConnection, 'j');
		checkComposingState("j", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, "N̂g-", "j");
	}

	@Test
	public void consonantAfterGlotal() {
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, true));
		// Bah
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Bah");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "a", 4, "", 'h', false, 0, 3, 0, 0, null, "Bah");
		// to Bah-ch
		ims.updateStateOnText(inputConnection, "ch");
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, "Bah-", "ch");
	}

	@Test
	public void vowelAfterGlotal() {
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, true));
		// Lok
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Lok");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "o", 4, "", 'k', false, 0, 3, 0, 0, null, "Lok");
		// to Lok-a
		ims.updateStateOnKey(inputConnection, 'a');
		checkComposingState("", "a", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, "Lok-", "a");
	}

	@Test
	public void glotalAfterGlotal() {
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, true));
		ims.setInputState(ims.getInputState().setBoolean(GlottalToggleTone, false));
		ims.setInputState(ims.getInputState().setBoolean(GlottalReplaceGlottal, false));
		// Sok
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Sok");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "o", 4, "", 'k', false, 0, 3, 0, 0, null, "Sok");
		// to Sok-k
		ims.updateStateOnKey(inputConnection, 'k');
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, "Sok-", "k");
	}

	@Test
	public void consonantAfterPojNN() {
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, true));
		ims.setInputState(ims.getInputState().setInteger(Subtype, SUBTYPE_POJ));
		// Sòaⁿ
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Sòaⁿ");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "oa", 3, "", GLOTTAL_NULL, true, 0, 4, 0, 0, null, "Sòaⁿ");
		// to Sòaⁿ-s
		ims.updateStateOnKey(inputConnection, 's');
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, "Sòaⁿ-", "s");
	}

	@Test
	public void vowelAfterPojNN() {
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, true));
		// Soâiⁿ
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Soâiⁿ");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "oai", 5, "", GLOTTAL_NULL, true, 0, 5, 0, 0, null, "Soâiⁿ");
		// to Soâiⁿ-a
		ims.updateStateOnKey(inputConnection, 'a');
		checkComposingState("", "a", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, "Soâiⁿ-", "a");
	}
}
