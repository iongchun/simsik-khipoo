/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.SharedPreferences;
import androidx.annotation.Nullable;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static tw.iongchun.taigikbd.TKComposingUtils.TONE_MARK_POJ;
import static tw.iongchun.taigikbd.TKComposingUtils.TONE_MARK_SUBTYPE;
import static tw.iongchun.taigikbd.TKComposingUtils.TONE_MARK_TL;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_VOWELS_CHAR;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_VOWELS_SINGLE;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_VOWELS_WHOLE;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_MULTICHAR_VOWEL;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_VOWELS;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_VOWEL_CLUSTER;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_POJ_TONE_MARK;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_TONE_MARK_STYLE;

/**
 * Created by iongchun on 7/4/17.
 */

@RunWith(AndroidJUnit4.class)
public class PreferenceTest {
	private TKInputMethodService ims;
	private SharedPreferences pref;

	@Before
	public void initMockito() {
		MockitoAnnotations.initMocks(this);
	}

	@Before
	public void initIMS() {
		ims = new TKInputMethodService();
	}

	@Before
	public void initMockSharedPreferences() {
		pref = new SharedPreferences() {
			private HashMap<String, Object> values = new HashMap<>();

			@Override
			public Map<String, ?> getAll() {
				return values;
			}

			@Nullable
			@Override
			public String getString(String key, @Nullable String defValue) {
				return (String)values.get(key);
			}

			@Nullable
			@Override
			public Set<String> getStringSet(String key, @Nullable Set<String> defValues) {
				return (Set<String>)values.get(key);
			}

			@Override
			public int getInt(String key, int defValue) {
				return (int)values.get(key);
			}

			@Override
			public long getLong(String key, long defValue) {
				return (long)values.get(key);
			}

			@Override
			public float getFloat(String key, float defValue) {
				return (float)values.get(key);
			}

			@Override
			public boolean getBoolean(String key, boolean defValue) {
				return (boolean)values.get(key);
			}

			@Override
			public boolean contains(String key) {
				return values.containsKey(key);
			}

			@Override
			public Editor edit() {
				return new Editor() {
					private HashMap<String, Object> tmps = new HashMap<>();
					private HashSet<String> dels = new HashSet<>();

					@Override
					public Editor putString(String key, @Nullable String value) {
						tmps.put(key, value);
						dels.remove(key);
						return this;
					}

					@Override
					public Editor putStringSet(String key, @Nullable Set<String> value) {
						tmps.put(key, value);
						dels.remove(key);
						return this;
					}

					@Override
					public Editor putInt(String key, int value) {
						tmps.put(key, value);
						dels.remove(key);
						return this;
					}

					@Override
					public Editor putLong(String key, long value) {
						tmps.put(key, value);
						dels.remove(key);
						return this;
					}

					@Override
					public Editor putFloat(String key, float value) {
						tmps.put(key, value);
						dels.remove(key);
						return this;
					}

					@Override
					public Editor putBoolean(String key, boolean value) {
						tmps.put(key, value);
						dels.remove(key);
						return this;
					}

					@Override
					public Editor remove(String key) {
						tmps.remove(key);
						dels.add(key);
						return this;
					}

					@Override
					public Editor clear() {
						tmps.clear();
						dels.addAll(values.keySet());
						return this;
					}

					@Override
					public boolean commit() {
						apply();
						return true;
					}

					@Override
					public void apply() {
						values.putAll(tmps);
						tmps.clear();
						for (String key : dels)
							values.remove(key);
						dels.clear();
					}
				};
			}

			@Override
			public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {

			}

			@Override
			public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {

			}
		};
	}

	@Test
	public void migrateDeleteVowelsMode() {
		pref.edit().putString(PREF_DEL_VOWELS, DEL_VOWELS_CHAR)
		    .putBoolean(PREF_DEL_MULTICHAR_VOWEL, false)
		    .putBoolean(PREF_DEL_VOWEL_CLUSTER, false)
		    .apply();
		assertFalse(pref.getBoolean(PREF_DEL_MULTICHAR_VOWEL, true));
		assertFalse(pref.getBoolean(PREF_DEL_VOWEL_CLUSTER, true));
		ims.migrateDelVowels(pref);
		assertFalse(pref.contains(PREF_DEL_MULTICHAR_VOWEL));
		assertFalse(pref.contains(PREF_DEL_VOWEL_CLUSTER));
		assertEquals(DEL_VOWELS_CHAR, pref.getString(PREF_DEL_VOWELS, null));

		pref.edit().putString(PREF_DEL_VOWELS, DEL_VOWELS_WHOLE)
		    .putBoolean(PREF_DEL_MULTICHAR_VOWEL, true)
		    .putBoolean(PREF_DEL_VOWEL_CLUSTER, false)
		    .apply();
		assertTrue(pref.getBoolean(PREF_DEL_MULTICHAR_VOWEL, false));
		assertFalse(pref.getBoolean(PREF_DEL_VOWEL_CLUSTER, true));
		ims.migrateDelVowels(pref);
		assertFalse(pref.contains(PREF_DEL_MULTICHAR_VOWEL));
		assertFalse(pref.contains(PREF_DEL_VOWEL_CLUSTER));
		assertEquals(DEL_VOWELS_SINGLE, pref.getString(PREF_DEL_VOWELS, null));

		pref.edit().putString(PREF_DEL_VOWELS, DEL_VOWELS_SINGLE)
		    .putBoolean(PREF_DEL_MULTICHAR_VOWEL, false)
		    .putBoolean(PREF_DEL_VOWEL_CLUSTER, true)
		    .apply();
		assertFalse(pref.getBoolean(PREF_DEL_MULTICHAR_VOWEL, true));
		assertTrue(pref.getBoolean(PREF_DEL_VOWEL_CLUSTER, false));
		ims.migrateDelVowels(pref);
		assertFalse(pref.contains(PREF_DEL_MULTICHAR_VOWEL));
		assertFalse(pref.contains(PREF_DEL_VOWEL_CLUSTER));
		assertEquals(DEL_VOWELS_WHOLE, pref.getString(PREF_DEL_VOWELS, null));

		pref.edit().putString(PREF_DEL_VOWELS, DEL_VOWELS_CHAR)
		    .putBoolean(PREF_DEL_MULTICHAR_VOWEL, true)
		    .putBoolean(PREF_DEL_VOWEL_CLUSTER, true)
		    .apply();
		assertTrue(pref.getBoolean(PREF_DEL_MULTICHAR_VOWEL, false));
		assertTrue(pref.getBoolean(PREF_DEL_VOWEL_CLUSTER, false));
		ims.migrateDelVowels(pref);
		assertFalse(pref.contains(PREF_DEL_MULTICHAR_VOWEL));
		assertFalse(pref.contains(PREF_DEL_VOWEL_CLUSTER));
		assertEquals(DEL_VOWELS_WHOLE, pref.getString(PREF_DEL_VOWELS, null));
	}

	@Test
	public void migrateToneMarkStyle() {
		// PojToneMark is false with TL and without POJ (default)
		pref.edit().putString(PREF_TONE_MARK_STYLE, TONE_MARK_SUBTYPE)
		    .putBoolean(PREF_POJ_TONE_MARK, false)
		    .apply();
		assertFalse(pref.getBoolean(PREF_POJ_TONE_MARK, true));
		ims.setSubtypeTailoEnabledMock(true);
		ims.setSubtypePojEnabledMock(false);
		ims.migrateToneMarkStyle(pref);
		assertFalse(pref.contains(PREF_POJ_TONE_MARK));
		assertEquals(TONE_MARK_SUBTYPE, pref.getString(PREF_TONE_MARK_STYLE, null));

		// PojToneMark is false with TL and with POJ
		pref.edit().putString(PREF_TONE_MARK_STYLE, TONE_MARK_SUBTYPE)
		    .putBoolean(PREF_POJ_TONE_MARK, false)
		    .apply();
		assertFalse(pref.getBoolean(PREF_POJ_TONE_MARK, true));
		ims.setSubtypeTailoEnabledMock(true);
		ims.setSubtypePojEnabledMock(true);
		ims.migrateToneMarkStyle(pref);
		assertFalse(pref.contains(PREF_POJ_TONE_MARK));
		assertEquals(TONE_MARK_SUBTYPE, pref.getString(PREF_TONE_MARK_STYLE, null));

		// PojToneMark is false with POJ and without TL
		pref.edit().putString(PREF_TONE_MARK_STYLE, TONE_MARK_SUBTYPE)
		    .putBoolean(PREF_POJ_TONE_MARK, false)
		    .apply();
		assertFalse(pref.getBoolean(PREF_POJ_TONE_MARK, true));
		ims.setSubtypeTailoEnabledMock(false);
		ims.setSubtypePojEnabledMock(true);
		ims.migrateToneMarkStyle(pref);
		assertFalse(pref.contains(PREF_POJ_TONE_MARK));
		assertEquals(TONE_MARK_TL, pref.getString(PREF_TONE_MARK_STYLE, null));

		// PojToneMark is false without TL and without POJ ?!
		pref.edit().putString(PREF_TONE_MARK_STYLE, TONE_MARK_SUBTYPE)
		    .putBoolean(PREF_POJ_TONE_MARK, false)
		    .apply();
		assertFalse(pref.getBoolean(PREF_POJ_TONE_MARK, true));
		ims.setSubtypeTailoEnabledMock(false);
		ims.setSubtypePojEnabledMock(false);
		ims.migrateToneMarkStyle(pref);
		assertFalse(pref.contains(PREF_POJ_TONE_MARK));
		assertEquals(TONE_MARK_SUBTYPE, pref.getString(PREF_TONE_MARK_STYLE, null));

		// PojToneMark is true with TL and without POJ
		pref.edit().putString(PREF_TONE_MARK_STYLE, TONE_MARK_SUBTYPE)
		    .putBoolean(PREF_POJ_TONE_MARK, true)
		    .apply();
		assertTrue(pref.getBoolean(PREF_POJ_TONE_MARK, false));
		ims.setSubtypeTailoEnabledMock(true);
		ims.setSubtypePojEnabledMock(false);
		ims.migrateToneMarkStyle(pref);
		assertFalse(pref.contains(PREF_POJ_TONE_MARK));
		assertEquals(TONE_MARK_POJ, pref.getString(PREF_TONE_MARK_STYLE, null));

		// PojToneMark is true with POJ and without TL
		pref.edit().putString(PREF_TONE_MARK_STYLE, TONE_MARK_SUBTYPE)
		    .putBoolean(PREF_POJ_TONE_MARK, true)
		    .apply();
		assertTrue(pref.getBoolean(PREF_POJ_TONE_MARK, false));
		ims.setSubtypeTailoEnabledMock(false);
		ims.setSubtypePojEnabledMock(true);
		ims.migrateToneMarkStyle(pref);
		assertFalse(pref.contains(PREF_POJ_TONE_MARK));
		assertEquals(TONE_MARK_SUBTYPE, pref.getString(PREF_TONE_MARK_STYLE, null));

		// PojToneMark is true with TL and with POJ
		pref.edit().putString(PREF_TONE_MARK_STYLE, TONE_MARK_SUBTYPE)
		    .putBoolean(PREF_POJ_TONE_MARK, true)
		    .apply();
		assertTrue(pref.getBoolean(PREF_POJ_TONE_MARK, false));
		ims.setSubtypeTailoEnabledMock(true);
		ims.setSubtypePojEnabledMock(true);
		ims.migrateToneMarkStyle(pref);
		assertFalse(pref.contains(PREF_POJ_TONE_MARK));
		assertEquals(TONE_MARK_POJ, pref.getString(PREF_TONE_MARK_STYLE, null));

		// PojToneMark is true without POJ and without TL ?!
		pref.edit().putString(PREF_TONE_MARK_STYLE, TONE_MARK_SUBTYPE)
		    .putBoolean(PREF_POJ_TONE_MARK, true)
		    .apply();
		assertTrue(pref.getBoolean(PREF_POJ_TONE_MARK, false));
		ims.setSubtypeTailoEnabledMock(false);
		ims.setSubtypePojEnabledMock(false);
		ims.migrateToneMarkStyle(pref);
		assertFalse(pref.contains(PREF_POJ_TONE_MARK));
		assertEquals(TONE_MARK_SUBTYPE, pref.getString(PREF_TONE_MARK_STYLE, null));
	}
}
