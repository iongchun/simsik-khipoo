/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static tw.iongchun.taigikbd.TKComposingUtils.AFTERS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.GLOTTAL_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.LEADS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.VOWELS_NULL;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_POJ;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_TL;
import static tw.iongchun.taigikbd.TKInputState.Afters;
import static tw.iongchun.taigikbd.TKInputState.AutoDash;
import static tw.iongchun.taigikbd.TKInputState.ComposingMode;
import static tw.iongchun.taigikbd.TKInputState.ComposingStarted;
import static tw.iongchun.taigikbd.TKInputState.ComposingState;
import static tw.iongchun.taigikbd.TKInputState.Glottal;
import static tw.iongchun.taigikbd.TKInputState.Hat;
import static tw.iongchun.taigikbd.TKInputState.InputStarted;
import static tw.iongchun.taigikbd.TKInputState.Leads;
import static tw.iongchun.taigikbd.TKInputState.PojNN;
import static tw.iongchun.taigikbd.TKInputState.TextMode;
import static tw.iongchun.taigikbd.TKInputState.Tone;
import static tw.iongchun.taigikbd.TKInputState.Vowels;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_UI_LOCALE;
import static tw.iongchun.taigikbd.TKViewState.CommitText;
import static tw.iongchun.taigikbd.TKViewState.ComposingCursor;
import static tw.iongchun.taigikbd.TKViewState.ComposingEnd;
import static tw.iongchun.taigikbd.TKViewState.ComposingStart;
import static tw.iongchun.taigikbd.TKViewState.ComposingText;
import static tw.iongchun.taigikbd.TKViewState.DeleteAfter;
import static tw.iongchun.taigikbd.TKViewState.DeleteBefore;
import static tw.iongchun.taigikbd.TKViewState.SelectionEnd;
import static tw.iongchun.taigikbd.TKViewState.SelectionStart;

/**
 * Created by iongchun on 7/7/17.
 */

@RunWith(AndroidJUnit4.class)
public class AutoCorrectionTest {
	private static final String INPUT_TITLE_TL = "SimSik:TL";
	private static final String INPUT_TITLE_POJ = "SimSik:POJ";
	private TKInputMethodService ims;
	@Mock
	private Context context;
	@Mock
	private SharedPreferences preferences;
	@Mock
	private Resources resources;
	@Mock
	private InputConnection inputConnection;
	@Mock
	private EditorInfo editorInfo;

	@Before
	public void init() {
		initMockito();
		initIMS();
	}

	@After
	public void fine() {
		fineIMS();
	}

	private void initMockito() {
		MockitoAnnotations.initMocks(this);
	}

	private void initIMS() {
		ims = new TKInputMethodService();
		when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(preferences);
		when(preferences.getString(eq(PREF_UI_LOCALE), anyString())).thenReturn(null);
		when(context.getResources()).thenReturn(resources);
		when(resources.getString(eq(R.string.input_title_tl))).thenReturn(INPUT_TITLE_TL);
		when(resources.getString(eq(R.string.input_title_poj))).thenReturn(INPUT_TITLE_POJ);
		ims.attachBaseContext(context);

		ims.initInputState();
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, false));
		ims.initViewState();

		ims.startInputState();

		editorInfo.initialSelStart = 0;
		editorInfo.initialSelEnd = 0;
		editorInfo.inputType = InputType.TYPE_CLASS_TEXT
		                       | InputType.TYPE_TEXT_VARIATION_NORMAL
		                       | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
		                       | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT;
		editorInfo.initialCapsMode = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
		ims.startViewState(editorInfo);
		ims.switchSubtype(SUBTYPE_TL);
		ims.updateMainKeyboard();
		ims.switchInputType(inputConnection, editorInfo);
	}

	private void fineIMS() {
		ims.finishInputState();
		ims.fineViewState();
		ims.fineInputState();
	}

	private void checkComposingState(
		String leads, String vowels, int tone,
		String afters, char glotal, boolean pojNN,
		int start, int end,
		int deleteBefore, int deleteAfter,
		String commitText,
		String composingText) {
		checkComposingState("", leads, vowels, tone, afters, glotal, pojNN,
		                    start, end, deleteBefore, deleteAfter,
		                    commitText, composingText);
	}

	private void checkComposingState(
		String hat, String leads, String vowels, int tone,
		String afters, char glotal, boolean pojNN,
		int start, int end,
		int deleteBefore, int deleteAfter,
		String commitText,
		String composingText) {
		TKInputState inputState = ims.getInputState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));

		TKInputState compoState = inputState.getState(ComposingState);
		if (leads != null) {
			if (leads == LEADS_NULL && hat.isEmpty())
				assertFalse(inputState.getBoolean(ComposingStarted));
			else
				assertTrue(inputState.getBoolean(ComposingStarted));
		}
		assertEquals(hat, compoState.getString(Hat));
		checkState(compoState, leads, vowels, tone, afters, glotal, pojNN);

		TKViewState viewState = ims.getViewState();
		assertEquals(start, (int)viewState.getInteger(ComposingStart));
		assertEquals(end, (int)viewState.getInteger(ComposingEnd));
		assertEquals(composingText, viewState.getString(ComposingText));

		assertFalse(viewState.isAnyChanged(SelectionStart, SelectionEnd));
		if (deleteBefore > 0) {
			assertTrue(viewState.contains(DeleteBefore));
			assertEquals(deleteBefore, (int)viewState.getInteger(DeleteBefore));
		} else {
			assertFalse(viewState.contains(DeleteBefore));
		}
		if (deleteAfter > 0) {
			assertTrue(viewState.contains(DeleteAfter));
			assertEquals(deleteAfter, (int)viewState.getInteger(DeleteAfter));
		} else {
			assertFalse(viewState.contains(DeleteAfter));
		}
		if (commitText != null) {
			assertTrue(viewState.contains(CommitText));
			assertEquals(commitText, viewState.getString(CommitText));
		} else {
			assertFalse(viewState.contains(CommitText));
		}
	}

	private void checkComposingRegion(int cursor,
	                                  String leads, String vowels, int tone,
	                                  String afters, char glotal, boolean pojNN) {
		checkState(ims.getComposingRegion(), leads, vowels, tone, afters, glotal, pojNN);
		TKViewState viewState = ims.getViewState();
		if (cursor >= 0) {
			assertEquals(cursor, (int)viewState.getInteger(ComposingCursor));
			int start = viewState.getInteger(ComposingStart);
			assertEquals(start + cursor, (int)viewState.getInteger(SelectionStart));
		} else {
			assertFalse(viewState.contains(ComposingCursor));
		}
	}

	private void checkState(TKInputState state,
	                        String leads, String vowels, int tone,
	                        String afters, char glotal, boolean pojNN) {
		if (leads != null)
			assertEquals(leads, state.getString(Leads));
		assertEquals(vowels, state.getString(Vowels));
		assertEquals(tone, (int)state.getInteger(Tone));
		assertEquals(afters, state.getString(Afters));
		assertEquals(glotal, (char)state.getCharacter(Glottal));
		assertEquals(pojNN, (boolean)state.getBoolean(PojNN));
	}

	@Test
	public void oongToOng() {
		// Ông
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ôong");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Oo", 5, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Ôong");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Ông-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Ông-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Liông
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Liôong");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "ioo", 5, "ng", GLOTTAL_NULL, false, 0, 6, 0, 0, null, "Liôong");
		ims.updateStateOnKey(inputConnection, ' ');
		assertEquals("Liông ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Só͘ng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Só͘ng");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "o͘", 2, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Só͘ng");
		ims.updateStateOnKey(inputConnection, 'b');
		assertEquals("Sóng", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("b", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 4, 5, 0, 0, null, "b");
	}

	@Test
	public void oonnToOnn() {
		// ionn
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ioonn");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "ioo", 1, "nn", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "ioonn");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("ionn-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "ionn-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// honnh
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ho͘hⁿ");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("H", "o͘", 4, "", 'h', true, 0, 5, 0, 0, null, "Ho͘hⁿ");
		ims.updateStateOnKey(inputConnection, '!');
		assertEquals("Hohⁿ!", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// kônn
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kôonn");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", "oo", 5, "nn", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "kôonn");
		ims.updateStateOnKey(inputConnection, 'i');
		assertEquals("kônn", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 4, 5, 0, 0, null, "i");
	}

	@Test
	public void oomToOm() {
		// Ōm
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ōom");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Oo", 7, "m", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Ōom");
		ims.updateStateOnKey(inputConnection, '.');
		assertEquals("Ōm.", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Som
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("So͘m");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "o͘", 1, "m", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "So͘m");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Som-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Som-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// tôm
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tôom");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "oo", 5, "m", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tôom");
		ims.updateStateOnText(inputConnection, "ng");
		assertEquals("tôm", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "ng", GLOTTAL_NULL, false, 3, 5, 0, 0, null, "ng");
	}

	@Test
	public void ookToOk() {
		// Ok
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("O͘k");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "O͘", 4, "", 'k', false, 0, 3, 0, 0, null, "O͘k");
		ims.updateStateOnText(inputConnection, "ch");
		assertEquals("Ok", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 4, 0, 0, null, "ch");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Sok
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Sook");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("S", "oo", 4, "", 'k', false, 0, 4, 0, 0, null, "Sook");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Sok-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Sok-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Bo̍k
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Bo̍ok");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "oo", 8, "", 'k', false, 0, 5, 0, 0, null, "Bo̍ok");
		ims.updateStateOnKey(inputConnection, ' ');
		assertEquals("Bo̍k ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
	}

	@Test
	public void oopToOp() {
		// Hop
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Hoop");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		checkComposingState("H", "oo", 4, "", 'p', false, 0, 4, 0, 0, null, "Hoop");
		ims.updateView(inputConnection);
		ims.updateStateOnText(inputConnection, "-");
		ims.updateView(inputConnection);
		checkComposingState("Hop-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Hop-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Ko̍p
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ko̍op");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("K", "oo", 8, "", 'p', false, 0, 5, 0, 0, null, "Ko̍op");
		ims.updateStateOnKey(inputConnection, 'l');
		assertEquals("Ko̍p", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("l", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 4, 5, 0, 0, null, "l");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Chhop
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chho͘p");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "o͘", 4, "", 'p', false, 0, 6, 0, 0, null, "Chho͘p");
		ims.updateStateOnKey(inputConnection, ' ');
		assertEquals("Chhop ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
	}

	@Test
	public void eekToEk() {
		// Ek
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Eek");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		checkComposingState("", "Ee", 4, "", 'k', false, 0, 3, 0, 0, null, "Eek");
		ims.updateView(inputConnection);
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Ek-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Ek-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Lek
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Leek");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "ee", 4, "", 'k', false, 0, 4, 0, 0, null, "Leek");
		ims.updateStateOnKey(inputConnection, 's');
		assertEquals("Lek", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 3, 4, 0, 0, null, "s");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Tshe̍k
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tshe̍ek");
		ims.updateStateOnSelection(inputConnection, 7, 7, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Tsh", "ee", 8, "", 'k', false, 0, 7, 0, 0, null, "Tshe̍ek");
		ims.updateStateOnText(inputConnection, "ts");
		assertEquals("Tshe̍k", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 6, 8, 0, 0, null, "ts");
	}

	@Test
	public void eengToEng() {
		// Peng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Peeng");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("P", "ee", 1, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Peeng");
		ims.updateStateOnKey(inputConnection, ' ');
		assertEquals("Peng ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Léng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Léeng");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "ee", 2, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Léeng");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Léng-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Léng-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Bêng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Bêeng");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "ee", 5, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Bêeng");
		ims.updateStateOnText(inputConnection, "ng");
		assertEquals("Bêng", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "ng", GLOTTAL_NULL, false, 4, 6, 0, 0, null, "ng");
	}

	@Test
	public void oaToUa() {
		// Jua̍h
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Joa̍h");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("J", "oa", 8, "", 'h', false, 0, 5, 0, 0, null, "Joa̍h");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Jua̍h-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "Jua̍h-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Huâinn
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Hoâinn");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("H", "oai", 5, "nn", GLOTTAL_NULL, false, 0, 6, 0, 0, null, "Hoâinn");
		ims.updateStateOnText(inputConnection, "g");
		assertEquals("Huâinn", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("g", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 6, 7, 0, 0, null, "g");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Buâ
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Boâ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "oa", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Boâ");
		ims.updateStateOnKey(inputConnection, ' ');
		assertEquals("Buâ ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
	}

	@Test
	public void oeToUe() {
		// Bue̍h
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Boe̍h");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "oe", 8, "", 'h', false, 0, 5, 0, 0, null, "Boe̍h");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Bue̍h-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "Bue̍h-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Juê
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Joê");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("J", "oe", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Joê");
		ims.updateStateOnKey(inputConnection, ' ');
		assertEquals("Juê ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// uē
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("oē");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "oe", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "oē");
		ims.updateStateOnText(inputConnection, "ts");
		assertEquals("uē", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 4, 0, 0, null, "ts");
	}

	@Test
	public void pojUaToOa() {
		ims.switchSubtype(SUBTYPE_POJ);

		// Joa̍h
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Jua̍h");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("J", "ua", 8, "", 'h', false, 0, 5, 0, 0, null, "Jua̍h");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Joa̍h-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "Joa̍h-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Hoâiⁿ
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Huâiⁿ");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("H", "uai", 5, "", GLOTTAL_NULL, true, 0, 5, 0, 0, null, "Huâiⁿ");
		ims.updateStateOnText(inputConnection, "g");
		assertEquals("Hoâiⁿ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("g", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 5, 6, 0, 0, null, "g");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Bôa
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Buâ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "ua", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Bûa");
		ims.updateStateOnKey(inputConnection, ' ');
		assertEquals("Bôa ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
	}

	@Test
	public void pojUeToOe() {
		ims.switchSubtype(SUBTYPE_POJ);

		// Boe̍h
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Bue̍h");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "ue", 8, "", 'h', false, 0, 5, 0, 0, null, "Bue̍h");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Boe̍h-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "Boe̍h-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Jôe
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Juê");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("J", "ue", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Jûe");
		ims.updateStateOnKey(inputConnection, ' ');
		assertEquals("Jôe ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// ōe
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("uē");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "ue", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ūe");
		ims.updateStateOnText(inputConnection, "ch");
		assertEquals("ōe", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 4, 0, 0, null, "ch");
	}

	@Test
	public void pojIkToEk() {
		ims.switchSubtype(SUBTYPE_POJ);

		// Be̍k
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Bi̍k");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", "i", 8, "", 'k', false, 0, 4, 0, 0, null, "Bi̍k");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Be̍k-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Be̍k-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// hek
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hik");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "i", 4, "", 'k', false, 0, 3, 0, 0, null, "hik");
		ims.updateStateOnKey(inputConnection, 's');
		assertEquals("hek", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 3, 4, 0, 0, null, "s");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Chhe̍k
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chhi̍k");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "i", 8, "", 'k', false, 0, 6, 0, 0, null, "Chhi̍k");
		ims.updateStateOnText(inputConnection, "ch");
		assertEquals("Chhe̍k", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 6, 8, 0, 0, null, "ch");
	}

	@Test
	public void pojIngToEng() {
		ims.switchSubtype(SUBTYPE_POJ);

		// Gêng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Gîng");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("G", "i", 5, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Gîng");
		ims.updateStateOnKey(inputConnection, ' ');
		assertEquals("Gêng ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Éng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Íng");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "I", 2, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Íng");
		ims.updateStateOnKey(inputConnection, '-');
		ims.updateView(inputConnection);
		checkComposingState("Éng-", LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Éng-");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);

		// Khèng
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Khìng");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Kh", "i", 3, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Khìng");
		ims.updateStateOnText(inputConnection, "ng");
		assertEquals("Khèng", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "ng", GLOTTAL_NULL, false, 5, 7, 0, 0, null, "ng");
	}

	@Test
	public void dontAutoCorrectInWord() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [Gîn_g]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Gîn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("g");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("G", "i", 5, "n", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Gîng");
		checkComposingRegion(3, "G", "i", 5, "ng", GLOTTAL_NULL, false);
		// Gîn _[g]
		ims.updateStateOnKey(inputConnection, ' ');
		assertEquals("Gîn ", ims.getViewState().getString(CommitText));
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 4, 5, 0, 0, null, "g");
		checkComposingRegion(0, "g", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
		ims.updateStateOnSelection(inputConnection, 4, 4, 4, 5);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 4, 5, 0, 0, null, "g");
		checkComposingRegion(0, "g", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}
}
