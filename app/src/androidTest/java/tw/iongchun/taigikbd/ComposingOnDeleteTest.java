/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static tw.iongchun.taigikbd.TKComposingUtils.AFTERS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.GLOTTAL_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.LEADS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.VOWELS_NULL;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_LEADINGS_CHAR;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_LEADINGS_VALID;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_LEADINGS_WHOLE;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_VOWELS_CHAR;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_VOWELS_SINGLE;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_VOWELS_WHOLE;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_POJ;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_TL;
import static tw.iongchun.taigikbd.TKInputState.Afters;
import static tw.iongchun.taigikbd.TKInputState.CapState;
import static tw.iongchun.taigikbd.TKInputState.ComposingMode;
import static tw.iongchun.taigikbd.TKInputState.ComposingStarted;
import static tw.iongchun.taigikbd.TKInputState.ComposingState;
import static tw.iongchun.taigikbd.TKInputState.DeleteAfterCluster;
import static tw.iongchun.taigikbd.TKInputState.DeleteLeadingsMode;
import static tw.iongchun.taigikbd.TKInputState.DeleteVowelsMode;
import static tw.iongchun.taigikbd.TKInputState.Glottal;
import static tw.iongchun.taigikbd.TKInputState.InputStarted;
import static tw.iongchun.taigikbd.TKInputState.Leads;
import static tw.iongchun.taigikbd.TKInputState.PojNN;
import static tw.iongchun.taigikbd.TKInputState.TextMode;
import static tw.iongchun.taigikbd.TKInputState.Tone;
import static tw.iongchun.taigikbd.TKInputState.Vowels;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_UI_LOCALE;
import static tw.iongchun.taigikbd.TKViewState.CommitText;
import static tw.iongchun.taigikbd.TKViewState.ComposingCursor;
import static tw.iongchun.taigikbd.TKViewState.ComposingEnd;
import static tw.iongchun.taigikbd.TKViewState.ComposingStart;
import static tw.iongchun.taigikbd.TKViewState.ComposingText;
import static tw.iongchun.taigikbd.TKViewState.DeleteAfter;
import static tw.iongchun.taigikbd.TKViewState.DeleteBefore;
import static tw.iongchun.taigikbd.TKViewState.SelectionEnd;
import static tw.iongchun.taigikbd.TKViewState.SelectionStart;
import static tw.iongchun.taigikbd.TKViewState.ShiftState;

/**
 * Created by iongchun on 6/28/17.
 */

@RunWith(AndroidJUnit4.class)
public class ComposingOnDeleteTest {
	private static final String INPUT_TITLE_TL = "SimSik:TL";
	private static final String INPUT_TITLE_POJ = "SimSik:POJ";
	private TKInputMethodService ims;
	@Mock
	private Context context;
	@Mock
	private SharedPreferences preferences;
	@Mock
	private Resources resources;
	@Mock
	private InputConnection inputConnection;
	@Mock
	private EditorInfo editorInfo;

	@Before
	public void init() {
		initMockito();
		initIMS();
	}

	@After
	public void fine() {
		fineIMS();
	}

	private void initMockito() {
		MockitoAnnotations.initMocks(this);
	}

	private void initIMS() {
		ims = new TKInputMethodService();
		when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(preferences);
		when(preferences.getString(eq(PREF_UI_LOCALE), anyString())).thenReturn(null);
		when(context.getResources()).thenReturn(resources);
		when(resources.getString(eq(R.string.input_title_tl))).thenReturn(INPUT_TITLE_TL);
		when(resources.getString(eq(R.string.input_title_poj))).thenReturn(INPUT_TITLE_POJ);
		ims.attachBaseContext(context);

		ims.initInputState();
		ims.initViewState();

		ims.startInputState();

		editorInfo.initialSelStart = 0;
		editorInfo.initialSelEnd = 0;
		editorInfo.inputType = InputType.TYPE_CLASS_TEXT
		                       | InputType.TYPE_TEXT_VARIATION_NORMAL
		                       | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
		editorInfo.initialCapsMode = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
		ims.startViewState(editorInfo);
		ims.switchSubtype(SUBTYPE_TL);
		ims.updateMainKeyboard();
		ims.switchInputType(inputConnection, editorInfo);
	}

	private void fineIMS() {
		ims.finishInputState();
		ims.fineViewState();
		ims.fineInputState();
	}

	private void checkComposingState(
		String vowels, int tone, String afters, char glotal,
		int start, int end,
		int deleteBefore, int deleteAfter,
		String commitText,
		String composingText) {
		checkComposingState(null, vowels, tone, afters, glotal, false,
		                    start, end,
		                    deleteBefore, deleteAfter,
		                    commitText, composingText);
	}

	private void checkComposingState(
		String leads, String vowels, int tone,
		String afters, char glotal, boolean pojNN,
		int start, int end,
		int deleteBefore, int deleteAfter,
		String commitText,
		String composingText) {
		TKInputState inputState = ims.getInputState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));

		TKInputState compoState = inputState.getState(ComposingState);
		if (leads != null) {
			if (leads == LEADS_NULL)
				assertFalse(inputState.getBoolean(ComposingStarted));
			else
				assertTrue(inputState.getBoolean(ComposingStarted));
		}
		checkState(compoState, leads, vowels, tone, afters, glotal, pojNN);

		TKViewState viewState = ims.getViewState();
		assertEquals(start, (int)viewState.getInteger(ComposingStart));
		assertEquals(end, (int)viewState.getInteger(ComposingEnd));
		assertEquals(composingText, viewState.getString(ComposingText));

		assertFalse(viewState.isAnyChanged(SelectionStart, SelectionEnd));
		if (deleteBefore > 0) {
			assertTrue(viewState.contains(DeleteBefore));
			assertEquals(deleteBefore, (int)viewState.getInteger(DeleteBefore));
		} else {
			assertFalse(viewState.contains(DeleteBefore));
		}
		if (deleteAfter > 0) {
			assertTrue(viewState.contains(DeleteAfter));
			assertEquals(deleteAfter, (int)viewState.getInteger(DeleteAfter));
		} else {
			assertFalse(viewState.contains(DeleteAfter));
		}
		if (commitText != null) {
			assertTrue(viewState.contains(CommitText));
			assertEquals(commitText, viewState.getString(CommitText));
		} else {
			assertFalse(viewState.contains(CommitText));
		}
	}

	private void checkComposingRegion(int cursor,
	                                  String leads, String vowels, int tone,
	                                  String afters, char glotal, boolean pojNN) {
		checkState(ims.getComposingRegion(), leads, vowels, tone, afters, glotal, pojNN);
		TKViewState viewState = ims.getViewState();
		if (cursor >= 0) {
			assertEquals(cursor, (int)viewState.getInteger(ComposingCursor));
			int start = viewState.getInteger(ComposingStart);
			assertEquals(start + cursor, (int)viewState.getInteger(SelectionStart));
		} else {
			assertFalse(viewState.contains(ComposingCursor));
		}
	}

	private void checkState(TKInputState state,
	                        String leads, String vowels, int tone,
	                        String afters, char glotal, boolean pojNN) {
		if (leads != null)
			assertEquals(leads, state.getString(Leads));
		assertEquals(vowels, state.getString(Vowels));
		assertEquals(tone, (int)state.getInteger(Tone));
		assertEquals(afters, state.getString(Afters));
		assertEquals(glotal, (char)state.getCharacter(Glottal));
		assertEquals(pojNN, (boolean)state.getBoolean(PojNN));
	}

	private void checkCursor(int pos) {
		TKViewState viewState = ims.getViewState();
		assertEquals(pos, (int)viewState.getInteger(SelectionStart));
		assertEquals(pos, (int)viewState.getInteger(SelectionEnd));
	}

	@Test
	public void toBeginComposing() {
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnKey(inputConnection, 'a');
		ims.updateView(inputConnection);
		checkComposingState("A", 1, AFTERS_NULL, GLOTTAL_NULL, 0, 1, 0, 0, null, "A");

		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("A");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("A", 1, AFTERS_NULL, GLOTTAL_NULL, 0, 1, 0, 0, null, "A");

		ims.updateStateOnKey(inputConnection, ' ');
		ims.updateView(inputConnection);
		checkComposingState(VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, -1, -1, 0, 0, null, "");

		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("A ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, -1, -1, 0, 0, null, "");

		ims.updateStateOnDelete(inputConnection);
		assertEquals(1, (int)ims.getViewState().getInteger(DeleteBefore));
		ims.updateView(inputConnection);
		checkComposingState("A", 1, AFTERS_NULL, GLOTTAL_NULL, 0, 1, 0, 0, null, "A");

		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("A");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("A", 1, AFTERS_NULL, GLOTTAL_NULL, 0, 1, 0, 0, null, "A");

		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		checkComposingState("A", 1, AFTERS_NULL, GLOTTAL_NULL, 0, 1, 0, 0, null, "A");
	}

	@Test
	public void toBeginComposing2() {
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("A ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, -1, -1, 0, 0, null, "");

		ims.updateStateOnDelete(inputConnection);
		assertEquals(1, (int)ims.getViewState().getInteger(DeleteBefore));
		ims.updateView(inputConnection);
		checkComposingState("A", 1, AFTERS_NULL, GLOTTAL_NULL, 0, 1, 0, 0, null, "A");

		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("A");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("A", 1, AFTERS_NULL, GLOTTAL_NULL, 0, 1, 0, 0, null, "A");
	}

	private void checkCapState(boolean value) {
		TKInputState inputState = ims.getInputState();
		assertEquals((boolean)inputState.getBoolean(CapState), value);
		TKViewState viewState = ims.getViewState();
		assertEquals((boolean)viewState.getBoolean(ShiftState), value);
	}

	@Test
	public void toBeginCap() {
		checkCapState(true);
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnKey(inputConnection, 'a');
		ims.updateView(inputConnection);
		checkComposingState("A", 1, AFTERS_NULL, GLOTTAL_NULL, 0, 1, 0, 0, null, "A");
		checkCapState(false);

		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("A");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("A", 1, AFTERS_NULL, GLOTTAL_NULL, 0, 1, 0, 0, null, "A");

		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, -1, -1, 0, 0, null, "");
		checkCapState(true);

		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, -1, -1, 0, 0, null, "");
		checkCapState(true);
	}

	@Test
	public void pojNnMovedToAfters() {
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kiⁿh");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, 4, 4, 0, 4);
		ims.updateView(inputConnection);
		checkComposingState("k", "i", 4, "ⁿ", 'h', false, 0, 4, 0, 0, null, "kiⁿh");

		ims.updateStateOnDelete(inputConnection);
		checkComposingState("k", "i", 1, "", GLOTTAL_NULL, true, 0, 4, 0, 0, null, "kiⁿ");
		ims.updateView(inputConnection);
		checkComposingState("k", "i", 1, "", GLOTTAL_NULL, true, 0, 3, 0, 0, null, "kiⁿ");

		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kiⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, 0, 3);
		ims.updateView(inputConnection);
		checkComposingState("k", "i", 1, "", GLOTTAL_NULL, true, 0, 3, 0, 0, null, "kiⁿ");
	}

	@Test
	public void onlyLeadsToneReset() {
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Lo");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("L", "o", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Lo");

		ims.updateStateOnDelete(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "L");
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "L");

		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("L");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "L");
	}

	@Test
	public void leadsByChar() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));

		// T
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Ph
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ph");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Ph", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ph");
		// to P
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("P", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "P");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("P");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("P", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "P");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Chh
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chh");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");
		// to Ch
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ch");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ch");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ch");
		// to C
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("C", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "C");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("C");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("C", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "C");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
	}

	@Test
	public void leadsByValid() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_VALID));

		// B
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("B");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("B", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "B");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Kh
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Kh");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Kh");
		// to K
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("K");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Chh
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chh");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");
		// to Ch
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ch");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ch");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ch");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Tsh
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tsh");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Tsh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Tsh");
		// to Ts
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ts");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");
		// to T
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
	}

	@Test
	public void leadsByWhole() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));

		// J
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("J");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("J", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "J");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Th
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Th");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Th", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Th");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Ts
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ts");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ts");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Chh
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chh");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
	}

	@Test
	public void vowelsByChar() {
		ims.setInputState(ims.getInputState().setString(DeleteVowelsMode, DEL_VOWELS_CHAR));

		// E
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("E");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "E", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "E");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Tê
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tê");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("T", "e", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Tê");
		// to T
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		// Chhài
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chhài");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "ai", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Chhài");
		// to Chhà
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "a", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Chhà");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chhà");
		ims.updateStateOnSelection(inputConnection, 4, 4, 0, 4);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "a", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Chhà");
		// to Chh
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chh");
		ims.updateStateOnSelection(inputConnection, 3, 3, 0, 3);
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");

		// Muâi
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Muâi");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("M", "uai", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Muâi");
		// to Muâ
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("M", "ua", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Muâ");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Muâ");
		ims.updateStateOnSelection(inputConnection, 3, 3, 0, 3);
		ims.updateView(inputConnection);
		checkComposingState("M", "ua", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Muâ");
		// to Mû
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("M", "u", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Mû");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Mû");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("M", "u", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Mû");
		// to M
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "M", GLOTTAL_NULL, false, 0, 1, 0, 0, null, "M");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("M");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "M", GLOTTAL_NULL, false, 0, 1, 0, 0, null, "M");

		// Iáu
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Iáu");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Iau", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Iáu");
		// to Iá
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "Ia", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Iá");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Iá");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("", "Ia", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Iá");
		// to Í
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "I", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Í");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Í");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("", "I", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Í");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Tir
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tir");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("T", "ir", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Tir");
		// to Ti
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("T", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ti");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ti");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("T", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ti");
		// to T
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		// Ker
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ker");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("K", "er", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Ker");
		// to Ke
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("K", "e", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ke");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ke");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("K", "e", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Ke");
		// to K
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("K");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");

		// Uēe
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Uēe");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Uee", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Uēe");
		// to Uē
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "Ue", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Uē");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Uē");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("", "Ue", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Uē");
		// to Ū
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "U", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Ū");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ū");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("", "U", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Ū");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Erê
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Erê");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Ere", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Erê");
		// to Êr
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "Er", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Êr");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Êr");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("", "Er", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Êr");
		// to Ê
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "E", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Ê");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ê");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("", "E", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Ê");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Lôo
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Lôo");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "oo", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Lôo");
		// to Lô
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("L", "o", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Lô");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Lô");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("L", "o", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Lô");
		// to L
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "L");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("L");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "L");

		// Khó͘
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Khó͘");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Kh", "o͘", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Khó͘");
		// to Kh
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Kh");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Kh");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Kh");
	}

	@Test
	public void vowelsBySingle() {
		ims.setInputState(ims.getInputState().setString(DeleteVowelsMode, DEL_VOWELS_SINGLE));

		// E
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("E");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "E", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "E");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Tê
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tê");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("T", "e", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Tê");
		// to T
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		// Chhài
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chhài");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "ai", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Chhài");
		// to Chhà
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "a", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Chhà");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chhà");
		ims.updateStateOnSelection(inputConnection, 4, 4, 0, 4);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "a", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Chhà");
		// to Chh
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chh");
		ims.updateStateOnSelection(inputConnection, 3, 3, 0, 3);
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");

		// Muâi
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Muâi");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("M", "uai", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Muâi");
		// to Muâ
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("M", "ua", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Muâ");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Muâ");
		ims.updateStateOnSelection(inputConnection, 3, 3, 0, 3);
		ims.updateView(inputConnection);
		checkComposingState("M", "ua", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Muâ");
		// to Mû
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("M", "u", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Mû");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Mû");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("M", "u", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Mû");
		// to M
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "M", GLOTTAL_NULL, false, 0, 1, 0, 0, null, "M");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("M");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "M", GLOTTAL_NULL, false, 0, 1, 0, 0, null, "M");

		// Iáu
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Iáu");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Iau", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Iáu");
		// to Iá
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "Ia", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Iá");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Iá");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("", "Ia", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Iá");
		// to Í
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "I", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Í");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Í");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("", "I", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Í");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Tir
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tir");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("T", "ir", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Tir");
		// to T
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		// Ker
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ker");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("K", "er", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Ker");
		// to K
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("K");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");

		// Uēe
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Uēe");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Uee", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Uēe");
		// to Ū
		ims.updateStateOnDelete(inputConnection);
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ū");
		ims.updateView(inputConnection);
		checkComposingState("", "U", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Ū");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("", "U", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "Ū");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Erê
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Erê");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Ere", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Erê");
		// to Êr
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "Er", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Êr");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Êr");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("", "Er", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Êr");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Lôo
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Lôo");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "oo", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Lôo");
		// to L
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "L");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("L");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "L");

		// Khó͘
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Khó͘");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Kh", "o͘", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Khó͘");
		// to Kh
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Kh");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Kh");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Kh");
	}

	@Test
	public void vowelsByWhole() {
		ims.setInputState(ims.getInputState().setString(DeleteVowelsMode, DEL_VOWELS_WHOLE));

		// E
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("E");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "E", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "E");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Tê
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tê");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("T", "e", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Tê");
		// to T
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		// Chhài
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chhài");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Chh", "ai", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "Chhài");
		// to Chh
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Chh");
		ims.updateStateOnSelection(inputConnection, 3, 3, 0, 3);
		ims.updateView(inputConnection);
		checkComposingState("Chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Chh");

		// Muâi
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Muâi");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("M", "uai", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Muâi");
		// to M
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "M", GLOTTAL_NULL, false, 0, 1, 0, 0, null, "M");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("M");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "M", GLOTTAL_NULL, false, 0, 1, 0, 0, null, "M");

		// Iáu
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Iáu");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Iau", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Iáu");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Tir
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Tir");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("T", "ir", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Tir");
		// to T
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("T");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("T", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "T");

		// Ker
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Ker");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("K", "er", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Ker");
		// to K
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("K");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("K", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "K");

		// Uēe
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Uēe");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Uee", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Uēe");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Erê
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Erê");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "Ere", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Erê");
		// to null
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");

		// Lôo
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Lôo");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("L", "oo", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "Lôo");
		// to L
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "L");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("L");
		ims.updateStateOnSelection(inputConnection, 1, 1, 0, 1);
		ims.updateView(inputConnection);
		checkComposingState("L", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "L");

		// Khó͘
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Khó͘");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("Kh", "o͘", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "Khó͘");
		// to Kh
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("Kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Kh");
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("Kh");
		ims.updateStateOnSelection(inputConnection, 2, 2, 0, 2);
		ims.updateView(inputConnection);
		checkComposingState("Kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "Kh");
	}

	@Test
	public void deleteLeadAfterEmptyBeforeEmptyWithBootToEmptyWithBoot() {
		ims.switchSubtype(SUBTYPE_POJ);
		// {[k]_-a}
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("k");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("-a");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "k-a");
		checkCursor(1);
		// {[]_-a}
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "-a");
		checkCursor(0);
	}

	@Test
	public void deleteLeadAfterEmptyBeforeLeadsToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [c_hāng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("c");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hāng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "chāng");
		checkComposingRegion(1, "ch", "a", 7, "ng", GLOTTAL_NULL, false);
		// _[hāng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "hāng");
		checkComposingRegion(0, "h", "a", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterEmptyBeforeLeadsToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// [ch_hāng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hāng");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "chhāng");
		checkComposingRegion(2, "chh", "a", 7, "ng", GLOTTAL_NULL, false);
		// _[hāng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "hāng");
		checkComposingRegion(0, "h", "a", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterEmptyBeforeLeadsToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [ts_hāng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hāng");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "tshāng");
		checkComposingRegion(2, "tsh", "a", 7, "ng", GLOTTAL_NULL, false);
		// [t_hāng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "thāng");
		checkComposingRegion(1, "th", "a", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterEmptyBeforeLeadsToLeads2() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [ch_hāng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hāng");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "chhāng");
		checkComposingRegion(2, "chh", "a", 7, "ng", GLOTTAL_NULL, false);
		// [c_hāng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "chāng");
		checkComposingRegion(1, "ch", "a", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterEmptyBeforeVowelsToEmpty() {
		// [p_āng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("p");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("āng");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "pāng");
		checkComposingRegion(1, "p", "a", 7, "ng", GLOTTAL_NULL, false);
		// [_āng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "āng");
		checkComposingRegion(0, "", "a", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterEmptyBeforeVowelsToEmpty() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// [ph_āng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ph");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("āng");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "phāng");
		checkComposingRegion(2, "ph", "a", 7, "ng", GLOTTAL_NULL, false);
		// [_āng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "āng");
		checkComposingRegion(0, "", "a", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterEmptyBeforeVowelsToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [ph_āng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ph");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("āng");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "phāng");
		checkComposingRegion(2, "ph", "a", 7, "ng", GLOTTAL_NULL, false);
		// [p_āng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "pāng");
		checkComposingRegion(1, "p", "a", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterEmptyBeforeVowelsToLeads2() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [chh_āng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("chh");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("āng");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("chh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "chhāng");
		checkComposingRegion(3, "chh", "a", 7, "ng", GLOTTAL_NULL, false);
		// [ch_āng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "chāng");
		checkComposingRegion(2, "ch", "a", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterEmptyBeforeVowelsToInvalidLeads() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [ch_āng]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("āng");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "chāng");
		checkComposingRegion(2, "ch", "a", 7, "ng", GLOTTAL_NULL, false);
		// [c]_āng
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "c");
		checkComposingRegion(-1, "c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterEmptyBeforeNasalToEmpty() {
		// [n_n̄g]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n̄g");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "n", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "nn̄g");
		checkComposingRegion(1, "n", "", 7, "ng", GLOTTAL_NULL, false);
		// _[n̄g]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "n̄g");
		checkComposingRegion(0, "", "", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterEmptyBeforeNasalToEmpty() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// [kh_ǹg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kh");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ǹg");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "khǹg");
		checkComposingRegion(2, "kh", "", 3, "ng", GLOTTAL_NULL, false);
		// [_ǹg]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ǹg");
		checkComposingRegion(0, "", "", 3, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterEmptyBeforeNasalToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [kh_ǹg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kh");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ǹg");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "khǹg");
		checkComposingRegion(2, "kh", "", 3, "ng", GLOTTAL_NULL, false);
		// [k_ǹg]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "kǹg");
		checkComposingRegion(1, "k", "", 3, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterEmptyBeforeNasalToInvalidLeads() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [ch_ǹg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ǹg");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "chǹg");
		checkComposingRegion(2, "ch", "", 3, "ng", GLOTTAL_NULL, false);
		// [c]_ǹg
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "c");
		checkComposingRegion(-1, "c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterLeadsBeforeLeadsToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// k[c_hiû]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kc");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hiû");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "chiû");
		checkComposingRegion(1, "ch", "iu", 5, AFTERS_NULL, GLOTTAL_NULL, false);
		// [k_hiû]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "khiû");
		checkComposingRegion(1, "kh", "iu", 5, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterLeadsBeforeLeadsToEmpty() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// k[ts_hiû]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hiû");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "tshiû");
		checkComposingRegion(2, "tsh", "iu", 5, AFTERS_NULL, GLOTTAL_NULL, false);
		// [k_hiû]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "khiû");
		checkComposingRegion(1, "kh", "iu", 5, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterLeadsBeforeLeadsToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// k[ts_hiû]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hiû");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "tshiû");
		checkComposingRegion(2, "tsh", "iu", 5, AFTERS_NULL, GLOTTAL_NULL, false);
		// k[t_hiû]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "thiû");
		checkComposingRegion(1, "th", "iu", 5, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterLeadsBeforeLeadsToEmpty2() {
		// k[t_siû]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kt");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("siû");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "tsiû");
		checkComposingRegion(1, "ts", "iu", 5, AFTERS_NULL, GLOTTAL_NULL, false);
		// [k]_siû
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "k");
		checkComposingRegion(-1, "k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterLeadsBeforeVowelsToEmpty() {
		// p[k_iap]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("pk");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("iap");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "kiap");
		checkComposingRegion(1, "k", "ia", 4, "", 'p', false);
		// [p_iap]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "piap");
		checkComposingRegion(1, "p", "ia", 4, "", 'p', false);
	}

	@Test
	public void deleteLeadsAfterLeadsBeforeVowelsToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// p[ch_iap]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("pch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("iap");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "chiap");
		checkComposingRegion(2, "ch", "ia", 4, "", 'p', false);
		// [p_iap]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "piap");
		checkComposingRegion(1, "p", "ia", 4, "", 'p', false);
	}

	@Test
	public void deleteLeadsAfterLeadsBeforeVowelsToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// p[ts_iap]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("pts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("iap");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "tsiap");
		checkComposingRegion(2, "ts", "ia", 4, "", 'p', false);
		// p[t_iap]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "tiap");
		checkComposingRegion(1, "t", "ia", 4, "", 'p', false);
	}

	@Test
	public void deleteLeadsAfterLeadsBeforeVowelsToInvalidLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// p[ch_iap]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("pch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("iap");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "chiap");
		checkComposingRegion(2, "ch", "ia", 4, "", 'p', false);
		// p[c]_iap
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "c");
		checkComposingRegion(-1, "c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterLeadsBeforeNasalToEmpty() {
		// l[p_ńg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("lp");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ńg");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "pńg");
		checkComposingRegion(1, "p", "", 2, "ng", GLOTTAL_NULL, false);
		// [l_ńg]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("l", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "lńg");
		checkComposingRegion(1, "l", "", 2, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterLeadsBeforeNasalToEmpty() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// s[ph_ngh]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("sph");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ngh");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "phngh");
		checkComposingRegion(2, "ph", "", 4, "ng", 'h', false);
		// [s_ngh]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "sngh");
		checkComposingRegion(1, "s", "", 4, "ng", 'h', false);
	}

	@Test
	public void deleteLeadsAfterLeadsBeforeNasalToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// k[ph_ngh]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kph");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ngh");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "phngh");
		checkComposingRegion(2, "ph", "", 4, "ng", 'h', false);
		// k[p_ngh]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "pngh");
		checkComposingRegion(1, "p", "", 4, "ng", 'h', false);
	}

	@Test
	public void deleteLeadsAfterLeadsBeforeNasalToInvalidLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// k[ch_ngh]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ngh");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "chngh");
		checkComposingRegion(2, "ch", "", 4, "ng", 'h', false);
		// k[c]_ngh
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "c");
		checkComposingRegion(-1, "c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterVowelsBeforeLeadsToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		// ê[c_hoân]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("êc");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hoân");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "choân");
		checkComposingRegion(1, "ch", "oa", 5, "n", GLOTTAL_NULL, false);
		// [e_h]oân
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "e", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "eh");
		checkComposingRegion(1, "", "e", 4, "", 'h', false);
	}

	@Test
	public void deleteLeadsAfterVowelsBeforeLeadsToEmpty() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// ê[ts_huân]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("êts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("huân");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 7, 0, 0, null, "tshuân");
		checkComposingRegion(2, "tsh", "ua", 5, "n", GLOTTAL_NULL, false);
		// [e_h]uân
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "e", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "eh");
		checkComposingRegion(1, "", "e", 4, "", 'h', false);
	}

	@Test
	public void deleteLeadsAfterVowelsBeforeLeadsToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// ê[ts_huân]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("êts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("huân");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 7, 0, 0, null, "tshuân");
		checkComposingRegion(2, "tsh", "ua", 5, "n", GLOTTAL_NULL, false);
		// ê[t_huân]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "thuân");
		checkComposingRegion(1, "th", "ua", 5, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterVowelsBeforeVowelsToEmpty() {
		// í[s_ap]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ís");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ap");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "sap");
		checkComposingRegion(1, "s", "a", 4, "", 'p', false);
		// [i_a̍p]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "ia̍p");
		checkComposingRegion(1, "", "ia", 8, "", 'p', false);
	}

	@Test
	public void deleteLeadsAfterVowelsBeforeVowelsToEmpty() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// í[ts_ap]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("íts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ap");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "tsap");
		checkComposingRegion(2, "ts", "a", 4, "", 'p', false);
		// [i_a̍p]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "ia̍p");
		checkComposingRegion(1, "", "ia", 8, "", 'p', false);
	}

	@Test
	public void deleteLeadsAfterVowelsBeforeVowelsToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// í[ts_ap]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("íts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ap");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "tsap");
		checkComposingRegion(2, "ts", "a", 4, "", 'p', false);
		// í[t_ap]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "tap");
		checkComposingRegion(1, "t", "a", 4, "", 'p', false);
	}

	@Test
	public void deleteLeadsAfterVowelsBeforeVowelsToInvalidLeads() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// í[ch_ap]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ích");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ap");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "chap");
		checkComposingRegion(2, "ch", "a", 4, "", 'p', false);
		// í[c]_ap
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "c");
		checkComposingRegion(-1, "c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterVowelsBeforeNasalToEmpty() {
		// ū[s_ǹg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ūs");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ǹg");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("s", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "sǹg");
		checkComposingRegion(1, "s", "", 3, "ng", GLOTTAL_NULL, false);
		// [ū_ng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "u", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ūng");
		checkComposingRegion(1, "", "u", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterVowelsBeforeNasalToEmpty() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// ū[ts_ǹg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ūts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ǹg");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "tsǹg");
		checkComposingRegion(2, "ts", "", 3, "ng", GLOTTAL_NULL, false);
		// [ūng] or [ùng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "u", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ūng");
		checkComposingRegion(1, "", "u", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterVowelsBeforeNasalToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// ū[ts_ǹg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ūts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ǹg");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "tsǹg");
		checkComposingRegion(2, "ts", "", 3, "ng", GLOTTAL_NULL, false);
		// ū[t_ǹg]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 4, 0, 0, null, "tǹg");
		checkComposingRegion(1, "t", "", 3, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterVowelsBeforeNasalToInvalidLeads() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// ū[ch_ǹg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ūch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ǹg");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "chǹg");
		checkComposingRegion(2, "ch", "", 3, "ng", GLOTTAL_NULL, false);
		// ū[c]_ǹg
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 2, 0, 0, null, "c");
		checkComposingRegion(-1, "c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterNasalBeforeLeadsToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		// m̄[c_ha̍h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("m̄c");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ha̍h");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "cha̍h");
		checkComposingRegion(1, "ch", "a", 8, "", 'h', false);
		// [m_h]a̍h
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "m", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "mh");
		checkComposingRegion(1, "", "", 4, "m", 'h', false);
	}

	@Test
	public void deleteLeadsAfterNasalBeforeLeadsToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// m̄[ch_ha̍h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("m̄ch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ha̍h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 8, 0, 0, null, "chha̍h");
		checkComposingRegion(2, "chh", "a", 8, "", 'h', false);
		// [m_h]a̍h
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "m", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "mh");
		checkComposingRegion(1, "", "", 4, "m", 'h', false);
	}

	@Test
	public void deleteLeadsAfterNasalBeforeLeadsToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// m̄[ch_ha̍h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("m̄ch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ha̍h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 8, 0, 0, null, "chha̍h");
		checkComposingRegion(2, "chh", "a", 8, "", 'h', false);
		// m̄[c_ha̍h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "cha̍h");
		checkComposingRegion(1, "ch", "a", 8, "", 'h', false);
	}

	@Test
	public void deleteLeadAfterNasalBeforeVowelsToEmpty() {
		// ng[j_iàu]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ngj");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("iàu");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("j", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "jiàu");
		checkComposingRegion(1, "j", "iau", 3, AFTERS_NULL, GLOTTAL_NULL, false);
		// [ng_iàu]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "ngiàu");
		checkComposingRegion(2, "ng", "iau", 3, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNasalBeforeVowelsToEmpty() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// ng[ts_iàu]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ngts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("iàu");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "tsiàu");
		checkComposingRegion(2, "ts", "iau", 3, AFTERS_NULL, GLOTTAL_NULL, false);
		// [ng_iàu]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "ngiàu");
		checkComposingRegion(2, "ng", "iau", 3, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNasalBeforeVowelsToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// ng[ts_iàu]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ngts");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("iàu");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ts", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "tsiàu");
		checkComposingRegion(2, "ts", "iau", 3, AFTERS_NULL, GLOTTAL_NULL, false);
		// ng[t_iàu]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "tiàu");
		checkComposingRegion(1, "t", "iau", 3, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNasalBeforeVowelsToInvalidLeads() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// ng[ch_iàu]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ngch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("iàu");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "chiàu");
		checkComposingRegion(2, "ch", "iau", 3, AFTERS_NULL, GLOTTAL_NULL, false);
		// ng[c]_iàu
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 3, 0, 0, null, "c");
		checkComposingRegion(-1, "c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterNasalBeforeNasalToEmpty() {
		// ń[b_n̄g]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ńb");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n̄g");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("b", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 5, 0, 0, null, "bn̄g");
		checkComposingRegion(1, "b", "", 7, "ng", GLOTTAL_NULL, false);
		// [n_ńg]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "nńg");
		checkComposingRegion(1, "n", "", 2, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNasalBeforeNasalToEmpty() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// n̄[kh_ńg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n̄kh");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ńg");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "khńg");
		checkComposingRegion(2, "kh", "", 2, "ng", GLOTTAL_NULL, false);
		// [n_n̄g]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "n", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "nn̄g");
		checkComposingRegion(1, "n", "", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNasalBeforeNasalToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// n̄[kh_ńg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n̄kh");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ńg");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "khńg");
		checkComposingRegion(2, "kh", "", 2, "ng", GLOTTAL_NULL, false);
		// n̄[k_ńg]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 5, 0, 0, null, "kńg");
		checkComposingRegion(1, "k", "", 2, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNasalBeforeNasalToInvalidLeads() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// n̄[ch_ńg]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n̄ch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ńg");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "chńg");
		checkComposingRegion(2, "ch", "", 2, "ng", GLOTTAL_NULL, false);
		// n̄[c]_ńg
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 3, 0, 0, null, "c");
		checkComposingRegion(-1, "c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterNNBeforeLeadsToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		// nn[c_hé]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnc");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hé");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 5, 0, 0, null, "ché");
		checkComposingRegion(1, "ch", "e", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// [nn_h]é
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "nnh");
		checkComposingRegion(2, "n", "", 4, "n", 'h', false);
	}

	@Test
	public void deleteLeadsAfterNNBeforeLeadsToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// nn[ch_hé]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hé");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "chhé");
		checkComposingRegion(2, "chh", "e", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// [nn_h]é
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "nnh");
		checkComposingRegion(2, "n", "", 4, "n", 'h', false);
	}

	@Test
	public void deleteLeadsAfterNNBeforeLeadsToLeads() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// nn[ch_hé]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hé");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "chhé");
		checkComposingRegion(2, "chh", "e", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// nn[c_hé]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 5, 0, 0, null, "ché");
		checkComposingRegion(1, "ch", "e", 2, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterNNBeforeVowelsToEmpty() {
		// nn[b_âi]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnb");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("âi");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("b", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 5, 0, 0, null, "bâi");
		checkComposingRegion(1, "b", "ai", 5, AFTERS_NULL, GLOTTAL_NULL, false);
		// [nn]_âi
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "nn");
		checkComposingRegion(-1, "n", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNNBeforeVowelsToEmpty() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// nn[ph_īnn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnph");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("īnn");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "phīnn");
		checkComposingRegion(2, "ph", "i", 7, "nn", GLOTTAL_NULL, false);
		// [nn]_īnn
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "nn");
		checkComposingRegion(-1, "n", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNNBeforeVowelsToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// nn[ph_īnn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnph");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("īnn");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ph", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "phīnn");
		checkComposingRegion(2, "ph", "i", 7, "nn", GLOTTAL_NULL, false);
		// nn[p_īnn]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "pīnn");
		checkComposingRegion(1, "p", "i", 7, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNNBeforeVowelsToInvalidLeads() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// nn[ch_īnn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("īnn");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "chīnn");
		checkComposingRegion(2, "ch", "i", 7, "nn", GLOTTAL_NULL, false);
		// nn[c]_īnn
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 3, 0, 0, null, "c");
		checkComposingRegion(-1, "c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadAfterNNBeforeNasalToEmpty() {
		// nn[l_n̄g]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnl");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n̄g");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("l", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "ln̄g");
		checkComposingRegion(1, "l", "", 7, "ng", GLOTTAL_NULL, false);
		// [nn]_n̄g
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "nn");
		checkComposingRegion(-1, "n", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNNBeforeNasalToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE));
		// nn[ch_n̄g]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n̄g");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "chn̄g");
		checkComposingRegion(2, "ch", "", 7, "ng", GLOTTAL_NULL, false);
		// [nn]_n̄g
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("n", "", 1, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "nn");
		checkComposingRegion(-1, "n", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNNBeforeNasalToLeads() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// nn[th_n̄g]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnth");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n̄g");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("th", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "thn̄g");
		checkComposingRegion(2, "th", "", 7, "ng", GLOTTAL_NULL, false);
		// nn[t_n̄g]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 6, 0, 0, null, "tn̄g");
		checkComposingRegion(1, "t", "", 7, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteLeadsAfterNNBeforeNasalToInvalidLeads() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// nn[chn̄g]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("nnch");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n̄g");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("ch", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 7, 0, 0, null, "chn̄g");
		checkComposingRegion(2, "ch", "", 7, "ng", GLOTTAL_NULL, false);
		// nn[c]_n̄g
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 2, 3, 0, 0, null, "c");
		checkComposingRegion(-1, "c", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeEmptyWithBootToEmptyWithBoot() {
		ims.switchSubtype(SUBTYPE_POJ);
		// {[í]_-king}
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("í");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("-king");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "í-king");
		checkCursor(1);
		// {[]_-king}
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "-king");
		checkCursor(0);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeVowelsToEmpty() {
		// [à_i]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("à");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("i");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "a", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ài");
		checkComposingRegion(1, "", "ai", 3, AFTERS_NULL, GLOTTAL_NULL, false);
		// _[i]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "i");
		checkComposingRegion(0, "", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeVowelsToVowels() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [uà_i]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("uà");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("i");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "ua", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "uài");
		checkComposingRegion(2, "", "uai", 3, AFTERS_NULL, GLOTTAL_NULL, false);
		// [u_ì]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "u", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "uì");
		checkComposingRegion(1, "", "ui", 3, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeNasalToEmpty() {
		// [à_m]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("à");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "a", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "àm");
		checkComposingRegion(1, "", "a", 3, "m", GLOTTAL_NULL, false);
		// _[m]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "m");
		checkComposingRegion(0, "", "", 1, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeNasalToVowels() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [ià_m]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ià");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "ia", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "iàm");
		checkComposingRegion(2, "", "ia", 3, "m", GLOTTAL_NULL, false);
		// [ì_m]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ìm");
		checkComposingRegion(1, "", "i", 3, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeNNToEmpty() {
		// [ā_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ā");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "a", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ānn");
		checkComposingRegion(1, "", "a", 7, "nn", GLOTTAL_NULL, false);
		// _[nn]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "nn");
		checkComposingRegion(0, "n", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeNNToVowels() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [iá_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("iá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "ia", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "iánn");
		checkComposingRegion(2, "", "ia", 2, "nn", GLOTTAL_NULL, false);
		// [í_nn]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ínn");
		checkComposingRegion(1, "", "i", 2, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeGlotalToEmpty() {
		// [a̍_p]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("a̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("p");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "a", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "a̍p");
		checkComposingRegion(2, "", "a", 8, "", 'p', false);
		// _[p]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "p");
		checkComposingRegion(0, "p", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeGlotalToVowels() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [ia̍_p]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ia̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("p");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "ia", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "ia̍p");
		checkComposingRegion(3, "", "ia", 8, "", 'p', false);
		// [i̍_p]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "i̍p");
		checkComposingRegion(2, "", "i", 8, "", 'p', false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforePojNnToEmpty() {
		ims.switchSubtype(SUBTYPE_POJ);
		// [â_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("â");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "a", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "âⁿ");
		checkComposingRegion(1, "", "a", 5, "", GLOTTAL_NULL, true);
		// _ⁿ
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, -1, -1, 0, 0, null, "");
		checkComposingRegion(-1, LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforePojNnToVowels() {
		ims.switchSubtype(SUBTYPE_POJ);
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [iâ_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("iâ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "ia", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "iâⁿ");
		checkComposingRegion(2, "", "ia", 5, "", GLOTTAL_NULL, true);
		// [î_ⁿ]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "îⁿ");
		checkComposingRegion(1, "", "i", 5, "", GLOTTAL_NULL, true);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeLeadsToEmpty() {
		// [á]_b
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("á");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("b");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "á");
		checkComposingRegion(-1, "", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// _[b]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "b");
		checkComposingRegion(0, "b", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterEmptyBeforeLeadsToVowels() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [iá]_b
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("iá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("b");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "ia", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "iá");
		checkComposingRegion(-1, "", "ia", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// [í]_b
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "í");
		checkComposingRegion(-1, "", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforeVowelsToEmpty() {
		// [ji_àm]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ji");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("àm");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("j", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "jiàm");
		checkComposingRegion(2, "j", "ia", 3, "m", GLOTTAL_NULL, false);
		// [j_àm]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("j", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "jàm");
		checkComposingRegion(1, "j", "a", 3, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforeVowelsToVowels() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [jiá_u]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("jiá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("u");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("j", "ia", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "jiáu");
		checkComposingRegion(3, "j", "iau", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// [ji_ú]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("j", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "jiú");
		checkComposingRegion(2, "j", "iu", 2, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforeNasalToEmpty() {
		// [jî_n]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("jî");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("j", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "jîn");
		checkComposingRegion(2, "j", "i", 5, "n", GLOTTAL_NULL, false);
		// [j_n]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("j", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "jn");
		checkComposingRegion(1, "j", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforeNasalToVowels() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [jía_m]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("jiá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("j", "ia", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "jiám");
		checkComposingRegion(3, "j", "ia", 2, "m", GLOTTAL_NULL, false);
		// [jí_m]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("j", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "jím");
		checkComposingRegion(2, "j", "i", 2, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforeNNToEmpty() {
		// [kà_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kà");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", "a", 3, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "kànn");
		checkComposingRegion(2, "k", "a", 3, "nn", GLOTTAL_NULL, false);
		// [k_n]n
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "kn");
		checkComposingRegion(1, "k", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforeNNToVowels() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [kiá_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kiá");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", "ia", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "kiánn");
		checkComposingRegion(3, "k", "ia", 2, "nn", GLOTTAL_NULL, false);
		// [kí_nn]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "kínn");
		checkComposingRegion(2, "k", "i", 2, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforeGlotalToEmpty() {
		// [kha̍_k]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kha̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("k");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "a", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "kha̍k");
		checkComposingRegion(4, "kh", "a", 8, "", 'k', false);
		// [kh]_k
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "kh");
		checkComposingRegion(-1, "kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforeGlotalToVowels() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [khia̍_k]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khia̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("k");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "ia", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "khia̍k");
		checkComposingRegion(5, "kh", "ia", 8, "", 'k', false);
		// [khi̍_k]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("kh", "i", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "khi̍k");
		checkComposingRegion(4, "kh", "i", 8, "", 'k', false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforePojNNToEmpty() {
		// [kha_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kha");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "a", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "khann");
		checkComposingRegion(3, "kh", "a", 1, "nn", GLOTTAL_NULL, false);
		// [kh_n]n
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("kh", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "khn");
		checkComposingRegion(2, "kh", "", 1, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforePojNNToVowels() {
		// [khiū_nn]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khiū");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "iu", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "khiūnn");
		checkComposingRegion(4, "kh", "iu", 7, "nn", GLOTTAL_NULL, false);
		// [khī_nn]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("kh", "i", 7, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "khīnn");
		checkComposingRegion(3, "kh", "i", 7, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforeLeadsToEmpty() {
		// [kó]_s
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kó");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("s");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", "o", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "kó");
		checkComposingRegion(-1, "k", "o", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// [k]_s
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "k");
		checkComposingRegion(-1, "k", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteVowelAfterLeadsBeforeLeadsToVowels() {
		// [kiò]_s
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kió");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("s");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", "io", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "kió");
		checkComposingRegion(-1, "k", "io", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// [kì]_s
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "kí");
		checkComposingRegion(-1, "k", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNasalAfterEmptyBeforeEmptyWithBootToEmptyWithBoot() {
		ims.switchSubtype(SUBTYPE_POJ);
		// {[m̄]_-thang}
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("m̄");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("-thang");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 7, "m", GLOTTAL_NULL, false, 0, 8, 0, 0, null, "m̄-thang");
		checkCursor(2);
		// {[]_-thang}
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "-thang");
		checkCursor(0);
	}

	@Test
	public void deleteNasalAfterEmptyBeforeGlotalToEmpty() {
		// [m_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("m");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "m", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "mh");
		checkComposingRegion(1, "", "", 4, "m", 'h', false);
		// _[h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "h");
		checkComposingRegion(0, "h", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterEmptyBeforeGlotalToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [n̍g_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n̍g");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 8, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "n̍gh");
		checkComposingRegion(3, "", "", 8, "ng", 'h', false);
		// [n̍_h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 8, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "n̍h");
		checkComposingRegion(2, "", "", 8, "n", 'h', false);
	}

	@Test
	public void deleteNgAfterEmptyBeforeVowelsToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [ng_í]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("í");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ngí");
		checkComposingRegion(2, "ng", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false);
		// [n_í]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "ní");
		checkComposingRegion(1, "n", "i", 2, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterEmptyBeforeNasalToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [ng_ḿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ḿ");
		ims.updateStateOnSelection(inputConnection, 2, 2, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "ngḿ");
		checkComposingRegion(2, "ng", "", 2, "m", GLOTTAL_NULL, false);
		// [n_ḿ]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 1, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "nḿ");
		checkComposingRegion(1, "n", "", 2, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterEmptyBeforePojNnToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [n̂g]_ⁿ
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n̂g");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 5, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "n̂g");
		checkComposingRegion(-1, "", "", 5, "ng", GLOTTAL_NULL, false);
		// [n̂]_ⁿ
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 5, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "n̂");
		checkComposingRegion(-1, "", "", 5, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterEmptyBeforeLeadsToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [n̂g]_b
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n̂g");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("b");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 5, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "n̂g");
		checkComposingRegion(-1, "", "", 5, "ng", GLOTTAL_NULL, false);
		// [n̂]_b
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 5, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "n̂");
		checkComposingRegion(-1, "", "", 5, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterEmptyBeforeGToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [n̂g]_g
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("n̂g");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("g");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "", 5, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "n̂g");
		checkComposingRegion(-1, "", "", 5, "ng", GLOTTAL_NULL, false);
		// [n̂_g]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "", 5, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "n̂g");
		checkComposingRegion(2, "", "", 5, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNasalAfterLeadsBeforeGlotalToEmpty() {
		// [hm̍_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hm̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "", 8, "m", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "hm̍h");
		checkComposingRegion(3, "h", "", 8, "m", 'h', false);
		// [h]_h
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 0, 1, 0, 0, null, "h");
		checkComposingRegion(-1, "h", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterLeadsBeforeGlotalToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [hn̍g_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hn̍g");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "", 8, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "hn̍gh");
		checkComposingRegion(4, "h", "", 8, "ng", 'h', false);
		// [hn̍_h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", "", 8, "n", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "hn̍h");
		checkComposingRegion(3, "h", "", 8, "n", 'h', false);
	}

	@Test
	public void deleteNgAfterLeadsBeforeVowelsToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [pńg]_òo
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("pńg");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("òo");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "pńg");
		checkComposingRegion(-1, "p", "", 2, "ng", GLOTTAL_NULL, false);
		// [pń]_òo
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "pń");
		checkComposingRegion(-1, "p", "", 2, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterLeadsBeforeNasalToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [pńg]_m̄
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("pńg");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m̄");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "pńg");
		checkComposingRegion(-1, "p", "", 2, "ng", GLOTTAL_NULL, false);
		// [pń]_m̄
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "pń");
		checkComposingRegion(-1, "p", "", 2, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterLeadsBeforeNnToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [pńg]_nn
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("pńg");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "pńg");
		checkComposingRegion(-1, "p", "", 2, "ng", GLOTTAL_NULL, false);
		// [pń]_nn
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "pń");
		checkComposingRegion(-1, "p", "", 2, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterLeadsBeforePojNnToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [pńg]_ⁿ
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("pńg");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "pńg");
		checkComposingRegion(-1, "p", "", 2, "ng", GLOTTAL_NULL, false);
		// [pń]_ⁿ
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "pń");
		checkComposingRegion(-1, "p", "", 2, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterLeadsBeforeLeadsToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [pńg]_s
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("pńg");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("s");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "pńg");
		checkComposingRegion(-1, "p", "", 2, "ng", GLOTTAL_NULL, false);
		// [pń]_s
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "n", GLOTTAL_NULL, false, 0, 2, 0, 0, null, "pń");
		checkComposingRegion(-1, "p", "", 2, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterLeadsBeforeGToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [pńg]_g
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("pńg");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("g");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "ng", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "pńg");
		checkComposingRegion(-1, "p", "", 2, "ng", GLOTTAL_NULL, false);
		// [pń_g]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("p", "", 2, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "pńg");
		checkComposingRegion(2, "p", "", 2, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNasalAfterVowelsBeforeGlotalToEmpty() {
		// [o̍m_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("o̍m");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 8, "m", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "o̍mh");
		checkComposingRegion(3, "", "o", 8, "m", 'h', false);
		// [o̍_h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "o̍h");
		checkComposingRegion(2, "", "o", 8, "", 'h', false);
	}

	@Test
	public void deleteNasalAfterVowelsBeforeGlotalPojNnToEmpty() {
		// [o̍m_h]ⁿ
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("o̍m");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 8, "m", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "o̍mh");
		checkComposingRegion(3, "", "o", 8, "m", 'h', false);
		// [o̍_hⁿ]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "o̍hⁿ");
		checkComposingRegion(2, "", "o", 8, "", 'h', true);
	}

	@Test
	public void deleteNgAfterVowelsBeforeGlotalToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [o̍ng_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("o̍ng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 8, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "o̍ngh");
		checkComposingRegion(4, "", "o", 8, "ng", 'h', false);
		// [o̍n_h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("", "o", 8, "n", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "o̍nh");
		checkComposingRegion(3, "", "o", 8, "n", 'h', false);
	}

	@Test
	public void deleteNgAfterVowelsBeforeVowelsToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [jiông]_ê
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("jiông");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ê");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("j", "io", 5, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "jiông");
		checkComposingRegion(-1, "j", "io", 5, "ng", GLOTTAL_NULL, false);
		// [jiôn]_ê
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("j", "io", 5, "n", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "jiôn");
		checkComposingRegion(-1, "j", "io", 5, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterVowelsBeforeNnToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [kàng]_nn
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kàng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", "a", 3, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "kàng");
		checkComposingRegion(-1, "k", "a", 3, "ng", GLOTTAL_NULL, false);
		// [kàn_n]n
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", "a", 3, "n", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "kànn");
		checkComposingRegion(3, "k", "a", 3, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterVowelsBeforePojNnToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [kàng]_ⁿ
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kàng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("k", "a", 3, "ng", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "kàng");
		checkComposingRegion(-1, "k", "a", 3, "ng", GLOTTAL_NULL, false);
		// [kàn]_ⁿ
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("k", "a", 3, "n", GLOTTAL_NULL, false, 0, 3, 0, 0, null, "kàn");
		checkComposingRegion(-1, "k", "a", 3, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterVowelsBeforeLeadsToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [kháng]_l
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kháng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("l");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "a", 2, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "kháng");
		checkComposingRegion(-1, "kh", "a", 2, "ng", GLOTTAL_NULL, false);
		// [khán]_l
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("kh", "a", 2, "n", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "khán");
		checkComposingRegion(-1, "kh", "a", 2, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNgAfterVowelsBeforeGToN() {
		ims.setInputState(ims.getInputState().setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR));
		// [kháng]_g
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("kháng");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("g");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "a", 2, "ng", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "kháng");
		checkComposingRegion(-1, "kh", "a", 2, "ng", GLOTTAL_NULL, false);
		// [khán_g]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("kh", "a", 2, "n", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "kháng");
		checkComposingRegion(4, "kh", "a", 2, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNnAfterVowelsBeforeGlotalToEmpty() {
		ims.setInputState(ims.getInputState().setBoolean(DeleteAfterCluster, true));
		// [hiu̍nn_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hiu̍nn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "hiu̍nnh");
		checkComposingRegion(6, "h", "iu", 8, "nn", 'h', false);
		// [hiu̍_h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "hiu̍h");
		checkComposingRegion(4, "h", "iu", 8, "", 'h', false);
	}

	@Test
	public void deleteNnAfterVowelsBeforeGlotalPojNnToEmpty() {
		ims.setInputState(ims.getInputState().setBoolean(DeleteAfterCluster, true));
		// [hiu̍nn_h]ⁿ
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hiu̍nn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hⁿ");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "hiu̍nnh");
		checkComposingRegion(6, "h", "iu", 8, "nn", 'h', false);
		// [hiu̍_hⁿ]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "hiu̍hⁿ");
		checkComposingRegion(4, "h", "iu", 8, "", 'h', true);
	}

	@Test
	public void deleteNnAfterVowelsBeforeGlotalToN() {
		ims.setInputState(ims.getInputState().setBoolean(DeleteAfterCluster, false));
		// [hiu̍nn_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hiu̍nn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 8, "nn", GLOTTAL_NULL, false, 0, 7, 0, 0, null, "hiu̍nnh");
		checkComposingRegion(6, "h", "iu", 8, "nn", 'h', false);
		// [hiu̍n_h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 8, "n", GLOTTAL_NULL, false, 0, 6, 0, 0, null, "hiu̍nh");
		checkComposingRegion(5, "h", "iu", 8, "n", 'h', false);
	}

	@Test
	public void deleteNnAfterVowelsBeforeNasalToEmpty() {
		ims.setInputState(ims.getInputState().setBoolean(DeleteAfterCluster, true));
		// [hiúnn]_m
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hiúnn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 2, "nn", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "hiúnn");
		checkComposingRegion(-1, "h", "iu", 2, "nn", GLOTTAL_NULL, false);
		// [hiú_m]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "hiúm");
		checkComposingRegion(3, "h", "iu", 2, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNnAfterVowelsBeforeNasalToN() {
		ims.setInputState(ims.getInputState().setBoolean(DeleteAfterCluster, false));
		// [hiúnn]_m
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hiúnn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 2, "nn", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "hiúnn");
		checkComposingRegion(-1, "h", "iu", 2, "nn", GLOTTAL_NULL, false);
		// [hiún]_m
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 2, "n", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "hiún");
		checkComposingRegion(-1, "h", "iu", 2, "n", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteNnAfterVowelsBeforeNToN() {
		ims.setInputState(ims.getInputState().setBoolean(DeleteAfterCluster, false));
		// [hiúnn]_n
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hiúnn");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("n");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 2, "nn", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "hiúnn");
		checkComposingRegion(-1, "h", "iu", 2, "nn", GLOTTAL_NULL, false);
		// [hiún_n]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 2, "n", GLOTTAL_NULL, false, 0, 5, 0, 0, null, "hiúnn");
		checkComposingRegion(4, "h", "iu", 2, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void deletePojNnAfterVowelsBeforeGlotalToEmpty() {
		ims.setInputState(ims.getInputState().setBoolean(DeleteAfterCluster, true));
		// [hiu̍ⁿ_h]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hiu̍ⁿ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 8, "", GLOTTAL_NULL, true, 0, 6, 0, 0, null, "hiu̍ⁿh");
		checkComposingRegion(5, "h", "iu", 8, "ⁿ", 'h', false);
		// [hiu̍_h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "hiu̍h");
		checkComposingRegion(4, "h", "iu", 8, "", 'h', false);
	}

	@Test
	public void deletePojNnAfterVowelsBeforeGlotalPojNnToEmpty() {
		ims.setInputState(ims.getInputState().setBoolean(DeleteAfterCluster, true));
		// [hiu̍ⁿ_h]ⁿ
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hiu̍ⁿ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hⁿ");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 8, "", GLOTTAL_NULL, true, 0, 6, 0, 0, null, "hiu̍ⁿh");
		checkComposingRegion(5, "h", "iu", 8, "ⁿ", 'h', false);
		// [hiu̍_hⁿ]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "hiu̍hⁿ");
		checkComposingRegion(4, "h", "iu", 8, "", 'h', true);
	}

	@Test
	public void deleteGlotalAfterVowelsBeforePojNnToEmpty() {
		// [khe̍h_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍h");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "", 'h', false, 0, 6, 0, 0, null, "khe̍hⁿ");
		checkComposingRegion(5, "kh", "e", 8, "", 'h', true);
		// [khé_ⁿ]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "khéⁿ");
		checkComposingRegion(3, "kh", "e", 2, "", GLOTTAL_NULL, true);
	}

	@Test
	public void deleteGlotalAfterVowelsBeforeNnToEmpty() {
		// [khe̍h]_nn
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍h");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "", 'h', false, 0, 5, 0, 0, null, "khe̍h");
		checkComposingRegion(-1, "kh", "e", 8, "", 'h', false);
		// [khé_nn]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "khénn");
		checkComposingRegion(3, "kh", "e", 2, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteGlotalAfterVowelsBeforeGlotalToEmpty() {
		// [khe̍h]_k
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("khe̍h");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("k");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, "", 'h', false, 0, 5, 0, 0, null, "khe̍h");
		checkComposingRegion(-1, "kh", "e", 8, "", 'h', false);
		// [khe̍_k]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("kh", "e", 8, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "khe̍k");
		checkComposingRegion(4, "kh", "e", 8, "", 'k', false);
	}

	@Test
	public void deleteGlotalAfterVowelsBeforeNasalToEmpty() {
		// [tsha̍k]_ng
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tsha̍k");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ng");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("tsh", "a", 8, "", 'k', false, 0, 6, 0, 0, null, "tsha̍k");
		checkComposingRegion(-1, "tsh", "a", 8, "", 'k', false);
		// [tshá_ng]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("tsh", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 6, 0, 0, null, "tsháng");
		checkComposingRegion(4, "tsh", "a", 2, "ng", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteGlotalAfterVowelsBeforeVowelsToEmpty() {
		// [tsha̍k]_î
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tsha̍k");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("î");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("tsh", "a", 8, "", 'k', false, 0, 6, 0, 0, null, "tsha̍k");
		checkComposingRegion(-1, "tsh", "a", 8, "", 'k', false);
		// [tshá_i]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("tsh", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 5, 0, 0, null, "tshái");
		checkComposingRegion(4, "tsh", "ai", 2, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteGlotalAfterVowelsBeforeLeadsToEmpty() {
		// [tsha̍k]_s
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tsha̍k");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("s");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("tsh", "a", 8, "", 'k', false, 0, 6, 0, 0, null, "tsha̍k");
		checkComposingRegion(-1, "tsh", "a", 8, "", 'k', false);
		// [tshá]_s
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("tsh", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tshá");
		checkComposingRegion(-1, "tsh", "a", 2, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deleteGlotalAfterNnBeforeGlotalToEmpty() {
		// [ti̍nnh]_h
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ti̍nnh");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, "nn", 'h', false, 0, 6, 0, 0, null, "ti̍nnh");
		checkComposingRegion(-1, "t", "i", 8, "nn", 'h', false);
		// [ti̍nn_h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, "nn", GLOTTAL_NULL, false, 0, 6, 0, 0, null, "ti̍nnh");
		checkComposingRegion(5, "t", "i", 8, "nn", 'h', false);
	}

	@Test
	public void deleteGlotalAfterNnBeforeLeadsToEmpty() {
		// [ti̍nnh]_b
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ti̍nnh");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("b");
		ims.updateStateOnSelection(inputConnection, 6, 6, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, "nn", 'h', false, 0, 6, 0, 0, null, "ti̍nnh");
		checkComposingRegion(-1, "t", "i", 8, "nn", 'h', false);
		// [tínn]_b
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 2, "nn", GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tínn");
		checkComposingRegion(-1, "t", "i", 2, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void deleteGlotalAfterPojNnBeforeGlotalToEmpty() {
		// [ti̍ⁿh]_h
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ti̍ⁿh");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("h");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, "ⁿ", 'h', false, 0, 5, 0, 0, null, "ti̍ⁿh");
		checkComposingRegion(-1, "t", "i", 8, "ⁿ", 'h', false);
		// [ti̍ⁿ_h]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, "", GLOTTAL_NULL, true, 0, 5, 0, 0, null, "ti̍ⁿh");
		checkComposingRegion(4, "t", "i", 8, "ⁿ", 'h', false);
	}

	@Test
	public void deleteGlotalAfterPojNnBeforeLeadsToEmpty() {
		// [ti̍ⁿh]_b
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ti̍ⁿh");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("b");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, "ⁿ", 'h', false, 0, 5, 0, 0, null, "ti̍ⁿh");
		checkComposingRegion(-1, "t", "i", 8, "ⁿ", 'h', false);
		// [tíⁿ]_b
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 2, "", GLOTTAL_NULL, true, 0, 3, 0, 0, null, "tíⁿ");
		checkComposingRegion(-1, "t", "i", 2, "", GLOTTAL_NULL, true);
	}

	@Test
	public void deletePojNnAfterGlotalBeforePojNnTo() {
		// [ti̍hⁿ]_ⁿ
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("ti̍hⁿ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 5, 5, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, "", 'h', true, 0, 5, 0, 0, null, "ti̍hⁿ");
		checkComposingRegion(-1, "t", "i", 8, "", 'h', true);
		// [ti̍h_ⁿ]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, "", 'h', false, 0, 5, 0, 0, null, "ti̍hⁿ");
		checkComposingRegion(4, "t", "i", 8, "", 'h', true);
	}

	@Test
	public void deletePojNnAfterVowelsBeforeVowelsToEmpty() {
		// [tîⁿ]_á
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tîⁿ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("á");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 5, "", GLOTTAL_NULL, true, 0, 3, 0, 0, null, "tîⁿ");
		checkComposingRegion(-1, "t", "i", 5, "", GLOTTAL_NULL, true);
		// [ti_â]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 1, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "tiâ");
		checkComposingRegion(2, "t", "ia", 5, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void deletePojNnAfterVowelsBeforeNasalToEmpty() {
		ims.setInputState(ims.getInputState().setBoolean(DeleteAfterCluster, true));
		// [hiúⁿ]_m
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("hiúⁿ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("m");
		ims.updateStateOnSelection(inputConnection, 4, 4, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 2, "", GLOTTAL_NULL, true, 0, 4, 0, 0, null, "hiúⁿ");
		checkComposingRegion(-1, "h", "iu", 2, "", GLOTTAL_NULL, true);
		// [hiú_m]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("h", "iu", 2, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "hiúm");
		checkComposingRegion(3, "h", "iu", 2, "m", GLOTTAL_NULL, false);
	}

	@Test
	public void deletePojNnAfterVowelsBeforeNnToEmpty() {
		// [tîⁿ]_nn
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tîⁿ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("nn");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 5, "", GLOTTAL_NULL, true, 0, 3, 0, 0, null, "tîⁿ");
		checkComposingRegion(-1, "t", "i", 5, "", GLOTTAL_NULL, true);
		// [tî_nn]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 4, 0, 0, null, "tînn");
		checkComposingRegion(2, "t", "i", 5, "nn", GLOTTAL_NULL, false);
	}

	@Test
	public void deletePojNnAfterVowelsBeforePojNnToEmpty() {
		// [tîⁿ]_ⁿ
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tîⁿ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 5, "", GLOTTAL_NULL, true, 0, 3, 0, 0, null, "tîⁿ");
		checkComposingRegion(-1, "t", "i", 5, "", GLOTTAL_NULL, true);
		// [tî_ⁿ]
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 3, 0, 0, null, "tîⁿ");
		checkComposingRegion(2, "t", "i", 5, "", GLOTTAL_NULL, true);
	}

	@Test
	public void deletePojNnAfterVowelsBeforeLeadsToEmpty() {
		// [tîⁿ]_j
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("tîⁿ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("j");
		ims.updateStateOnSelection(inputConnection, 3, 3, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 5, "", GLOTTAL_NULL, true, 0, 3, 0, 0, null, "tîⁿ");
		checkComposingRegion(-1, "t", "i", 5, "", GLOTTAL_NULL, true);
		// [tî]_j
		ims.updateStateOnDelete(inputConnection);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false, 0, 2, 0, 0, null, "tî");
		checkComposingRegion(-1, "t", "i", 5, AFTERS_NULL, GLOTTAL_NULL, false);
	}
}
