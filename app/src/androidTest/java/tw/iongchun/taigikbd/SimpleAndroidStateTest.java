/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static tw.iongchun.taigikbd.TKComposingUtils.TONE_MARK_POJ;
import static tw.iongchun.taigikbd.TKComposingUtils.TONE_MARK_SUBTYPE;
import static tw.iongchun.taigikbd.TKComposingUtils.TONE_MARK_TL;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_LEADINGS_CHAR;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_LEADINGS_VALID;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_LEADINGS_WHOLE;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_VOWELS_CHAR;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_VOWELS_SINGLE;
import static tw.iongchun.taigikbd.TKInputMethodService.DEL_VOWELS_WHOLE;
import static tw.iongchun.taigikbd.TKInputMethodService.LANGKEY_ASCII;
import static tw.iongchun.taigikbd.TKInputMethodService.LANGKEY_LAST_IM;
import static tw.iongchun.taigikbd.TKInputMethodService.LANGKEY_NEXT_IM;
import static tw.iongchun.taigikbd.TKInputMethodService.LANGKEY_PICK_IM;
import static tw.iongchun.taigikbd.TKInputState.AspirationToggle;
import static tw.iongchun.taigikbd.TKInputState.AutoDash;
import static tw.iongchun.taigikbd.TKInputState.DeleteAfterCluster;
import static tw.iongchun.taigikbd.TKInputState.DeleteLeadingsMode;
import static tw.iongchun.taigikbd.TKInputState.DeleteVowelsMode;
import static tw.iongchun.taigikbd.TKInputState.GlottalReplaceGlottal;
import static tw.iongchun.taigikbd.TKInputState.GlottalToggleTone;
import static tw.iongchun.taigikbd.TKInputState.LanguageKey;
import static tw.iongchun.taigikbd.TKInputState.LeadReplaceLead;
import static tw.iongchun.taigikbd.TKInputState.PhoneticInput;
import static tw.iongchun.taigikbd.TKInputState.ToneMarkStyle;
import static tw.iongchun.taigikbd.TKInputState.VoicelessToggle;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_ASPIRATION_TOGGLE;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_AUTO_DASH;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_AFTER_CLUSTER;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_LEADINGS;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_DEL_VOWELS;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_GLOTTAL_REPLACE;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_GLOTTAL_TOGGLE;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_IM_SWITCH;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_LEAD_REPLACE_LEAD;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_PHONETIC_INPUT;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_TONE_MARK_STYLE;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_VOICELESS_TOGGLE;

/**
 * Created by iongchun on 6/27/17.
 */

@RunWith(AndroidJUnit4.class)
public class SimpleAndroidStateTest {
	@Mock
	SharedPreferences pref;
	@Mock
	Context context;
	private TKInputMethodService ims;

	@Before
	public void initMockito() {
		MockitoAnnotations.initMocks(this);
	}

	@Before
	public void initIMS() {
		ims = new TKInputMethodService();
	}

	@Test
	public void initialize_state() {
		assertNull(ims.getInputState());
		ims.initInputState();
		assertNotNull(ims.getInputState());
		ims.fineInputState();
		assertNull(ims.getInputState());
	}

	@Test
	public void test_property() {
		ims.initInputState();
		TKInputState state = ims.getInputState();
		state = state.setBoolean(AutoDash, false)
		             .setString(ToneMarkStyle, TONE_MARK_SUBTYPE)
		             .setBoolean(AspirationToggle, false)
		             .setBoolean(VoicelessToggle, false)
		             .setBoolean(PhoneticInput, true)
		             .setBoolean(LeadReplaceLead, false)
		             .setBoolean(GlottalReplaceGlottal, false)
		             .setBoolean(GlottalToggleTone, false)
		             .setString(DeleteLeadingsMode, DEL_LEADINGS_CHAR)
		             .setString(DeleteVowelsMode, DEL_VOWELS_CHAR)
		             .setBoolean(DeleteAfterCluster, false)
		             .setString(LanguageKey, LANGKEY_PICK_IM);
		assertFalse(state.getBoolean(AutoDash));
		assertEquals(TONE_MARK_SUBTYPE, state.getString(ToneMarkStyle));
		assertFalse(state.getBoolean(AspirationToggle));
		assertFalse(state.getBoolean(VoicelessToggle));
		assertTrue(state.getBoolean(PhoneticInput));
		assertFalse(state.getBoolean(LeadReplaceLead));
		assertFalse(state.getBoolean(GlottalReplaceGlottal));
		assertFalse(state.getBoolean(GlottalToggleTone));
		assertEquals(DEL_VOWELS_CHAR, state.getString(DeleteVowelsMode));
		assertEquals(DEL_LEADINGS_CHAR, state.getString(DeleteLeadingsMode));
		assertFalse(state.getBoolean(DeleteAfterCluster));
		assertEquals(LANGKEY_PICK_IM, state.getString(LanguageKey));
		state = state.setBoolean(AutoDash, true)
		             .setString(ToneMarkStyle, TONE_MARK_POJ)
		             .setBoolean(AspirationToggle, true)
		             .setBoolean(VoicelessToggle, true)
		             .setBoolean(PhoneticInput, false)
		             .setBoolean(LeadReplaceLead, true)
		             .setBoolean(GlottalReplaceGlottal, true)
		             .setBoolean(GlottalToggleTone, true)
		             .setString(DeleteLeadingsMode, DEL_LEADINGS_WHOLE)
		             .setString(DeleteVowelsMode, DEL_VOWELS_SINGLE)
		             .setBoolean(DeleteAfterCluster, true)
		             .setString(LanguageKey, LANGKEY_ASCII);
		assertTrue(state.getBoolean(AutoDash));
		assertEquals(TONE_MARK_POJ, state.getString(ToneMarkStyle));
		assertTrue(state.getBoolean(AspirationToggle));
		assertTrue(state.getBoolean(VoicelessToggle));
		assertFalse(state.getBoolean(PhoneticInput));
		assertTrue(state.getBoolean(LeadReplaceLead));
		assertTrue(state.getBoolean(GlottalReplaceGlottal));
		assertTrue(state.getBoolean(GlottalToggleTone));
		assertEquals(DEL_LEADINGS_WHOLE, state.getString(DeleteLeadingsMode));
		assertEquals(DEL_VOWELS_SINGLE, state.getString(DeleteVowelsMode));
		assertTrue(state.getBoolean(DeleteAfterCluster));
		assertEquals(LANGKEY_ASCII, state.getString(LanguageKey));
		ims.fineInputState();
	}

	@Test
	public void property_update() {
		ims.initInputState();
		ims.setInputState(ims.getInputState().setBoolean(AutoDash, false)
		                     .setString(ToneMarkStyle, TONE_MARK_POJ)
		                     .setBoolean(AspirationToggle, false)
		                     .setBoolean(VoicelessToggle, true)
		                     .setBoolean(PhoneticInput, true)
		                     .setBoolean(LeadReplaceLead, false)
		                     .setBoolean(GlottalReplaceGlottal, false)
		                     .setBoolean(GlottalToggleTone, true)
		                     .setString(DeleteLeadingsMode, DEL_LEADINGS_VALID)
		                     .setString(DeleteVowelsMode, DEL_VOWELS_SINGLE)
		                     .setBoolean(DeleteAfterCluster, true)
		                     .setString(LanguageKey, LANGKEY_LAST_IM));

		assertFalse(ims.getInputState().getBoolean(AutoDash));
		when(pref.getBoolean(eq(PREF_AUTO_DASH), anyBoolean())).thenReturn(true);
		ims.onSharedPreferenceChanged(pref, PREF_AUTO_DASH);
		assertTrue(ims.getInputState().getBoolean(AutoDash));

		assertEquals(TONE_MARK_POJ, ims.getInputState().getString(ToneMarkStyle));
		when(pref.getString(eq(PREF_TONE_MARK_STYLE), anyString())).thenReturn(TONE_MARK_TL);
		ims.onSharedPreferenceChanged(pref, PREF_TONE_MARK_STYLE);
		assertEquals(TONE_MARK_TL, ims.getInputState().getString(ToneMarkStyle));

		assertFalse(ims.getInputState().getBoolean(AspirationToggle));
		when(pref.getBoolean(eq(PREF_ASPIRATION_TOGGLE), anyBoolean())).thenReturn(true);
		ims.onSharedPreferenceChanged(pref, PREF_ASPIRATION_TOGGLE);
		assertTrue(ims.getInputState().getBoolean(AspirationToggle));

		assertTrue(ims.getInputState().getBoolean(VoicelessToggle));
		when(pref.getBoolean(eq(PREF_VOICELESS_TOGGLE), anyBoolean())).thenReturn(false);
		ims.onSharedPreferenceChanged(pref, PREF_VOICELESS_TOGGLE);
		assertFalse(ims.getInputState().getBoolean(VoicelessToggle));

		assertTrue(ims.getInputState().getBoolean(PhoneticInput));
		when(pref.getBoolean(eq(PREF_PHONETIC_INPUT), anyBoolean())).thenReturn(false);
		ims.onSharedPreferenceChanged(pref, PREF_PHONETIC_INPUT);
		assertFalse(ims.getInputState().getBoolean(PhoneticInput));

		assertFalse(ims.getInputState().getBoolean(LeadReplaceLead));
		when(pref.getBoolean(eq(PREF_LEAD_REPLACE_LEAD), anyBoolean())).thenReturn(true);
		ims.onSharedPreferenceChanged(pref, PREF_LEAD_REPLACE_LEAD);
		assertTrue(ims.getInputState().getBoolean(LeadReplaceLead));

		assertFalse(ims.getInputState().getBoolean(GlottalReplaceGlottal));
		when(pref.getBoolean(eq(PREF_GLOTTAL_REPLACE), anyBoolean())).thenReturn(true);
		ims.onSharedPreferenceChanged(pref, PREF_GLOTTAL_REPLACE);
		assertTrue(ims.getInputState().getBoolean(GlottalReplaceGlottal));

		assertTrue(ims.getInputState().getBoolean(GlottalToggleTone));
		when(pref.getBoolean(eq(PREF_GLOTTAL_TOGGLE), anyBoolean())).thenReturn(false);
		ims.onSharedPreferenceChanged(pref, PREF_GLOTTAL_TOGGLE);
		assertFalse(ims.getInputState().getBoolean(GlottalToggleTone));

		assertEquals(DEL_LEADINGS_VALID, ims.getInputState().getString(DeleteLeadingsMode));
		when(pref.getString(eq(PREF_DEL_LEADINGS), anyString())).thenReturn(DEL_LEADINGS_CHAR);
		ims.onSharedPreferenceChanged(pref, PREF_DEL_LEADINGS);
		assertEquals(DEL_LEADINGS_CHAR, ims.getInputState().getString(DeleteLeadingsMode));

		assertEquals(DEL_VOWELS_SINGLE, ims.getInputState().getString(DeleteVowelsMode));
		when(pref.getString(eq(PREF_DEL_VOWELS), anyString())).thenReturn(DEL_VOWELS_WHOLE);
		ims.onSharedPreferenceChanged(pref, PREF_DEL_VOWELS);
		assertEquals(DEL_VOWELS_WHOLE, ims.getInputState().getString(DeleteVowelsMode));

		assertTrue(ims.getInputState().getBoolean(DeleteAfterCluster));
		when(pref.getBoolean(eq(PREF_DEL_AFTER_CLUSTER), anyBoolean())).thenReturn(false);
		ims.onSharedPreferenceChanged(pref, PREF_DEL_AFTER_CLUSTER);
		assertFalse(ims.getInputState().getBoolean(DeleteAfterCluster));

		assertEquals(LANGKEY_LAST_IM, ims.getInputState().getString(LanguageKey));
		when(pref.getString(eq(PREF_IM_SWITCH), anyString())).thenReturn(LANGKEY_NEXT_IM);
		ims.onSharedPreferenceChanged(pref, PREF_IM_SWITCH);
		assertEquals(LANGKEY_NEXT_IM, ims.getInputState().getString(LanguageKey));

		ims.fineInputState();
	}

	//@Test
	public void bindUnbind() {
		when(context.getPackageName()).thenReturn("tw.iongchun.tagikbd");
		TKInputMethodService ims = new TKInputMethodService();
		ims.attachBaseContext(context);
		//ims.onCreate();
	}
}
