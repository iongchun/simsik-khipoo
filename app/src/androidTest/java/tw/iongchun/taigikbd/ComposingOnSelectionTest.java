/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.text.InputType;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static tw.iongchun.taigikbd.TKComposingUtils.AFTERS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.GLOTTAL_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.LEADS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.VOWELS_NULL;
import static tw.iongchun.taigikbd.TKInputMethodService.SUBTYPE_TL;
import static tw.iongchun.taigikbd.TKInputState.Afters;
import static tw.iongchun.taigikbd.TKInputState.ComposingMode;
import static tw.iongchun.taigikbd.TKInputState.ComposingStarted;
import static tw.iongchun.taigikbd.TKInputState.ComposingState;
import static tw.iongchun.taigikbd.TKInputState.Glottal;
import static tw.iongchun.taigikbd.TKInputState.InputStarted;
import static tw.iongchun.taigikbd.TKInputState.Leads;
import static tw.iongchun.taigikbd.TKInputState.PojNN;
import static tw.iongchun.taigikbd.TKInputState.TextMode;
import static tw.iongchun.taigikbd.TKInputState.Tone;
import static tw.iongchun.taigikbd.TKInputState.Vowels;
import static tw.iongchun.taigikbd.TKSettingsActivity.PREF_UI_LOCALE;
import static tw.iongchun.taigikbd.TKViewState.CommitText;
import static tw.iongchun.taigikbd.TKViewState.ComposingCursor;
import static tw.iongchun.taigikbd.TKViewState.ComposingEnd;
import static tw.iongchun.taigikbd.TKViewState.ComposingStart;
import static tw.iongchun.taigikbd.TKViewState.ComposingText;
import static tw.iongchun.taigikbd.TKViewState.DeleteAfter;
import static tw.iongchun.taigikbd.TKViewState.DeleteBefore;
import static tw.iongchun.taigikbd.TKViewState.SelectionEnd;
import static tw.iongchun.taigikbd.TKViewState.SelectionStart;

/**
 * Created by iongchun on 6/27/17.
 */

@RunWith(AndroidJUnit4.class)
public class ComposingOnSelectionTest {
	private static final String INPUT_TITLE_TL = "SimSik:TL";
	private static final String INPUT_TITLE_POJ = "SimSik:POJ";
	private TKInputMethodService ims;
	@Mock
	private Context context;
	@Mock
	private SharedPreferences preferences;
	@Mock
	private Resources resources;
	@Mock
	private InputConnection inputConnection;
	@Mock
	private EditorInfo editorInfo;

	@Before
	public void init() {
		initMockito();
		initIMS();
	}

	@After
	public void fine() {
		fineIMS();
	}

	private void initMockito() {
		MockitoAnnotations.initMocks(this);
	}

	private void initIMS() {
		ims = new TKInputMethodService();
		when(context.getSharedPreferences(anyString(), anyInt())).thenReturn(preferences);
		when(preferences.getString(eq(PREF_UI_LOCALE), anyString())).thenReturn(null);
		when(context.getResources()).thenReturn(resources);
		when(resources.getString(eq(R.string.input_title_tl))).thenReturn(INPUT_TITLE_TL);
		when(resources.getString(eq(R.string.input_title_poj))).thenReturn(INPUT_TITLE_POJ);
		ims.attachBaseContext(context);

		ims.initInputState();
		ims.initViewState();

		ims.startInputState();

		editorInfo.initialSelStart = 0;
		editorInfo.initialSelEnd = 0;
		editorInfo.inputType = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL;
		ims.startViewState(editorInfo);
		ims.switchSubtype(SUBTYPE_TL);
		ims.updateMainKeyboard();
		ims.switchInputType(inputConnection, editorInfo);
	}

	private void fineIMS() {
		ims.finishInputState();
		ims.fineViewState();
		ims.fineInputState();
	}

	@Test
	public void validState() {
		TKInputState inputState = ims.getInputState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));
		assertFalse(inputState.getBoolean(ComposingStarted));
	}

	@Test
	public void empty() {
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn("");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, 0, 0, -1, -1);
		ims.updateView(inputConnection);
		TKInputState inputState = ims.getInputState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));
		assertFalse(inputState.getBoolean(ComposingStarted));
	}

	private void invalidSequence(String str) {
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(str);
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, str.length(), str.length(), -1, -1);
		ims.updateView(inputConnection);
		TKInputState inputState = ims.getInputState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));
		assertFalse(inputState.getBoolean(ComposingStarted));
	}

	@Test
	public void invalidSequence1() {
		invalidSequence("mkgmvky");
	}

	@Test
	public void invalidSequence2() {
		invalidSequence("2463486853687");
	}

	@Test
	public void invalidSequence3() {
		invalidSequence("asdfkajsgkasdkgkznknbzzkx?");
	}

	private void checkComposing(String str, int strLen, String vowels, int tone, String afters, char glotal) {
		checkComposing(str, strLen, vowels, tone, afters, glotal, false);
	}

	private void checkComposing(String str, int strLen,
	                            String vowels, int tone, String afters, char glotal, boolean pojNN) {
		checkComposing(str, strLen, null, vowels, tone, afters, glotal, pojNN);
	}

	private void checkComposing(String str, int strLen,
	                            String leads, String vowels, int tone, String afters,
	                            char glotal, boolean pojNN) {
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(str);
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("");
		ims.updateStateOnSelection(inputConnection, str.length(), str.length(), -1, -1);
		ims.updateView(inputConnection);

		// check input state
		TKInputState inputState = ims.getInputState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));
		assertTrue(inputState.getBoolean(ComposingStarted));
		TKInputState compoState = inputState.getState(ComposingState);
		if (leads != null)
			assertEquals(leads, compoState.getString(Leads));
		assertEquals(vowels, compoState.getString(Vowels));
		assertEquals(tone, (int)compoState.getInteger(Tone));
		assertEquals(afters, compoState.getString(Afters));
		assertEquals(glotal, (char)compoState.getCharacter(Glottal));
		assertEquals(pojNN, (boolean)compoState.getBoolean(PojNN));

		// check view state
		TKViewState viewState = ims.getViewState();
		int start = str.length() - strLen;
		int end = str.length();
		assertEquals(start, (int)viewState.getInteger(ComposingStart));
		assertEquals(end, (int)viewState.getInteger(ComposingEnd));
		assertFalse(viewState.isAnyChanged(SelectionStart, SelectionEnd));
		assertFalse(viewState.contains(DeleteBefore));
		assertFalse(viewState.contains(DeleteAfter));
		assertFalse(viewState.contains(CommitText));
		assertEquals(str.substring(start, end), viewState.getString(ComposingText));
	}

	private void leadsOnly(String str, int strLen, String leads) {
		checkComposing(str, strLen, leads, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void leadsOnly1() {
		leadsOnly("L", 1, "L");
	}

	@Test
	public void leadsOnly2() {
		leadsOnly(" ch", 2, "ch");
	}

	@Test
	public void leadsOnly3() {
		leadsOnly("s sdfj tsh", 3, "tsh");
	}

	@Test
	public void leadsOnly4() {
		leadsOnly("ksjdf kjcvkj asjdkj.kh", 2, "kh");
	}

	@Test
	public void leadsOnly5() {
		leadsOnly("klsdkf zxcv dj", 1, "j");
	}

	private void vowelsOnly(String str, int strLen, String vowels, int tone) {
		checkComposing(str, strLen, vowels, tone, AFTERS_NULL, GLOTTAL_NULL);
	}

	private void vowelsOnly(String str, int strLen, String leads, String vowels, int tone) {
		checkComposing(str, strLen, leads, vowels, tone, AFTERS_NULL, GLOTTAL_NULL, false);
	}

	@Test
	public void vowelsOnly1() {
		vowelsOnly("i", 1, "i", 1);
	}

	@Test
	public void vowelsOnly2() {
		vowelsOnly(" e", 1, "e", 1);
	}

	@Test
	public void vowelsOnly3() {
		vowelsOnly("kj asdk ksjdf ai", 2, "ai", 1);
	}

	@Test
	public void vowelsOnly4() {
		vowelsOnly("kskf kasdf iau", 3, "iau", 1);
	}

	@Test
	public void vowelsOnly5() {
		vowelsOnly("asdfk kjkasd oài", 3, "oai", 3);
	}

	@Test
	public void vowelsOnly6() {
		vowelsOnly("zxvcs kjdf ió͘", 3, "io͘", 2);
	}

	@Test
	public void vowelsOnly7() {
		vowelsOnly("zxvcs kjdf lôo", 3, "l", "oo", 5);
	}

	@Test
	public void vowelsOnly8() {
		vowelsOnly("zxvcs kjdf kee", 3, "k", "ee", 1);
	}

	@Test
	public void vowelsOnly9() {
		vowelsOnly("vm, sd chiáu", 5, "ch", "iau", 2);
	}

	private void withAftersOnly(String str, int strLen, String vowels, int tone, String afters) {
		checkComposing(str, strLen, vowels, tone, afters, GLOTTAL_NULL);
	}

	private void withAftersOnly(String str, int strLen, String leads, String vowels, int tone, String afters) {
		checkComposing(str, strLen, leads, vowels, tone, afters, GLOTTAL_NULL, false);
	}

	private void withAftersOnly(String str, int strLen, String vowels, int tone, String afters, boolean pojNN) {
		checkComposing(str, strLen, vowels, tone, afters, GLOTTAL_NULL, pojNN);
	}

	private void withAftersOnly(String str, int strLen, String leads, String vowels, int tone, String afters, boolean pojNN) {
		checkComposing(str, strLen, leads, vowels, tone, afters, GLOTTAL_NULL, pojNN);
	}

	@Test
	public void withAftersOnly1() {
		withAftersOnly("sdf sf N̂g", 3, "", 5, "Ng");
	}

	@Test
	public void withAftersOnly2() {
		withAftersOnly("adf kvcnvi  ḿ", 1, "", 2, "m");
	}

	@Test
	public void withAftersOnly3() {
		withAftersOnly("klasd mmv N̄", 2, "", 7, "N");
	}

	@Test
	public void withAftersOnly4() {
		withAftersOnly("skd  sfdsf asdk Ông", 3, "O", 5, "ng");
	}

	@Test
	public void withAftersOnly5() {
		withAftersOnly("kfg kjkji zcv iáuⁿ", 4, "iau", 2, "", true);
	}

	@Test
	public void withAftersOnly6() {
		withAftersOnly("kxzc gkoe ùn", 2, "u", 3, "n");
	}

	@Test
	public void withAftersOnly7() {
		withAftersOnly("kgk jkj iam", 3, "ia", 1, "m");
	}

	@Test
	public void withAftersOnly8() {
		withAftersOnly("zxcvkm kjkjj irînn", 5, "iri", 5, "nn");
	}

	@Test
	public void withAftersOnly9() {
		withAftersOnly("vsa sakdfj nn", 2, "n", "", 1, "n");
	}

	@Test
	public void withAftersOnly10() {
		withAftersOnly("asdk v sndnk ln̄g", 4, "l", "", 7, "ng");
	}

	@Test
	public void withAftersOnly11() {
		withAftersOnly("skjdk jk nng", 3, "n", "", 1, "ng");
	}

	@Test
	public void withAftersOnly12() {
		withAftersOnly("dvm tshńg", 5, "tsh", "", 2, "ng");
	}

	@Test
	public void withAftersOnly13() {
		withAftersOnly("kkwiwieu z mzxcvj n mn̂g", 4, "m", "", 5, "ng");
	}

	@Test
	public void withAftersOnly14() {
		withAftersOnly("ksd cxvkjs kdj lián", 4, "l", "ia", 2, "n");
	}

	@Test
	public void withAftersOnly15() {
		withAftersOnly("klld kva kōnn", 4, "k", "o", 7, "nn");
	}

	@Test
	public void withAftersOnly16() {
		withAftersOnly("klmcv kōnn huâiⁿ", 5, "h", "uai", 5, "", true);
	}

	private void withGlotalOnly(String str, int strLen, String vowels, int tone, char glotal) {
		checkComposing(str, strLen, vowels, tone, "", glotal);
	}

	private void withGlotalOnly(String str, int strLen, String leads, String vowels, int tone, char glotal) {
		checkComposing(str, strLen, leads, vowels, tone, "", glotal, false);
	}

	@Test
	public void withGlotalOnly1() {
		withGlotalOnly("jks oat", 3, "oa", 4, 't');
	}

	@Test
	public void withGlotalOnly2() {
		withGlotalOnly("jks kjre vnm io̍h", 4, "io", 8, 'h');
	}

	@Test
	public void withGlotalOnly3() {
		withGlotalOnly("kk mzsidj ie ok", 2, "o", 4, 'k');
	}

	@Test
	public void withGlotalOnly4() {
		withGlotalOnly("skf czcxvj qjwij ia̍p", 4, "ia", 8, 'p');
	}

	@Test
	public void withGlotalOnly5() {
		withGlotalOnly("sdkf  kvcxvs iauh", 4, "iau", 4, 'h');
	}

	@Test
	public void withGlotalOnly6() {
		withGlotalOnly("sdkf  kvcxvs ere̍h", 5, "ere", 8, 'h');
	}

	@Test
	public void withGlotalOnly7() {
		withGlotalOnly("zcvmz, thuh", 4, "th", "u", 4, 'h');
	}

	@Test
	public void withGlotalOnly8() {
		withGlotalOnly("m,zxcv sdjfk okoi ngia̍uh", 7, "ng", "iau", 8, 'h');
	}

	@Test
	public void withGlotalOnly9() {
		withGlotalOnly("kkd sdk kjv jiok", 4, "j", "io", 4, 'k');
	}

	@Test
	public void withGlotalOnly10() {
		withGlotalOnly("kl skd kiap", 4, "k", "ia", 4, 'p');
	}

	@Test
	public void withGlotalOnly11() {
		withGlotalOnly("qkwu ti̍t", 4, "t", "i", 8, 't');
	}

	private void withAftersGlotal(String str, int strLen, String vowels, int tone, String afters, char glotal) {
		checkComposing(str, strLen, vowels, tone, afters, glotal);
	}

	private void withAftersGlotal(String str, int strLen, String leads, String vowels, int tone, String afters, char glotal) {
		checkComposing(str, strLen, leads, vowels, tone, afters, glotal, false);
	}

	@Test
	public void withAftersGlotal1() {
		withAftersGlotal("n̍gh", 4, "", 8, "ng", 'h');
	}

	@Test
	public void withAftersGlotal2() {
		withAftersGlotal("iuw cvs iannh", 5, "ia", 4, "nn", 'h');
	}

	@Test
	public void withAftersGlotal3() {
		withAftersGlotal("cxvmz sdfk mxcv ue̍mh", 5, "ue", 8, "m", 'h');
	}

	@Test
	public void withAftersGlotal4() {
		withAftersGlotal("sgks ha̍unnh", 7, "h", "au", 8, "nn", 'h');
	}

	@Test
	public void withAftersGlotal5() {
		withAftersGlotal("kls klsd hiannh", 6, "h", "ia", 4, "nn", 'h');
	}

	private void checkComposingState(
		String leads, String vowels, int tone,
		String afters, char glotal, boolean pojNN,
		int start, int end,
		int deleteBefore, int deleteAfter,
		String commitText,
		String composingText) {
		TKInputState inputState = ims.getInputState();
		assertTrue(inputState.getBoolean(InputStarted));
		assertTrue(inputState.getBoolean(TextMode));
		assertTrue(inputState.getBoolean(ComposingMode));

		TKInputState compoState = inputState.getState(ComposingState);
		if (leads != null) {
			if (leads == LEADS_NULL)
				assertFalse(inputState.getBoolean(ComposingStarted));
			else
				assertTrue(inputState.getBoolean(ComposingStarted));
		}
		checkState(compoState, leads, vowels, tone, afters, glotal, pojNN);

		TKViewState viewState = ims.getViewState();
		assertEquals(start, (int)viewState.getInteger(ComposingStart));
		assertEquals(end, (int)viewState.getInteger(ComposingEnd));
		assertEquals(composingText, viewState.getString(ComposingText));

		assertFalse(viewState.isAnyChanged(SelectionStart, SelectionEnd));
		if (deleteBefore > 0) {
			assertTrue(viewState.contains(DeleteBefore));
			assertEquals(deleteBefore, (int)viewState.getInteger(DeleteBefore));
		} else {
			assertFalse(viewState.contains(DeleteBefore));
		}
		if (deleteAfter > 0) {
			assertTrue(viewState.contains(DeleteAfter));
			assertEquals(deleteAfter, (int)viewState.getInteger(DeleteAfter));
		} else {
			assertFalse(viewState.contains(DeleteAfter));
		}
		if (commitText != null) {
			assertTrue(viewState.contains(CommitText));
			assertEquals(commitText, viewState.getString(CommitText));
		} else {
			assertFalse(viewState.contains(CommitText));
		}
	}

	private void checkComposingRegion(int cursor,
	                                  String leads, String vowels, int tone,
	                                  String afters, char glotal, boolean pojNN) {
		checkState(ims.getComposingRegion(), leads, vowels, tone, afters, glotal, pojNN);
		TKViewState viewState = ims.getViewState();
		if (cursor >= 0) {
			assertEquals(cursor, (int)viewState.getInteger(ComposingCursor));
			int start = viewState.getInteger(ComposingStart);
			assertEquals(start + cursor, (int)viewState.getInteger(SelectionStart));
		} else {
			assertFalse(viewState.contains(ComposingCursor));
		}
	}

	private void checkState(TKInputState state,
	                        String leads, String vowels, int tone,
	                        String afters, char glotal, boolean pojNN) {
		if (leads != null)
			assertEquals(leads, state.getString(Leads));
		assertEquals(vowels, state.getString(Vowels));
		assertEquals(tone, (int)state.getInteger(Tone));
		assertEquals(afters, state.getString(Afters));
		assertEquals(glotal, (char)state.getCharacter(Glottal));
		assertEquals(pojNN, (boolean)state.getBoolean(PojNN));
	}

	@Test
	public void moveInGlotalWithPojNn() {
		// _[ti̍hⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ti̍hⁿ");
		ims.updateStateOnSelection(inputConnection, 1, 1, -1, -1);
		ims.updateView(inputConnection);
		checkComposingState(LEADS_NULL, VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "ti̍hⁿ");
		checkComposingRegion(0, "t", "i", 8, "", 'h', true);
		// [ti̍h_ⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ti̍h");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("ⁿ");
		ims.updateStateOnSelection(inputConnection, 5, 5, 1, 6);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, "", 'h', false, 1, 6, 0, 0, null, "ti̍hⁿ");
		checkComposingRegion(4, "t", "i", 8, "", 'h', true);
		// [t_i̍hⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" t");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("i̍hⁿ");
		ims.updateStateOnSelection(inputConnection, 2, 2, 1, 6);
		ims.updateView(inputConnection);
		checkComposingState("t", VOWELS_NULL, 0, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "ti̍hⁿ");
		checkComposingRegion(1, "t", "i", 8, "", 'h', true);
		// [ti̍hⁿ]_
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ti̍hⁿ");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn(null);
		ims.updateStateOnSelection(inputConnection, 6, 6, 1, 6);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, "", 'h', true, 1, 6, 0, 0, null, "ti̍hⁿ");
		checkComposingRegion(-1, "t", "i", 8, "", 'h', true);
		// [ti̍_hⁿ]
		when(inputConnection.getTextBeforeCursor(anyInt(), anyInt())).thenReturn(" ti̍");
		when(inputConnection.getTextAfterCursor(anyInt(), anyInt())).thenReturn("hⁿ");
		ims.updateStateOnSelection(inputConnection, 4, 4, 1, 6);
		ims.updateView(inputConnection);
		checkComposingState("t", "i", 8, AFTERS_NULL, GLOTTAL_NULL, false, 1, 6, 0, 0, null, "ti̍hⁿ");
		checkComposingRegion(3, "t", "i", 8, "", 'h', true);
	}
}
