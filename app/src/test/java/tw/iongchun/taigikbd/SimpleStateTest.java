/*
Copyright 2017 Âng Iōngchun

This file is part of Sim-sik ê Khí-pòo.

Sim-sik ê Khí-pòo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Sim-sik ê Khí-pòo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sim-sik ê Khí-pòo.  If not, see <http://www.gnu.org/licenses/>.
 */
package tw.iongchun.taigikbd;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static tw.iongchun.taigikbd.TKComposingUtils.AFTERS_NULL;
import static tw.iongchun.taigikbd.TKComposingUtils.GLOTTAL_NULL;
import static tw.iongchun.taigikbd.TKInputState.Leads;

/**
 * Created by iongchun on 6/27/17.
 */

public class SimpleStateTest {
    @Before
    public void initMockito() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testToneMarks() {
        TKInputState state;

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "ai")
                .setInteger(TKInputState.Tone, 3)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("ài", TKComposingUtils.getComposingTextTL(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "io")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("iô", TKComposingUtils.getComposingTextTL(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "iu")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("iû", TKComposingUtils.getComposingTextTL(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "ui")
                .setInteger(TKInputState.Tone, 7)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("uī", TKComposingUtils.getComposingTextTL(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "oo")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("ôo", TKComposingUtils.getComposingTextTL(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, "ng")
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("n̂g", TKComposingUtils.getComposingTextTL(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "")
                .setInteger(TKInputState.Tone, 7)
                .setString(TKInputState.Afters, "m")
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("m̄", TKComposingUtils.getComposingTextTL(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "ere")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("erê", TKComposingUtils.getComposingTextTL(state));
    }

    @Test
    public void testToneMarksPOJ() {
        TKInputState state;

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "ai")
                .setInteger(TKInputState.Tone, 3)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("ài", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "io")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("iô", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "iu")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("iû", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "ui")
                .setInteger(TKInputState.Tone, 7)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("ūi", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, "ng")
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("n̂g", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "")
                .setInteger(TKInputState.Tone, 7)
                .setString(TKInputState.Afters, "m")
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("m̄", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "oa")
                .setInteger(TKInputState.Tone, 2)
                .setString(TKInputState.Afters, AFTERS_NULL)
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("óa", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "oa")
                .setInteger(TKInputState.Tone, 8)
                .setString(TKInputState.Afters, "")
                .setCharacter(TKInputState.Glottal, 't');
        assertEquals("oa̍t", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "ui")
                .setInteger(TKInputState.Tone, 8)
                .setString(TKInputState.Afters, "")
                .setCharacter(TKInputState.Glottal, 'h');
        assertEquals("u̍ih", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "oa")
                .setInteger(TKInputState.Tone, 3)
                .setString(TKInputState.Afters, "ⁿ")
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("òaⁿ", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "ia")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, "ⁿ")
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("iâⁿ", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "oa")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, "n")
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL);
        assertEquals("oân", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "iau")
                .setInteger(TKInputState.Tone, 8)
                .setString(TKInputState.Afters, "ⁿ")
                .setCharacter(TKInputState.Glottal, 'h');
        assertEquals("ia̍uⁿh", TKComposingUtils.getComposingTextPOJ(state));
    }

    @Test
    public void testPojNn() {
        TKInputState state;

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "a")
                .setInteger(TKInputState.Tone, 4)
                .setString(TKInputState.Afters, "")
                .setCharacter(TKInputState.Glottal, 'h')
                .setBoolean(TKInputState.PojNN, true);
        assertEquals("ahⁿ", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "ia")
                .setInteger(TKInputState.Tone, 4)
                .setString(TKInputState.Afters, "")
                .setCharacter(TKInputState.Glottal, 'h')
                .setBoolean(TKInputState.PojNN, true);
        assertEquals("iahⁿ", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "iau")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, "")
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL)
                .setBoolean(TKInputState.PojNN, true);
        assertEquals("iâuⁿ", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "oai")
                .setInteger(TKInputState.Tone, 4)
                .setString(TKInputState.Afters, "")
                .setCharacter(TKInputState.Glottal, 'h')
                .setBoolean(TKInputState.PojNN, true);
        assertEquals("oaihⁿ", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "o͘")
                .setInteger(TKInputState.Tone, 4)
                .setString(TKInputState.Afters, "")
                .setCharacter(TKInputState.Glottal, 'h')
                .setBoolean(TKInputState.PojNN, true);
        assertEquals("o͘hⁿ", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "au")
                .setInteger(TKInputState.Tone, 7)
                .setString(TKInputState.Afters, "")
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL)
                .setBoolean(TKInputState.PojNN, true);
        assertEquals("āuⁿ", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "io")
                .setInteger(TKInputState.Tone, 5)
                .setString(TKInputState.Afters, "")
                .setCharacter(TKInputState.Glottal, GLOTTAL_NULL)
                .setBoolean(TKInputState.PojNN, true);
        assertEquals("iôⁿ", TKComposingUtils.getComposingTextPOJ(state));

        state = new TKInputState().setString(Leads, "")
                .setString(TKInputState.Vowels, "iau")
                .setInteger(TKInputState.Tone, 8)
                .setString(TKInputState.Afters, "")
                .setCharacter(TKInputState.Glottal, 'h')
                .setBoolean(TKInputState.PojNN, true);
        assertEquals("ia̍uhⁿ", TKComposingUtils.getComposingTextPOJ(state));
    }
}
